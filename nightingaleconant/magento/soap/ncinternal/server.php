<?php
/*
 *      server.php
 *
 *      Copyright 2015 wmikrut <wmikrut72@gmail.com>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 *
 *
 */
session_start();
                                                                                                                                                              require_once('nusoap/lib/nusoap.php');
require('soap/XMLRequest.inc');

$https = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : "off";
if ( $https != 'on') {
	echo "Please use SSL for connections to this service.";
	exit;
}


$server = new nusoap_server;                                                                                                                                                          $server->configureWSDL('server', 'urn:ncinternal');
$server->wsdl->schemaTargetNamespace = 'urn:ncinternal';

$server->register('XMLRequest',
                        array('XMLRequest' => 'xsd:string'),
                        array('XMLResult' => 'xsd:string'),
                        'urn:ncinternal', 
                        'urn:ncinternal#XMLRequest',
                        'rpc', 
                        'encoded', 
                        'Main Servicing Process');

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';

$server->service($HTTP_RAW_POST_DATA);
?>