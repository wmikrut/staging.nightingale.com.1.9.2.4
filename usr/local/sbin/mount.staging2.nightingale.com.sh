#!/bin/bash

mount --bind /var/www/vhosts/staging2.nightingale.com/httpdocs/skin/frontend/default/ves_superstore_nc/css/nccustom.css /home/nccustom/staging2.nightingale.com/css/nccustom.css
mount --bind /var/www/vhosts/staging2.nightingale.com/httpdocs/js/ves_superstore_nc/nccustom.js /home/nccustom/staging2.nightingale.com/js/nccustom.js
mount --bind /var/www/vhosts/staging2.nightingale.com/httpdocs/media /home/nccustom/staging2.nightingale.com/media
