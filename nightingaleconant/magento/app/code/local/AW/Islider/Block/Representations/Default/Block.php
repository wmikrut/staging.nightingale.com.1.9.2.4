<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Block_Representations_Default_Block extends Mage_Core_Block_Template
{
    const DEFAULT_IFRAME_WIDTH = 720;
    const DEFAULT_IFRAME_HEIGHT = 405;
    const DEFAULT_IFRAME_CLASS = 'awislider-video-slide';

    protected $_uniqueId = null;

    public function getUniqueBlockId()
    {
        if (null === $this->_uniqueId) {
            $this->_uniqueId = uniqid('awiSlider' . $this->getSlider()->getId());
        }
        return $this->_uniqueId;
    }

    public function getStyleString()
    {
        $style = '';
        if ($this->getSlider()->getWidth()) {
            $style .= sprintf('width: %spx;', $this->getSlider()->getWidth());
        }
        if ($this->getSlider()->getHeight()) {
            $style .= sprintf('height: %spx;', $this->getSlider()->getHeight());
        }
        return $style;
    }

    protected function _getSlideUrl(AW_Islider_Model_Image $slide)
    {
        if (!$url = $slide->getData('url')) {
            return;
        }
        return Mage::helper('awislider')->isClickStatisticsEnabled()
            ? $this->getUrl('awislider/link/out', array('sid' => $slide->getData('id')))
            : $url;
    }

    public function prepareContent(AW_Islider_Model_Image $slide)
    {
        $content = '';
        if ($url = $this->_getSlideUrl($slide)) {
            $additionalLinkParams = '';
            if ($slide->getData('new_window')) {
                $additionalLinkParams .= ' target="_blank"';
            }
            if ($slide->getData('nofollow')) {
                $additionalLinkParams .= ' rel="nofollow"';
            }
            $content .= "<a href='{$url}'{$additionalLinkParams}>";
        }
        if ($this->isImageContentType($slide)) {
            $title = $this->helper('awislider')->stripTags($slide->getTitle());
            $src = $this->getImageUrl($slide);
            $content .= "<img src='{$src}' title='{$title}' alt='{$title}' />";
        } else {
            //prepare iframe video content
            $src = $slide->getLocation();
            $class = self::DEFAULT_IFRAME_CLASS;
            $width = self::DEFAULT_IFRAME_WIDTH;
            $height = self::DEFAULT_IFRAME_HEIGHT;

            $content .= "<iframe class='{$class}' type='text/html' width='{$width}' height='{$height}' src='{$src}' frameborder='0'></iframe>";
        }
        if ($url = $slide->getUrl()) {
            $content .= "</a>";

            /** @var AW_Islider_Helper_Ga $gaHelper */
            $gaHelper = Mage::helper('awislider/ga');
            $googleAccountId = $gaHelper->canUseGa()
                ? $gaHelper->getGaAccount()
                : null;

            if ($slide->getGaValue() && $this->getSlider()->getGaCategory() && $googleAccountId) {
                $slideId = $this->getUniqueBlockId() . '_' . $slide->getId();
                $content .= "<script type='text/javascript'>
                if (ga) {
                    Event.observe($('" . $slideId . "'), 'click', function(){
                        ga('send', 'event', '{$this->getSlider()->getGaCategory()}', 'click', '{$slide->getId()}', {$slide->getGaValue()}, {useBeacon: true});
                    });
                }
                </script>";
            }
        }
        return $content;
    }

    public function getSlides()
    {
        $collection = $this->getSlider()->getImagesCollection();
        $collection
            ->addActualDateFilter()
            ->addActiveFilter();
        if (!$this->getSlider()->getRandomOrder()) {
            $collection->sortBySortOrder();
        } else {
            $collection->sortByRandom();
        }
        return $collection;
    }

    public function getSlideRedirectUrl($slide)
    {
        //todo check if '?' already persist
        return $slide->getUrl() . '?sid=' . $slide->getId();
    }

    public function getImageUrl(AW_Islider_Model_Image $slide)
    {
        return $slide->isImageRemote()
            ? $slide->getLocation()
            : AW_Islider_Helper_Image::getImageUrl($slide->getLocation());
    }

    public function getTransitionSpeed()
    {
        return $this->getSlider()->getTransitionSpeed();
    }

    public function isVideoContentType($slide)
    {
        if ($slide->getContentType() == AW_Islider_Model_Source_Images_Contenttype::CONTENT_TYPE_VIDEO) {
            return true;
        }
        return false;
    }

    public function isImageContentType($slide)
    {
        if ($slide->getContentType() == AW_Islider_Model_Source_Images_Contenttype::CONTENT_TYPE_IMAGE) {
            return true;
        }
        return false;
    }

    public function setSlider(AW_Islider_Model_Slider $slider)
    {
        $this->addData(array(
            'slider' => $slider,
            'cache_tags' => Mage::helper('awislider')->getCacheTagsForSlider($slider),
        ));
        return $this;
    }
}
