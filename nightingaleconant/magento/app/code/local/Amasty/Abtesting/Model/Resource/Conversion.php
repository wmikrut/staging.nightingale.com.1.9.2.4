<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Model_Resource_Conversion extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('amabtesting/conversion', 'id');
    }
}
