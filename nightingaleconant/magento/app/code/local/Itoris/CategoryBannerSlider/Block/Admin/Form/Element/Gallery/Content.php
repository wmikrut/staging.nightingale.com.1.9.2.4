<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_CATEGORYBANNERSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_CategoryBannerSlider_Block_Admin_Form_Element_Gallery_Content extends Mage_Adminhtml_Block_Widget {

    public function __construct() {
        parent::__construct();
        $this->setTemplate('itoris/categorybannerslider/banners/gallery.phtml');
    }

    protected function _prepareLayout() {
        $this->setChild('uploader',
            $this->getLayout()->createBlock('adminhtml/media_uploader')
        );

        $this->getUploader()->getConfig()
            ->setUrl(Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('adminhtml/itoriscategorybannerslider_banners_gallery/upload'))
            ->setFileField('image')
            ->setFilters(array(
                'images' => array(
                    'label' => Mage::helper('adminhtml')->__('Images (.gif, .jpg, .png)'),
                    'files' => array('*.gif', '*.jpg','*.jpeg', '*.png')
                )
            ));

        return parent::_prepareLayout();
    }

    /**
     * Retrive uploader block
     *
     * @return Mage_Adminhtml_Block_Media_Uploader
     */
    public function getUploader() {
        return $this->getChild('uploader');
    }

    /**
     * Retrive uploader block html
     *
     * @return string
     */
    public function getUploaderHtml() {
        return $this->getChildHtml('uploader');
    }

    public function getJsObjectName() {
        return $this->getHtmlId() . 'JsObject';
    }

    public function getAddImagesButton() {
        return $this->getButtonHtml(
            Mage::helper('catalog')->__('Add New Images'),
            $this->getJsObjectName() . '.showUploader()',
            'add',
            $this->getHtmlId() . '_add_images_button'
        );
    }

	/**
	 * Get json for current banners set if it exists
	 *
	 * @return string
	 */
	public function getBannersScript() {
		$banner_set = Mage::registry('banner_set');
		if(!$banner_set){
			return '[]';
		}
		$banners = $banner_set['banners'];
		if(empty($banners)){
			return '[]';
		}
		$banners = explode('.-,', $banners);
		foreach($banners as $key => $banner){
			$notTitled = explode('._,', $banners[$key]);
			$titled = array('title' => $notTitled[0],
							'alt' => $notTitled[1],
							'delay' => $notTitled[2],
							'link_url' => $notTitled[3],
							'open_in' => $notTitled[4],
							'file_name' => $notTitled[5],
							'url' => Mage::getBaseUrl('media') . 'catalog/category/banners' . $notTitled[5],
							'enabled' => $notTitled[6],
							'order' => $notTitled[7],
						);
			$banners[$key] = $titled;
		}

		return Zend_Json::encode($banners);
	}
}

?>
