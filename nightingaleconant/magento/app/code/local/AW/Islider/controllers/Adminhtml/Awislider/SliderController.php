<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Adminhtml_Awislider_SliderController extends Mage_Adminhtml_Controller_Action
{
    protected function _initSlider()
    {
        $sliderModel = Mage::getModel('awislider/slider');
        $sliderId  = (int) $this->getRequest()->getParam('id');
        if ($sliderId) {
            try {
                $sliderModel->load($sliderId);
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }
        if (null !== $this->_getSession()->getSliderData()) {
            $sliderModel->addData($this->_getSession()->getSliderData());
            $this->_getSession()->setSliderData(null);
        }
        if (null === $sliderModel->getId()) {
            $sliderModel
                ->setIsActive(AW_Islider_Model_Source_Status::ENABLED)
                ->setNavAutohide(AW_Islider_Model_Source_Navigation::SHOWED)
                ->setAnimationSpeed(10)
                ->setFirstTimeout(0)
                ->setStore(0)
            ;
        }
        Mage::register('current_slider', $sliderModel);
        return $sliderModel;
    }

    protected function _initAction()
    {
        return $this->loadLayout()->_setActiveMenu('cms/awislider');
    }

    public function indexAction()
    {
        return $this->_redirect('*/*/list');
    }

    public function newAction()
    {
        return $this->_redirect('*/*/edit');
    }
    
    public function listAction()
    {
        $this->_initAction()->_setTitle($this->__('List Sliders'));
        $this->renderLayout();
    }

    public function editAction()
    {
        $slider = $this->_initSlider();
        if ($slider->getId() && $slider->getRepresentation() == AW_Islider_Model_Source_Representation::FIXED) {
            $this->_getSession()->addNotice($this->__('Use images with the same width and height for best results'));
        }
        $this->_initAction()->_setTitle($this->__('Slider'));
        $this->renderLayout();
    }

    public function saveAction()
    {
        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getParams();
            $slider = $this->_initSlider();
            $slider->addData($postData);
            try {
                $slider->save();
                Mage::helper('awislider')->cleanFPCForSlider($slider);
                $this->_getSession()->setSliderData(null);
                $this->_getSession()->addSuccess($this->__('Block has been succesfully saved'));
                if ($this->getRequest()->getParam('continue')) {
                    $this->_redirect(
                        '*/*/edit',
                        array(
                            'id'           => $slider->getId(),
                            'continue_tab' => $this->getRequest()->getParam('continue_tab', null)
                        )
                    );
                    return $this;
                }
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $this->_getSession()->setSliderData($slider->getData());
                $this->_redirect(
                    '*/*/edit',
                    array(
                        'id'           => $this->getRequest()->getParam('id', null),
                        'continue_tab' => $this->getRequest()->getParam('continue_tab', null)
                    )
                );
                return $this;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        $slider = $this->_initSlider();
        try {
            $slider->delete();
            $this->_getSession()->addSuccess(
                $this->__('Slider block has been successfully deleted')
            );
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
        $this->_redirect('*/*/list');
    }

    public function massactionstatusAction()
    {
        $sliderIds = $this->getRequest()->getParam('awislider', null);
        $status = $this->getRequest()->getParam('status', null);
        try {
            if (!is_array($sliderIds)) {
                throw new Mage_Core_Exception($this->__('Invalid block id(s)'));
            }

            if (null === $status) {
                throw new Mage_Core_Exception($this->__('Invalid status value'));
            }
            foreach ($sliderIds as $id) {
                Mage::getModel('awislider/slider')
                    ->load($id)
                    ->setIsActive($status)
                    ->save()
                ;
            }
            if (count($sliderIds) == 1) {
                $this->_getSession()->addSuccess(
                    $this->__('%d block has been updated successfully', count($sliderIds))
                );
            } else {
                $this->_getSession()->addSuccess(
                    $this->__('%d blocks have been updated successfully', count($sliderIds))
                );
            }
        } catch (Exception $e) {
            $this->_getSession()->addError($this->__($e->getMessage()));
        }
        $this->_redirectReferer();
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/awislider');
    }

    /**
     * Set title of page
     */
    protected function _setTitle($action)
    {
        if (method_exists($this, '_title')) {
            $this->_title($this->__('Image Slider + Video'))->_title($this->__($action));
        }
        return $this;
    }
}