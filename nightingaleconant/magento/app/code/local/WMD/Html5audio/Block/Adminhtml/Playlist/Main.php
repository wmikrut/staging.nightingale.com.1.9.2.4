<?php
/**
 * WMD_Html5audio_Block_Adminhtml_Playlist_Main
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Block_Adminhtml_Playlist_Main extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	
    /**
     * Construct
     *
     * @return void
     */		
    public function __construct() 
    {
        parent::__construct();
        $this->_blockGroup = 'html5audio';
        $this->_controller = 'adminhtml_playlist_main';         
		    $this->setTemplate('html5audio/playlist/list.phtml');  
    }
    
    /**
     * Before HTML
     *
     * @return object
     */		
    public function _beforeToHtml()
    {
        $this->setChild('new_playlist',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('html5audio')->__('New Playlist'),
                    'onclick'   => 'setLocation(\''.$this->getUrl('html5audio/adminhtml_playlist_main/new').'\')'
                  ))
        );	

        return parent::_beforeToHtml();
    }		
    
    /**
     * Return Header Text
     *
     * @return string
     */	
    public function getHeaderText()
    {
		    return Mage::helper('html5audio')->__('Playlist List');
    }
}
