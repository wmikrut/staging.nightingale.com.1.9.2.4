<?php 
/**
 * Mas_Masonotes extension by Makarovsoft.com
 * 
 * @category   	Mas
 * @package		Mas_Masonotes
 * @copyright  	Copyright (c) 2014
 * @license		http://makarovsoft.com/license.txt
 * @author		makarovsoft.com
 */
/**
 * Masonotes default helper
 *
 * @category	Mas
 * @package		Mas_Masonotes
 * 
 */
class Mas_Masonotes_Helper_Data extends Mage_Core_Helper_Abstract{
    public function getAdmins()
	{
		$admins = Mage::getModel('admin/user')->getCollection();
		$return = array();
		foreach($admins as $admin){
			$return[$admin->getUserId()] = $admin->getFirstname() . ' ' . $admin->getLastname(); 
		}
		return $return;
	}
}