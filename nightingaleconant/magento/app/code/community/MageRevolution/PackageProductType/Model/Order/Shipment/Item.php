<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Model_Order_Shipment_Item extends Mage_Sales_Model_Order_Shipment_Item {

    /**
     * Applying qty to order item
     *
     * @return Mage_Sales_Model_Order_Shipment_Item
     */
    public function register()
    {
        if($this->getOrderItem()->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE){
            $this->getOrderItem()->setQtyShipped($this->getOrderItem()->getQtyOrdered());
        } else{
            $this->getOrderItem()->setQtyShipped(
                $this->getOrderItem()->getQtyShipped()+$this->getQty()
            );
        }
        return $this;
    }
}

