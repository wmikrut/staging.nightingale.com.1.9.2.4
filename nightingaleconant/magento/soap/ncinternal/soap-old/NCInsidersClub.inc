<?php
/*
 *      NCInsidersClub.inc
 *
 *      Copyright 2015 wmikrut <wmikrut72@gmail.com>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 *
 *
 */

function NCInsidersClub($xml) {                                                                                                                                     
	include('mysql-credentials-production.php');
	include('mysql-credentials-web4.php');
    include_once('XmlWriter.class.php');
    include_once('soap/ValidateCredentials.inc');

    $result = '';
    $status = 'success';
    $status_message = '';  
    $ary_sku = array();
    
    $ip_address = '0.0.0.0';    
    if ( isset($_SERVER['REMOTE_ADDR'])) {
    	$ip_address = $_SERVER['REMOTE_ADDR'];
    } 
    
    $xml_out = new XmlWriter2();
    $mysqli = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_db);
    $mysqli_web4 = new mysqli($mysql_hostname_web4, $mysql_username_web4, $mysql_password_web4, $mysql_db_web4);
    $xml_data = new SimpleXMLElement($xml);
            
    $xml_application_area = (string) $xml_data->XMLData->ApplicationArea;
    $xml_user_name = (string) $xml_data->XMLData->UserName;
    $xml_password = (string) $xml_data->XMLData->Password;
    $xml_version = (string) $xml_data->XMLData->Version;
    $xml_service_name = (string) $xml_data->XMLData->ServiceName;
    
	$credentials_valid = ValidateCredentials($xml_application_area, $xml_user_name, $xml_password);
    
    if ( $credentials_valid == 'Y') {
    	$xml_email_address = (string) $xml_data->XMLData->EmailAddress;
    	$xml_start_free_trial = (string) $xml_data->XMLData->StartFreeTrial;
    	$xml_email_password = (string) $xml_data->XMLData->EmailPassword;
    	
    	$xml_email_address = trim(strtolower($xml_email_address));
    	
    	$free_trial = 0;
    	$free_trial_active = 0;
    	$free_trial_days_remaining = 0;
    	$active_member = 0;
    	$app_access_allowed = 0;
    	$digital_only_member = 0;
    	
    	if ( $xml_start_free_trial == '' ) {
    		$xml_start_free_trial = '0';
    	}
    	
    	if ( $xml_start_free_trial == '0' ) {
			$mysqlstm = <<<STRING
select count(*) as rcds 
from nccustom_insiders_membership_state
where email_address = '$xml_email_address';
STRING;
    		$f001 = $mysqli->query($mysqlstm);
    		$db1 = $f001->fetch_assoc();
    		if ( $db1['rcds'] == 1 ) {  		
    			    			    		    		
    			$mysqlstm = <<<STRING
select *
from nccustom_insiders_membership_state
where email_address = '$xml_email_address';
STRING;
	    		$f002 = $mysqli->query($mysqlstm);
	    		$db2 = $f002->fetch_assoc();
	    		$db2_rid = $db2['rid'];
	    		$db2_free_trial_customer = $db2['free_trial_customer'];
	    		$db2_free_trial_active = $db2['free_trial_active'];
	    		$db2_free_trial_start = $db2['free_trial_start'];
	    		$db2_free_trial_end = $db2['free_trial_end'];
	    		$db2_active_member = $db2['active_member'];
	    		$db2_membership_order_id = $db2['membership_order_id'];
	    		$db2_membership_start_date = $db2['membership_start_date'];
	    		$db2_digital_only_member = $db2['digital_only_member'];
				
	    		$free_trial = $db2_free_trial_customer;
	    		$free_trial_active = $db2_free_trial_active;
	    		$active_member = $db2_active_member;
	    		$digital_only_member = $db2_digital_only_member;
	    		
	    		
	    		$now_date = date("Y-m-d H:i:s");
	    		
	    		if ( $db2_free_trial_end >= $now_date) {
	    		    $date_begin = new DateTime($now_date);
	    		    $date_end = new DateTime($db2_free_trial_end);
	    		    $free_trial_days_remaining = $date_end->diff($date_begin)->format("%a");   		
	    		} else {
	    		    $free_trial_days_remaining = -1;	
	    		}
	    		if ( $free_trial_days_remaining >= 0 ) {
	    			$app_access_allowed = 1;
	    		} else {
	    			$free_trial_days_remaining = 0;
	    			$free_trial_active = 0;
	    			$app_access_allowed = 0;
	    		}
	    		
	    		if ( $app_access_allowed == 0 ) {
	    			if ( $active_member == 1 ) {
	    				$app_access_allowed = 1;
	    			}
	    		}
	    		
	    		//*********************************************************************
	    		//* Build a list of all the SKUs purchased by this customer's email
	    		//*********************************************************************
	    		$ary_sku = array();
	    		$lcl_email_address = trim(strtolower($xml_email_address));
	    		
	    		//*********************************************************************
	    		//* 1. Get all accounts with a matching email (should only be one).
	    		//*********************************************************************
	    		$mysqlstm = <<<STRING
select *
from nc_customer_entity
where lcase(email) = '$lcl_email_address';
STRING;
	    		$f005 = $mysqli->query($mysqlstm);
	    		while ( $db5 = $f005->fetch_assoc()) {
	    			$db5_entity_id = $db5['entity_id'];
	    			$db5_group_id = $db5['group_id'];
	    			
	    			//*********************************************************************
	    			//* 2. Build a list of all the paid orders for this customer account.
	    			//* The reason I do not go directly to nc_sales_flat_invoice is that
	    			//* this table does not have a direct 'customer_id' field.
	    			//*********************************************************************
	    			$mysqlstm = <<<STRING
select *
from nc_sales_flat_order
where customer_id = $db5_entity_id
and state = 'complete'
and base_total_paid > 0;
STRING;
	    			$f006 = $mysqli->query($mysqlstm);
	    			while ( $db6 = $f006->fetch_assoc()) {
	    				$db6_entity_id =$db6['entity_id'];
	    				
	    				//*********************************************************************
	    				//* 3. Build a list of all the SKUs puchased by this customer's order.
	    				//*********************************************************************
	    				$mysqlstm = <<<STRING
select *
from nc_sales_flat_order_item
where order_id = $db6_entity_id
and (qty_shipped - qty_refunded) > 0;
STRING;
	    				$f007 = $mysqli->query($mysqlstm);
	    				while ( $db7 = $f007->fetch_assoc()) {
	    					$db7_product_id = $db7['product_id'];
	    					$db7_product_type = $db7['product_type'];
	    					$db7_sku = $db7['sku'];

	    					//*********************************************************************
	    					//* Virtual products have no entry in the 
	    					//* nc_catalog_product_entity_varchar.  The SKU is actually stored
	    					//* in the SKU field on the nc_sales_flat_order_item table.
	    					//*********************************************************************
	    					if ( $db7_product_type == 'virtual' ) {
	    						$ary_sku[$db7_sku] =  $db7_sku;
	    					} else {
	    						
	    						//*********************************************************************
	    						//* 4. If this is not a virtual product, go and get the product SKU 
	    						//* from the product_id field.
	    						//*********************************************************************
	    						$mysqlstm = <<<STRING
select *
from nc_catalog_product_entity_varchar
where entity_id = $db7_product_id
and attribute_id = 141;
STRING;
	    						$f008 = $mysqli->query($mysqlstm);
	    						$db8 = $f008->fetch_assoc();
	    						$db8_value = $db8['value'];
	    						$ary_sku[$db8_value] = 1;

	    					}
	    				}	    				
	    			}
	    		}
	    		
	    		//*********************************************************************
	    		//* Now look at the local table nccustom_product_sales_history table
	    		//* for any ordered products from QB, OAP or IBM data.
	    		//*********************************************************************
	    		$mysqlstm = <<<STRING
select *
from nccustom_insiders_order_history
where lcase(email_address) = '$lcl_email_address'
and amount > 0
and status = '1'
and product_id not like 'FR%'
and product_id not like 'TA%'
and product_id not like '7001%';	    		
STRING;
	    		$f009 = $mysqli->query($mysqlstm);
	    		while ( $db9 = $f009->fetch_assoc()) {
	    			$db9_product_id = $db9['product_id'];
	    			$ary_sku[$db9_product_id] = 1;
	    		}
	    		
	    		$mysqlstm = <<<STRING
select * 
from ncdb_amazon.qb_customer_email
where lcase(email) = '$lcl_email_address';
STRING;
	    		$f010 = $mysqli_web4->query($mysqlstm);
	    		while ( $db10 = $f010->fetch_assoc()) {
	    			$db10_ListID = $db10['list_id'];
	    			$mysqlstm = <<<STRING
select *
from qb_invoiceline
where CustomerRefListID = '$db10_ListID'
and InvoiceLineRate > 0
and InvoiceLineItemRefFullName not like 'FRE%'
and InvoiceLineItemRefFullName not like '7001%'
and InvoiceLineItemRefFullName <> ''
STRING;
	    			$f011 = $mysqli_web4->query($mysqlstm);
	    			while ( $db11 = $f011->fetch_assoc()) {
	    				$db11_product_id = $db11['InvoiceLineItemRefFullName'];
	    				$ark_sku[$db11_product_id] = 0;
	    			}
	    		}
    		} else {
    			$status = "fault";
    			$status_message = "Email address ". $xml_email_address. " cannot be found in the system.";		
    		}
    	} 
    	
    	//***********************************************************************
    	//* We just want to try and start a free trial for this email address
    	//***********************************************************************
    	if ( $xml_start_free_trial == '1' ) {
    		$free_trial_started = 0;
    		
    		$mysqlstm = <<<STRING
select count(*) as rcds
from nccustom_insiders_membership_state
where email_address = '$xml_email_address';
STRING;
    		$f003 = $mysqli->query($mysqlstm);
    		$db3 = $f003->fetch_assoc();
    		if ( $db3['rcds'] == 0 ) {
    			
    			$start_date = date("Y-m-d H:i:s");
    			$wrk_date = strtotime($start_date);
    			$wrk_date+= 24 * 60 * 60 * 30;
    			$end_date = date("Y-m-d H:i:s", $wrk_date);
    						
    			$mysqlstm = <<<STRING
insert into nccustom_insiders_membership_state
values(0, '$xml_email_address',1,1, '$start_date','$end_date',0,0,'', '','');
STRING;
    			$fnull = $mysqli->query($mysqlstm);
    			
    			$mysqlstm = <<<STRING
select count(*) as rcds 
from nccustom_insiders_membership_state
where email_address = '$xml_email_address';
STRING;
    			$f004 = $mysqli->query($mysqlstm);
    			$db4 = $f004->fetch_assoc();
    			if ( $db4['rcds'] == 1 ) {
    				
    				$free_trial_started = 1;
    				
    				$app_access_allowed = 1;
    				$free_trial = 1;    				
    				$free_trial_active = 1;
    				$free_trial_days_remaining = 30;
    				
    				    				
    			} else {
    				$status = "fault";
    				$status_message = "Email ". $xml_email_address. " could not be added to the system.";
    			}	
    		} else {
    			$status = "fault";
    			$status_message = $xml_email_address. " has already participated in a free trial. Please login with your Nightingale-Conant Insiders Club member username and password.";
    			//$status_message = $xml_email_address. " has already participated in a free trial. Visit InsidersClubApp.com to learn more.";
    			//$status_message = "Email ". $xml_email_address. " already participating/participated in the free trial.";
    		}
    	}
	} else {
		$status = "fault";
		$status_message = "You are not authorized to use this service.";
	}
    
	$active_member = 0;
	
	if ( $db5_group_id == 4 ) {
	    $app_access_allowed = 1;    
	    $active_member = 1;
	}
	
	if ( $db5_group_id == 6 ) {
		$app_access_allowed = 1;
		$active_member = 1;
	}
	
	if ( $db5_group_id == 7 ) {
		$app_access_allowed = 1;
		$active_member = 1;
	}
	
	if ( $db5_group_id == 14 ) {
		$app_access_allowed = 1;
		$active_member = 1;
	}
    
	    $xml_out->push("XMLResponse");
    	$xml_out->push("XMLData");
    		
    		$xml_out->element("ServiceName", "NCInsidersClub");
    		$xml_out->element("Version", $xml_version);
    		$xml_out->element("EmailAddress", $xml_email_address);
    		$xml_out->element("StartFreeTrial", $xml_start_free_trial);
    		$xml_out->element("ServiceStatus", $status);
    		$xml_out->element("ServiceStatusMessage", $status_message);
    		
    		if ( $xml_start_free_trial == 0 ) {
    			$xml_out->element("MediaFlyAccessAllowed", $app_access_allowed);
    			$xml_out->element("FreeTrialCustomer", $free_trial);
    			$xml_out->element("FreeTrialActive", $free_trial_active);
    			$xml_out->element("FreeTrialDaysLeft", $free_trial_days_remaining);
    			$xml_out->element("ActiveMember", $active_member);
    			$xml_out->element("DigitalOnlyMember", $digital_only_member);
    		}
    		//**********************************************************************************
    		//* If this was a start free trial, return the state of the process.
    		//**********************************************************************************
    		if ( $xml_start_free_trial == '1' ) {
    			$xml_out->element("FreeTrialStarted", $free_trial_started);
    		}
    		//**********************************************************************************
    		//* Return the Purchase History if this is not a 'StartFreeTrial' customer.
    		//* New free trials will not have any purchase history.
    		//**********************************************************************************
    		if ( $xml_start_free_trial == 0 ) {
    			$xml_out->push("PurchaseHistory");
    				foreach ( $ary_sku as $i => $v ) {
    					$xml_out->push("Purchase");
    						$xml_out->element("SKU", $i);
    					$xml_out->pop();
    				}
    			$xml_out->pop();
			}		
    	$xml_out->pop();
    $xml_out->pop();
    
    $result = $xml_out->getXml();
    
    $mysqlstm = <<<STRING
insert into nccustom_webservice_log
values(0, '$xml_service_name', '$xml_user_name','$ip_address','$xml','$result',CURRENT_TIMESTAMP);
STRING;
    $fnull = $mysqli->query($mysqlstm);
    
    mysqli_close($mysqli);
    mysqli_close($mysqli_web4);
    
    return $result;
 }
 ?>