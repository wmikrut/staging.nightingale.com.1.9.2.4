<?php
/**
 * Mas_Mascartdirect extension by Makarovsoft.com
 * 
 * @category   	Mas
 * @package		Mas_Mascartdirect
 * @copyright  	Copyright (c) 2014
 * @license		http://makarovsoft.com/license.txt
 * @author		makarovsoft.com
 */
/**
 * Adminhtml observer
 *
 * @category	Mas
 * @package		Mas_Mascartdirect
 * 
 */
class Mas_Mascartdirect_Model_Adminhtml_Observer {
	
	public function addDirect(Varien_Event_Observer $observer)
    {   
        $block = $observer->getEvent()->getBlock();
        if ($block instanceof Mage_Adminhtml_Block_Catalog_Product_Grid) {
            $block->addColumnAfter('direct_links', array(
                    'header'    => Mage::helper('mascartdirect')->__('Direct Links'),
                    'index'     => 'direct_links',
                    'sortable'  => false,
                    'filter'  => false,
                    'width' => '100px',
                    'type'  => 'text',                    
                    'renderer'  => 'Mas_Mascartdirect_Block_Adminhtml_Links'
            ),  'status');
        }
    }
}