<?php

/* @var $installer NC_DataInstaller_Model_Resource_Eav_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

$installer->addAttribute(
    'order',
    'is_infulfillment',
    array(
        'type' => 'int',
        'default' => 0,
        'grid' => true,
        'unsigned'  => true,
    )
);

// Required tables
$statusTable = $installer->getTable('sales/order_status');
$statusStateTable = $installer->getTable('sales/order_status_state');

// Insert statuses
$installer->getConnection()->insertArray(
    $statusTable,
    array(
        'status',
        'label'
    ),
    array(
        array('status' => 'orderfulfillment_pending', 'label' => 'Pending Order Fulfillment'),
    )
);

// Insert states and mapping of statuses to states
$installer->getConnection()->insertArray(
    $statusStateTable,
    array(
        'status',
        'state',
        'is_default'
    ),
    array(
        array(
            'status' => 'orderfulfillment_pending',
            'state' => 'orderfulfillment_state',
            'is_default' => 0
        ),
    )
);

$installer->endSetup();