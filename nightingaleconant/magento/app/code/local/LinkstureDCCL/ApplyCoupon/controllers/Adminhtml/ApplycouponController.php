<?php

class LinkstureDCCL_ApplyCoupon_Adminhtml_ApplycouponController extends Mage_Adminhtml_Controller_Action
{
  
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('applycoupon/set_time')
                ->_addBreadcrumb('applycoupon Manager','applycoupon Manager');
       return $this;
     }
    public function indexAction()
    {
      $this->_initAction();
      $this->renderLayout();
    }
      
    public function updateTitleAction()
    {
        $fieldId = (int) $this->getRequest()->getParam('id');
        $title = $this->getRequest()->getParam('title');
        if ($fieldId) {
            $model = Mage::getModel('applycoupon/applycoupon')->load($fieldId);
            if ($title) {
              $couponcode=$model->getCouponCode();

              echo $base_url = Mage::app()->getWebsite($model->getWebsites())->getDefaultStore()->getBaseUrl();
              
              $link_with_redirection = $base_url.'applycoupon/index/?code='.$couponcode.'&return_url='.$title;
            }else{
              $link_with_redirection = "First Specify Redirect Url." ;
            }
            $model->setRedirectUrl($title);
            $model->setLinkWithRedirection($link_with_redirection);
            $model->save();
        }
    }
    public function deleteAction()
        {
            if($this->getRequest()->getParam('id') > 0)
            {
              try
              {
                 $obj = Mage::getModel('applycoupon/applycoupon');
                 $result = $obj->load($this->getRequest()->getParam('id'));
                 $obj->delete();
                  if($result)
                  {
                      Mage::getSingleton('core/session')->addSuccess("Delete Successfully");
                  }
                  else
                  {
                      Mage::getSingleton('core/session')->addError("Error in Delete data.");
                  }
                  $this->_redirect('*/*/');
               }
               catch (Exception $e)
                {
                         Mage::getSingleton('adminhtml/session')
                              ->addError($e->getMessage());
                         $this->_redirect('*/*/');
                }
           }
          $this->_redirect('*/*/');
     }

    public function massDeleteAction() {
        $couponIds = $this->getRequest()->getParam('applycoupon');
        if (!is_array($couponIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($couponIds as $couponId) {
                    $model = Mage::getModel('applycoupon/applycoupon')->load($couponId);
                    $_helper = Mage::helper('applycoupon');
                    $model->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('adminhtml')->__(
                                'Total of %d record(s) were successfully deleted', count($couponIds)
                        )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }

    public function importrulesAction(){
      try {
            $coll = Mage::getResourceModel('salesrule/rule_collection')->load();
            $applycoupon = Mage::getModel('applycoupon/applycoupon');
                $applycoupon_collections = $applycoupon->getCollection();
                $total_records =0;
            foreach($coll as $rule){
              // print_r($rule->afterLoad()->getData()); 
              $id=$rule->getRuleId();
              $rulename = $rule->getName();
              $website_ids = $rule->getWebsiteIds();
              // print_r($website_ids);
              //$fromDate="";$toDate="";
              $fromDate = $rule->getFromDate();
              $toDate = $rule->getToDate();
              $toDate = ($toDate) ? $toDate : date('Y-m-d');
              $fromDate = ($fromDate) ? $fromDate : date('Y-m-d');
              if(strtotime(date('Y-m-d')) <= strtotime($toDate) && strtotime(date('Y-m-d')) >= strtotime($fromDate) && $rule->getIsActive()){
                  $status = 1;
              } else {
                  $status = 0;
              }
             

              $collections = Mage::getResourceModel('salesrule/coupon_collection');
              $collections->addRuleToFilter($rule->getRuleId());
              
              foreach ($collections as $collection) {
                
                // print_r($collection->getData());
                
                $couponcode = $collection->getCode();
                $coupon_id = $collection->getCouponId();
                $total_records += count($website_ids)*1;

                $websites=array();
                $nid=array();
                $i = 0;
               
                  foreach ($applycoupon_collections as $applycoupon_collection) {
                    if ($applycoupon_collection->getSid() == $id && $applycoupon_collection->getCouponId() == $coupon_id ) {

                    $nid[$i] = $applycoupon_collection->getId();
                    $websites[$i] = $applycoupon_collection->getWebsites();
                    $i += 1;
                  }
                }
                if (count($website_ids) == count($nid)) {
                    //echo "Update Rule" .'</br>';
                  for ($i=0; $i < count($nid); $i++) {
                    if (isset($nid[$i])) {
                      $base_url = Mage::app()->getWebsite($website_ids[$i])->getDefaultStore()->getBaseUrl();
                      $link_without_redirection  = $base_url.'applycoupon/index/?code='.$couponcode.'&return_url=no';
                      $applycoupon->setData(array('id' => $nid[$i],'rule_name' => $rulename,'coupon_code' => $couponcode,'websites' => $website_ids[$i], 'link_without_redirection' => $link_without_redirection, 'status' => $status,'sid' => $id,'coupon_id' => $coupon_id));
                      $applycoupon->save();
                     }
                  }
                }elseif (count($website_ids) > count($nid)) {
                  for ($i=0; $i < count($website_ids); $i++) { 
                    if (!in_array($website_ids[$i], $websites)) {
                      // echo "Add new Rules" .'</br>';
                      $base_url = Mage::app()->getWebsite($website_ids[$i])->getDefaultStore()->getBaseUrl();
                      $link_without_redirection  = $base_url.'applycoupon/index/?code='.$couponcode.'&return_url=no';
                      $applycoupon->setData(array('coupon_id' => $coupon_id,'sid' => $id,'rule_name' => $rulename, 'coupon_code' => $couponcode,'websites' => $website_ids[$i], 'link_without_redirection' => $link_without_redirection, 'status' => $status));
                      $applycoupon->save();
                    }
                    else{
                      // echo "old Rule + New Rule".'<br>';
                        $_collection_for_id = Mage::getModel('applycoupon/applycoupon')->getCollection()->addFieldToFilter('coupon_id', $coupon_id)->addFieldToFilter('websites', $website_ids[$i]);
                        $array_id = $_collection_for_id->getData();
                        $myid= $array_id[0]['id'];
                        $base_url = Mage::app()->getWebsite($website_ids[$i])->getDefaultStore()->getBaseUrl();
                        $link_without_redirection  = $base_url.'applycoupon/index/?code='.$couponcode.'&return_url=no';
                        if (isset($myid)) {
                          $applycoupon->setData(array('id' => $myid,'rule_name' => $rulename,'coupon_code' => $couponcode,'websites' => $website_ids[$i], 'link_without_redirection' => $link_without_redirection,'status' => $status,'sid' => $id,'coupon_id' => $coupon_id));
                          $applycoupon->save();
                        }
                    }
                  }
                }
              }
            }    
          Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('adminhtml')->__(
                                'Total of %d record(s) were successfully imported.', $total_records
                        )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
          $this->_redirect('*/*/');
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('applycoupon/applycoupon');  
    }
}