<?php # HelloServer.php
# Copyright (c) 2005 by Dr. Herong Yang
#
include('soap/XMLRequest.inc');

$https = isset($_SERVER['HTTPS']) ? $_SERVER['HTTPS'] : "off";
if ( $https != 'on') {
    echo "Please use SSL for connections to this service.";
    exit;
}

$server = new SoapServer(null, array('uri' => "urn://www.nightingale.com"));

$server->addFunction("XMLRequest");
$server->handle();

?>