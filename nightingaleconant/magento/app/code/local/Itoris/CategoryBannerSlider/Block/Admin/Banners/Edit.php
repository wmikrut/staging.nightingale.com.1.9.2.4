<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_CATEGORYBANNERSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_CategoryBannerSlider_Block_Admin_Banners_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

	public function __construct() {
        $this->_blockGroup = 'itoris_categorybannerslider';
        $this->_controller = 'admin_banners';
		$this->_headerText = $this->__('Edit Banners Set');
		parent::__construct();
		$onClickSaveAction = "if (!$('categories_list').childElements().length) { alert('"
						     . $this->__('Please select category before saving this banner set')
						     . "'); return;}
						     if (!$$('#banners_gallery_content_list .preview').length) { alert('"
						     . $this->__('Please upload banner(s) before saving this banner set')
						     ."'); return;}";
		$this->updateButton('save', array(
            'label'     => $this->__('Save'),
            'onclick'   => $onClickSaveAction . 'validateForm(false);',
        ), 1);
		$this->_addButton('save_continue_edit', array(
            'label'     => $this->__('Save and Continue Edit'),
            'onclick'   => $onClickSaveAction . 'validateForm(true);',
            'class'     => 'save',
        ), 1);
    }

	/**
	 * Get delete banners set url
	 *
	 * @return string
	 */
	public function getDeleteUrl(){
        return $this->getUrl('*/*/delete', array('id' => $this->getRequest()->getParam('id')));
    }

	public function updateButton($id, $data, $level){
		if (isset($this->_buttons[$level])){
            $this->_buttons[$level][$id]['label'] = $data['label'];
            $this->_buttons[$level][$id]['onclick'] = $data['onclick'];
        }
	}
}
 
?>