<?php

/* @var $installer NC_DataInstaller_Model_Resource_Eav_Mysql4_Setup */
$installer = $this;

$installer->startSetup();

$customer_group = Mage::getModel('customer/group');
$customer_group->setCode('Insiders Club');
$customer_group->setTaxClassId(3);
$customer_group->save();

$installer->endSetup();