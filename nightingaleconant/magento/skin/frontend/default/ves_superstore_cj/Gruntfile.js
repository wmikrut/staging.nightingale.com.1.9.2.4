module.exports = function(grunt) {
  var skinDir = './';
  grunt.initConfig({
                     pkg: grunt.file.readJSON('package.json'),
                     clean: {
                       build: {
                         src: [
                           skinDir + 'css/styles.css',
                           skinDir + 'css/styles-ie.css'
                         ],
                         options: {
                           force: true
                         }
                       }
                     },
                     compass: {
                       dist: {
                         options: {
                           sassDir: 'sass',
                           cssDir: skinDir + 'css',
                           environment: 'development',
                           outputStyle: 'compressed',
                           noLineComments: true
                         }
                       }
                     },
                     watch: {
                       options: {
                         livereload: true
                       },
                       livereload: {
                         files: [
                           'sass/**/*.scss'
                         ],
                         tasks: [
                           'clean',
                           'compass',
                         ]
                       }
                     }
                   });
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default', [
    'clean',
    'compass',
    'watch'
  ]);
};