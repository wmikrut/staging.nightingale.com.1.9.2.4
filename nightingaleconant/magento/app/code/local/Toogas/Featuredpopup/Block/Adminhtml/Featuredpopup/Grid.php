<?php
/**
 * Toogas Lda.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA (End-User License Agreement)
 * that is bundled with this package in the file licence_toogas_community.txt.
 * It is also available at this URL:
 * http://www.toogas.com/licences/toogas_community.txt
 *
 * @category   Toogas
 * @package    Toogas_Featuredpopup
 * @copyright  Copyright (c) 2010 Toogas Lda. (http://www.toogas.com)
 * @license    http://www.toogas.com/licences/toogas_community.txt
 */
class Toogas_Featuredpopup_Block_Adminhtml_Featuredpopup_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

  const TOOGAS_FEATUREDPOPUP_GRID_PREFIX = 'toogas_featuredpopup_';
	
  public function __construct()
  {
      parent::__construct();
      $this->setId('featuredpopupGrid');
      $this->setDefaultSort('featuredpopup_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('featuredpopup/featuredpopup')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
  	  
      $this->addColumn('featuredpopup_id', array(
          'header'    => Mage::helper('featuredpopup')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'featuredpopup_id',
      ));

      $this->addColumn('popup_name', array(
          'header'    => Mage::helper('featuredpopup')->__('Name'),
          'align'     =>'left',
          'index'     => 'popup_name',
      ));
      
	  $this->addColumn('tipo', array(
	      'header'    => Mage::helper('featuredpopup')->__('Type'),
	      'index'     => 'tipo',
	      'type'      => 'options',
	      'options'   => Mage::helper('featuredpopup')->getTipoOptions(true)
	  ));                   	  	  
	  
	  $this->addColumn('pages', array(
	      'header'    => Mage::helper('featuredpopup')->__('Pages'),
	      'index'     => 'pages',
	      'type'      => 'options',
	      'options'   => Mage::helper('featuredpopup')->getPageOptions(true)
	  ));
	  
      $this->addColumn('url_link', array(
          'header'    => Mage::helper('featuredpopup')->__('Link'),
          'align'     =>'left',
          'index'     => 'url_link',
      ));  
	  
      $this->addColumn('from_date', array(
          'header'    => Mage::helper('featuredpopup')->__('From:'),
          'type' => 'datetime',
          'index'     => 'from_date',
      )); 
      
      $this->addColumn('to_date', array(
          'header'    => Mage::helper('featuredpopup')->__('To:'),
          'type' => 'datetime',
          'index'     => 'to_date',
      ));
      
      $this->addColumn('priority', array(
          'header'    => Mage::helper('featuredpopup')->__('Priority'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'priority',
      ));          
	  
      $this->addColumn('is_active', array(
          'header'    => Mage::helper('featuredpopup')->__('Status'),
          'options' => $this->helper('featuredpopup/data')->sacaStatus(),
          'type' => 'options',
          'index'     => 'is_active',
      ));         
	  
      if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'        => Mage::helper('featuredpopup')->__('Store View'),
                'index'         => 'store_id',
                'type'          => 'store',
                'store_all'     => true,
                'store_view'    => true,
                'sortable'      => false,
                'filter_condition_callback'
                                => array($this, '_filterStoreCondition'),
            ));
        }

	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('featuredpopup')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('featuredpopup')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )                    
                ),
                'filter'    => false,
                'sortable'  => false,
        ));
		/*
        $this->addColumn('preview',
            array(
                'header'    =>  Mage::helper('featuredpopup')->__('Preview'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('featuredpopup')->__('Preview'),
                        'onclick' => 'initialParams_'.$this->getId().'(win_'.$this->getRowId().');',
                    )                    
                ),
                'filter'    => false,
                'sortable'  => false,               
        ));
		*/
        $this->addColumn('preview', array(
            'header'    => $this->helper('featuredpopup')->__('Preview'),
            'align'     => 'left',
		    'frame_callback' => array($this, 'previewPopup'),          
            'index'     => 'preview',
            'filter'    => false,
            'sortable'  => false,             
        )); 		
	  
        return parent::_prepareColumns();
  }

   public function previewPopup($value, $row, $column, $isExport) {
  	   //return '<a href="#" onclick="'.'initialParams_'.$row->getId().'(win_'.$row->getId().');'.'">'.$this->helper('featuredpopup')->__('Preview').'</a>';
  	   $popup = Mage::getModel('featuredpopup/featuredpopup')->load($row->getId());
  	   if (!$popup->getId()) {return '#';}
  	   $opacity = $popup->getOpacity();
  	   
  	   $cont = "";
  	  if ($popup->getTipo() == 1) {
  	  	if (is_numeric($popup->getWidthImage()) && $popup->getWidthImage() > 0 
		    && is_numeric($popup->getHeightImage()) && $popup->getHeightImage() > 0) {
  	  		$cont = "<div id='toogas'><img id='close_pop_up' onclick='closePopUp();' style='cursor:pointer;z-index:1000;position:absolute;top:0px;left:".($popup->getWidthImage()-10)."px' alt='".$this->__('Close Window')."' src='".$this->getSkinUrl('images/Toogas/Featuredpopup/close_button.png')."' border='0' width='26' height='27'/>";
  	  		if ($popup->getUrlLink()) {
  	  			$cont .= "<a href='".$popup->getUrlLink()."'><img src='".Mage::getBlockSingleton('featuredpopup/featuredpopup')->obtainImageAdmin($popup->getImageLink())."' alt='".$popup->getPopupName()."' style='width:".$popup->getWidthImage()."px;height:".$popup->getHeightImage()."px'></a>";
  	  		}
  	  		else {
  	  			$cont .= "<img src='".Mage::getBlockSingleton('featuredpopup/featuredpopup')->obtainImageAdmin($popup->getImageLink())."' alt='".$popup->getPopupName()."' style='width:".$popup->getWidthImage()."px;height:".$popup->getHeightImage()."px;'>";
  	  		}
  	  		$cont .= "</div>";
  	  	}
  	  }
  	  else if ($popup->getTipo() == 2) {
  	  	$cont = "<div id='toogas'><img id='close_pop_up' onclick='closePopUp();' style='cursor:pointer;z-index:1000;position:absolute;top:0px;left:".($popup->getWidthImage()-10)."px' alt='".$this->__('Close Window')."' src='".$this->getSkinUrl('images/Toogas/Featuredpopup/close_button.png')."' border='0' width='26' height='27'/>".Mage::getBlockSingleton('featuredpopup/featuredpopup')->obtainHtmlEditorAdmin($popup->getHtmlEditor())."</div>";
  	  	//$cont = "";
  	  }
/*
		  if ($tipo == 2):
			  win.getContent().innerHTML= "<div id='toogas'><img id='close_pop_up' onclick='closePopUp();' style='cursor:pointer;z-index:1000;position:absolute;top:0px;left:<?php echo ($width-10)?>px' alt='<?php echo $this->__('Close Window') ?>' src='<?php echo $this->getSkinUrl('images/Toogas/Featuredpopup/close_button.png') ?>' border='0' width='26' height='27'/><?php echo Mage::getBlockSingleton('featuredpopup/featuredpopup')->obtainHtmlEditorAdmin($popup->getHtmlEditor()) ?></div>"
		  endif;
	  endif; 	   
  */	   
  	    
		return "<script>var cont_".$popup->getId()." = \"".$cont."\"</script>".'<a href="#" onclick="'.'initialParams(newWindow('.$popup->getWidthImage().', '.$popup->getHeightImage().
	   ', '.$popup->getShowEffect().', '.$popup->getHideEffect().'), '.$opacity.', cont_'.$popup->getId().');'.'">'.$this->helper('featuredpopup')->__('Preview').'</a>';  	   
  	   /*
  	   return '<a href="#" onclick="'.'initialParams_'.$row->getId().'(newWindow('.$popup->getWidthImage().', '.$popup->getHeightImage().
	   ', '.$popup->getShowEffect().', '.$popup->getHideEffect().'));'.'">'.$this->helper('featuredpopup')->__('Preview').'</a>';
	   */
   }

   protected function _afterLoadCollection()
   {
       $this->getCollection()->walk('afterLoad');
       parent::_afterLoadCollection();
   }

    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $this->getCollection()->addStoreFilter($value);
    }

	public function getRowClass($row)
	{
		if ($row->getIsActive()) {
			return self::TOOGAS_FEATUREDPOPUP_GRID_PREFIX.'active';
		}
		else {
			return self::TOOGAS_FEATUREDPOPUP_GRID_PREFIX.'inactive';			
		}
	} 

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('featuredpopup_id');
        $this->getMassactionBlock()->setFormFieldName('featuredpopup');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('featuredpopup')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('featuredpopup')->__('Are you sure?')
        ));
        
        $statuses = $this->helper('featuredpopup')->sacaStatus();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('catalog')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => $this->helper('featuredpopup')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));       

        
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}