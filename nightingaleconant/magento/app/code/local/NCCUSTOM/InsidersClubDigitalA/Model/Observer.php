<?php
class NCCUSTOM_InsidersClubDigitalA_Model_Observer 
{
    public function exportFeed($observer) 
    {		
    	include('mysql-credentials-production.php');
    	
    	$digital_item = 971;
    	$customer_group = 7;
    	
    	$mysqli = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_db);
    	$log = "nccustom_InsidersClubDigitalA.log";
    	//Mage::log("InsidersClubDigitalA", null, $log);
	
		$order = Mage::getModel('sales/order')->load(Mage::getSingleton('checkout/session')->getLastRealOrderId(), 'increment_id');
		//$order = $observer->getEvent()->getOrder();
		$order_id = $order->getId();
		$customer_id = $order->getCustomerId();
		$customer_email = $order->getCustomerEmail();
		
		$cartItems = $order->getAllItems();
		foreach ( $cartItems as $item )
		{	
		    $product_id = $item->getProductId();
		    if ( $product_id == $digital_item )
		    {
				
				$line = "Debug-20151003-". $order_id. "-". $customer_id;
				if ( isset($_SESSION['nccustom_coupon_code']))
				{
				    $line.= "-". $_SESSION['nccustom_coupon_code'];
				}
				//Mage::log($line, null, $log);
				
				$_customer = Mage::getModel('customer/customer')->load($customer_id);
				$_customer->setGroupId($customer_group);
				$_customer->save();
				
				$mysqlstm = <<<STRING
select count(*) as rcds
from $mysql_db.nccustom_insiders_membership_state
where email_address = '$customer_email';
STRING;
				$f003 = $mysqli->query($mysqlstm);
				$db3 = $f003->fetch_assoc();
				$db3_rcds = $db3['rcds'];
				
				if ( $db3_rcds == 0 )
				{
					$start_date = date("Y-m-d H:i:s");
					$mysqlstm = "insert ignore into $mysql_db.nccustom_insiders_membership_state ";
					$mysqlstm.= "values(0, '$customer_email', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0, '$order_id', '$start_date', '', 0);";
				} 
				else 
				{
					$mysqlstm = "update $mysql_db.nccustom_insiders_membership_state ";
					$mysqlstm.= "set active_member = 1 ";
					$mysqlstm.= "where email_address = '$customer_email';";
				}
				//Mage::log($mysqlstm, null, $log);		
				$fnull = $mysqli->query($mysqlstm);
				
				if ( isset($_SESSION['nccustom_coupon_code']))
				{
					$nccustom_coupon_code = $_SESSION['nccustom_coupon_code'];
					$_order = Mage::getModel('sales/order')->load($order_id);
					$_order->setCouponCode($nccustom_coupon_code)->save();
					unset($_SESSION['nccustom_coupon_code']);
				}			
	    	}
		}		
		mysqli_close($mysqli);
    }
}
?>