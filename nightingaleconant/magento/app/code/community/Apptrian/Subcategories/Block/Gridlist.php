<?php
/**
 * @category  Apptrian
 * @package   Apptrian_Subcategories
 * @author    Apptrian
 * @copyright Copyright (c) 2016 Apptrian (http://www.apptrian.com)
 * @license   http://www.apptrian.com/license Proprietary Software License EULA
 */
class Apptrian_Subcategories_Block_Gridlist extends Mage_Core_Block_Template
{
    
    /**
     * Used in .phtml file and returns array of options and categories.
     *
     * @return array
     */
    public function getSubcategoriesData()
    {
        
        $data = array();
        
        $helper = Mage::helper('apptrian_subcategories');
        
        $data['name_in_layout']   = $this->getNameInLayout();
        $data['full_action_name'] = Mage::app()->getFrontController()
            ->getAction()->getFullActionName();
        $data['cat_param']        = Mage::app()->getRequest()->getParam('cat');
        $data['options']          = null;
        
        $encodedOptions = $this->getEncodedOptions();
        
        if ($encodedOptions !== null) {
            $data['options'] = $helper->decode($encodedOptions);
        }
        
        return $helper->getCategories($data);
        
    }
    
}
