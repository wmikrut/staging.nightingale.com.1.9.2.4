<?php
class Toogas_Featuredpopup_Block_Adminhtml_Headerlic
	extends Mage_Adminhtml_Block_System_Config_Form_Fieldset
{
	protected function _getHeaderHtml($element)
	{
		$html = parent::_getHeaderHtml($element);
		
		$block = new Toogas_Featuredpopup_Block_Featuredpopup();
		
		$website = Mage::app()->getRequest()->getParam('website');
		$store = Mage::app()->getRequest()->getParam('store');		
		
		$storeInfo = null;
		if (strlen($store)) {
			$storeInfo = $store;
		}
		$serial = Mage::helper('featuredpopup')->getSerial($storeInfo);
		
		if ($block->validaDomAlias($block->limpaDominio($this->_getStoreUrlByCode($storeInfo)), $serial)) {
			$html.='<ul class="messages"><li class="success-msg"><ul><li><span>'.
			$this->__('License is active.').'</span></li></ul></li></ul>';
		}
		else if (!strlen($serial)) {
				$html.='<ul class="messages"><li class="error-msg"><ul><li><span>'.
				$this->__('Please, enter key.').'</span></li></ul></li></ul>';
			}
			else {
				$html.='<ul class="messages"><li class="error-msg"><ul><li><span>'.
				$this->__('Incorrect key.').'</span></li></ul></li></ul>';
			}	
		return $html;
	}
	
	protected function _getStoreUrlByCode($storeCode) {
		if (strlen($storeCode)) { //por codigo
			$store = Mage::getModel("core/store")->load($storeCode);
			if ($store->getId()) {
				return Mage::app()->getStore($store->getId())
				->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
			}
		}
		return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB); //sem codigo vai buscar defeito
	}
	
}