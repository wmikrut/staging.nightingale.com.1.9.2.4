<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_CATEGORYBANNERSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_CategoryBannerSlider_Adminhtml_Itoriscategorybannerslider_BannersController extends Itoris_CategoryBannerSlider_Controller_Admin_Controller {

	/**
	 * Banners sets grid page
	 *
	 * @return mixed
	 */
	public function indexAction() {
		$this->loadLayout();
		$this->renderLayout();
	}

	/**
	 * Edit banners set page
	 */
	public function editAction() {
		$setId = (int)$this->getRequest()->getParam('id');
		if ($setId) {
			try {
				$set = $this->getSetsModel()->loadSet($setId);
				Mage::register('banner_set', $set);
			} catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
			}
		}
		$this->loadLayout();
		$this->renderLayout();
	}

	/**
	 * Create a new banners set action
	 */
	public function newAction() {
		$this->_redirect('*/*/edit');
	}

	/**
	 * Save a banners set action
	 */
	public function saveAction() {
		$set = $this->getRequest()->getPost('set');
		$savedId = 0;
		try {
			$savedId = $this->getSetsModel()->saveSet($set);
			$this->_getSession()->addSuccess($this->__('Banners Set has been saved'));
		} catch (Exception $e) {
			$this->_getSession()->addError($this->__('Banners Set has not been saved'));
		}
		$back = $this->getRequest()->getParam('back');
		if ($back) {
			$this->_redirect('*/*/edit/id/' . $savedId);
		} else {
			$this->_redirect('*/*/');
		}
	}

	/**
	 * Delete a banners set action
	 */
	public function deleteAction() {
		$id = (int)$this->getRequest()->getParam('id');
		try {
			$this->getSetsModel()->deleteSets(array($id));
			$this->_getSession()->addSuccess($this->__('Banners Set has been deleted'));
		} catch (Exception $e) {
			$this->_getSession()->addError($this->__('Banners Set has not been deleted'));
		}
		$this->_redirect('*/*/');
	}

	/**
	 * Mass delete banners sets action
	 */
	public function massDeleteAction() {
		$ids = $this->getRequest()->getPost('set_id');
		try {
			$this->getSetsModel()->deleteSets($ids);
			$this->_getSession()->addSuccess($this->__('Banners Sets have been deleted'));
		} catch (Exception $e) {
			$this->_getSession()->addError($this->__('Banners Sets have not been deleted'));
		}
		$this->_redirect('*/*/');
	}

	/**
	 * Mass change status of banners sets
	 */
	public function massStatusAction() {
		$ids = $this->getRequest()->getPost('set_id');
		$status = (int)$this->getRequest()->getPost('status');
		try {
			$this->getSetsModel()->changeSetsStatus($ids, $status);
			$this->_getSession()->addSuccess($this->__('Statuses have been changed'));
		} catch (Exception $e) {
			$this->_getSession()->addError($this->__('Statuses have not been changed'));
		}
		$this->_redirect('*/*/');
	}

	/**
	 * Get image by a banner id from the request
	 *
	 * @return bool
	 */
	public function getImageAction() {
		ini_set('gd.jpeg_ignore_warning', 1);
		$id = (int)$this->getRequest()->getParam('id');
		$fileName = $this->getSetsModel()->loadBanner($id);
		$file = Mage::getBaseDir() . '/media/catalog/category/banners' . $fileName['file_name'];
		$size = getimagesize($file);
		list($width, $height) = $size;

        switch ($size['mime']) {
			case 'image/jpg':
            case 'image/jpeg':
		              $img=   imagecreatefromjpeg($file);
        		       break;
            case 'image/gif':
                      $img = imagecreatefromgif($file);
                	  break;
            case 'image/png':
	        	       $img = imagecreatefrompng($file);
					   ImageAlphaBlending($img,true);
        				ImageSaveAlpha($img,true);
                	    break;
        }
		if ($height > $width) {
			$scale = $height/100;
		} else {
			$scale = $width/100;
		}
		$newHeight = $height/$scale;
		$newWidth = $width/$scale;
		$thumb = imagecreatetruecolor($newWidth, $newHeight);
		if ($size['mime'] == 'image/gif') {
			$originalTransparentColor = imagecolortransparent($img);
			if ($originalTransparentColor >= 0 && $originalTransparentColor < imagecolorstotal($img)) {
    			$transparentColor = imagecolorsforindex($img, $originalTransparentColor );
    			$newTransparentColor = imagecolorallocate($thumb, $transparentColor['red'], $transparentColor['green'], $transparentColor['blue']);
    			imagefill( $thumb, 0, 0, $newTransparentColor);
			    imagecolortransparent( $thumb, $newTransparentColor);
			}
		}
		
		if ($size['mime'] == 'image/png') {
			ImageAlphaBlending($thumb,false);
            ImageSaveAlpha($thumb,true);
        }

	    imagecopyresized($thumb, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
		ob_get_clean();
		header("Pragma: public"); // required
    	header("Expires: 0");
    	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    	header("Cache-Control: private",false); // required for certain browsers
    	header("Content-Type: {$size['mime']}");

		switch ($size['mime']) {
		  	case 'image/gif':
				imagegif($thumb);
		  		break;
			case 'image/jpg':
		  	case 'image/jpeg':
				imagejpeg($thumb);
		  		break;
		  	case 'image/png':
				imagepng($thumb);
		  		break;
		  	default:
				return false;
		}
		
		imagedestroy($thumb);
        exit;
	}

	/**
	 * Get banners set model
	 *
	 * @return Itoris_CategoryBannerSlider_Model_Sets
	 */
	private function getSetsModel(){
		return Mage::getModel('itoris_categorybannerslider/sets');
	}

	protected function _isAllowed(){
		return Mage::getSingleton('admin/session')->isAllowed('admin/catalog/itoris/itoris_categorybannerslider');
	}
}
?>