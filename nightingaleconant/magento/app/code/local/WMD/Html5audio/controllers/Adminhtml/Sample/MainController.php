<?php
/**
 * WMD_Html5audio_Adminhtml_Sample_MainController
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2013 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Adminhtml_Sample_MainController extends Mage_Adminhtml_Controller_Action
{   
    /**
     * load layout, activate menu and add breadcrumbs
     *
     * @return object
     */                         
    protected function _initAction()
    {
        $this->loadLayout()
  		      ->_setActiveMenu('catalog/html5audio')
            ->_addBreadcrumb(
                Mage::helper('html5audio')->__('Samples'), 
                Mage::helper('html5audio')->__('Samples')
            );
        return $this;
    }
    	
	  /**
     * Show download links
     *
     * @return void
     */	
    public function indexAction()
    {
  		  $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('html5audio/adminhtml_sample_main'))
            ->renderLayout();	
    } 
    
    /**
     * Show download form
     *
     * @return void
     */	
    public function newAction()
    {
		    $this->loadLayout()
            ->_setActiveMenu('html5audio/sample')
            ->_addBreadcrumb(
                Mage::helper('html5audio')->__('New Sample'), 
                Mage::helper('html5audio')->__('New Sample')
            )
            ->_addContent($this->getLayout()->createBlock('html5audio/adminhtml_sample_new'))
    				->_addLeft($this->getLayout()->createBlock('html5audio/adminhtml_sample_new_tabs'))
    		    ->renderLayout();	
    }	
      
    /**
     * Save sample form
     *
     * @return void
     */	
    public function saveAction()
    {  
        if ( $this->getRequest()->getPost() ) {
            try {
                $postData = $this->getRequest()->getPost();
                $model = Mage::getModel('html5audio/sample');
                
                $model->saveSample($postData);
    
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('html5audio')->__('Sample was successfully created and saved.')
                );
    
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/'); 
    }	

    /**
     * Delete sample 
     *
     * @return void
     */
    public function deleteAction()
    {
        
        $id = $this->getRequest()->getParam('id');
        try
        {
    				Mage::getModel('html5audio/sample')->setId($id)->delete();					
            
            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('html5audio')->__('Entry was deleted successfully.')
            );
        } 
  			catch (Exception $e)
  			{
//                   Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
		
        $this->indexAction();	
    		
    }                   
    
    /**
     * Mass delete samples
     *
     * @return void
     */
    public function massDeleteAction()
    {
        
        $ids = $this->getRequest()->getParam('id');
        if(!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('html5audio')->__('Please select at least one item.')
          	);
        }
    		else
    		{
            try 
      			{
                foreach ($ids as $id)
        				{
        					   Mage::getModel('html5audio/sample')->setId($id)->delete();					
                }
				
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('html5audio')->__('Entries were deleted successfully.')
                );
            } 
      			catch (Exception $e)
      			{
//                 Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
		
        $this->indexAction();	
    		
    }           
   
         
    /**
     * Associations grid-load for the ajax request
     * 
     * @return void          
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('html5audio/adminhtml_sample_main_grid')->toHtml()
        );
    }         
}

