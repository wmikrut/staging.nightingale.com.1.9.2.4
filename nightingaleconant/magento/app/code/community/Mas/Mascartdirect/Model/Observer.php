<?php
/**
 * Mas_Mascartdirect extension by Makarovsoft.com
 * 
 * @category   	Mas
 * @package		Mas_Mascartdirect
 * @copyright  	Copyright (c) 2014
 * @license		http://makarovsoft.com/license.txt
 * @author		makarovsoft.com
 */
/**
 * Adminhtml observer
 *
 * @category	Mas
 * @package		Mas_Mascartdirect
 * 
 */
class Mas_Mascartdirect_Model_Observer {
	
	public function bypassCart(Varien_Event_Observer $observer) {
        if (Mage::registry('bypass')) {
        	
            $response = $observer->getResponse();
            $response->setRedirect(Mage::getUrl('checkout/onepage'));
            Mage::getSingleton('checkout/session')->setNoCartRedirect(true);
            
        }
    }
}