var awiSlider = Class.create({
    initialize: function (elId, options) {
        this._options = options;
        this.settings = {
            jalousie: {
                boxCols: 10,
                sliceDuration: 0.15,
                sliceTimeout: 100
            }
        };
        this._elId = elId;
        this._watchedEvent = "";
        this._animating = false;
        this._jalousieSlices = [];
        this._types = ['jalousie', 'fade-appear', 'simple-slider', 'blind-up-down', 'slide-and-blink', 'slide-up-down'];

        //system variables
        this._slides = $(this._elId).select(".slides > li");
        this._currentSlideIndex = 0;
        this._animatedSlides = null;
        this._playing = false;
        this.canAnimating = true;
        this._sliderDirection = 'next';
        this._animatingTo = this._currentSlideIndex;
        this._slider = $(this._elId);
        this._watchedEventClearTimer = null;
        this._options.iframeMouseOver = false;
        this._slider.pagingCount = this._slides.length;
        this._slider.last = this._slides.length - 1;
        this._videoSlideToRefresh = null;

        this._initSlider();
        this._initVideoController();

        if (this._options.autohideNavigation != 0) {
            this._initControlNav();
            this._initDirection();
        }

        this._slider.observe('mouseover', function (event) {
            this._pause();
        }.bind(this));
        this._slider.observe('mouseout', function (event) {
            this._play();
        }.bind(this));

        var initDelay = parseInt(this._options.initDelay);
        initDelay = !isNaN(initDelay)
            ? (Math.abs(initDelay) * 1000)
            : 0;
        setTimeout(this._play.bind(this), initDelay);

        this._slider.down().setAttribute("draggable", "false");
    },
    _initVideoController: function () {
        var me = this;
        $$(this._options.videoSlideClass).each(function(el) {
            el.observe("mouseover", function (){
                me._options.iframeMouseOver = true;
            });
            el.observe("mouseout", function (){
                me._options.iframeMouseOver = false;
            });
        });
        Event.observe(window, "blur", function (){
            if (me._options.iframeMouseOver) {
                me.canAnimating = false;
            }
        });

    },
    _pause: function () {
        clearInterval(this._animatedSlides);
        this._animatedSlides = null;
        this._playing = false;
    },
    _animateSlides: function () {
        if (!this._animating) {
            var target = this._getTarget("next");
            this._animate(target);
        }
    },
    _play: function () {
        if (!this.canAnimating) {
            clearInterval(this._animatedSlides);
            this._animatedSlides = null;
            this._playing = false;
            return;
        }
        if (this._playing) {
            clearInterval(this._animatedSlides);
        }

        if (this._options.slideSpeed > 0 && !this._animatedSlides) {
            this._animatedSlides = setInterval(function () {
                this._animateSlides()
            }.bind(this), this._options.slideSpeed * 1000);
        }
        this._playing = true;
    },
    _initSlider: function () {
        var width = '100%';
        var height = '100%';
        if (this._options.width > 0 && this._options.height > 0) {
            width = this._options.width + 'px';
            height = this._options.height + 'px';
        }

        this._slides.each(function (el) {
            el.setStyle({
                "maxWidth": width,
                "maxHeight": height,
                "opacity": 0
            });
            if (el.down().tagName !== "IMG") {
                el.down().setStyle({
                    "maxWidth": width,
                    "maxHeight": height
                });
            }
            el.removeClassName(this._options.namespace + "active-slide");
        }.bind(this));
        this._whSetter = new PeriodicalExecuter(this._setWH.bind(this), 0.1);

        var me = this;
        Event.observe(window, 'load', function() {
            me._setWH();
        });
        // fix for the slow connect (ISLIDER-80)
        document.observe("dom:loaded", function() {
            me._setWH();
        });
        Event.observe(window, 'resize', function(){
            me._setWH();
        }.bind(this));

        $$(this._options.videoSlideClass).each(function (el) {
            el.setStyle({'maxWidth': "100%"})
        });

        this._slides[this._currentSlideIndex].setStyle({"zIndex": 2, "display": "block"});

        new Effect.Morph(this._slides[this._currentSlideIndex].getAttribute('id'), {
            style: 'opacity:1;',
            duration: this._options.animationSpeed
        });

        this._slides[this._currentSlideIndex].addClassName(this._options.namespace + "active-slide");
        return this;
    },

    _setWH: function() {
        if(this._slides[this._currentSlideIndex].getWidth() && this._slides[this._currentSlideIndex].getHeight()) {
            if(this._whSetter) {
                this._whSetter.stop();
                this._whSetter = null;
            }

            this._slider.select('.slides').first().setStyle({"height": 'auto'});
            var maxHeight = 0;
            var maxWidth = this._slider.select('.slides').first().getWidth();
            this._slides.each(function(el) {
                el.setStyle({"padding": '0px'})
                if (el.getWidth() > maxWidth) {
                    maxWidth = el.getWidth();
                }
                if (el.getHeight() > maxHeight) {
                    maxHeight = el.getHeight();
                }
            });
            this._slides.each(function(el) {
                el.setStyle({"paddingLeft": '0px', "paddingTop": '0px'});
                var offsetLeft = maxWidth/2 - el.getWidth()/2;
                var offsetTop = maxHeight/2 - el.getHeight()/2;
                el.setStyle({"paddingLeft": offsetLeft+'px', "paddingTop": offsetTop+'px'});
            });
            this._slider.select('.slides').first().setStyle({"height": maxHeight+'px'});
        }
    },

    _initControlNav: function () {
        var control = document.createElement('ol');
        var className = 'control-nav ';
        if (this._options.autohideNavigation == 2) {
            className += 'auto-hide ';
        }
        control.setAttribute('class', this._options.namespace + className + this._options.namespace + 'control-paging');
        if (this._slider.pagingCount > 1) {
            for (var i = 0; i < this._slider.pagingCount; i++) {
                var item = '<a>' + i + '</a>';
                var li = document.createElement('li');
                li.innerHTML = item;
                control.appendChild(li);
            }
        }
        this._slider.appendChild(control);
        this._controlNav = this._slider.select('.' + this._options.namespace + 'control-nav li a');
        this._setActiveControlNav();
        this._controlNav.each(function (el) {
            el.observe('click', function (event) {
                event.preventDefault();
                if (this._watchedEvent === "" || this._watchedEvent === event.type) {
                    target = this._controlNav.indexOf(el);
                    if (!el.hasClassName(this._options.namespace + 'active')) {
                        this._sliderDirection = (target > this._currentSlideIndex) ? "next" : "prev";
                        this._animate(target);
                        this.canAnimating = true;
                    }
                }
                if (this._watchedEvent === "") {
                    this._watchedEvent = event.type;
                }
                this._setToClearWatchedEvent();
            }.bind(this));
        }.bind(this));
    },
    _setToClearWatchedEvent: function () {
        clearTimeout(this._watchedEventClearTimer);
        this._watchedEventClearTimer = setTimeout(function () {
            this._watchedEvent = "";
        }.bind(this), 3000);
    },
    _setActiveControlNav: function () {
        if (this._controlNav) {
            this._controlNav.each(function (el) {
                el.removeClassName(this._options.namespace + "active")
            }.bind(this));
            this._controlNav[this._animatingTo].addClassName(this._options.namespace + "active");
        }
        return this;
    },
    _canAdvance: function (target) {
        return (target !== this._currentSlideIndex);
    },
    _initDirection: function () {
        var ul = document.createElement('ul');
        var additionalClass = (this._options.autohideNavigation == 2)
            ? ' auto-hide'
            : '';
        ul.setAttribute('class', this._options.namespace + 'direction-nav' + additionalClass);
        ul.innerHTML = '<li><a class="' + this._options.namespace + 'prev" href="#">' + this._options.prevText + '</a></li><li><a class="' + this._options.namespace + 'next" href="#">' + this._options.nextText + '</a></li>';

        this._slider.appendChild(ul);
        this._directionNav = this._slider.select('.' + this._options.namespace + 'direction-nav li a');

        this._directionUpdate();

        this._directionNav.each(function (el) {
            el.observe('click', function (event) {
                event.preventDefault();
                var target;
                if (this._watchedEvent === "" || this._watchedEvent === event.type) {
                    target = (el.hasClassName(this._options.namespace + 'next')) ? this._getTarget('next') : this._getTarget('prev');
                    this._animate(target);
                }
                if (this._watchedEvent === "") {
                    this._watchedEvent = event.type;
                }
                this.canAnimating = true;
                this._setToClearWatchedEvent();
            }.bind(this));
        }.bind(this));
    },
    _getTarget: function (dir) {
        this._sliderDirection = dir;
        if (dir === "next") {
            return (this._currentSlideIndex === this._slider.last) ? 0 : this._currentSlideIndex + 1;
        }
        return (this._currentSlideIndex === 0) ? this._slider.last : this._currentSlideIndex - 1;
    },
    _directionUpdate: function () {
        if (this._directionNav) {
            var disabledClass = this._options.namespace + 'disabled';
            if (this._slider.pagingCount === 1) {
                this._directionNav.each(function (el) {
                    el.addClassName(disabledClass);
                    el.setAttribute('tabindex', '-1');
                });
            } else {
                this._directionNav.each(function (el) {
                    el.removeClassName(disabledClass);
                    el.removeAttribute('tabindex');
                });
            }
        }
        return this;
    },
    _jalousieGetSliceId: function (slide, index) {
        return this._slides[this._currentSlideIndex].getAttribute('id') + 'slide' + slide + index;
    },
    _jalousieGetSliceElement: function (slide, index) {
        var newEl = new Element('div', {
            id: this._jalousieGetSliceId(slide, index)
        });
        newEl.addClassName('awis-jalousie-slice');
        return newEl;
    },
    _animateJalousie: function (target) {
        var currentImage = this._slides[this._currentSlideIndex].down('img');
        //animateJalousie effect supports image slides only
        if (!currentImage) {
            this._animateFade(target);
            return this;
        }
        if (!this._jalousieSlices[target]) {
            this._jalousieSlices[target] = [];
        }
        this.settings.jalousie.boxCols = Math.ceil(this._slides[this._currentSlideIndex].getWidth() / 50);
        var sliceWidth = Math.round(this._slides[this._currentSlideIndex].getWidth() / this.settings.jalousie.boxCols);
        var rest = this._slides[this._currentSlideIndex].getWidth() - sliceWidth * this.settings.jalousie.boxCols;
        var restMargin = 0;
        var sliceHeight = this._slides[this._currentSlideIndex].getHeight();
        this.settings.jalousie.sliceDuration = this._options.animationSpeed / this.settings.jalousie.boxCols;
        this.settings.jalousie.sliceTimeout = 75 * this.settings.jalousie.sliceDuration;
        var imgSrc = currentImage.readAttribute('src');
        for (var i = 0; i < this.settings.jalousie.boxCols; i++) {
            this._slides[target].insert(this._jalousieGetSliceElement(target, i));
            this._jalousieSlices[target].push($(this._jalousieGetSliceId(target, i)));
            this._jalousieSlices[target][i].setStyle({
                zIndex: 3,
                width: sliceWidth + rest + 'px',
                height: sliceHeight + 'px',
                marginLeft: i * sliceWidth + restMargin + 'px',
                backgroundImage: "url('" + imgSrc + "')",
                backgroundSize: this._slides[this._currentSlideIndex].getWidth() + 'px ' + sliceHeight + 'px',
                backgroundRepeat: 'no-repeat',
                backgroundPosition: -i * sliceWidth - restMargin + 'px top'
            });
            if (rest) {
                restMargin = rest;
                rest = 0;
            }
        }
        this._slides[target].setStyle({
            "zIndex": 2,
            "opacity": 1
        });
        this._slides[this._currentSlideIndex].setStyle({
            "zIndex": 1,
            "opacity": 0
        });
        for (var i = 0; i < this._jalousieSlices[target].length; i++) {
            setTimeout(this._jalousieShowSlice.bind(this, target, i), i * this.settings.jalousie.sliceTimeout);
        }
        setTimeout(this._jalousieSlicesShowed.bind(this, target), i * this.settings.jalousie.sliceTimeout + i * this.settings.jalousie.sliceDuration * 100);
    },
    _jalousieShowSlice: function (slide, index) {
        if (this._jalousieSlices[slide] && this._jalousieSlices[slide][index]) {
            new Effect.Morph(this._jalousieGetSliceId(slide, index), {
                style: 'width: 0px',
                duration: this.settings.jalousie.sliceDuration
            });
        }
    },
    _jalousieSlicesShowed: function (slide) {
        for (var i = 0; i < this._jalousieSlices[slide].length; i++)
            this._jalousieSlices[slide][i].remove();
        delete this._jalousieSlices[slide];
        this._animateAfter();
    },
    _animateFade: function (target) {
        this._slides[this._currentSlideIndex].setStyle({"zIndex": 1});
        new Effect.Morph(this._slides[this._currentSlideIndex].getAttribute('id'), {
            style: 'opacity:0;',
            duration: this._options.animationSpeed
        });
        this._slides[target].setStyle({"zIndex": 2});
        new Effect.Morph(this._slides[target].getAttribute('id'), {
            style: 'opacity:1;',
            duration: this._options.animationSpeed,
            afterFinish: function () {
                this._animateAfter();
            }.bind(this)
        });
    },
    _animateAfter: function () {
        this._animating = false;
        this._currentSlideIndex = this._animatingTo;
        if (this._videoSlideToRefresh) {
            this._refreshVideoSlide(this._videoSlideToRefresh);
            this._videoSlideToRefresh = null;
        }
    },
    _animateSimpleSlide: function (target) {
        var moveDir = "";
        var offsetDir = "-";
        if (this._sliderDirection == 'next') {
            moveDir = "-";
            offsetDir = "";
        }

        new Effect.Move(this._slides[this._currentSlideIndex].getAttribute('id'), {
            x: moveDir + this._slides[this._currentSlideIndex].getWidth(),
            mode: 'absolute',
            transition: Effect.Transitions.sinoidal,
            duration: this._options.animationSpeed,
            afterFinish: function () {
                this._slides[this._currentSlideIndex].setStyle({"zIndex": 1, "opacity": 0, "left": 0});
            }.bind(this)
        });
        var offset = this._slider.getWidth();
        this._slides[target].style.left = offsetDir + offset + 'px';
        this._slides[target].setStyle({"zIndex": 2, "opacity": 1});
        new Effect.Move(this._slides[target].getAttribute('id'), {
            x: 0,
            mode: 'absolute',
            transition: Effect.Transitions.sinoidal,
            duration: this._options.animationSpeed,
            afterFinish: function () {
                this._animateAfter();
            }.bind(this)
        });
        return this;
    },
    _animateBlindUp: function (target) {
        new Effect.BlindUp(this._slides[this._currentSlideIndex].getAttribute('id'), {
            duration: this._options.animationSpeed / 2,
            queue: {position: 'end', scope: this._slides[target].getAttribute('id')},
            afterFinish: function () {
                this._slides[target].setStyle({"display": "none"});
                this._slides[target].addClassName('blind');
            }.bind(this)
        });
        new Effect.BlindDown(this._slides[target].getAttribute('id'), {
            duration: this._options.animationSpeed / 2,
            queue: {position: 'end', scope: this._slides[target].getAttribute('id')},
            afterFinish: function () {
                this._slides[target].setStyle({"zIndex": 2, "opacity": 1, "display": "block"});
                this._slides[this._currentSlideIndex].setStyle({"zIndex": 1, "opacity": 0, "display": "block"});
                this._slides[target].removeClassName('blind');
                this._animateAfter();
            }.bind(this)
        });
    },
    _animateSlideAndBlink: function (target) {
        var moveDir = "";
        var offsetDir = "-";
        if (this._sliderDirection == 'next') {
            moveDir = "-";
            offsetDir = "";
        }

        var offset = this._slides[target].getWidth();
        this._slides[target].style.left = offsetDir + offset + 'px';
        new Effect.Parallel([
            new Effect.Move(this._slides[this._currentSlideIndex].getAttribute('id'), {
                sync: true,
                x: moveDir + this._slides[this._currentSlideIndex].getWidth(),
                mode: 'absolute',
                transition: Effect.Transitions.sinoidal,
                afterFinish: function () {
                    this._slides[this._currentSlideIndex].setStyle({"zIndex": 1, "opacity": 0, "left": 0});
                    this._animateAfter();
                }.bind(this)
            }),
            new Effect.Opacity(this._slides[this._currentSlideIndex].getAttribute('id'), { sync: true, from: 1, to: 0 }),
            new Effect.Move(this._slides[target].getAttribute('id'), {
                sync: true,
                x: 0,
                mode: 'absolute',
                transition: Effect.Transitions.sinoidal,
                afterFinish: function () {
                    this._slides[target].setStyle({"zIndex": 2});
                    this._animateAfter();
                }.bind(this)
            }),
            new Effect.Opacity(this._slides[target].getAttribute('id'), { sync: true, from: 0, to: 1 })
        ], {
            duration: this._options.animationSpeed,
            delay: 0
        });

        return this;
    },

    _animateSlideUpDown: function (target) {
        var currentSlide = this._slides[this._currentSlideIndex],
            targetSlide = this._slides[target];

        new Effect.Move(currentSlide.identify(), {
            y: -currentSlide.getHeight(),
            mode: 'absolute',
            transition: Effect.Transitions.sinoidal,
            duration: this._options.animationSpeed / 2,
            afterFinish: function () {
                currentSlide.setStyle({'zIndex': 1, 'opacity': 0, 'top': 0});
                targetSlide.setStyle({'zIndex': 2, 'opacity': 1, 'top': -targetSlide.getHeight() + 'px'});

                new Effect.Move(targetSlide.identify(), {
                    y: 0,
                    mode: 'absolute',
                    transition: Effect.Transitions.sinoidal,
                    duration: this._options.animationSpeed / 2,
                    afterFinish: function () {
                        this._animateAfter();
                    }.bind(this)
                });
            }.bind(this)
        });

        return this;
    },

    _isVideoSlide: function (el) {
        return !!$(el).down('iframe');
    },

    _getCurrentSlide: function () {
        return this._slides[this._currentSlideIndex];
    },

    _refreshVideoSlide: function (slide) {
        var iframeElement = slide.down('iframe');
        if (iframeElement) {
            iframeElement.src += '';
        }
    },

    _animate: function (target) {
        if (!this._animating && this._canAdvance(target) && this._slider.visible()) {
            this._animating = true;
            this._animatingTo = target;

            if (this._isVideoSlide(this._getCurrentSlide())) {
                this._videoSlideToRefresh = this._getCurrentSlide();
            }

            this._setActiveControlNav();
            if (this._controlNav) {
                if (this._slides[target].down().hasClassName('video-slide')) {
                    this._slider.select('.' + this._options.namespace + 'control-nav').first().setStyle({"display": 'none'});
                } else {
                    this._slider.select('.' + this._options.namespace + 'control-nav').first().setStyle({"display": 'block'});
                }
            }
            this._slides.each(function (el) {
                el.removeClassName(this._options.namespace + 'active-slide');
            }.bind(this));
            this._slides[target].addClassName(this._options.namespace + 'active-slide');

            this._directionUpdate();
            var type = (this._options.animationType == 'random')
                ? this._types[Math.floor(Math.random() * this._types.length)]
                : this._options.animationType;
            switch (type) {
                case 'jalousie':
                    this._animateJalousie(target);
                    break;
                case 'fade-appear':
                    this._animateFade(target);
                    break;
                case 'simple-slider':
                    this._animateSimpleSlide(target);
                    break;
                case 'blind-up-down':
                    this._animateBlindUp(target);
                    break;
                case 'slide-and-blink':
                    this._animateSlideAndBlink(target);
                    break;
                case 'slide-up-down':
                    this._animateSlideUpDown(target);
                    break;
            }
        }
        return this;
    }
});
