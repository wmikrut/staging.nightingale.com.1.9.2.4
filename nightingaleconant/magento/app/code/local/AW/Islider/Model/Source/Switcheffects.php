<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Model_Source_Switcheffects extends AW_Islider_Model_Source_Abstract
{
    const SE_FADE_APPEAR = 'fade-appear';
    const SE_SIMPLE_SLIDER = 'simple-slider';
    const SE_BLIND_UPDOWN = 'blind-up-down';
    const SE_SLIDE_UPDOWN = 'slide-up-down';
    const SE_SLIDE_BLINK = 'slide-and-blink';
    const SE_JALOUSIE = 'jalousie';
    const SE_RANDOM = 'random';

    public function toOptionArray()
    {
        return array(
            array('value' => self::SE_SIMPLE_SLIDER, 'label' => Mage::helper('awislider')->__('Simple Slider')),
            array('value' => self::SE_FADE_APPEAR, 'label' => Mage::helper('awislider')->__('Fade / Appear')),
            array('value' => self::SE_BLIND_UPDOWN, 'label' => Mage::helper('awislider')->__('Blind Up / Blind Down')),
            array('value' => self::SE_SLIDE_UPDOWN, 'label' => Mage::helper('awislider')->__('Slide Up / Slide Down')),
            array('value' => self::SE_SLIDE_BLINK, 'label' => Mage::helper('awislider')->__('Slide and Blink')),
            array('value' => self::SE_JALOUSIE, 'label' => Mage::helper('awislider')->__('Jalousie')),
            array('value' => self::SE_RANDOM, 'label' => Mage::helper('awislider')->__('Random effect for each switch'))
        );
    }
}