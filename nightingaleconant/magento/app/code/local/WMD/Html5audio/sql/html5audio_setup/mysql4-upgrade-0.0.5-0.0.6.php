<?php
/**
 * WMD_Html5audio Installer 0.0.6
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/

$installer = $this;
$installer->startSetup();

$installer->run("

CREATE TABLE IF NOT EXISTS {$installer->getTable('html5audio/mapper')} (
  `mapper_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Mapper ID',                   
  `link_id` int(11) NOT NULL COMMENT 'Link ID',                   
  `sample_id` int(11) NOT NULL COMMENT 'Sample ID',                  
  `product_id` int(11) NULL COMMENT 'Product ID',
  `playlist_id` int(11) NULL COMMENT 'Playlist ID',
  PRIMARY KEY (`mapper_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='HTML5 Audio Mapper Backend Table' ;

");

$installer->endSetup(); 


