<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Adminhtml_Awislider_ImageController extends Mage_Adminhtml_Controller_Action
{
    /**
     * @return AW_Islider_Model_Image
     */
    protected function _initImage()
    {
        /** @var AW_Islider_Model_Image $imageModel */
        $imageModel = Mage::getModel('awislider/image');
        $imageId  = (int) $this->getRequest()->getParam('id');
        if ($imageId) {
            try {
                $imageModel->load($imageId);
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }

        if (null !== $this->_getSession()->getImageSliderData()) {
            $imageModel->addData($this->_getSession()->getImageSliderData());
            $this->_getSession()->setImageSliderData(null);
        }
        if (null === $imageModel->getId()) {
            $imageModel
                ->setPid((int) $this->getRequest()->getParam('pid'))
                ->setStatus(AW_Islider_Model_Source_Status::ENABLED)
                ->setSortOrder(0)
                ->setContentType(AW_Islider_Model_Source_Images_Contenttype::CONTENT_TYPE_IMAGE)
            ;
        }
        Mage::register('current_image_slider', $imageModel);
        return $imageModel;
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function ajaxformAction()
    {
        $this->_initImage();
        $this->loadLayout();
        $this->renderLayout();
    }

    public function imageOptionsAction()
    {
        $this->_initImage();
        $html = Mage::getSingleton('core/layout')
            ->createBlock('awislider/adminhtml_slider_edit_tabs_images_ajaxform_form_image')
            ->toHtml()
        ;
        $this->getResponse()->setBody(Zend_Json::encode(array('text' => $html)));
        return;
    }

    public function videoOptionsAction()
    {
        $this->_initImage();
        $html = Mage::getSingleton('core/layout')
            ->createBlock('awislider/adminhtml_slider_edit_tabs_images_ajaxform_form_video')
            ->toHtml()
        ;
        $this->getResponse()->setBody(Zend_Json::encode(array('text' => $html)));
        return;
    }

    protected function _uploadFile($pId)
    {
        if (isset($_FILES['image_file']['name']) && $_FILES['image_file']['name']) {
            if (Mage::helper('awislider/image')->isAllowedImage($_FILES['image_file']['type'])) {
                if ($_FILES['image_file']['error'] == UPLOAD_ERR_OK) {
                    $uploader = new Varien_File_Uploader('image_file');
                    $uploader
                        ->setAllowedExtensions(null)
                        ->setAllowCreateFolders(TRUE)
                        ->setFilesDispersion(FALSE)
                        ->setAllowRenameFiles(true);
                    $fileName=$pId.'_'.uniqid('',true).'.'.pathinfo($_FILES['image_file']['name'],PATHINFO_EXTENSION );
                    $result=$uploader->save(AW_Islider_Helper_Image::getImagePath(), $fileName);

                    if($result){
                        return $result['file'];
                    }
                }
            }
            throw new Exception('Unallowed file type');
        }
        return false;
    }

    protected function _prepareDates(&$postData)
    {
        if ($postData['active_from']) {
            $postData['active_from'] = Mage::app()->getLocale()->date($postData['active_from'], null, null, false);
            $postData['active_from'] = $postData['active_from']->toString(Varien_Date::DATE_INTERNAL_FORMAT);
        }
        if ($postData['active_to']) {
            $postData['active_to'] = Mage::app()->getLocale()->date($postData['active_to'], null, null, false);
            $postData['active_to'] = $postData['active_to']->toString(Varien_Date::DATE_INTERNAL_FORMAT);
        }
        if($postData['active_to'] && $postData['active_to'] < $postData['active_from']) {
            throw new Exception('Value of \'Date To\' should be equal or greater than value of \'Date From\' field');
        }
        return $this;
    }

    protected function _getAjaxErrorHtml($message)
    {
        $_messagesBlock = Mage::getSingleton('core/layout')->getMessagesBlock();
        $_messagesBlock->addMessage(Mage::getModel('core/message')->error($message));
        return $_messagesBlock->getGroupedHtml();
    }

    protected function _sendAjaxResponse($content)
    {
        $response = Mage::app()->getResponse();
        $response->setBody('<script type="text/javascript">if(this && this.parent && this.parent.awISAjaxForm)'
            . ' {this.parent.awISAjaxForm.formAfterPost(' . Zend_Json::encode($content) . ')}</script>'
        );
        return $this;
    }

    public function saveimageAction()
    {
        $_result = array(
            's'      => false,
            'errors' => ''
        );
        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getParams();
            $imageModel = $this->_initImage();
            try {
                $this->_prepareDates($postData);
            } catch (Exception $e) {
                $_result['errors'] = $this->_getAjaxErrorHtml($e->getMessage());
                $this->_sendAjaxResponse($_result);
                return $this;
            }
            $currentLocation = $imageModel->getLocation();
            $imageModel->setData($postData);

            try {
                $newFileName = $this->_uploadFile($imageModel->getPid());
                if ($imageModel->isImageUploaded() && $newFileName) {
                    $imageModel->setLocation($newFileName);
                }
                if ($imageModel->isImageRemote() && $imageModel->getImageRemote()) {
                    $imageModel->setLocation($imageModel->getImageRemote());
                }
                if (!$imageModel->getLocation() && $currentLocation) {
                    $imageModel->setLocation($currentLocation);
                }
                if (!$imageModel->getLocation()) {
                    throw new Exception('Please, specify image file');
                }

                if ($imageModel->getGaValue() && !is_numeric($imageModel->getGaValue())) {
                    throw new Exception('Please set a number 0 or greater for Google Analytics Value.');
                }

                $imageModel->save();
                Mage::helper('awislider')->cleanFPCForSlider($imageModel->getSlider());
                $this->_getSession()->setImageSliderData(null);
                $_result['s'] = true;
            } catch (Exception $e) {
                $_result['errors'] = $this->_getAjaxErrorHtml($e->getMessage());
            }
        }
        $this->_sendAjaxResponse($_result);
        return $this;
    }

    public function removeAction()
    {
        $image = $this->_initImage();
        try {
            $image->delete();
            $this->_getSession()->addSuccess(
                $this->__('Image has been successfully deleted')
            );
        } catch (Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        }
        $this->_redirect('adminhtml/awislider_slider/edit',
            array(
                'id'           => $this->getRequest()->getParam('pid'),
                'continue_tab' => $this->getRequest()->getParam('continue_tab')
            )
        );
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/awislider');
    }
}