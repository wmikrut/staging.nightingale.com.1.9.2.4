<?php     
/**
 * WMD_Html5audio_Model_System_Config_Source_Skin
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.com>
 * @copyright 2013 Dominik Wyss | Digiswiss (http://www.digiswiss.ch)
 * @link      http://wmdextensions.com/
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Model_System_Config_Source_Skin
{
    
    public function toOptionArray()
    {
        
        $data = array(                                         
            array('value' => 'pink', 'label' => 'Pink Flag'),
            array('value' => 'blue', 'label' => 'Blue Monday'),
        );
        if (!Mage::getStoreConfig('html5audio/player/overlay'))
        {                                                                                    
            array_unshift($data, array('value' => 'anthracite', 'label' => 'Anthracite Mini'));
            array_unshift($data, array('value' => 'flex', 'label' => 'Anthracite Flex'));
            array_unshift($data, array('value' => 'button', 'label' => 'Anthracite Button'));
            array_unshift($data, array('value' => 'minimal', 'label' => 'Pink Flag Mini'));
        }  
        if (Mage::getStoreConfig('html5audio/sample/stream'))
        {                                                                                    
            $data = array(
                array('value' => 'stream', 'label' => 'Anthracite Stream')
            );
            array_unshift($data, array('value' => 'button', 'label' => 'Anthracite Button'));
        }
        array_unshift($data, array('value'=>'', 'label'=> Mage::helper('adminhtml')->__('-- Please Select --')));
    		
        return $data;
    }

}