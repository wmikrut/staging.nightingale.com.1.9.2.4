<?php
/**
 * Pulsar Plugins
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.pulsarplugins.com/pulsarplugins-license.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento community edition
 * Pulsar Plugins does not guarantee correct work of this extension
 * on any other Magento edition except Magento community edition.
 * Pulsar Plugins does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   PP
 * @package    PP_EmailByShipping
 * @version    1.1
 * @copyright  Copyright (c) 2010-2014 Pulsar Plugins (http://www.pulsarplugins.com/)
 * @license    http://www.pulsarplugins.com/pulsarplugins-license.txt
 *
*/
class PP_EmailByShipping_Model_Mysql4_Ordermail_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('ordermailtable/ordermail');
    }
    
    public function addStoreFilter($storeId)
    {
        $storeId = intVal($storeId);
        $this->getSelect()->where('stores="" OR stores LIKE "%,'.$storeId.',%"');
        
        return $this;
    }    
    
    public function addCustomerGroupFilter($groupId)
    {
        $groupId = intVal($groupId);
        $this->getSelect()->where('cust_groups="" OR cust_groups LIKE "%,'.$groupId.',%"');
        
        return $this;
    }    
}