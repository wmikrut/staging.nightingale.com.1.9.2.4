<?php
/**
 * WMD_Html5audio_Helper_Data_Rwd
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Helper_Data_Rwd extends WMD_Html5audio_Helper_Data
{      

        
    public function returnSkinPath()
    {                 
        $skinPath = 'css/skin/anthracite.mini/jplayer.anthracite.mini.css';
        switch ($this->_getConfig()) {
            case 'pink':
                $skinPath = 'css/skin/pink.flag/jplayer.pink.flag.css';
                break;
            case 'blue':
                $skinPath = 'css/skin/blue.monday/jplayer.blue.monday.css';
                break;
            case 'minimal':
                $skinPath = 'css/skin/minimal.skin/jplayer.minimal.skin.css';
                break;
            case 'anthracite':
                $skinPath = 'css/skin/anthracite.mini/jplayer.anthracite.mini.css';
                break;
            case 'stream':
                $skinPath = 'css/skin/anthracite.stream/jplayer.anthracite.stream.css';
                break;  
            case 'flex':
                $skinPath = 'css/skin/anthracite.flex/jplayer.anthracite.flex.css'; 
                break;      
            case 'button':
                $skinPath = 'css/skin/anthracite.button/jplayer.anthracite.button.css'; 
                break;        
        }
        return $skinPath;        
    }
}