<?php
/**
 * @category   Apptrian
 * @package    Apptrian_ProductSlider
 * @author     Apptrian
 * @copyright  Copyright (c) 2015 Apptrian (http://www.apptrian.com)
 * @license    http://www.apptrian.com/license    Proprietary Software License (EULA)
 */
class Apptrian_ProductSlider_Model_Config_Descriptionlimit extends Mage_Core_Model_Config_Data
{
    
    public function _beforeSave()
    {
    
        $result = $this->validate();
        
        if ($result !== true) {
            
            Mage::throwException(implode("\n", $result));
            
        }
        
        return parent::_beforeSave();
        
    }
    
    public function validate()
    {
        
        $errors = array();
        $helper = Mage::helper('apptrian_productslider');
        $value  = $this->getValue();
        
        if (!Zend_Validate::is($value, 'Digits')) {
            $errors[] = $helper->__('Maximum number of characters for description text must be an integer.');
        }
        
        if (!Zend_Validate::is($value, 'GreaterThan', array(-1))) {
            $errors[] = $helper->__('Maximum number of characters for description text must be positive integer.');
        }
        
        if (empty($errors)) {
            return true;
        }
        
        return $errors;
        
    }
    
}