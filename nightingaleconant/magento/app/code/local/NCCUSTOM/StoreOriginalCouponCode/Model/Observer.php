<?php
class NCCUSTOM_StoreOriginalCouponCode_Model_Observer {

    public function exportFeed($observer) {

        include('mysql-credentials-production.php');
        
        if ( $nccustomer_customer = Mage::getSingleton('customer/session')->isLoggedIn()) {
    	    $nccustom_customer_id = trim($_SESSION['customer_base']['id']);
    	    $nccustom_customer_data = Mage::getModel('customer/customer')->load($nccustom_customer_id);
    	    
    	    $nccustom_original_coupon_code = trim($nccustom_customer_data->getoriginal_coupon_code());
    	
    	    if ( $nccustom_original_coupon_code == '' ) {
    	    
    		if (isset( $_SESSION['checkout']['coupon_code'])) {
    		    $nccustom_coupon_code = $_SESSION['checkout']['coupon_code'];
    		} else {
    		    $nccustom_coupon_code = 'NoCoupon';
    		}
		$nccustom_customer_data->setoriginal_coupon_code($nccustom_coupon_code);
    		    
		 try {
		    $nccustom_customer_data->save();
		 } catch (Exception $ex) {
		    Mage::log($ex);
		 }
    	    }
        }
    }
}
?>