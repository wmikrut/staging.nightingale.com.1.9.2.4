if (!Itoris) {
	var Itoris = {};
}
Itoris.PageContentSlider = Class.create({
	defaultTitleWidth : 185,
	defaultTitleHeight : 75,
	initialize: function(blockId, config) {
		this.block = $(blockId);
		this.config = config;
		this.currentSlide = 0;
		this.slideHeight = 0;
		this.slideWidth = 0;
		this.titleHeight = this.defaultTitleHeight;
		this.titleWidth = this.defaultTitleWidth;
		this.visibleTitleQty = 0;
		this.movedUnvisibleTitleQty = 0;
		this.slideQty = 0;
		this.prepareBlock();
		this.addEvents();
		if (parseInt(this.config.slide_automatically)) {
			this.autoSliding = new PeriodicalExecuter(this.nextSlide.bind(this), this.config.delay || 5);
		}
		var obj = this;
		Event.observe(document, 'dom:loaded', function() {
			obj.hideSlides();
		});
		Event.observe(window, 'resize', this.updateSlideWidth.bind(this));
	},
	hideSlides : function() {
		var slides = this.block.select('.itoris_pagecontentslider_slide');
		for (var i = 1; i < slides.length; i++) {
			slides[i].hide();
		}
	},
	prepareBlock: function() {
		var titles = this.block.select('.itoris_pagecontentslider_title');
		this.changeClass(titles[0], 'itoris_pagecontentslider_title_active');
		var slides = this.block.select('.itoris_pagecontentslider_slide');
		this.slideQty = slides.length;
		this.block.origOffsetWidth = this.block.offsetWidth;
		if (this.config.mode == 'top' || this.config.mode == 'bottom') {
			if (this.config.mode == 'bottom') {
				this.block.select('.itoris_pagecontentslider_slides_box')[0].insert({after: this.block.select('.itoris_pagecontentslider_titles_box')[0]});
			}
			var blockWidth = this.block.offsetWidth - this.getBorderWidth(this.block);
			var maxTitlesQty = parseInt(blockWidth / this.defaultTitleWidth);
			this.visibleTitleQty = (maxTitlesQty < titles.length ? maxTitlesQty : titles.length);
			var width = blockWidth / this.visibleTitleQty - this.getBorderWidth(titles[0]);
			titles.each(function(elm) {
				elm.setStyle({
					width  : width + 'px',
					borderBottom : 'none'
				});
			});
			this.titleWidth = width;
			this.block.select('.itoris_pagecontentslider_titles_box')[0].setStyle({height: this.defaultTitleHeight + 'px'});
			var contentHeight = this.block.offsetHeight - this.getBorderWidth(this.block, true) - this.getBorderWidth(slides[0], true) - parseFloat(this.block.select('.itoris_pagecontentslider_titles_box')[0].getStyle('height'));
			this.block.select('.itoris_pagecontentslider_slides_box')[0].setStyle({height: contentHeight + 'px'});
			slides.each(function(elm) {
				elm.setStyle({height: contentHeight + 'px'});
			});
			if (this.visibleTitleQty < this.slideQty) {
				this.block.select('.itoris_pagecontentslider_titles')[0].setStyle({width: titles[0].getWidth() * this.slideQty + 'px'});
			}
			this.slideHeight = contentHeight;
			this.slideWidth = this.block.select('.itoris_pagecontentslider_slides_box')[0].up().getWidth();
		} else {
			var blockHeight = this.block.offsetHeight - this.getBorderWidth(this.block, true);
			var maxTitlesQty = parseInt(blockHeight / this.defaultTitleHeight);
			this.visibleTitleQty = (maxTitlesQty < titles.length ? maxTitlesQty : titles.length);
			var height = blockHeight / this.visibleTitleQty - this.getBorderWidth(titles[0], true);
			titles.each(function(elm) {
				elm.setStyle({
					height : height + 'px',
					borderRight : 'none'
				});
			});
			this.titleHeight = height;
			this.block.select('.itoris_pagecontentslider_titles_box')[0].setStyle({width : this.defaultTitleWidth + 'px', float: this.config.mode});
			var contentWidth = this.block.offsetWidth - this.getBorderWidth(this.block) - this.getBorderWidth(slides[0]) - parseFloat(this.block.select('.itoris_pagecontentslider_titles_box')[0].getStyle('width'));
			this.block.select('.itoris_pagecontentslider_slides_box')[0].setStyle({width: contentWidth + 'px'});
			slides.each(function(elm) {
				elm.setStyle({width: contentWidth + 'px'});
			});
			this.slideHeight = this.block.select('.itoris_pagecontentslider_slides_box')[0].up().getHeight();
			this.slideWidth = contentWidth;
		}
		slides[0].setStyle({left: '0px'});
	},
	updateSlideWidth: function() {
		if (this.block.offsetWidth != this.block.origOffsetWidth) {
			var contentWidth = this.block.select('.itoris_pagecontentslider_slides_box')[0].getWidth();
			if (this.block.select('.itoris_pagecontentslider_slides_box')[0].increaseWidth) {
				contentWidth++;
			}
			contentWidth += this.block.offsetWidth - this.block.origOffsetWidth;
			this.block.origOffsetWidth = this.block.offsetWidth;
			this.block.select('.itoris_pagecontentslider_slides_box')[0].setStyle({width: (contentWidth - 1) + 'px'});
			this.block.select('.itoris_pagecontentslider_slides_box')[0].increaseWidth = true;
			this.block.select('.itoris_pagecontentslider_slide').each(function(elm){
				elm.setStyle({width: contentWidth + 'px'});
				if (parseNumber(elm.style.left) == this.slideWidth) {
					elm.setStyle({left: contentWidth + 'px'});
				}
			}.bind(this));
			this.slideWidth = contentWidth;

			if (this.config.mode == 'top' || this.config.mode == 'bottom') {
				var titles = this.block.select('.itoris_pagecontentslider_title');
				var blockWidth = this.block.offsetWidth - this.getBorderWidth(this.block);
				var maxTitlesQty = parseInt(blockWidth / this.defaultTitleWidth);
				this.visibleTitleQty = (maxTitlesQty < titles.length ? maxTitlesQty : titles.length);
				var width = blockWidth / this.visibleTitleQty - this.getBorderWidth(titles[0]);
				titles.each(function(elm) {
					elm.setStyle({
						width  : width + 'px',
						borderBottom : 'none'
					});
				});
				this.titleWidth = width;
				if (this.visibleTitleQty < this.slideQty) {
					this.block.select('.itoris_pagecontentslider_titles')[0].setStyle({width: titles[0].getWidth() * this.slideQty + 'px'});
				}

			}
		}
	},
	getBorderWidth: function(elm, topAndBottom) {
		if (topAndBottom) {
			var width = parseNumber(elm.getStyle('borderTopWidth')) + parseNumber(elm.getStyle('borderBottomWidth'));
		} else {
			var width = parseNumber(elm.getStyle('borderLeftWidth')) + parseNumber(elm.getStyle('borderRightWidth'));
		}
		if (isNaN(width)) {
			return 0;
		}

		return width;
	},
	addEvents: function() {
		var titles = this.block.select('.itoris_pagecontentslider_title');
		for (var i = 0; i < titles.length; i++) {
			Event.observe(titles[i], 'click', this.showSlide.bind(this, i));
		}
		Event.observe(this.block, 'mouseover', this.stopAutosliding.bind(this, true));
		Event.observe(this.block, 'mouseout', this.stopAutosliding.bind(this, false));
	},
	stopAutosliding : function(flag) {
		if (this.autoSliding) {
			if (flag) {
				this.autoSliding.stop();
			} else {
				this.autoSliding.registerCallback();
			}
		}
	},
	nextSlide: function() {
		if (this.slideQty <= this.currentSlide + 1) {
			this.showSlide(0);
		} else {
			this.showSlide(this.currentSlide + 1);
		}
	},
	hideNotCurrentSlides : function() {
		var slides = this.block.select('.itoris_pagecontentslider_slide');
		for (var i = 0; i < slides.length; i++) {
			if (i != this.currentSlide) {
				slides[i].hide();
			}
		}
	},
	afterFadeEffect : function(num, effect) {
		if (num != this.currentSlide) {
			effect.element.hide();
		}
		this.hideNotCurrentSlides();
		this.showCurrentSlide();
	},
	showSlide: function(num) {
		if (this.currentSlide == num) {
			return;
		}
		var currentSlide = this.block.select('.itoris_pagecontentslider_slide')[this.currentSlide];
		var nextSlide = this.block.select('.itoris_pagecontentslider_slide')[num];
		var obj = this;
		switch (this.config.slide_effect) {
			case 'fade':
				nextSlide.setStyle({'display':'block', left: '0'});
				nextSlide.hide();
				new Effect.Fade(currentSlide, {duration: 0.5,
					afterFinishInternal: function(effect) {
						obj.afterFadeEffect(num, effect);
					}});
				new Effect.Appear(nextSlide, {duration: 0.5,
					afterFinishInternal: function(effect) {
						obj.afterFadeEffect(num, effect);
					}});
				break;
			case 'horizontal':
				nextSlide.setStyle({'display':'block'});
				if (num > this.currentSlide) {
					nextSlide.setStyle({left: this.slideWidth + 'px'});
					var step = - this.slideWidth;
				} else {
					nextSlide.setStyle({left: - this.slideWidth + 'px'});
					var step = this.slideWidth;
				}
				new Effect.Morph(currentSlide, {
					style: 'left:' + step + 'px',
					duration: 0.5,
					afterFinishInternal: function(effect) {
						if (num != obj.currentSlide) {
							effect.element.hide();
						}
						obj.showCurrentSlide();
					}
				});
				new Effect.Morph(nextSlide, {
					style: 'left:0px',
					duration: 0.5,
					afterFinishInternal: function(effect) {
						new Effect.Move(effect.element,{ x: -parseInt(effect.element.getWidth() * 0.1) * (step > 0 ? -1 : 1), y: 0, duration: 0.1, afterFinishInternal: function(effect) {
							new Effect.Move(effect.element,	{ x: parseInt(effect.element.getWidth() * 0.1) * (step > 0 ? -1 : 1), y: 0, duration: 0.2, afterFinishInternal: function(effect) {
								if (num != obj.currentSlide) {
									effect.element.hide();
								}
								obj.showCurrentSlide();
							}});
						}});
					}
				});
				break;
			case 'vertical':
			default:
				nextSlide.setStyle({'display':'block', left: '0'});
				if (num > this.currentSlide) {
					nextSlide.setStyle({top: this.slideHeight + 'px'});
					var step = - this.slideHeight;
				} else {
					nextSlide.setStyle({top: - this.slideHeight + 'px'});
					var step = this.slideHeight;
				}
				new Effect.Morph(currentSlide, {
					style: 'top:' + step + 'px',
					duration: 0.5,
					afterFinishInternal: function(effect) {
						if (num != obj.currentSlide) {
							effect.element.hide();
						}
						obj.showCurrentSlide();
					}
				});
				new Effect.Morph(nextSlide, {
					style: 'top:0px',
					duration: 0.5,
					afterFinishInternal: function(effect) {
						if (num != obj.currentSlide) {
							effect.element.hide();
						}
						obj.showCurrentSlide();
					}
				});
		}
		if (this.visibleTitleQty != this.slideQty) {
			var moveTitleQty = 0;
			if (num == 0 && this.currentSlide == this.slideQty - 1) {
				moveTitleQty = true;
				this.movedUnvisibleTitleQty = 0;
			} else {
				var titlePos = num + 1 + this.movedUnvisibleTitleQty;
				var middlePos = Math.ceil(this.visibleTitleQty / 2);
				var difFromMiddlePos = middlePos - titlePos;
				if (titlePos < middlePos) {
					moveTitleQty = difFromMiddlePos > -this.movedUnvisibleTitleQty ? -this.movedUnvisibleTitleQty : difFromMiddlePos;
				} else if (titlePos > middlePos) {
					difFromMiddlePos = -difFromMiddlePos;
					var unmovedUnvisibleTitleQty = this.slideQty + this.movedUnvisibleTitleQty - this.visibleTitleQty;
					moveTitleQty = - (difFromMiddlePos > unmovedUnvisibleTitleQty ? unmovedUnvisibleTitleQty : difFromMiddlePos);
				} else if (middlePos == 1 && this.visibleTitleQty == 2) {
					if (titlePos == 1) {
						moveTitleQty = this.movedUnvisibleTitleQty ? 1 : 0;
					}
				}
				this.movedUnvisibleTitleQty += moveTitleQty;
			}
			if (moveTitleQty) {
				var titlesBox = this.block.select('.itoris_pagecontentslider_titles')[0];
				if (this.config.mode == 'top' || this.config.mode == 'bottom') {
					new Effect.Morph(titlesBox, {
						style: 'left:' + (this.titleWidth * this.movedUnvisibleTitleQty) + 'px',
						duration: 0.5
					});
				} else {
					new Effect.Morph(titlesBox, {
						style: 'top:' + (this.titleHeight * this.movedUnvisibleTitleQty) + 'px',
						duration: 0.5
					});
				}
			}
		}
		var titles = this.block.select('.itoris_pagecontentslider_title');
		this.changeClass(titles[num], 'itoris_pagecontentslider_title_active');
		this.changeClass(titles[this.currentSlide], null, 'itoris_pagecontentslider_title_active');
		this.currentSlide = num;
	},
	showCurrentSlide : function() {
		var slide = this.block.select('.itoris_pagecontentslider_slide')[this.currentSlide];
		slide.show();
		slide.setStyle({left:'0px', top: '0px'});
		return this;
	},
	changeClass : function(elm, addClass, removeClass) {
		if (addClass) {
			if (!elm.hasClassName(addClass)) {
				elm.addClassName(addClass);
			}
		}
		if (removeClass) {
			if (elm.hasClassName(removeClass)) {
				elm.removeClassName(removeClass);
			}
		}
	}
});