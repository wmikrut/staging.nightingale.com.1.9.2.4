<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Model_Source_Representation extends AW_Islider_Model_Source_Abstract
{
    const RESPONSIVE = 1;
    const FIXED = 2;

    const RESPONSIVE_LABEL = 'Responsive';
    const FIXED_LABEL = 'Fixed';

    public function toOptionArray()
    {
        return array(
            array('value' => self::RESPONSIVE, 'label' => Mage::helper('awislider')->__(self::RESPONSIVE_LABEL)),
            array('value' => self::FIXED, 'label' => Mage::helper('awislider')->__(self::FIXED_LABEL))
        );
    }
}