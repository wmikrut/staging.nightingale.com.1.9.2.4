<?php
class NCCUSTOM_RestrictPaymentMethods_Model_Observer 
{
    public function filterpaymentmethod(Varien_Event_Observer $observer) 
    {		
	//$_order = Mage::getModel('sales/order')->load(Mage::getSingleton('checkout/session')->getLastRealOrderId(), 'increment_id');
	$_quote = $observer->getEvent()->getQuote();
	$method = $observer->getEvent()->getMethodInstance();
	$postDatas =  Mage::app()->getRequest()->getParams();
	$code = $method->getCode();
	$currentUrl = Mage::helper('core/url')->getCurrentUrl();
	
	//*****************************************************************************
	//* Do not show Authorize.net Installments on the Frontend for now
	//*****************************************************************************
	if ( $code == "authorizenet" ) 
	{
	    $result = $observer->getEvent()->getResult(); 
	    $result->isAvailable = false;
	}

	if ( stripos($currentUrl, 'staging' ) === false )
	{

	    //******************************************************************************
	    //* Do not show these methods on the Frontend for the production site only.
	    //* These methods will always show on staging sites.
	    //******************************************************************************
	    if ( $code == "checkmo" )
	    {
		$result = $observer->getEvent()->getResult();
		$result->isAvailable = false;
	    }
	    if ( $code == "purchaseorder" )
	    {
		$result = $observer->getEvent()->getResult();   
		$result->isAvailable = false;
	    }
	}
	
	//******************************************************************************
	//* Do not allow Amazon payments for customers ordering digital products
	//* that are not logged into an account.
	//******************************************************************************
	if ( $code == "amazon_payments" )
	{
	    $restrict = 0;

	    $_cartItems = $_quote->getAllVisibleItems();
	    foreach ( $_cartItems as $_item )
	    {
		if ( $_item->getProductType() == 'downloadable' )
	        {
	    	    if(!Mage::getSingleton('customer/session')->isLoggedIn())
	    	    {
	    		$restrict = 1;
	    		break;
	    	    }
	        }
	    }
	    if ( $restrict == 1 ) 
	    {
		$result = $observer->getEvent()->getResult();   
		$result->isAvailable = false;
	    }
	}
    }
}
?>