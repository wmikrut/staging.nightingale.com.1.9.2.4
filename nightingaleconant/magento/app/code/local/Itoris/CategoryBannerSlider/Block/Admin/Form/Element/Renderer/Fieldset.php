<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_CATEGORYBANNERSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_CategoryBannerSlider_Block_Admin_Form_Element_Renderer_Fieldset extends Mage_Adminhtml_Block_Widget_Form_Renderer_Fieldset {

	protected $bannerSet = array();

    protected function _construct() {
        $this->setTemplate('itoris/categorybannerslider/banners/renderer/fieldset.phtml');
		$this->bannerSet = Mage::registry('banner_set');
    }

	/**
	 * Get categories of the banners set
	 *
	 * @return array|null
	 */
	public function getCategories() {
		if(isset($this->bannerSet['categories'])){
			$data = $this->getDataHelper()->prepareCategories($this->bannerSet['categories']);
			if(empty($data)){
				return null;
			}
			$categories = array();
			foreach($data as $row){
				$categories[$row['website_id']]['name'] = $row['website'];
				$categories[$row['website_id']]['stores'][$row['store_id']]['name'] = $row['store'];
				$categories[$row['website_id']]['stores'][$row['store_id']]['categories'][] = $row;
			}
			return $categories;
		}
		return null;
	}

	/**
	 * Get categories html for categories of the banners set
	 *
	 * @param array $data
	 * @return string
	 */
	public function getCategoriesHtml(array $data) {
		$html = '';
		foreach($data as $websiteId => $website){
			$html .= '<div class="website_label" id="w_'. $websiteId .'">' . $website['name'];
			foreach($website['stores'] as $storeId => $store){
				$html .= '<div class="store_label" id="w_'. $websiteId .'s_'. $storeId .'">' . $store['name'];
				foreach($store['categories'] as $category){
					$categoryLabel = (!empty($category['categories'])) ? $category['categories'] : '<b>'. $category['current_category']['name'] .'</b>';
					$html .= '<div class="category_label" id="w_'. $websiteId .'s_'. $storeId .'c_'. $category['current_category']['id'] .'">'
							 .'<div class="text">' . $categoryLabel . '</div>'
							. '<div class="category_delete" onclick="category_delete('. $websiteId .', '. $storeId .', '. $category['current_category']['id'] .')">'. $this->getDataHelper()->__('delete') .'</div>'
							. '<div class="category_show"><a href="'. $category['current_category']['url'] .'" target="_blank">'. $this->getDataHelper()->__('show') .'</a></div>'
							. '<input type="hidden" name="set[categories]['. $storeId .'_'. $category['current_category']['id'] .']" value="1" />'
							.'<br/></div>';
				}
				$html .= '</div>';
			}
			$html .= '</div>';
		}
		return $html;
	}

	public function getStoreDropdownHtml(){
        $html = '<select id="store_dropdown" onchange="getCategoriesDropdown(this.value)">';
		$websites = Mage::app()->getWebsites();
		/** @var $website Mage_Core_Model_Website */
        foreach ($websites as $website){
			$html .= '<optgroup label="' . $website->getName() . '">';
			$stores = $website->getStores();
            foreach ($stores as $store){
                $html .= '<option  value="'. $store->getId() .'" >' . $store->getName() . '</option>';
				$html .= '<script type="text/javascript">
							stores['. $store->getId() .'] = [];
							stores['. $store->getId() .'].storeName = "'. $store->getName() .'";
							stores['. $store->getId() .'].websiteId = "'. $website->getId() .'";
							stores['. $store->getId() .'].websiteName = "'. $website->getName() .'";
						  </script>';
            }
            $html .= '</optgroup>';
        }
        $html.='</select>';
		return $html;
	}

	public function getCategoryDropdownHtml(){
        $html = '<select id="category_dropdown">';
        $html.='</select>';
		$html .= '<script type="text/javascript">'
				.'deleteLabel = "' . $this->getDataHelper()->__('delete') . '";'
				.'categories = ' . Mage::helper('itoris_categorybannerslider/category')->getCategoriesJson() . ';'
				.'showLabel = "' . $this->getDataHelper()->__('show') . '";'
				.'getCategoriesDropdown($("store_dropdown").value);'
				.'</script>';
        return $html;
    }

	public function getEffectDropdownHtml(){
		if(!empty($this->bannerSet)){
			$effect = $this->bannerSet['effect'];
		}else{
			$effect = Itoris_CategoryBannerSlider_Model_Sets::EFFECT_HORIZONTAL_SLIDING;
		}
		$effects = array(
						Itoris_CategoryBannerSlider_Model_Sets::EFFECT_HORIZONTAL_SLIDING => $this->__('Horizontal sliding'),
						Itoris_CategoryBannerSlider_Model_Sets::EFFECT_FADE => $this->__('Fade'),
						Itoris_CategoryBannerSlider_Model_Sets::EFFECT_SIMPLE => $this->__('Simple'),
						);
		$html = '<select name="set[effect]">';
		foreach($effects as $value => $title){
			$html .= '<option value="' . $value . '" ';
			$html .= ($value == $effect) ? 'selected="selected"' : '';
			$html .= '>' . $title . '</option>';
		}
		$html .= '</select>';
		return $html;
	}

	public function getSkinDropdownHtml(){
		if(!empty($this->bannerSet)){
			$skin = $this->bannerSet['skin'];
		}else{
			$skin = Itoris_CategoryBannerSlider_Model_Sets::SKIN_NUM_PAGER_ARROWS;
		}
		$skins = array(
						Itoris_CategoryBannerSlider_Model_Sets::SKIN_NUM_PAGER_ARROWS => $this->__('Numbered Pager and Arrows'),
						Itoris_CategoryBannerSlider_Model_Sets::SKIN_TITLED_PAGER_ARROWS => $this->__('Titled Pager and Arrows'),
						Itoris_CategoryBannerSlider_Model_Sets::SKIN_NUM_PAGER => $this->__('Numbered Pager'),
						Itoris_CategoryBannerSlider_Model_Sets::SKIN_TITLED_PAGER => $this->__('Titled Pager'),
						Itoris_CategoryBannerSlider_Model_Sets::SKIN_ARROWS => $this->__('Arrows'),
						Itoris_CategoryBannerSlider_Model_Sets::SKIN_NO => $this->__('No skin'),
					);
		$html = '<select name="set[skin]">';
		foreach($skins as $value => $title){
			$html .= '<option value="' . $value . '" ';
			$html .= ($value == $skin) ? 'selected="selected"' : '';
			$html .= '>' . $title . '</option>';
		}
		$html .= '</select>';
		return $html;
	}

	/**
	 * Return checked="checked" if autorotation enabled for banners set, else return empty string
	 *
	 * @return string
	 */
	public function getAutorotationValue(){
		if(!empty($this->bannerSet)){
			return ($this->bannerSet['autorotation']) ? 'checked="checked"' : '';
		}
		return 'checked="checked"';
	}

	public function getBannerSetId(){
		if(!empty($this->bannerSet)){
			return $this->bannerSet['set_id'];
		}
		return;
	}

	public function getBannerSetWidth(){
		if(!empty($this->bannerSet)){
			return $this->bannerSet['width'];
		}
		return Itoris_CategoryBannerSlider_Model_Sets::DEFAULT_WIDTH;
	}

	public function getBannerSetHeight(){
		if(!empty($this->bannerSet)){
			return $this->bannerSet['height'];
		}
		return Itoris_CategoryBannerSlider_Model_Sets::DEFAULT_HEIGHT;
	}

	/** @return Itoris_CategoryBannerSlider_Helper_Data */
	public function getDataHelper(){
		return Mage::helper('itoris_categorybannerslider');
	}
}

?>
 
