<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Model_Slider extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('awislider/slider');
    }

    public function _afterLoad()
    {
        if(is_string($this->getData('store'))) {
            $this->setData('store', @explode(',', $this->getData('store')));
        }
        return parent::_afterLoad();
    }

    protected function _getIsBlockIdUnique()
    {
        return $this->getResource()->getIsBlockIdUnique($this);
    }

    public function _beforeSave()
    {
        if (!preg_match('/^[a-zA-Z0-9-_]*$/', $this->getBlockId())) {
            throw new Exception('The following symbols are allowed to be used in the \'Block Code ID\' field: a-z 0-9 - _');
        }
        if (!$this->_getIsBlockIdUnique()) {
            throw new Exception('Block with the same \'Block Code ID\' already exists in selected store views');
        }
        if (is_array($this->getData('store'))) {
            $this->setData('store', @implode (',', $this->getData('store')));
        }
        if (!$this->getData('transition_speed')) {
            $this->setData('transition_speed', 1);
        }
        if ($this->getRepresentation() == AW_Islider_Model_Source_Representation::RESPONSIVE) {
            $this->setWidth(0);
            $this->setHeight(0);
        }
        return parent::_beforeSave();
    }

    public function getImagesCollection()
    {
        $_collection = Mage::getModel('awislider/image')->getCollection();
        $_collection->addSliderFilter($this->getId());
        return $_collection;
    }

    protected function _beforeDelete()
    {
        $imageCollection = $this->getImagesCollection();
        foreach ($imageCollection as $image) {
            $image->delete();
        }
        return parent::_beforeDelete();
    }
}