<?php
/**
 * WMD_Html5audio Installer 0.0.2
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/

$installer = $this;
$installer->startSetup();

$installer->run("
 
CREATE TABLE IF NOT EXISTS {$installer->getTable('html5audio/playlist_sample')} (
  `playlist_id` int(11) NOT NULL COMMENT 'Playlist ID',
  `sample_id` int(11) NOT NULL COMMENT 'Sample ID',
  KEY (`playlist_id`, `sample_id`),
  KEY `IDX_HA_PLAYLIST_SAMPLE_PLAYLIST_ID` (`playlist_id`),
  KEY `IDX_HA_PLAYLIST_SAMPLE_SAMPLE_ID` (`sample_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Html5 Audio Playlist Sample Linkage Table'; 
 
");

$installer->getConnection()->addForeignKey(
    $installer->getFkName('html5audio/playlist_sample', 'sample_id', 'html5audio/sample', 'sample_id'),
    $installer->getTable('html5audio/playlist_sample'),
    'sample_id',
    $installer->getTable('html5audio/sample'),
    'sample_id'
);

$installer->getConnection()->addForeignKey(
    $installer->getFkName('html5audio/playlist_sample', 'playlist_id', 'html5audio/sample', 'playlist_id'),
    $installer->getTable('html5audio/playlist_sample'),
    'playlist_id',
    $installer->getTable('html5audio/playlist'),
    'playlist_id'
); 

$installer->endSetup(); 

