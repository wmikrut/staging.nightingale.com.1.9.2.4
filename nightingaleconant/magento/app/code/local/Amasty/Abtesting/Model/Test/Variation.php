<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Model_Test_Variation extends Mage_Core_Model_Abstract
{
    const META_PRODUCT_TYPE = 'amabtesting_variation';

    protected $_metaproduct = null;

    protected function _construct()
    {
        $this->_init('amabtesting/test_variation');
    }

    public function getMetaproduct()
    {
        if ($this->_metaproduct === null) {
            $this->_metaproduct = Mage::getModel('amabtesting/test_variation_metaproduct')
                ->load($this->getMetaproductId());
        }

        return $this->_metaproduct;
    }

    public function setMetaproduct(Mage_Catalog_Model_Product $product)
    {
        $this->_metaproduct = $product;
    }

    public function getResult($variation)
    {
        $controlP = max(
            $this->getConversions() / $this->getVisits(),
            Amasty_Abtesting_Model_Test::ZERO_CONVERSION
        );

        $controlSE = sqrt($controlP * (1 - $controlP) / $this->getVisits());

        $variationP = max(
            $variation->getConversions() / $variation->getVisits(),
            Amasty_Abtesting_Model_Test::ZERO_CONVERSION
        );
        $variationSE = sqrt($variationP * (1 - $variationP) / $variation->getVisits());

        $z = ($controlP - $variationP) / sqrt($controlSE * $controlSE + $variationSE * $variationSE);

        $change = ($variationP - $controlP) / $controlP;

        $p = $this->cumnormdist($z);

        if ($p < .5) {
            $p = 1 - $p;
        }

        return array($change, $p);
    }

    protected function cumnormdist($z)
    {
        $b1 = 0.319381530;
        $b2 = -0.356563782;
        $b3 = 1.781477937;
        $b4 = -1.821255978;
        $b5 = 1.330274429;
        $p = 0.2316419;
        $c = 0.39894228;

        if ($z >= 0.0) {
            $t = 1.0 / (1.0 + $p * $z);
            return (1.0 - $c * exp(-$z * $z / 2.0) * $t *
                ($t * ($t * ($t * ($t * $b5 + $b4) + $b3) + $b2) + $b1));
        } else {
            $t = 1.0 / (1.0 - $p * $z);
            return ($c * exp(-$z * $z / 2.0) * $t *
                ($t * ($t * ($t * ($t * $b5 + $b4) + $b3) + $b2) + $b1));
        }
    }
}
