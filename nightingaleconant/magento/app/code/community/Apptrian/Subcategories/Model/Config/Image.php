<?php
/**
 * @category  Apptrian
 * @package   Apptrian_Subcategories
 * @author    Apptrian
 * @copyright Copyright (c) 2016 Apptrian (http://www.apptrian.com)
 * @license   http://www.apptrian.com/license Proprietary Software License EULA
 */
class Apptrian_Subcategories_Model_Config_Image
{
    
    protected $_options;
    const IMAGE     = 'image';
    const THUMBNAIL = 'thumbnail';
    
    public function toOptionArray()
    {
        if (!$this->_options) {
            $this->_options[] = array(
                'value' => self::IMAGE,
                'label' => Mage::helper('apptrian_subcategories')
                    ->__('Image')
            );
            $this->_options[] = array(
                'value' => self::THUMBNAIL,
                'label' => Mage::helper('apptrian_subcategories')
                    ->__('Thumbnail')
            );
        }
        return $this->_options;
    }
    
}
