<?php
/**
 * WMD_Html5audio_Block_Adminhtml_Playlist_Edit_Tabs
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2013 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Block_Adminhtml_Playlist_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    
    /**
     * Construct
     *
     * @return void
     */	
    public function __construct()
    {
        parent::__construct();		
        $this->setId('html5audio_playlist_edit_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('html5audio')->__('Playlist'));
    }

    /**
     * Before HTML
     *
     * @return object
     */	
    protected function _beforeToHtml()
    {
        
        $this->addTab('playlist', array(
            'label'     => Mage::helper('html5audio')->__('Playlist'),
            'title'     => Mage::helper('html5audio')->__('Playlist'),
            'content'   => $this->getLayout()
                ->createBlock('html5audio/adminhtml_playlist_edit_tab_playlist')
                ->toHtml(),
            'active'    => true,
        ));             
        
        $this->addTab('sample', array(
            'label'     => Mage::helper('html5audio')->__('Samples'),
            'title'     => Mage::helper('html5audio')->__('Samples'),
            'content'   => $this->getLayout()
                ->createBlock('html5audio/adminhtml_playlist_edit_tab_sample')
                ->toHtml(),
            'active'    => false,
        ));
        
        return parent::_beforeToHtml();
    }
}