<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Block_Sales_Order_Item_Renderer_Default extends Mage_Sales_Block_Order_Item_Renderer_Default {

    public function isPackage($_item){
        $itemId = $_item->getOrderItemId();
        $product = Mage::getModel('sales/order_item')->load($itemId);

        if($product->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE){
            return true;
        } else {
            return false;
        }
        return false;
    }
}