<?php

//Load Magento API
require_once 'app/Mage.php';
Mage::app();

//First we load the model
$model = Mage::getModel('authnetcim/payment');

//Then execute the task
$model->runDailyBilling();

?>