<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Model_Observer extends Varien_Object
{
    public function catalogProductPrepareSave($observer)
    {
        $event = $observer->getEvent();

        $product = $event->getProduct();
        $request = $event->getRequest();

        $links = $request->getPost('links');
        if (isset($links[MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE]) && !$product->getPackageReadonly()) {
            $product->setPackageLinkData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($links[MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE]));
        }
    }


    public function catalogModelProductDuplicate($observer)
    {
        $event = $observer->getEvent();

        $currentProduct = $event->getCurrentProduct();
        $newProduct = $event->getNewProduct();

        $data = array();
        $currentProduct->getLinkInstance()->usePackageLinks();
        $attributes = array();
        foreach ($currentProduct->getLinkInstance()->getAttributes() as $_attribute) {
            if (isset($_attribute['code'])) {
                $attributes[] = $_attribute['code'];
            }
        }
        foreach ($currentProduct->getPackageLinkCollection() as $_link) {
            $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
        }
        $newProduct->setPackageLinkData($data);
    }


    //Set product out of stock
    public function setOutOfStock(Varien_Event_Observer $observer) {
        $orderIds = $observer->getEvent()->getOrderIds();
        if (empty($orderIds) || !is_array($orderIds)) {
            return;
        }

        foreach($orderIds as $id){
            $order = Mage::getModel('sales/order')->load($id);

            $orderItems = $order->getItemsCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('product_type', array('eq' => MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE))
                ->load();


            foreach($orderItems as $item){
                if($item->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE) {

                    $parentProduct = Mage::getModel('catalog/product')->load($item->getProductId());
                    $collection = $parentProduct->getPackageProductCollection()
                        ->addAttributeToSelect('required_options')
                        ->setPositionOrder()
                        ->addStoreFilter();

                    foreach($collection as $col) {
                        $associatedQty[$col->getId()] = $col->getPackageQty();
                        $associatedIds[] = $col->getId();
                    }

                    if(is_array($associatedQty)){

                        foreach($associatedIds as $ids){
                            $associatedProduct = Mage::getModel('catalog/product')->load($ids);
                            $associatedStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($associatedProduct);
                            $qty = $associatedStock->getQty();

                            if($associatedQty[$ids] > $qty) {
                                $product = Mage::getModel('catalog/product')->load($item->getProductId());
                                $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);

                                try{
                                    $stockItem->setData('is_in_stock', 0);
                                    $stockItem->save();
                                } catch (Exception $e){
                                    Mage::logException($e);
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    //Update the cart weight
    public function updateCart(Varien_Event_Observer $observer) {
        $totalWeight = null;
        $item = $observer->getQuoteItem();

        if($item){
            if ($item->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE) {
                $product = Mage::getModel('catalog/product')->load($item->getProductId());

                $collection = $product->getPackageProductCollection()
                    ->addAttributeToSelect('required_options')
                    ->setPositionOrder()
                    ->addStoreFilter();

                foreach($collection as $col) {
                    $associated = Mage::getModel('catalog/product')->load($col->getId());
                    $associatedWeight[] = $associated->getWeight() * $col->getPackageQty();
                }

                $i=0;
                foreach($associatedWeight as $weight) {
                    if($totalWeight){
                        $weight = $weight ;
                        $totalWeight+= $weight;
                    } else {
                        $weight = $weight;
                        $totalWeight = $weight;
                    }
                    $i++;
                }
                $totalWeight = number_format($totalWeight, 4, '.', '');

                $item->setWeight($totalWeight);
            }
        }
    }


    //Updated the admin order weight
    public function updateProductOrder(Varien_Event_Observer $observer){
        $totalWeight = null;
        $item = $observer->getQuoteItem();

        if($item){
            if ($item->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE) {

                $product = Mage::getModel('catalog/product')->load($item->getProductId());

                $collection = $product->getPackageProductCollection()
                    ->addAttributeToSelect('required_options')
                    ->setPositionOrder()
                    ->addStoreFilter();

                foreach($collection as $col) {
                    $associated = Mage::getModel('catalog/product')->load($col->getId());
                    $associatedWeight[] = $associated->getWeight() * $col->getPackageQty();
                }

                $i=0;
                foreach($associatedWeight as $weight) {
                    if($totalWeight){
                        $weight = $weight;
                        $totalWeight+= $weight;
                    } else {
                        $weight = $weight;
                        $totalWeight = $weight;
                    }
                    $i++;
                }
                $totalWeight = number_format($totalWeight, 4, '.', '');

                $item->setWeight($totalWeight);
            }
        }
    }

    public function salesOrderShipmentSaveAfter(Varien_Event_Observer $observer)
    {
        $shipment = $observer->getEvent()->getShipment();

        $shippedItems = $shipment->getItemsCollection();
        foreach ($shippedItems as $item) {
            $shippedItemIds[] = $item->getOrderItemId();
            $product = Mage::getModel('sales/order_item')->load($item->getOrderItemId());

            if($product->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE
            && ($product->getQtyOrdered() == $product->getQtyShipped() || $product->getQtyOrdered() <= $product->getQtyShipped())){
                $item->delete();
            }
        }
    }

}