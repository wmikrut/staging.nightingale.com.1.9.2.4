<?php
class NCCUSTOM_InsidersClubDigital55_Model_Observer 
{
    const INSIDERS_CLUB_DIGITAL_ITEM = 919;
    const INSIDERS_CLUB_DIGITAL_PRICING = 7;
    
    public function exportFeed($observer) 
    {		
    	include('mysql-credentials-production.php');
    	$mysqli = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_db);
    	$log = "nccustom_InsidersClubDigital55.log";
    	//Mage::log("InsidersClubDigital55", null, $log);
	    	
	    	$order = Mage::getModel('sales/order')->load(Mage::getSingleton('checkout/session')->getLastRealOrderId(), 'increment_id');
		//$order = $observer->getEvent()->getOrder();
		
		$customer_id = $order->getCustomerId();
		$customer_email = $order->getCustomerEmail();
		
		$cartItems = $order->getAllItems();
		foreach ( $cartItems as $item )
		{	
		    $product_id = $item->getProductId();
		    if ( $product_id == NCCUSTOM_InsidersClubDigital55_Model_Observer::INSIDERS_CLUB_DIGITAL_ITEM )
		    {
				$order_id = $order->getId();
				//mage::log($order_id, null, $log);		
				
				$mysqlstm = <<<STRING
select *
from $mysql_db.nc_sales_recurring_profile_order
where order_id = $order_id;
STRING;
				$f001 = $mysqli->query($mysqlstm);
				$db1 = $f001->fetch_assoc();
				$db1_profile_id = $db1['profile_id'];
		
		
				if ( $db1_profile_id > 0 )
				{
				    $mysqlstm = <<<STRING
select *
from $mysql_db.nc_sales_recurring_profile
where profile_id = $db1_profile_id;
STRING;
				    $f002 = $mysqli->query($mysqlstm);
				    $db2 = $f002->fetch_assoc();
				    $db2_additional_info = $db2['additional_info'];
				    		    	    
				    $ary_additional_info = unserialize($db2_additional_info);
					    
				    $next_cycle = $ary_additional_info['next_cycle'];		    
				    $next_cycle_date = date("Y-m-d H:i:s", $db2_next_cycle);
				    
				    $current_date = new DateTime($db2_next_cycle_date);
				    $current_date->add(new DateInterval('P6M'));
				    $new_date = strtotime( $current_date->format('Y-m-d H:i:s'));		
				    $ary_additional_info['next_cycle'] = $new_date;
				    
				    $new_additional_info = serialize($ary_additional_info);
				    
				    $mysqlstm = <<<STRING
update $mysql_db.nc_sales_recurring_profile
set additional_info = '$new_additional_info'
where profile_id = $db1_profile_id;
STRING;
		    		$fnull = $mysqli->query($mysqlstm);	   
				}	
				$_customer = Mage::getModel('customer/customer')->load($customer_id);
				$_customer->setGroupId(NCCUSTOM_InsidersClubDigital55_Model_Observer::INSIDERS_CLUB_DIGITAL_PRICING);
				$_customer->save();
				
				$mysqlstm = <<<STRING
select count(*) as rcds
from $mysql_db.nccustom_insiders_membership_state
where email_address = '$customer_email';
STRING;
				$f003 = $mysqli->query($mysqlstm);
				$db3 = $f003->fetch_assoc();
				$db3_rcds = $db3['rcds'];
				
				if ( $db3_rcds == 0 )
				{
					$start_date = date("Y-m-d H:i:s");
					$mysqlstm = "insert ignore into $mysql_db.nccustom_insiders_membership_state ";
					$mysqlstm.= "values(0, '$customer_email', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0, '$order_id', '$start_date', '', 0);";
				} 
				else 
				{
					$mysqlstm = "update $mysql_db.nccustom_insiders_membership_state ";
					$mysqlstm.= "set active_member = 1 ";
					$mysqlstm.= "where email_address = '$customer_email';";
				}
				
				//Mage::log($mysqlstm, null, $log);				
				$fnull = $mysqli->query($mysqlstm);
				
				if ( isset($_SESSION['nccustom_coupon_code']))
				{
					$nccustom_coupon_code = $_SESSION['nccustom_coupon_code'];
					$order->setCouponCode($nccustom_coupon_code)->save();
					unset($_SESSION['nccustom_coupon_code']);
				}			
	    	}
		}		
		mysqli_close($mysqli);
    }
}
?>