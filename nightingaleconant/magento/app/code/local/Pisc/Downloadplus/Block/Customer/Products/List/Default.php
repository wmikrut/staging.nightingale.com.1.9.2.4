<?php
/**
 *
 * @category    Pisc
 * @package     Pisc_Downloadplus
 * @copyright   Copyright (c) 2009 PILLWAX Industrial Solutions Consulting
 * @license		Commercial Unlimited License (https://technology.pillwax.com/license)
 */

/**
 * Downloadable Products List for Customer
 *
 * @category    Pisc
 * @package     Pisc_Downloadplus
 * @author		Software Group @ PILLWAX Industrial Solutions Consulting (technology.license@pillwax.com)
 * @version		0.1.7
 */

class Pisc_Downloadplus_Block_Customer_Products_List_Default extends Pisc_Downloadplus_Block_Customer_Products_List
{

    public function __construct()
    {
        $this->_session = Mage::getSingleton('customer/session');

        // Load dependencies
        Mage::getModel('downloadplus/download_detail');
        Mage::getModel('downloadable/link');
        Mage::getModel('downloadable/link_purchased_item');
        
        $this->setTemplate('downloadplus/customer/products/list/default.phtml');
    }

}
