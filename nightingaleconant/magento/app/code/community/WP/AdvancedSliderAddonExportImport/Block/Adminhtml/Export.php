<?php

class WP_AdvancedSliderAddonExportImport_Block_Adminhtml_Export
    extends Mage_Adminhtml_Block_Widget_Container
{
    public function __construct()
    {
        $this->_headerText = $this->__('Export Sliders');
        $this->_controller = 'adminhtml_sliders_export';

        $this->_addButton('back', array(
            'label'     => $this->__('Back to Manage Sliders'),
            'onclick'   => 'setLocation(\'' . Mage::helper('adminhtml')->getUrl('advancedslider/sliders') . '\')',
            'class'     => 'back',
        ));

        $action = $this->getRequest()->getActionName();
        if ($action == 'index') {
            $this->_addButton('export', array(
                'label'     => $this->__('Export'),
                'onclick'   => 'setLocation(\'' . Mage::helper('adminhtml')->getUrl('advancedslideraddonexportimport/export/process') . '\')',
                'class'     => 'save',
            ));
        }

        parent::__construct();
    }

    public function getHeaderHtml()
    {
        return '<h3 class="icon-head ' . $this->getHeaderCssClass() . '">' . $this->getHeaderText() . '</h3>';
    }

    public function getListOfSliders()
    {
        return Mage::getSingleton('core/session')->getListOfSliders();
    }
}
