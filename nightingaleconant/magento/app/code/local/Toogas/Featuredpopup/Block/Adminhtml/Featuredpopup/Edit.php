<?php
/**
 * Toogas Lda.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA (End-User License Agreement)
 * that is bundled with this package in the file licence_toogas_community.txt.
 * It is also available at this URL:
 * http://www.toogas.com/licences/toogas_community.txt
 *
 * @category   Toogas
 * @package    Toogas_Featuredpopup
 * @copyright  Copyright (c) 2010 Toogas Lda. (http://www.toogas.com)
 * @license    http://www.toogas.com/licences/toogas_community.txt
 */
class Toogas_Featuredpopup_Block_Adminhtml_Featuredpopup_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'featuredpopup';
        $this->_controller = 'adminhtml_featuredpopup';
        
        $this->_updateButton('save', 'label', Mage::helper('featuredpopup')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('featuredpopup')->__('Delete'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);
        
                   
        $this->_formScripts[] =  "
          
          var valor_dropdown_tipo = $('tipo').value;
          if (valor_dropdown_tipo == 1) { 
          	$('image_link').enable();
          	$('url_link').enable();
          	$('html_editor').disable(); 
          }
          else if (valor_dropdown_tipo == 2) {
          	$('image_link').disable();
          	$('url_link').disable();
          	$('html_editor').enable();          	
          }
          
          var valor_dropdown_pages = $('pages').value;
          if (valor_dropdown_pages != 5) { $('pages_url').disable(); }
          
	      function OnChange(dropdown) {
			var myindex  = dropdown.selectedIndex
		    var SelValue = dropdown.options[myindex].value
		    
		    if (SelValue == 5) { 
		    	$('pages_url').enable();	    	
		    } 
		    else {
		    	$('pages_url').disable();
		    }
		  }
		  
	      function OnChangeTipo(dropdown) {
			var myindex  = dropdown.selectedIndex
		    var SelValue = dropdown.options[myindex].value
		    
		    if (SelValue == 1) { 
	          	$('image_link').enable();
	          	$('url_link').enable();
	          	$('html_editor').disable(); 	    	
		    } 
		    else if (SelValue == 2) {
	          	$('image_link').disable();
	          	$('url_link').disable();
	          	$('html_editor').enable();
		    }
		  }		  
        
	        function toggleEditor() {
	            if (tinyMCE.getInstanceById('featuredpopup_content') == null) {
	                tinyMCE.execCommand('mceAddControl', false, 'featuredpopup_content');
	            } else {
	                tinyMCE.execCommand('mceRemoveControl', false, 'featuredpopup_content');
	            }
	        }
	
	        function saveAndContinueEdit(){
	            editForm.submit($('edit_form').action+'back/edit/');
	        }
        ";
       
    }
    
	protected function _prepareLayout() {
		parent::_prepareLayout();
		if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
			$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
		}
	}     

    public function getHeaderText()
    {
        if( Mage::registry('featuredpopup_data') && Mage::registry('featuredpopup_data')->getId() ) {
            return Mage::helper('featuredpopup')->__("Edit Featured Popup '%s'", $this->htmlEscape(Mage::registry('featuredpopup_data')->getPopupName()));
        } else {
            return Mage::helper('featuredpopup')->__('New Featured Popup');
        }
    }
}