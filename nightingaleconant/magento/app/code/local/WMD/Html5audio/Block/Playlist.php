<?php
/**
 * WMD_Html5audio_Block_Widget_Playlist
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2013 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Block_Playlist extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface 
{
	
    /**
     * A model to serialize attributes
     * @var Varien_Object
     */
    protected $_serializer = null;

    /**
     * Initialization
     */
    protected function _construct()
    {
        $this->_serializer = new Varien_Object();
        parent::_construct();
    }

    /**
     * Produce link rendered as html
     *
     * @return string
     */
    protected function _toHtml()
    {
        $html = '';
        $playlistId = $this->getData('enabled_playlist');
        if (empty($playlistId)) {
            return $html;
        }
        $this->assign('playlist', $this->_generatePlayer($playlistId));
        return parent::_toHtml();
    }

    
    /**
     * Generate player
     *
     * @param string $linkId
     * @return array
     */
    protected function _generatePlayer($playlistId)
    {
                
        /**
         * Load Playlist
         */                 
        $playlist = Mage::getModel('html5audio/playlist')->load($playlistId);
        
        $samples = $playlist->getSamples();
        
        $mode = (1 < count($samples)) ? 'playlist' : 'single' ;
        
        $item = array(
            'id'      => $playlistId,
            'tracks'  => $samples,
            'mode'    => $mode,
        );    
        
        return $item;
        
    }
}