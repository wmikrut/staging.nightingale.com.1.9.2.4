<?php
/**
 * @category   Apptrian
 * @package    Apptrian_ProductSlider
 * @author     Apptrian
 * @copyright  Copyright (c) 2015 Apptrian (http://www.apptrian.com)
 * @license    http://www.apptrian.com/license    Proprietary Software License (EULA)
 */
class Apptrian_ProductSlider_AjaxController extends Mage_Core_Controller_Front_Action
{
	/**
	 * This is the action used by jQuery ajax function.
	 * 
	 */
	public function productsAction()
	{
		
		if (Mage::app()->getRequest()->isAjax()) {
			
			$helper = Mage::helper('apptrian_productslider');
			
			$page = $helper->filterAndValidatePageParam(
						$this->getRequest()->getPost('page'));
			
			$pageType = $helper->filterAndValidateTypeParam(
						$this->getRequest()->getPost('type'));
			
			$id = $helper->filterAndValidateIdParam(
						$this->getRequest()->getPost('id'));
			
			$uenc = $helper->filterAndValidateUencParam(
						$this->getRequest()->getPost('uenc'));
			
			$data = $helper->getDataAjax($page, $pageType, $id, $uenc);
			
			$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
			
		} else {
			
			$this->getResponse()->setHeader('HTTP/1.1', '400 Bad Request', true);
			$this->getResponse()->setHeader('Status', '400 Bad Request', true);
			
		}
		
	}
	
	
}