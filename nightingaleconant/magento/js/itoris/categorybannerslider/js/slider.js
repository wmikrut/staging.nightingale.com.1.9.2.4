var Slider = Class.create();
var bannerSlider;
Slider.prototype = {
	container : null,
	effect : null,
	skin : null,
	autorotation : null,
	width : 0,
	height : 0,
	titles : false,
	arrows : false,
	nums : false,
	banners : {},
	timeVar : null,
	hoverTitle : false,
	hoverNum : false,
	position : 0,
	titlesWidth : [],
	toLeft : 0,
	noSlide : false,
	imageSize : [],
	initialize : function(options, banners){
		this.effect = options.effect;
		this.skin = options.skin;
		this.autorotation = parseInt(options.autorotation);
		this.width = $('banner_slider').getWidth(); //parseInt(options.width);
		this.height = parseInt(options.height);
		this.banners = banners;
		if(this.banners.length){
			this.deleteDefaultImage();
			this.createBox();
			Event.observe(window, 'resize', this.responsive);
		}
	},
	deleteDefaultImage : function(){
		if($$('.category-image').length){
			$$('.category-image')[0].remove();
		}else if($$('#main p img').length){
			$$('#main p img')[0].remove();
		}
		this.container = $('banner_slider');
		Element.extend(this.container);
		this.container.addClassName('category-image');
		if($$('.page-title')[0]){
			$$('.page-title')[0].insert({
				'after' : this.container
			});
		}else if($$('.category-head')[0]){
			$$('.category-head')[0].insert({
				'after' : this.container
			});
		}else if($$('.category-view')[0]){
			$$('.category-view')[0].insert({
				'top' : this.container
			});
		}
	},
	createBox : function(){
		var box = document.createElement('div');
		Element.extend(box);
		var height = 0;
		var width = 0;
		for(var i = 0; i < this.banners.length; i++){
			var bannerDiv = document.createElement('div');
			Element.extend(bannerDiv);
			bannerDiv.addClassName('banner_box');
			var bannerLink = document.createElement('a');
			Element.extend(bannerLink);
			bannerLink.href = this.banners[i].link_url;
			bannerLink.target = this.banners[i].open_in;
			var banner = document.createElement('img');
			Element.extend(banner);
			banner.src = this.banners[i].url;
			banner.title = this.banners[i].title;
			banner.alt = this.banners[i].alt;
			if(height < banner.height){
				height = banner.height;
			}
			if(width < banner.width){
				width = banner.width;
			}
			bannerLink.appendChild(banner);
			bannerDiv.appendChild(bannerLink);
			box.appendChild(bannerDiv);
			if(i){
				bannerDiv.hide();
			}
		}
		if(this.autorotation){
			setTimeout(this.nextBanner, (this.banners[0].delay * 1000) + 1000 );
		}
		box.addClassName('slider_box');
		if(this.skin == 'num_arrows' || this.skin == 'titled_arrows' || this.skin == 'arrows'){
			this.arrows = true;
		}
		this.container.appendChild(box);
		setTimeout(this.resizeImages, 1000);
	},
	createSkins : function(){
		var slider = this;
		var box = $$('.slider_box')[0];
		var height = this.height;
		var width = this.width;
		if(this.arrows){
			var leftArrow = document.createElement('div');
			Element.extend(leftArrow);
			leftArrow.addClassName('arrow_left');
			var top = (height/2)-20 + 'px';
			leftArrow.setStyle({'top' : top});
			leftArrow.addClassName('arrow_left_last');
			Event.observe(leftArrow, 'mouseover', function(){
				if(!this.hasClassName('arrow_left_last')){
					this.addClassName('arrow_left_active');
				}
			});
			Event.observe(leftArrow, 'mouseout', function(){
				this.removeClassName('arrow_left_active');
			});
			leftArrow.hide();
			box.appendChild(leftArrow);
			var rightArrow = document.createElement('div');
			Element.extend(rightArrow);
			rightArrow.addClassName('arrow_right');
			if($$('.banner_box').length == 1){
				rightArrow.addClassName('arrow_right_last');
			}
			rightArrow.setStyle({'top' : top});
			Event.observe(rightArrow, 'click', function(){
				slider.nextBanner();
			});
			Event.observe(rightArrow, 'mouseover', function(){
				if(!this.hasClassName('arrow_right_last')){
					this.addClassName('arrow_right_active');
				}
			});
			Event.observe(rightArrow, 'mouseout', function(){
				this.removeClassName('arrow_right_active');
			});
			rightArrow.hide();
			box.appendChild(rightArrow);
		}

		if(this.skin == 'titled' || this.skin == 'titled_arrows'){
			this.titles = true;
		}
		//box.setStyle({'height' : height + 'px', 'width' : width + 'px'});
		if(this.arrows){
			Event.observe($$('.slider_box')[0], 'mouseover', function(){
				$$('.arrow_left')[0].show();
				$$('.arrow_right')[0].show();
			});
			Event.observe($$('.slider_box')[0], 'mouseout', function(){
				$$('.arrow_left')[0].hide();
				$$('.arrow_right')[0].hide();
			});
		}
		if(this.titles){
			var titlesBox = document.createElement('div');
			Element.extend(titlesBox);
			titlesBox.addClassName('titles_box');
			var titlesBackgr = document.createElement('div');
			Element.extend(titlesBackgr);
			titlesBackgr.addClassName('titles_backgr');
			titlesBox.appendChild(titlesBackgr);
			var titlesLeftBackgr = document.createElement('div');
			Element.extend(titlesLeftBackgr);
			titlesLeftBackgr.addClassName('titles_left_backgr');
			titlesBox.appendChild(titlesLeftBackgr);
			var titlesRightBackgr = document.createElement('div');
			Element.extend(titlesRightBackgr);
			titlesRightBackgr.addClassName('titles_right_backgr');
			titlesBox.appendChild(titlesRightBackgr);
			titlesBox.setStyle({'width' : width + 'px'});
			var titlesElements = document.createElement('div');
			Element.extend(titlesElements);
			titlesElements.addClassName('titles_elements');
			for(var i = this.banners.length - 1; i >= 0; i--){
				var title = document.createElement('div');
				Element.extend(title);
				title.addClassName('title_name');
				title.update(this.banners[i].title);
				Event.observe(title, 'mouseover', (
							function(num){
								return function(){slider.hoverTitle = true;slider.moveSelect(num);}
							}
						)(i));
				Event.observe(title, 'click', (
							function(num){
								return function(){
									var activeNum = slider.getActive();
									slider.position = num + 1;
									slider.hoverTitleSkip = true;
									setTimeout(function(){slider.hoverTitleSkip = false}, 1100);
									if(slider.position > activeNum){
										slider.nextBanner();
									}else{
										slider.prevBanner();
									}
								}
							}
						)(i));
				Event.observe(title, 'mouseout', function(){
					slider.hoverTitle = false;
					setTimeout(bannerSlider.returnSelect, 1000);
				});
				titlesElements.appendChild(title);
			}
			var titlesElementsBox = document.createElement('div');
			Element.extend(titlesElementsBox);
			titlesElementsBox.addClassName('titles_elements_box');
			titlesElementsBox.appendChild(titlesElements);
			titlesBox.appendChild(titlesElementsBox);
			this.container.appendChild(titlesBox);
			var titlesElementsWidth = 1;
			for(var i = 0; i < this.banners.length; i++){
				var titleWidth = $$('.title_name')[i].getWidth() + 18;
				titlesElementsWidth += titleWidth;
				this.titlesWidth.unshift(titleWidth);
			}
			titlesElements.setStyle({'width' : titlesElementsWidth + 'px'});
			titlesElementsBox.setStyle({'width' : (width - 22) + 'px', 'left' : '11px'});
			Event.observe(titlesBox, 'mouseover', function(){
					slider.hoverTitle = true;
			});
			Event.observe(titlesBox, 'mouseout', function(){
					slider.hoverTitle = false;
					setTimeout(bannerSlider.returnSelect, 1000);
			});
			var selectBox = document.createElement('div');
			Element.extend(selectBox);
			selectBox.addClassName('select_title');
			var selectLeftBackgr = document.createElement('div');
			Element.extend(selectLeftBackgr);
			selectLeftBackgr.addClassName('select_left_backgr');
			selectBox.appendChild(selectLeftBackgr);
			var selectRightBackgr = document.createElement('div');
			Element.extend(selectRightBackgr);
			selectRightBackgr.addClassName('select_right_backgr');
			selectBox.appendChild(selectRightBackgr);
			titlesBox.appendChild(selectBox);
			for(var i = $$('.title_name').length -1; i >= 0; i--){
				if( !(Prototype.Browser.IE && parseInt(navigator.userAgent.substring(navigator.userAgent.indexOf("MSIE")+5)) == 7) ){
					$$('.title_name')[i].absolutize();
				}
			}
			titlesElements.absolutize();
			if(titlesBox.getWidth() > titlesElementsWidth){
				this.noSlide = true;
				//titlesElements.setStyle({'left' : (titlesBox.getWidth() - titlesElementsWidth - 5) + 'px'});
				//this.toLeft = -(titlesBox.getWidth() - titlesElementsWidth);
			}
			this.moveSelect(0);
		}
		if(this.skin == 'num_arrows' || this.skin == 'num'){
			this.nums = true;
		}
		if(this.nums){
			var numsBox = document.createElement('div');
			Element.extend(numsBox);
			numsBox.addClassName('nums_box');
			var numsBackgr = document.createElement('div');
			Element.extend(numsBackgr);
			numsBackgr.addClassName('nums_backgr');
			numsBox.appendChild(numsBackgr);
			var numsLeftBackgr = document.createElement('div');
			Element.extend(numsLeftBackgr);
			numsLeftBackgr.addClassName('nums_left_backgr');
			numsBox.appendChild(numsLeftBackgr);
			var numsRightBackgr = document.createElement('div');
			Element.extend(numsRightBackgr);
			numsRightBackgr.addClassName('nums_right_backgr');
			numsBox.appendChild(numsRightBackgr);
			//numsBox.setStyle({'width' : width + 'px'});
			var numsElements = document.createElement('div');
			Element.extend(numsElements);
			numsElements.addClassName('nums_elements');
			for(var i = this.banners.length - 1; i >= 0; i--){
				var num = document.createElement('div');
				Element.extend(num);
				num.addClassName('num_img');
				num.addClassName('num_norm_' + (i + 1));
				Event.observe(num, 'mouseover', (
							function(number){
								return function(){
										slider.hoverNum = true;
										slider.numSelect(number);
									}
							}
						)(i + 1));
				Event.observe(num, 'mouseout', (
							function(number){
								return function(){
										slider.hoverNum = false;
										setTimeout(slider.numActive, 100);
									}
							}
						)(i + 1));
				Event.observe(num, 'click', (
							function(n){
								return function(){
									var activeNum = slider.getActive();
									slider.position = n + 1;
									if(slider.position > activeNum){
										slider.nextBanner();
									}else{
										slider.prevBanner();
									}
								}
							}
						)(i));
				numsElements.appendChild(num);
			}
			numsBox.appendChild(numsElements);
			var numsElementsWidth = 28 * this.banners.length;
			numsElements.setStyle({'width' : numsElementsWidth + 'px'});
			Event.observe(numsBox, 'mouseover', function(){
				slider.hoverNum = true;
			});
			Event.observe(numsBox, 'mouseout', function(){
				slider.hoverNum = false;
				setTimeout(slider.numActive, 100);
			});
			this.container.appendChild(numsBox);
			for(var i = $$('.num_img').length - 1; i >= 0; i--){
				$$('.num_img')[i].absolutize();
			}
			numsElements.absolutize();
			//numsElements.setStyle({'right' : '5px', 'left': 'auto'});
			if(numsBox.getWidth() > numsElementsWidth){
				this.noSlide = true;
				if( !(Prototype.Browser.IE && parseInt(navigator.userAgent.substring(navigator.userAgent.indexOf("MSIE")+5)) == 7) ){
					numsElements.setStyle({'left' : (numsBox.getWidth() - numsElementsWidth) + 'px'});
					//numsElements.setStyle({'right' : '5px', 'left': 'auto'});
				}
			}
			this.numSelect(1);
		}
	},
	responsive: function() {
		$$('.banner_box').each(function(banner, index){
			var img = banner.select('img')[0];
			var w = img.width, h = img.height, k = h > 0 ? w / h : 1, nw = img.up('.slider_box').getWidth(), nh = nw / k;
			img.setStyle({height: nh + 'px', width: nw + 'px'});			
			img.up('.banner_box').setStyle({height: nh + 'px', width: nw + 'px'});
			if(banner.visible()) {
				img.up('.slider_box').setStyle({height: nh + 'px'});
			}
		});
		var numsBox = $$('.nums_box')[0], numsElements = $$('.nums_elements')[0];
		if (numsBox && numsElements) numsElements.setStyle({left: numsBox.getWidth() - numsElements.getWidth() + 'px'});
		var arrowLeft = $$('#banner_slider .arrow_left')[0], arrowRight = $$('#banner_slider .arrow_right')[0];
		if (arrowLeft) arrowLeft.setStyle({top: ($$('.slider_box')[0].getHeight() - arrowLeft.getHeight()) / 2 + 'px'});
		if (arrowRight) arrowRight.setStyle({top: ($$('.slider_box')[0].getHeight() - arrowRight.getHeight()) / 2 + 'px'});
		var titlesBox = $$('#banner_slider .titles_box')[0], titlesElementsBox = $$('#banner_slider .titles_elements_box')[0], titlesElements = $$('#banner_slider .titles_elements')[0];
		if (titlesBox && titlesElementsBox) {
			titlesBox.setStyle({width: $$('.slider_box')[0].getWidth() + 'px'});
		}
	},
	resizeImages : function(){
		for(var i = 0; i < $$('.banner_box').length; i++){
			bannerSlider.resize(i);
		}
		$$('.slider_box')[0].setStyle({'height' : bannerSlider.height + 'px' /*, 'width' : bannerSlider.width + 'px'*/});
		bannerSlider.createSkins();
		bannerSlider.responsive();
	},
	resize : function(i){return;
		var width = $$('.banner_box img')[i].width;
		var height = $$('.banner_box img')[i].height;
		var marginLeft = 0;
		var marginTop = 0;
		if(bannerSlider.width < width && width > height){
			$$('.banner_box img')[i].width = bannerSlider.width;
			var scale = width/bannerSlider.width;
			marginTop = (bannerSlider.height - height/scale)/2;
		} else if (bannerSlider.height < height && height > width) {
			$$('.banner_box img')[i].height = bannerSlider.height;
			var scale = height/bannerSlider.height;
			marginLeft = (bannerSlider.width - width/scale)/2;
		} else if (width < bannerSlider.width || height < bannerSlider.height){
			marginTop = (bannerSlider.height - height)/2;
			marginLeft = (bannerSlider.width - width)/2;
		}
		if(width && height){
			bannerSlider.imageSize[i] = true;
			$$('.banner_box img')[i].setStyle({'marginTop' : marginTop + 'px', 'marginLeft' : marginLeft + 'px'});
		}
	},
	moveSelect : function(num){
		var title = $$('.title_name')[$$('.banner_box').length - 1 - parseInt(num)];
		$$('.title_name').each(function(elm){
			elm.removeClassName('title_name_black');
		});
		title.addClassName('title_name_black');
		var correction = 3;
		if(parseInt($$('.titles_elements')[0].getStyle('left')) > 5){
			correction -= 5;
		}
		var left = title.positionedOffset()[0] + correction - this.toLeft;
		var width = title.getWidth() + 16;
		if(left < 0){
			width += left;
			left = 0;
		}
		if(bannerSlider.width < (left + width) ){
			width = bannerSlider.width - left;
		}
		new Effect.Morph($$('.select_title')[0], {style : {'left' : left + 'px', 'width' : width + 'px'},
													duration : 0.1});
	},
	returnSelect : function(){
		setTimeout(function(){
			if(bannerSlider.hoverTitle || bannerSlider.hoverTitleSkip){
				return;
			}
			var banners = $$('.banner_box');
			for(var i = 0; i < banners.length; i++){
				if(banners[i].visible()){
					bannerSlider.moveSelect(i);
					break;
				}
			}
		}, 100);
	},
	numSelect : function(num){
		var number = $$('.num_img')[[$$('.banner_box').length - parseInt(num)]];
		for(var i = 0; i < $$('.num_img').length; i++){
			$$('.num_img')[i].removeClassName('num_active_' + ($$('.num_img').length - i));
		}
		number.addClassName('num_active_' + num);
	},
	numActive : function(){
		if(bannerSlider.hoverNum){
			return;
		}
		var banners = $$('.banner_box');
		var count = 0;
		var num = 0;
		for(var i = 0; i < banners.length; i++){
			if(banners[i].visible()){
				count++;
				num = i;
			}
		}
		if(count == 1){
			bannerSlider.numSelect(num + 1);
		}else{
			setTimeout(bannerSlider.numActive, 100);
		}
	},
	nextBanner : function(){
		clearTimeout(bannerSlider.timeVar);
		var banners = $$('.banner_box');
		for(var i = 0; i < banners.length; i++){
			if(banners[i].visible()){
				if(banners.length == 1){
					return;
				}
				if(bannerSlider.checkVisible()){
					return;
				}
				if(bannerSlider.position && (bannerSlider.position - 1) == i){
					break;
				}
				var pos = i + 1;
				if(pos == banners.length){
					pos = 0;
				}
				if(bannerSlider.effect == 'sliding'){
					new Effect.Move(banners[i], { x : -banners[i].offsetWidth, queue: 'front'});
					Effect.Fade(banners[i], { queue: 'end', duration: 0.1 })
				}else if(bannerSlider.effect == 'simple'){
					banners[i].hide();
				}else if(bannerSlider.effect == 'fade'){
					Effect.Fade(banners[i]);
				}
				if(bannerSlider.position){
					pos = bannerSlider.position - 1;
					bannerSlider.position = 0;
				}
				if(bannerSlider.titles){
					if(!bannerSlider.noSlide){
						var moveLeft = 0;
						if(pos == 0 && bannerSlider.toLeft){
							moveLeft = -bannerSlider.toLeft;
							bannerSlider.toLeft = 0;
						}else{
							var totalWidth = $$('.titles_elements')[0].getWidth();
							var visibleWidth = $$('.titles_elements_box')[0].getWidth();
							var left = 0;
                            var leftVisibleWithoutElement = 0;
							var titlesWidth = bannerSlider.titlesWidth;
							for(var i = 0; i <= pos; i++){
                                if(i == pos){
                                    leftVisibleWithoutElement = left - bannerSlider.toLeft;
                                }
								left += titlesWidth[i];
							}
							var leftVisible = left - bannerSlider.toLeft;
							var rightVisible = visibleWidth - leftVisible;
							if( ( (rightVisible > 0) && (rightVisible < visibleWidth/3) ) || (rightVisible < 0) ){
								var unvisibleRight = totalWidth - bannerSlider.toLeft - visibleWidth;
								if(unvisibleRight < 50){
									moveLeft = unvisibleRight;
								}else{
                                    var rightUnvisibleTitlePart = leftVisibleWithoutElement + titlesWidth[pos] - visibleWidth;
									if(rightUnvisibleTitlePart > 0){
                                        moveLeft = rightUnvisibleTitlePart;
                                    } else {
    									moveLeft = 50;
                                    }
								}
							}
							bannerSlider.toLeft += moveLeft;
						}
						new Effect.Move($$('.titles_elements')[0], { x : -moveLeft});
					}
					bannerSlider.moveSelect(pos);
				}
				if(bannerSlider.arrows){
					bannerSlider.arrowsToggle(pos);
				}
				if(bannerSlider.nums){
					bannerSlider.numSelect(pos + 1);
					if(!bannerSlider.noSlide){
						var numLine = $$('.nums_elements')[0];
						var moveLeft = 0;
						var numWidth = 28;
						if(pos == 0){
							moveLeft = -bannerSlider.toLeft - 3;
						}else{
							var totalWidth = bannerSlider.banners.length * numWidth;
							var leftWidth = (pos + 1) * numWidth;
							var visibleWidth = $$('.nums_box')[0].getWidth();
							var positionWidth = visibleWidth - (leftWidth - bannerSlider.toLeft);
							if(positionWidth < (visibleWidth/3)){
								var unvisibleRight = totalWidth - bannerSlider.toLeft - visibleWidth;
								if(unvisibleRight > numWidth){
									moveLeft = 28;
								}else{
									moveLeft = unvisibleRight;
								}
							}
						}
						bannerSlider.toLeft += moveLeft;
						new Effect.Move(numLine, { x : -moveLeft });
					}
				}
				if(bannerSlider.effect == 'sliding'){
					banners[pos].show();
					banners[pos].setStyle({left : banners[i].offsetWidth + 'px'})
					new Effect.Move(banners[pos], { x : -banners[i].offsetWidth});
					if(bannerSlider.banners[pos].delay < 3){
						bannerSlider.banners[pos].delay = 3;
					}
				}else if(bannerSlider.effect == 'simple'){
					banners[pos].show();
				}else if(bannerSlider.effect == 'fade'){
					Effect.Appear(banners[pos]);
				}
				new Effect.Morph(banners[i].up('.slider_box'), {style : {height: banners[pos].getHeight() + 'px'}, duration : 0.3, afterUpdate: function(){
					var arrowLeft = $$('#banner_slider .arrow_left')[0], arrowRight = $$('#banner_slider .arrow_right')[0];
					if (arrowLeft) arrowLeft.setStyle({top: ($$('.slider_box')[0].getHeight() - arrowLeft.getHeight()) / 2 + 'px'});
					if (arrowRight) arrowRight.setStyle({top: ($$('.slider_box')[0].getHeight() - arrowRight.getHeight()) / 2 + 'px'});
				}});
				if(bannerSlider.autorotation){
					bannerSlider.timeVar = setTimeout(bannerSlider.nextBanner, bannerSlider.banners[pos].delay * 1000);
				}
				if(!bannerSlider.imageSize[pos]){
					bannerSlider.resize(pos);
				}
				break;
			}
		}
	},
	prevBanner : function(){
		clearTimeout(bannerSlider.timeVar);
		var banners = $$('.banner_box');
		for(var i = 0; i < banners.length; i++){
			if(banners[i].visible()){
				if(bannerSlider.checkVisible()){
					return;
				}
				if(bannerSlider.effect == 'sliding'){
					new Effect.Move(banners[i], { x : banners[i].offsetWidth, queue: 'front'});
					Effect.Fade(banners[i], { queue: 'end', duration: 0.1 })
				}else if(bannerSlider.effect == 'simple'){
					banners[i].hide();
				}else if(bannerSlider.effect == 'fade'){
					Effect.Fade(banners[i]);
				}
				var pos = i - 1;
				if(pos < 0){
					pos = banners.length - 1;
				}
				if(bannerSlider.position){
					pos = bannerSlider.position - 1;
					bannerSlider.position = 0;
				}
				if(bannerSlider.titles){
					var totalWidth = $$('.titles_elements')[0].getWidth();
					var visibleWidth = $$('.titles_elements_box')[0].getWidth();
					var left = 0;
					var titlesWidth = bannerSlider.titlesWidth;
					var visibleTitlePart = 0;
					for(var i = 0; i <= pos; i++){
						if(i == pos){
							visibleTitlePart = left + titlesWidth[i] - bannerSlider.toLeft;
						}else{
							left += titlesWidth[i];
						}
					}
					var leftVisible = left - bannerSlider.toLeft;
					var moveLeft = 0;
					if( ((leftVisible > 0) && (leftVisible < visibleWidth/3)) || (leftVisible < 0) ){
						if(bannerSlider.toLeft < 50){
							moveLeft = bannerSlider.toLeft;
						}else{
                            var leftUnvisibleElementPart = titlesWidth[pos] - visibleTitlePart;
                            if (leftUnvisibleElementPart > 50) {
                                moveLeft = leftUnvisibleElementPart;
                            } else {
    							moveLeft = 50;
                            }
						}
					}
					new Effect.Move($$('.titles_elements')[0], { x : moveLeft});
					bannerSlider.toLeft -= moveLeft;
					bannerSlider.moveSelect(pos);
				}
				if(bannerSlider.arrows){
					bannerSlider.arrowsToggle(pos);
				}
				if(bannerSlider.nums){
					bannerSlider.numSelect(pos + 1);
					var numLine = $$('.nums_elements')[0];
					var moveLeft = 0;
					var numWidth = 28;
					var totalWidth = bannerSlider.banners.length * numWidth;
					var leftWidth = pos * numWidth;
					var visibleWidth = $$('.nums_box')[0].getWidth();
					var positionWidth = leftWidth - bannerSlider.toLeft;
					if(positionWidth < (visibleWidth/3)){
						if(bannerSlider.toLeft > numWidth){
							moveLeft = 28;
						}else{
							moveLeft = bannerSlider.toLeft;
						}
					}
					bannerSlider.toLeft -= moveLeft;
					new Effect.Move(numLine, { x : moveLeft });
				}
				if(bannerSlider.effect == 'sliding'){
					banners[pos].show();
					banners[pos].setStyle({left : -banners[i].offsetWidth + 'px'})
					new Effect.Move(banners[pos], { x : banners[i].offsetWidth});
					if(bannerSlider.banners[pos].delay < 3){
						bannerSlider.banners[pos].delay = 3;
					}
				}else if(bannerSlider.effect == 'simple'){
					banners[pos].show();
				}else if(bannerSlider.effect == 'fade'){
					Effect.Appear(banners[pos]);
				}
				new Effect.Morph(banners[i].up('.slider_box'), {style : {height: banners[pos].getHeight() + 'px'}, duration : 0.3, afterUpdate: function(){
					var arrowLeft = $$('#banner_slider .arrow_left')[0], arrowRight = $$('#banner_slider .arrow_right')[0];
					if (arrowLeft) arrowLeft.setStyle({top: ($$('.slider_box')[0].getHeight() - arrowLeft.getHeight()) / 2 + 'px'});
					if (arrowRight) arrowRight.setStyle({top: ($$('.slider_box')[0].getHeight() - arrowRight.getHeight()) / 2 + 'px'});
				}});
				if(bannerSlider.autorotation){
					bannerSlider.timeVar = setTimeout(bannerSlider.nextBanner, bannerSlider.banners[pos].delay * 1000);
				}
				if(!bannerSlider.imageSize[pos]){
					bannerSlider.resize(pos);
				}
				break;
			}
		}
	},
	arrowsToggle : function(position){
		if(position == 0){
			var arrowLeft = $$('.arrow_left')[0];
			arrowLeft.addClassName('arrow_left_last');
			arrowLeft.stopObserving('click');
		}else{
			var arrowLeft = $$('.arrow_left')[0];
			if(arrowLeft.hasClassName('arrow_left_last')){
				arrowLeft.removeClassName('arrow_left_last');
				Event.observe(arrowLeft, 'click', function(){
					bannerSlider.prevBanner();
				});
			}
		}
		if(position == (bannerSlider.banners.length - 1)){
			var arrowRight = $$('.arrow_right')[0];
			arrowRight.addClassName('arrow_right_last');
			arrowRight.stopObserving('click');
		}else{
			var arrowRight = $$('.arrow_right')[0];
			if(arrowRight.hasClassName('arrow_right_last')){
				arrowRight.removeClassName('arrow_right_last');
				Event.observe(arrowRight, 'click', function(){
					bannerSlider.nextBanner();
				});
			}
		}
	},
	checkVisible : function(){
		var banners = $$('.banner_box');
		var count = 0;
		for(var i = 0; i < banners.length; i++){
			if(banners[i].visible()){
				count++;
			}
			if(count > 1){
				return true;
			}
		}
	},
	getActive : function(){
		var banners = $$('.banner_box');
		var count = 0;
		for(var i = 0; i < banners.length; i++){
			if(banners[i].visible()){
				return i;
			}
		}
	}
}