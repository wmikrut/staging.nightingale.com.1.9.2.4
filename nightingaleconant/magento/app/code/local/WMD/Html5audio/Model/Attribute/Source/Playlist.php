<?php
/**
 * WMD_Html5audio_Model_Attribute_Source_Playlist
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2013 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Model_Attribute_Source_Playlist extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
   
   protected $_options;
   
   /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions()
    {
        
        if (is_null($this->_options)) {
            
            $this->_options = array();                                                         
            
            $playlists = Mage::getModel('html5audio/playlist')->getCollection();
        
            foreach ($playlists as $list)
            {
                $this->_options[] = array(                           
                    'value' => $list->getId(),
                    'label' => $list->getPlaylistTitle(),
                );
            }
            
            array_unshift($this->_options, array('value' => '', 'label' => Mage::helper('adminhtml')->__('None')));
            
    		}
    		
        return $this->_options;
    }
    
   /**
     * Retrieve all options array
     *
     * @return array
     */
    public function toOptionArray()
    {
        
        if (is_null($this->_options)) {
            
            $this->_options = array();                                                         
            
            $playlists = Mage::getModel('html5audio/playlist')->getCollection();
        
            foreach ($playlists as $list)
            {
                $this->_options[] = array(                           
                    'value' => $list->getId(),
                    'label' => $list->getPlaylistTitle(),
                );
            }
            
            array_unshift($this->_options, array('value' => '', 'label' => Mage::helper('adminhtml')->__('None')));
            
    		}
    		
        return $this->_options;
    }

}