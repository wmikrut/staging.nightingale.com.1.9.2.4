<?php
/**
 * Toogas Lda.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA (End-User License Agreement)
 * that is bundled with this package in the file licence_toogas_community.txt.
 * It is also available at this URL:
 * http://www.toogas.com/licences/toogas_community.txt
 *
 * @category   Toogas
 * @package    Toogas_Featuredpopup
 * @copyright  Copyright (c) 2013 Toogas Lda. (http://www.toogas.com)
 * @license    http://www.toogas.com/licences/toogas_community.txt
 */

$installer = $this;

$installer->startSetup();

$featuredPopupSampleData = array(
    array(
        'popup_name' => 'Popup Homepage',
        'tipo'    => '1',
        'image_link' => 'toogas/featuredpopup/banner1.png',
        'width_image' => '1022',
        'height_image'    => '490',
        'url_link' => 'http://www.toogas.com/featured-pop-up.html',
        'pages'    => '1', 
        'num_aparece'    => '100', 
        'is_active' => '0',
        'show_effect' => 'Effect.Appear',
        'hide_effect' => 'Effect.Fade',
		'from_date' => '2013-01-01 00:00:00',
		'to_date' => '2020-01-31 00:00:00',        
		'delay_start' => '0',
		'delay_close' => '0',
		'priority' => '0',        
        'opacity' => '0.40',                                                   
    ),
    array(
        'popup_name' => 'Default Newsletter Box',
        'tipo'    => '2',
        'html_editor'    => '<p>{{widget type="featuredpopup/widgets_newsletterbox" text="Subscribe Our Newsletter" template="toogas_featuredpopup/widgets/newsletterbox/default.phtml"}}</p>',
        'width_image' => '550',
        'height_image'    => '360',
        'pages'    => '1', 
        'num_aparece'    => '100', 
        'is_active' => '0',
        'show_effect' => 'Effect.Appear',
        'hide_effect' => 'Effect.Fade',
		'from_date' => '2013-01-01 00:00:00',
		'to_date' => '2020-01-31 00:00:00',        
		'delay_start' => '0',
		'delay_close' => '0',
		'priority' => '0',         
        'opacity' => '0.40',
    ),
    array(
        'popup_name' => 'Newsletter Box 2',
        'tipo'    => '2',
        'html_editor'    => '<p>{{widget type="featuredpopup/widgets_newsletterbox" text="Subscribe our Newsletter" header_title="Get it NOW!" header_text="Increase more than 700% of Email Subscribers!" content_title="Pellentesque habitant" content_text="Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae." info_one="Pellentesque habitant mo tristique" info_two="Lorem ipsum dolor sit amet" info_three="Lorem ipsum dolor sit amet" input_text="Please enter your email!" button="Subscribe now!" template="toogas_featuredpopup/widgets/newsletterbox/newsletter2.phtml"}}</p>',
        'width_image' => '550',
        'height_image'    => '360',
        'pages'    => '1', 
        'num_aparece'    => '100', 
        'is_active' => '0',
        'show_effect' => 'Effect.Appear',
        'hide_effect' => 'Effect.Fade',
		'from_date' => '2013-01-01 00:00:00',
		'to_date' => '2020-01-31 00:00:00',        
		'delay_start' => '0',
		'delay_close' => '0',
		'priority' => '0',          
        'opacity' => '0.40',
    ),
    array(
        'popup_name' => 'Newsletter Box - Beautiful Pink',
        'tipo'    => '2',
        'html_editor'    => '<p>{{widget type="featuredpopup/widgets_newsletterbox" text="Subscribe our Newsletter" header_title="Get it NOW!" header_text="Increase more than 700% of Email Subscribers!" content_title="Pellentesque habitant" content_text="Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae." info_one="Pellentesque habitant mo tristique" info_two="Lorem ipsum dolor sit amet" info_three="Lorem ipsum dolor sit amet" input_text="Please enter your email!" button="Subscribe now!" template="toogas_featuredpopup/widgets/newsletterbox/beautifulpink.phtml"}}</p>',
        'width_image' => '550',
        'height_image'    => '360',
        'pages'    => '1', 
        'num_aparece'    => '100', 
        'is_active' => '0',
        'show_effect' => 'Effect.BlindDown',
        'hide_effect' => 'Effect.SwitchOff',
		'from_date' => '2013-01-01 00:00:00',
		'to_date' => '2020-01-31 00:00:00',        
		'delay_start' => '0',
		'delay_close' => '0',
		'priority' => '0',          
        'opacity' => '0.40',
    ),
    array(
        'popup_name' => 'Newsletter Box - Beautiful Blue',
        'tipo'    => '2',
        'html_editor'    => '<p>{{widget type="featuredpopup/widgets_newsletterbox" text="Subscribe our Newsletter" header_title="Get it NOW!" header_text="Increase more than 700% of Email Subscribers!" content_title="Pellentesque habitant" content_text="Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae." info_one="Pellentesque habitant mo tristique" info_two="Lorem ipsum dolor sit amet" info_three="Lorem ipsum dolor sit amet" input_text="Please enter your email!" button="Subscribe now!" template="toogas_featuredpopup/widgets/newsletterbox/beautifulblue.phtml"}}</p>',
        'width_image' => '550',
        'height_image'    => '360',
        'pages'    => '1', 
        'num_aparece'    => '100', 
        'is_active' => '0',
        'show_effect' => 'Effect.Grow',
        'hide_effect' => 'Effect.Shrink',
		'from_date' => '2013-01-01 00:00:00',
		'to_date' => '2020-01-31 00:00:00',        
		'delay_start' => '0',
		'delay_close' => '0',
		'priority' => '0',          
        'opacity' => '0.40',
    ),
    array(
        'popup_name' => 'Newsletter Box - On',
        'tipo'    => '2',
        'html_editor'    => '<p>{{widget type="featuredpopup/widgets_newsletterbox" text="Subscribe and GET your Bonus!" header_title="Get it NOW!" header_text="Sign Up to Receive Updates on New Arrivals, Sales and Special Events" content_title="Pellentesque habitant" content_text="Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae." info_one="Pellentesque habitant mo tristique" info_two="Lorem ipsum dolor sit amet" info_three="Lorem ipsum dolor sit amet" input_text="Please enter your email!" button="Subscribe now!" template="toogas_featuredpopup/widgets/newsletterbox/on.phtml"}}</p>',
        'width_image' => '550',
        'height_image'    => '360',
        'pages'    => '1', 
        'num_aparece'    => '100', 
        'is_active' => '0',
        'show_effect' => 'Effect.SlideDown',
        'hide_effect' => 'Effect.SlideUp',
		'from_date' => '2013-01-01 00:00:00',
		'to_date' => '2020-01-31 00:00:00',        
		'delay_start' => '0',
		'delay_close' => '0',
		'priority' => '0',          
        'opacity' => '0.40',
    ),
    array(
        'popup_name' => 'Newsletter Box - Beautiful Yellow',
        'tipo'    => '2',
        'html_editor'    => '<p>{{widget type="featuredpopup/widgets_newsletterbox" text="Subscribe and GET your Bonus!" header_title="Get it NOW!" header_text="Sign Up to Receive Updates on New Arrivals, Sales and Special Events" content_title="Pellentesque habitant" content_text="Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae." info_one="Pellentesque habitant mo tristique" info_two="Lorem ipsum dolor sit amet" info_three="Lorem ipsum dolor sit amet" input_text="Please enter your email!" button="Subscribe now!" template="toogas_featuredpopup/widgets/newsletterbox/beautifulyellow.phtml"}}</p>',
        'width_image' => '550',
        'height_image'    => '360',
        'pages'    => '1', 
        'num_aparece'    => '100', 
        'is_active' => '0',
        'show_effect' => 'Effect.SlideDown',
        'hide_effect' => 'Effect.Fold',
		'from_date' => '2013-01-01 00:00:00',
		'to_date' => '2020-01-31 00:00:00',        
		'delay_start' => '0',
		'delay_close' => '0',
		'priority' => '0',          
        'opacity' => '0.40',
    ),
    array(
        'popup_name' => 'Popup Page Url',
        'tipo'    => '2',
        'html_editor'    => '<p>{{widget type="featuredpopup/widgets_site" page_url="http://www.toogas.com" iframe_width="1000" iframe_height="500"}}</p>',
        'width_image' => '1000',
        'height_image'    => '500',
        'pages'    => '1', 
        'num_aparece'    => '100', 
        'is_active' => '0',
        'show_effect' => 'Effect.Appear',
        'hide_effect' => 'Effect.Fade',
		'from_date' => '2013-01-01 00:00:00',
		'to_date' => '2020-01-31 00:00:00',        
		'delay_start' => '0',
		'delay_close' => '0',
		'priority' => '0',          
        'opacity' => '0.40',
    ),            
    array(
        'popup_name' => 'Popup Youtube Video',
        'tipo'    => '2',
        'html_editor'    => '<p>{{widget type="featuredpopup/widgets_embedvideo" youtube_code="https://www.youtube.com/watch?v=BBvsB5PcitQ" width="560" height="315"}}</p>',
        'width_image' => '560',
        'height_image'    => '315',
        'pages'    => '1', 
        'num_aparece'    => '100', 
        'is_active' => '0',
        'show_effect' => 'Effect.BlindDown',
        'hide_effect' => 'Effect.Puff',
		'from_date' => '2013-01-01 00:00:00',
		'to_date' => '2020-01-31 00:00:00',        
		'delay_start' => '0',
		'delay_close' => '0',
		'priority' => '0',          
        'opacity' => '0.40',
    )
);

foreach ($featuredPopupSampleData as $row) {
    $installer->getConnection()->insertForce($installer->getTable('toogas_featuredpopup'), $row);
}

/**
 * Prepare database after data upgrade
 */
$installer->endSetup();