<?php
/**
 * Toogas Lda.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA (End-User License Agreement)
 * that is bundled with this package in the file licence_toogas_community.txt.
 * It is also available at this URL:
 * http://www.toogas.com/licences/toogas_community.txt
 *
 * @category   Toogas
 * @package    Toogas_Featuredpopup
 * @copyright  Copyright (c) 2013 Toogas Lda. (http://www.toogas.com)
 * @license    http://www.toogas.com/licences/toogas_community.txt
 */
 
class Toogas_Featuredpopup_Block_Widgets_Embedvideo
    extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface
{
	
	protected  function _construct() {
		parent::_construct();
		$this->setTemplate('toogas_featuredpopup/widgets/embedvideo/code.phtml');
	}	
	
	public function obtainYouTubeVideoId() {
		if ($this->hasData('youtube_code')) { 
			$ytUrl = $this->_getData('youtube_code');
			$step1=explode('v=', $ytUrl);
			if (isset($step1[1])) {
				$step2 =explode('&amp;', $step1[1]);
				return $step2[0];
			}
		}
		return '';
	}
	
	public function obtainWidth() {
		return $this->hasData('width') ? 
		$this->_getData('width') : 
		1;		
	}
	
	public function obtainHeight() {
		return $this->hasData('height') ? 
		$this->_getData('height') : 
		1;		
	}
    
}