<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGECONTENTSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PageContentSlider_Helper_Form extends Itoris_PageContentSlider_Helper_Data {

	private $yesNoValues = array(
		array(
			'value' => 1,
			'label' => 'Yes',
		),
		array(
			'value' => 0,
			'label' => 'No',
		),
	);

	private $modeValues = array(
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::MODE_TOP,
			'label' => 'Top',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::MODE_RIGHT,
			'label' => 'Right',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::MODE_BOTTOM,
			'label' => 'Bottom',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::MODE_LEFT,
			'label' => 'Left'
		),
	);

	private $slideEffectValues = array(
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::SLIDE_EFFECT_VERTICALLY,
			'label' => 'Slide Vertically'
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::SLIDE_EFFECT_HORIZONTALLY,
			'label' => 'Slide Horizontally',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::SLIDE_EFFECT_FADE,
			'label' => 'Fade',
		),
	);

	private $colorSchemeValues = array(
/*		array(
			'value' => Itoris_PageContentSlider_Model_Settings::THEME_DEFAULT,
			'label' => 'Default',
		),*/
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::THEME1,
			'label' => 'White',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::THEME2,
			'label' => 'Grey',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::THEME3,
			'label' => 'Black',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::THEME4,
			'label' => 'Green',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::THEME5,
			'label' => 'Magenta',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::THEME6,
			'label' => 'Dark Red',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::THEME7,
			'label' => 'Blue',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::THEME8,
			'label' => 'Green & Violet',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::THEME9,
			'label' => 'Light Yellow & Blue',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::THEME10,
			'label' => 'Grey & Red',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::THEME11,
			'label' => 'Red & Black',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::THEME12,
			'label' => 'Orange',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::THEME13,
			'label' => 'Blue & Orange',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::THEME14,
			'label' => 'Purple',
		),
		array(
			'value' => Itoris_PageContentSlider_Model_Settings::THEME15,
			'label' => 'Light Red & Blue',
		),
	);

	public function getYesNoOptionValues() {
		return $this->prepareValues($this->yesNoValues);
	}

	public function getModeValues() {
		return $this->prepareValues($this->modeValues);
	}

	public function getSlideEffectValues() {
		return $this->prepareValues($this->slideEffectValues);
	}

	public function getColorSchemeValues() {
		return $this->prepareValues($this->colorSchemeValues);
	}

	private function prepareValues($values, $withoutValues = array()) {
		foreach ($values as $key => $value) {
			if (!in_array($value['value'], $withoutValues)) {
				$values[$key]['label'] = $this->__($value['label']);
			} else {
				unset($values[$key]);
			}
		}

		return $values;
	}

	/**
	 * Prepare elements values for form
	 *
	 * @param $form Varien_Data_Form
	 *
	 * @return array
	 */
	public function prepareElementsValues($form) {
		$values = array();
		$fieldsets = $form->getElements();
		$checkWebsite = (bool)Mage::app()->getRequest()->getParam('website');
		$checkStore = (bool)Mage::app()->getRequest()->getParam('store');
		foreach ($fieldsets as $fieldset) {
			if (get_class($fieldset) == 'Varien_Data_Form_Element_Fieldset') {
				foreach ($fieldset->getElements() as $element) {
					if ($id = $element->getId()) {
						$value = $this->getSettings(true)->getSettingsValue($id);
						if ($value !== null) {
							$values[$id] = $value;
						}
						if ($element->getType() == 'checkbox' && $value) {
							$element->setIsChecked($value);
						}
						$element->setUseParent($this->getSettings(true)->isParentValue($id, (!$checkStore && $checkWebsite)));
						$element->setUseScope($checkWebsite ? ($checkStore ? $this->__('Use Website') : $this->__('Use Default')) : null);
					}
				}
			}
		}

		return $values;
	}

	public function getUseScope() {
		$checkWebsite = (bool)Mage::app()->getRequest()->getParam('website');
		$checkStore = (bool)Mage::app()->getRequest()->getParam('store');
		return $checkWebsite ? ($checkStore ? $this->__('Use Website') : $this->__('Use Default')) : null;
	}

	public function getUseScopeWithoutWebsite() {
		$checkStore = (bool)Mage::app()->getRequest()->getParam('store');
		return $checkStore ? $this->__('Use Default') : null;
	}

	public function getIsParentValue($settingId) {
		$checkWebsite = (bool)Mage::app()->getRequest()->getParam('website');
		$checkStore = (bool)Mage::app()->getRequest()->getParam('store');
		return $this->getSettings(true)->isParentValue($settingId, (!$checkStore && $checkWebsite));
	}

	/**
	 * Is setting value default
	 *
	 * @return bool
	 */
	protected function isDefaultSettings() {
		return !Mage::app()->getRequest()->getParam('website');
	}

}
?>