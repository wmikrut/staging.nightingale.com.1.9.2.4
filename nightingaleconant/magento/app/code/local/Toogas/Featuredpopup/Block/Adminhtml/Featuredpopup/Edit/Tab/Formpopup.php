<?php
/**
 * Toogas Lda.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA (End-User License Agreement)
 * that is bundled with this package in the file licence_toogas_community.txt.
 * It is also available at this URL:
 * http://www.toogas.com/licences/toogas_community.txt
 *
 * @category   Toogas
 * @package    Toogas_Featuredpopup
 * @copyright  Copyright (c) 2010 Toogas Lda. (http://www.toogas.com)
 * @license    http://www.toogas.com/licences/toogas_community.txt
 */
class Toogas_Featuredpopup_Block_Adminhtml_Featuredpopup_Edit_Tab_Formpopup extends Mage_Adminhtml_Block_Widget_Form 

{
  protected function _prepareForm()
  {  
	  $form = new Varien_Data_Form();	
	  
	  $model = Mage::registry('featuredpopup_data');
	  
	  $fieldset = $form->addFieldset('featuredpopup_popup', array('legend'=>Mage::helper('featuredpopup')->__('Popup Settings')));
      
      $fieldset->addField('show_effect', 'select', array(
          'label'     => Mage::helper('featuredpopup')->__('Show Effect'),
          'name'      => 'show_effect',
          'values'    => $this->helper('featuredpopup')->getShowEffectsOptions(),
          'note'	  => Mage::helper('featuredpopup')->__('To open Popup.'),
	  ));
	  
      $fieldset->addField('hide_effect', 'select', array(
          'label'     => Mage::helper('featuredpopup')->__('Hide Effect'),
          'name'      => 'hide_effect',
          'values'    => $this->helper('featuredpopup')->getHideEffectsOptions(),
          'note'	  => Mage::helper('featuredpopup')->__('To close Popup.'),      
	  ));	        
      
      $fieldset->addField('delay_start', 'text', array(
          'label'     => Mage::helper('featuredpopup')->__('Delay Start'),
          'name'      => 'delay_start',
          'class'     => 'validate-zero-or-greater',
          'note'	  => Mage::helper('featuredpopup')->__('Time to show Popup.'),         
	  ));
	  
      $fieldset->addField('delay_close', 'text', array(
          'label'     => Mage::helper('featuredpopup')->__('Delay Close'),
          'name'      => 'delay_close',
          'class'     => 'validate-zero-or-greater',
          'note'	  => Mage::helper('featuredpopup')->__('Time to close Popup.'),          
	  ));	  
	  	       
      $fieldset->addField('priority', 'text', array(
          'label'     => Mage::helper('featuredpopup')->__('Priority'),
          'name'      => 'priority',
          'class'     => 'validate-zero-or-greater',
          'note'	  => Mage::helper('featuredpopup')->__('Popup with the lowest priority will be applied first.'),         
	  ));
	  
      $fieldset->addField('opacity', 'text', array(
          'label'     => Mage::helper('featuredpopup')->__('Opacity'),
          'name'      => 'opacity',
          'class'     => 'validate-zero-or-greater',
          'note'	  => Mage::helper('featuredpopup')->__('Opacity of overlay background-color.'),         
	  ));	
	  
	  $form->setValues($model->getData());
	  
	  $this->setForm($form);
      return parent::_prepareForm();
  }

  
}  
