<?php
// help via http://www.atwix.com/magento/add-button-to-system-configuration/
class NC_OrderFulfillment_Block_Adminhtml_System_Config_Form_Runimportbutton extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /*
     * Set template
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('nc/system/config/run-import-button.phtml');
    }

    /**
     * Return element html
     *
     * @param  Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        return $this->_toHtml();
    }

    /**
     * Return ajax url for button
     *
     * @return string
     */
    public function getAjaxRunImportUrl()
    {
        return Mage::helper('adminhtml')->getUrl('orderfulfillment/index/runimport');
    }

    /**
     * Generate button html
     *
     * @return string
     */
    public function getButtonHtml()
    {
        $button = $this->getLayout()->createBlock('adminhtml/widget_button')
                       ->setData(array(
                                     'id'        => 'ordershipment_run',
                                     'label'     => $this->helper('adminhtml')->__('Run Shipment Import'),
                                     'onclick'   => 'javascript:runImport(); return false;'
                                 ));

        return $button->toHtml();
    }
}