<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
require_once(Mage::getModuleDir('controllers','Mage_Adminhtml').DS.'Catalog'.DS.'ProductController.php');

class MageRevolution_PackageProductType_Adminhtml_Catalog_ProductController extends Mage_Adminhtml_Catalog_ProductController
{

    /**
     * Get package products grid
     */
    public function packageGridAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.package')
            ->setProductsPackage($this->getRequest()->getPost('products_package', null));
        $this->renderLayout();
    }

    /**
     * Get package products grid and serializer block
     */
    public function packageProductAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.package')
            ->setProductsPackage($this->getRequest()->getPost('products_package', null));
        $this->renderLayout();
    }

    /**
     * Get associated grouped products grid only
     *
     */
    public function packageGridOnlyAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('catalog.product.edit.tab.package')
            ->setProductsRelated($this->getRequest()->getPost('products_package', null));
        $this->renderLayout();
    }

}
