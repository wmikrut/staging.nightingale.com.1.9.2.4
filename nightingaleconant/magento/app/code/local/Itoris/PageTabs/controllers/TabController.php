<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGETABS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */


class Itoris_PageTabs_TabController extends Mage_Core_Controller_Front_Action {

	/**
	 * Load tab content via Ajax
	 *
	 * @throws Exception
	 */
	public function loadAction() {
		$result = array();
		if ($this->getDataHelper()->isRegisteredAutonomous()) {
			$tabId = $this->getRequest()->getParam('id');
			try {
				$tab = Mage::getModel('itoris_pagetabs/tab')->load($tabId);
				if ($tab->getId()) {
					$result['tab_content'] = Mage::getModel('widget/template_filter')->filter($tab->getContent());
				} else {
					throw new Exception('No such tab with id = ' . $tabId);
				}
			} catch (Exception $e) {
				Mage::logException($e);
				$result['error'] = $this->__('Tab have not been loaded!');
			}
		} else {
			$result['error'] = $this->__('Home Page Tabs is not registered');
		}

		$this->getResponse()->setBody(Zend_Json::encode($result));
	}

	/**
	 * @return Itoris_PageTabs_Helper_Data
	 */
	public function getDataHelper() {
		return Mage::helper('itoris_pagetabs');
	}
}
?>