<?php     
/**
 * WMD_Html5audio_Model_System_Config_Source_Sample
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.com>
 * @copyright 2013 Dominik Wyss | Digiswiss (http://www.digiswiss.ch)
 * @link      http://wmdextensions.com/
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Model_System_Config_Source_Sample
{
    /**
     * Actions
     * @var array
     */		
    protected $_options;
	
    protected function _getCollection()
    {
        return Mage::getModel('html5audio/sample')->getCollection();
    }
    
    /**
     * Return the avaiable job status
     * 
     * @param boolean $isMultiselect if Multiselect for the order status selection is allowed 
     *
     * @return array
     */	      
	  public function toOptionArray($isMultiselect=false)
    {		
		    if (!$this->_options) {
            foreach ($this->_getCollection() as $sample)
            {
                $label = $sample->getTitle() . '(' . $sample->getMp3Path() . ')';
                $this->_options[] = array('value' => $sample->getSampleId(), 'label' => $label);
            } 	
            array_unshift($this->_options, array('value'=>'', 'label'=> Mage::helper('adminhtml')->__('-- Please Select --')));	
            return $this->_options;
        }		
    }   
    
//     public function toOptionHash()
//     {                           
//         $data = array();
//         foreach ($this->_getCollection() as $link)
//         {
//             $data[$link->getLinkId()] = $link->getLinkFile();
//         } 
//     		return $data;
//     }

}