/*!
 * @category   Apptrian
 * @package    Apptrian_ProductSlider
 * @author     Apptrian
 * @copyright  Copyright (c) 2015 Apptrian (http://www.apptrian.com)
 * @license    http://www.apptrian.com/license    Proprietary Software License (EULA)
 */

;(function ($, window, document, undefined) {

    $(function() {
    	
    	$slider = $(".apptrian-productslider");
        
        if ($slider.length > 0) {
        
            var reg = {};
            
            reg                   = apptrianProductSlider;
            
            reg.$slider           = $slider;
            reg.$slideContainer   = reg.$slider.find(".apptrian-productslider-products");
            reg.$slides           = reg.$slideContainer.find(".apptrian-productslider-product");
            
            reg.$showMore = $('<a href="#" class="apptrian-productslider-show-more"><span>' + reg.showMoreText + '</span></a>' ).appendTo( ".apptrian-productslider-next-wrapper" );
            reg.$showMore.hide();
            
            reg.$ajaxLoader = $('<span class="apptrian-productslider-ajax-loader"><img src="' + reg.ajaxLoaderUrl + '" alt=" "></span>').appendTo( ".apptrian-productslider-next-wrapper" );
            reg.$ajaxLoader.hide();
            
            reg.$next             = $(".apptrian-productslider-next");
            reg.$previous         = $(".apptrian-productslider-previous");
            
            reg.$scrollbarWrapper   = $(".apptrian-productslider-scroll-bar-wrapper");
            // Horizontal gap for scrollbar in px
            reg.scrollbarWrapperGap = 20;
            
            reg.mouseScrollActive = 0;
            
            reg.currentScroll   = 0;
            reg.currentMargin   = 0;
            reg.maxScroll;
            reg.currentItem     = 1;
            reg.totalItems      = 0;
            reg.itemWidth;
            reg.totalWidth      = 0;
            reg.currentView     = 1;
            reg.totalViews      = 0;
            reg.viewWidth;
            reg.visibleViewWidth;
            
            reg.interval;
            reg.autoslideActive = 0;
            reg.tapHold         = 0;
            
            reg.busy            = 0;
            reg.currentPage     = 1;
            reg.pageCount       = Math.ceil(reg.productCount / reg.pageSize);
            
            slider(reg);
        
        $(window).resize(function() {
            
            recalculate(reg);
            resetScrollBarValue(reg);
	    	
        });
        
        }
        
    });
    
    /**
     * Slider initializer.
     */
    function slider(reg)
    {
        
        clicking(reg);
        
        scrolling(reg);
        
        swiping(reg);
        
        recalculate(reg);
        
        scrollBar(reg);
        
        if (reg.autoslideInterval > 0) {
        	
        	startSlider(reg);
        	
    		reg.$slider.on("mouseenter", function () { stopSlider(reg); })
    				   .on("mouseleave", function () { startSlider(reg); });
    		
    		reg.$slider.on("taphold", function () {
    			
    			if (reg.tapHold % 2 == 0) {
    				stopSlider(reg);
    			} else {
    				startSlider(reg);
    			}
    			
    			reg.tapHold++;
    			
    		});
    		
        }
        
    }
    
    /* bof Scrollbar functions */
    
    /**
     * Scrollbar initializer.
     */
    function scrollBar(reg)
    {
    	
    	if (reg.scrollBarEnabled) {
    		
	    	//build slider
	    	reg.$scrollbar = $( ".apptrian-productslider-scroll-bar" ).slider({
	    		// In updateContentHolder() animate option will be set
	    		// Should be false by default
	    		animate: false,
		    	slide: function( event, ui ) {
		    		
		    		if ( reg.totalWidth > reg.viewWidth ) {
		    			
		    			// Recalculate currentMargin
		    			reg.currentMargin = Math.round((ui.value / 100 * ( reg.viewWidth - reg.totalWidth )) + reg.controlsFixPrevious);
		    			
		    			// Recalculate currentScroll
		    			reg.currentScroll = reg.controlsFixPrevious - reg.currentMargin;
		    			
		    			// Recalculate currentItem
		    			reg.currentItem = Math.ceil(reg.currentScroll / reg.itemWidth) + 1;
		    			
		    			// Recalculate currentView
		    			reg.currentView = Math.ceil(reg.currentScroll / reg.visibleViewWidth) + 1;
		    			
		    			// Update control buttons
		    			updateButtons(reg);
		    			
		    			// Set margin based on scrollbar handle
		    			reg.$slideContainer.css( "margin-left", reg.currentMargin + "px" );
		    			
			    	} else {
			    		
			    		reg.$slideContainer.css( "margin-left", reg.controlsFixPrevious );
			    		
			    	}
		    	}
	    	});
	    	
    	}
    	
    }
    
    /**
     * Resets slider value based on scroll content position.
     */
	function resetScrollBarValue(reg) {
		
		if (reg.scrollBarEnabled) {
			
			reg.$scrollbar.slider("value", Math.abs(Math.round(((reg.currentMargin - reg.controlsFixPrevious) / ( reg.viewWidth - reg.totalWidth )) * 100)));
		
		}
		
	}
	
    /* eof Scrollbar functions */
    
    /**
     * Autoslide start.
     */
    function startSlider(reg)
    {
    	// If interval is already set just return true do not start another one
    	if (reg.autoslideActive === 1) {
    		return true;
    	}
        reg.interval        = setInterval(function(){ showNextSlide(reg, 1); }, reg.autoslideInterval);
        reg.autoslideActive = 1;
    }
    
    /**
     * Autoslide stop.
     */
    function stopSlider(reg)
    {
    	// Just return if there is no interval
    	if (reg.autoslideActive === 0) {
    		return true;
    	}
        clearInterval(reg.interval);
        reg.autoslideActive = 0;
    }
    
    /**
     * Recalculate function used in initialization and window resize.
     */
    function recalculate(reg)
    {
    	reg.controlsFixPrevious = Math.ceil($(".apptrian-productslider-previous-wrapper").outerWidth());
    	reg.controlsFixNext     = Math.ceil($(".apptrian-productslider-next-wrapper").outerWidth());
    	
        reg.itemWidth        = Math.round(reg.$slides.outerWidth(true));
        reg.viewWidth        = Math.ceil(reg.$slider.width());
        reg.visibleViewWidth = reg.viewWidth - reg.controlsFixPrevious - reg.controlsFixNext;
        reg.totalItems       = reg.$slides.length;
        reg.totalWidth       = reg.totalItems * reg.itemWidth + reg.controlsFixPrevious + reg.controlsFixNext;
        reg.maxMargin        = reg.totalWidth - reg.viewWidth;
        reg.totalViews       = Math.ceil(reg.totalWidth / reg.visibleViewWidth);
        
        // Set total width of container
        reg.$slideContainer.width(reg.totalWidth);
        
        if (reg.scrollBarEnabled) {
        	
	        // Set Scrollbar width
	        var padding = Math.ceil(reg.$scrollbarWrapper.outerWidth()) - Math.ceil(reg.$scrollbarWrapper.width());
	    	reg.$scrollbarWrapper.width(Math.round(reg.viewWidth - reg.controlsFixPrevious - reg.controlsFixNext - padding - reg.scrollbarWrapperGap));
	    	
	    	// Hide Scrollbar if viewWidth is wider than totalWidth
	        // (Hide Scrollbar if there are only few products)
	        if (reg.viewWidth > reg.totalWidth) {
	        	reg.$scrollbarWrapper.css("visibility", "hidden");
	        } else {
	        	reg.$scrollbarWrapper.css("visibility", "visible");
	        }
	    	
        }
    	
        updateCurrentScroll(reg);
        
        // This is a fix for resize from smaller width to higher width displays
        // second part is there if view width is wider than total width
        // (Second part happens when there are very few products)
        if (reg.currentScroll > reg.maxMargin && reg.totalWidth > reg.viewWidth) {
        	
            // Fix to move the slider to the end
        	
        	// Recalculate currentMargin
            reg.currentMargin = -(reg.maxMargin - reg.controlsFixPrevious);
        	
            // Recalculate currentScroll
            reg.currentScroll = reg.maxMargin;
    		
    		// Recalculate currentItem
            reg.currentItem = Math.ceil(reg.currentScroll / reg.itemWidth) + 1;
            
            // Recalculate currentView
            reg.currentView = Math.ceil(reg.currentScroll / reg.visibleViewWidth) + 1;
    		
        }
        
        reg.$slideContainer.css("margin-left", reg.currentMargin);
        
        updateButtons(reg);
        
    }
    
    /**
     * Attach click event to controls.
     */
    function clicking(reg)
    {
    
        reg.$previous.on("click", function (event) {
            showPreviousSlide(reg);
            event.preventDefault();
        });
        
        reg.$next.on("click", function (event) {
            showNextSlide(reg, 0);
            event.preventDefault();
        });
        
        reg.$showMore.on("click", function (event) {
        	
        	stopSlider(reg);
        	
        	// This if is to sync tapHold
        	if (reg.tapHold % 2 != 0) {
        		reg.tapHold++;
			}
        	
        	getProducts(reg);
        	
        	reg.$showMore.hide();
        	
        	showAjaxLoader(reg);
        	
            event.preventDefault();
        });
        
    }
    
    /**
     * Attach mouse scroll event to slider.
     */
    function scrolling(reg)
    {
        reg.$slider.on( 'DOMMouseScroll mousewheel', function ( event ) {
        	if (reg.mouseScrollActive == 0) {
	        	if( event.originalEvent.detail > 0 || event.originalEvent.wheelDelta < 0 ) { //alternative options for wheelData: wheelDeltaX & wheelDeltaY
	        		//scroll down
	        		showNextSlide(reg, 0);
	        		reg.mouseScrollActive = 1;
	        	} else {
	        		//scroll up
	        		showPreviousSlide(reg);
	        		if (reg.scrollByItem === 1 && reg.currentItem === 1) {
	        			reg.mouseScrollActive = 0;
	        		} else if (reg.scrollByItem === 0 && reg.currentView === 1) {
	        			reg.mouseScrollActive = 0;
	        		} else {
	        			reg.mouseScrollActive = 1;
	        		}
	        	}
        	}
        	//prevent page fom scrolling
        	event.preventDefault();
        });
    }
    
    /**
     * Attach swipe events to slider.
     */
    function swiping(reg)
    {
    	reg.$slider.on("swipeleft", function () {
    		showNextSlide(reg, 0);
    	});
    	
    	reg.$slider.on("swiperight", function () {
    		showPreviousSlide(reg);
    	});
    }
    
    /**
     * Shows previous slide (item/visibleView)
     */
    function showPreviousSlide(reg)
    {
        var update = false;
        
        if (reg.currentItem > 1) {
            reg.currentItem--;
            update = true;
        }
        
        if (reg.currentView > 1) {
            reg.currentView--;
            update = true;
        }
        
        if (update) {
            updateCurrentScroll(reg);
            updateContentHolder(reg);
            updateButtons(reg);
        }
        
        // on every interaction/change with slider contact server for more items
        getProducts(reg);
        
    }
    
    /**
     * Shows next slide (item/visibleView)
     */
    function showNextSlide(reg, autoslideCall)
    {
        
        if (reg.currentItem < reg.totalItems 
            && reg.currentScroll < reg.maxMargin 
        ) {
            reg.currentItem++;
        } else {
            reg.currentItem = 1;
        }
        
        if (reg.currentView < reg.totalViews
            && reg.currentScroll < reg.maxMargin 
        ) {
            reg.currentView++;
        } else {
            reg.currentView = 1;
        }
        
        updateCurrentScroll(reg);
        updateContentHolder(reg);
        updateButtons(reg);
        
        // On every interaction/change with slider contact server for more items
        // On autoslideCall it will not contact server only on user interaction
        if (autoslideCall !== 1) {
        	getProducts(reg);
    	}
        
    }
    
    /**
     * Animates slider and moves it to new position.
     */
    function updateContentHolder(reg)
    {
    	
    	reg.$slideContainer.animate(
    			{marginLeft: reg.currentMargin}, 
    			{
    				duration: reg.animationDuration, 
    				complete: function() {
    					reg.mouseScrollActive = 0;
    					// Disables animation for scroll bar 
    					// (if user clicks directly on slider track animation is not needed)
    					if (reg.scrollBarEnabled) {
    						reg.$scrollbar.slider( "option", "animate", false);
    					}
    				}
    			}
    	);
        
    	// Sets animation duration for scroll bar
    	if (reg.scrollBarEnabled) {
    		reg.$scrollbar.slider( "option", "animate", reg.animationDuration);
    	}
    	
        resetScrollBarValue(reg);
        
    }
    
    /**
     * Update controls.
     */
    function updateButtons(reg)
    {
    	
        if (reg.scrollByItem) {
        
            if (reg.currentItem < reg.totalItems
                && reg.currentScroll < reg.maxMargin
            ) {
                reg.$next.show();
                reg.$showMore.hide();
            } else {
                reg.$next.hide();
                
                // Show More Products button if there are more products to download from server
                if (reg.totalItems < reg.productCount) {
                	reg.$showMore.show();
                } else {
                	reg.$showMore.hide();
                }
                
            }
            if(reg.currentItem > 1) {
                reg.$previous.show();
            } else {
                reg.$previous.hide();
                
                // On First item hide Show More button
                reg.$showMore.hide();
            }
            
        } else {
            
            if (reg.currentView < reg.totalViews
                && reg.currentScroll < reg.maxMargin
            ) {
                reg.$next.show();
                reg.$showMore.hide();
            } else {
                reg.$next.hide();
                
                // Show More Products button if there are more products to download from server
                if (reg.totalItems < reg.productCount) {
                	reg.$showMore.show();
                } else {
                	reg.$showMore.hide();
                }
                
            }
            if(reg.currentView > 1) {
                reg.$previous.show();
            } else {
                reg.$previous.hide();
                
                // On First view hide Show More button
                // Potential bug would happen if pageSize (number of products returned per server request)
                // is someting small like 1 or 5 so that there is only one view. But this will never happen
                // In magento admin minimum number of products per request is 10.
                reg.$showMore.hide();
            }
        
        }
        
    }
    
    /**
     * Updates current postion of a slider.
     */
    function updateCurrentScroll(reg)
    {
        
        if (reg.scrollByItem) {
        
            reg.currentScroll = (reg.currentItem - 1) * reg.itemWidth;
            
            updateCurrentMargin(reg);
            
        } else {
        
            reg.currentScroll = (reg.currentView - 1) * reg.visibleViewWidth;
            
            updateCurrentMargin(reg);
            
        }
        
    }
    
    /**
     * Updates current margin value od the content.
     */
    function updateCurrentMargin(reg)
    {
    
        if (reg.currentScroll === 0) {
        	reg.currentMargin = reg.controlsFixPrevious;
        } else if (reg.currentScroll > reg.maxMargin) {
        	reg.currentMargin = -(reg.maxMargin - reg.controlsFixPrevious);
        } else {
        	reg.currentMargin = -(reg.currentScroll - reg.controlsFixPrevious);
        }
        
    }
    
    /**
     * Ajax get products from server.
     * 
     * @param Object reg
     */
	function getProducts(reg)
	{
		
		if (reg.busy == 0 && reg.currentPage < reg.pageCount ) {
    		
    		reg.busy = 1;
    		
			$.ajax({
	            type: "POST",
	            url: reg.baseUrl + "apptrian_productslider/ajax/products",
	            data: {page: reg.currentPage + 1, type: reg.pageType, id: reg.categoryId, uenc: reg.uenc}
	        }).done(function(msg) {
	            
	          	var result = $.parseJSON(msg);
	          	
	          	process(reg, result);
	          	
	          	// increment currentPage
	          	reg.currentPage += 1;
	          	
	          	// not busy any more
				reg.busy = 0;
				
				// hide ajax loader
				hideAjaxLoader(reg);
				
	        })
	        .fail(function() {
	        	
	        	// Not loading anymore
				hideAjaxLoader(reg);
	          	
	          	// not busy any more
				reg.busy = 0;
	        	
			});
			
		}
        
	}
    
	/**
	 * Adds new products to the slider.
	 */
	function process(reg, result)
	{
		
		if (result !== null && result.length > 0) {
			
			var html  = "";
			var count = result.length;
			var i     = 0;
			
			var elements = reg.infoSortOrder;
			var elCount  = elements.length;
			var j;
			
			var p;
			var rating;
			var customRating = reg.customRating;
			
			for (i; i < count; i++) {
				
				p = result[i];
				
				html += '<div class="apptrian-productslider-product">';
				
				j = 0;
				
				for (j; j < elCount; j++) {
					
					switch (elements[j]) {
					    case 'image':
					    	html += '<div class="apptrian-productslider-product-image"><a href="' + p.url + '"><img src="' + p.image + '" alt="' + p.name + '"/></a></div>';
					        break;
					    case 'name':
					    	html += '<div class="apptrian-productslider-product-name"><a href="' + p.url + '"><span>' + p.name + '</span></a></div>';
					        break;
					    case 'price':
					    	if (p.type_id != 'bundle') {
					    		html += '<div class="apptrian-productslider-product-price">';
					    		if (p.type_id == 'configurable' || p.type_id == 'grouped') {
						    		html += '<span class="apptrian-productslider-product-price-from">' + reg.priceFromText + '</span>';
						    	}
						    	if (p.price != p.final_price) {
						    		html += '<span class="apptrian-productslider-product-price-old">' + p.price + '</span>';
						    	}
						    	html += '<span>' + p.final_price + '</span></div>';
					    	}
					        break;
					    case 'sku':
					    	html += '<div class="apptrian-productslider-product-sku"><span>' + p.sku + '</span></div>';
					        break;
					    case 'description':
					    	html += '<div class="apptrian-productslider-product-description"><span>' + p.description + '</span></div>';
					        break;
					    case 'rating':
					    	rating = p.rating_summary;
					    	if (rating > 0) {
								if (customRating == 1) {
									html += '<div class="apptrian-productslider-product-ratings"><div class="apptrian-productslider-product-rating-box"><div class="apptrian-productslider-product-rating" style="width: ' + rating + '%"></div></div></div>';
								} else {
									html += '<div class="apptrian-productslider-product-rating-default"><div class="ratings"><div class="rating-box"><div class="rating" style="width: ' + rating + '%"></div></div></div></div>';
								}
					    	}
					        break;
					    case 'cart':
				        	if (p.is_saleable) {
				        		if (p.cart_url != "") {
				        			html += '<div class="apptrian-productslider-product-add-to-cart"><a href="' + p.cart_url + '"><span>' + reg.addToCartText + '</span></a></div>';
				        		} else {
				        			html += '<div class="apptrian-productslider-product-view-details"><a href="' + p.url + '"><span>' + reg.viewDetailsText + '</span></a></div>';
				        		}
				        	} else {
				        		html += '<div class="apptrian-productslider-product-out-of-stock"><a href="' + p.url + '"><span>' + reg.outOfStockText + '</span></a></div>';
				        	}
				        	break;
					    case 'compare':
					    	html += '<div class="apptrian-productslider-product-compare"><a href="' + p.compare_url + '"><span>' + reg.addToCompareText + '</span></a></div>';
					        break;
					    case 'wishlist':
					    	html += '<div class="apptrian-productslider-product-wishlist"><a href="' + p.wishlist_url + '"><span>' + reg.addToWishlistText + '</span></a></div>';
					        break;
					    default:
						    break;
					}
					
				}
          		
				html += '</div>';
          		
			}
			
			if (html != "") {
				
				reg.$slideContainer.append(html);
	            
	            reg.$slides = reg.$slideContainer.find(".apptrian-productslider-product");
	            
	            recalculate(reg);
				
			}
			
		}
		
	}
	
	/**
     * Show ajax loader.
     */
	function showAjaxLoader(reg)
	{
		reg.$ajaxLoader.show( "slow", function() {
			// Animation complete.
		});
    }

	/**
     * Hide ajax loader.
     */
	function hideAjaxLoader(reg)
	{
		reg.$ajaxLoader.hide( "slow", function() {
			// Animation complete.
		});
	}

})(jQuery, window, document);
