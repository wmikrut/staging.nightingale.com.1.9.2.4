<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGETABS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */


class Itoris_PageTabs_Block_Admin_Settings_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

	protected function _prepareForm() {
		$form = new Varien_Data_Form(array(
			'action'        => $this->getUrl('*/*/save', array('_current' => true)),
			'use_container' => true,
			'id'            => 'edit_form',
			'method'        => 'post',
		));

		$configurationFieldset = $form->addFieldset('configuration_fieldset', array('legend' => $this->__('Configuration')));

		$configurationFieldset->addField('enabled', 'select', array(
			'label'  => $this->__('Enabled'),
			'title'  => $this->__('Enabled'),
			'name'   => 'settings[enabled][value]',
			'values' => $this->getFormHelper()->getYesNoOptionValues(),
		))->getRenderer()->setTemplate('itoris/pagetabs/settings/form/fieldset/element.phtml');

		$configurationFieldset->addField('slide_effect', 'select', array(
			'label'  => $this->__('Slide effect'),
			'title'  => $this->__('Slide effect'),
			'name'   => 'settings[slide_effect][value]',
			'values' => $this->getFormHelper()->getSlideEffectValues(),
		));

		$configurationFieldset->addField('auto_height', 'checkbox', array(
			'label'  => $this->__('Auto-Height'),
			'title'  => $this->__('Auto-Height'),
			'name'   => 'settings[auto_height][value]',
			'value'  => 1,
		));

		$configurationFieldset->addField('height_hidden', 'hidden', array(
			'name'  => 'settings[height][value]',
		));

		$configurationFieldset->addField('height', 'text', array(
			'label'    => $this->__('Height'),
			'title'    => $this->__('Height'),
			'name'     => 'settings[height][value]',
			'note'     => $this->__('pixels'),
			'required' => true,
			'class'    => 'validate-number-range number-range-1-10000',
			'depends'  => 'auto_height',
		));

		$configurationFieldset->addField('switch_on', 'select', array(
			'label'  => $this->__('Switch on'),
			'title'  => $this->__('Switch on'),
			'name'   => 'settings[switch_on][value]',
			'values' => $this->getFormHelper()->getSwitchOnValues(),
		));

		$configurationFieldset->addField('load_method', 'select', array(
			'label'  => $this->__('Load Method'),
			'title'  => $this->__('Load Method'),
			'name'   => 'settings[load_method][value]',
			'values' => $this->getFormHelper()->getLoadMethodValues(),
		));

		$configurationFieldset->addField('responsive_width', 'text', array(
			'label'    => $this->__('Switch to mobile view if browser width less than (px)'),
			'title'    => $this->__('Switch to mobile view if browser width less than (px)'),
			'name'     => 'settings[responsive_width][value]',
		));

		if (!$this->getRequest()->getParam('website')) {
			$tabsInstances = $form->addFieldset('tabs_instances', array(
				'legend' => $this->__('Tabs instances')
			));
			$tabsInstances->setRenderer(new Itoris_PageTabs_Block_Admin_Settings_Renderer_Instance());
		}

		$values = $this->getFormHelper()->prepareElementsValues($form);
		$values['height_hidden'] = $this->getFormHelper()->getSettings(true)->getHeight();
		$form->setValues($values);
		$this->setForm($form);

		return $this;
	}

	/**
	 * @return Itoris_PageTabs_Helper_Form
	 */
	protected function getFormHelper() {
		return Mage::helper('itoris_pagetabs/form');
	}
}

?>