if (!Itoris) {
	var Itoris = {};
}
Itoris.PageTabs = Class.create({
	initialize: function(blockId, config, tabIds) {
		this.block = $(blockId);
		this.config = config;
		this.currentTab = 0;
		this.tabWidth = 0;
		this.tabIds = tabIds;
		this.loadedTabs = [0];
		this.loadingTabs = [];
		this.titlesMaxMargin = 0;
		this.defaultTitlesPosition = 0;
		this.arrowLeft = this.block.select('.itoris_pagetabs_arrow_left')[0];
		this.arrowRight = this.block.select('.itoris_pagetabs_arrow_right')[0];
		this.loader = this.block.select('.itoris_pagetabs_loading')[0];
		this.isMobile = (function(a){ return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4));})(navigator.userAgent||navigator.vendor||window.opera);
		this.isResponse = false;
		this.prepareBlock();
		this.addEvents();
		this.updateResponsive();
	},
	prepareBlock: function() {
		this.changeClass(this.block.select('.itoris_pagetabs_title')[0], 'itoris_pagetabs_title_active');
		var tabs = this.block.select('.itoris_pagetabs_tab');
		this.updateTabWidth();
		tabs[0].setStyle({left: '0px'});
		this.setTabsBlockHeight(tabs[0].getHeight(), true);
		var titlesBoxWrapper = this.block.select('.itoris_pagetabs_titles_wrapper')[0];
		var titlesBox = this.block.select('.itoris_pagetabs_titles')[0];
		var titles = this.block.select('.itoris_pagetabs_title');
		var titlesWidth = 0;
		var obj = this;
		titles.each(function(elm){
			if (obj.isIe7()) {
				var width = 1;
				elm.setStyle({height: 'auto',width: width + 'px'});
				while (elm.getHeight() > 31) {
					elm.setStyle({width: width + 'px'});
					width++;
				}
			}
			titlesWidth += elm.getWidth() + parseNumber(elm.getStyle('marginLeft'))+ parseNumber(elm.getStyle('marginRight'));
		});
		this.titlesWidth = titlesWidth;
		var wrapperMargins = parseNumber(titlesBoxWrapper.getStyle('marginLeft'))+ parseNumber(titlesBoxWrapper.getStyle('marginRight'));
		if (titlesBox.getHeight() > 50) {
			this.changeClass(titlesBoxWrapper, 'itoris_pagetabs_with_scroll');
			titlesBox.setStyle({
				width: titlesWidth + 'px'
			});
			while (titlesBox.getHeight() > 50) {
				titlesWidth++;
				titlesBox.setStyle({
					width: titlesWidth + 'px'
				});
			}

			this.defaultTitlesPosition = parseNumber(titlesBox.getStyle('marginLeft'));
			setTimeout(function(){
				this.titlesMaxMargin = titlesWidth - titlesBoxWrapper.getWidth() + this.defaultTitlesPosition;
			}.bind(this), 50);
			this.titlesWidth = titlesWidth;
			this.arrowLeft.show();
			this.arrowRight.show();
		}
	},
	updateTabWidth: function() {
		var tabs = this.block.select('.itoris_pagetabs_tab');
		this.tabWidth = this.block.offsetWidth - this.getBorderWidth(this.block);
		if (this.config.auto_height) {
			// 10 = left + right margins of content
			var contentWidth = this.tabWidth - 10, curTab = this.currentTab;
			tabs.each(function(elm, index) {
				elm.select('.itoris_pagetabs_tab_content')[0].setStyle({width: contentWidth + 'px'});
				elm.setStyle({zIndex: (index == curTab ? 2 : 1)});
			});
		}
	},
	updateScroll: function() {
		if (this.isResponsive) {
			this.arrowLeft.hide();
			this.arrowRight.hide();
		} else {
			var titlesBoxWrapper = this.block.select('.itoris_pagetabs_titles_wrapper')[0];
			var titlesBox = this.block.select('.itoris_pagetabs_titles')[0];
			var titles = this.block.select('.itoris_pagetabs_title');
			var wrapperMargins = parseNumber(titlesBoxWrapper.getStyle('marginLeft'))+ parseNumber(titlesBoxWrapper.getStyle('marginRight'));
			titlesBox.style.width = '';
			if (titlesBox.getHeight() > 50) {
				this.changeClass(titlesBoxWrapper, 'itoris_pagetabs_with_scroll');
				titlesBox.setStyle({
					width: this.titlesWidth + 'px'
				});
				this.arrowLeft.show();
				this.arrowRight.show();
				this.titlesMaxMargin = this.titlesWidth - titlesBoxWrapper.getWidth() + this.defaultTitlesPosition;
				this._moveTitles('right', true);
			} else {
				this.changeClass(titlesBoxWrapper, null, 'itoris_pagetabs_with_scroll');
				titlesBox.style.marginLeft = '';
				this.arrowLeft.hide();
				this.arrowRight.hide();
			}
		}
	},
	isIe7: function() {
		if (Prototype.Browser.IE) {
			var ieVersion = parseInt(navigator.userAgent.substring(navigator.userAgent.indexOf("MSIE")+5));
			return ieVersion == 7;
		}
		return false;
	},
	setTabsBlockHeight: function(height, withoutEffect) {
		var tabsBlock = this.block.select('.itoris_pagetabs_tabs')[0];
		if (withoutEffect) {
			tabsBlock.setStyle({height: height + 'px'});
			if (this.config.auto_height) {
				this.calculateHeightExecuter = new PeriodicalExecuter(this.setFirstTabHeight.bind(this), 0.1);
			}
		} else {
			new Effect.Morph(tabsBlock, {style: 'height: ' + height + 'px', duration: 0.3});
		}
	},
	setFirstTabHeight: function() {
		if (this.calculateHeightExecuter) {
			if (this.currentTab == 0) {
				if (!this.isResponsive) {
					this.setTabsBlockHeight(this.block.select('.itoris_pagetabs_tab')[0].getHeight());
				}
			} else {
				this.calculateHeightExecuter.stop();
			}
		}
	},
	getBorderWidth: function(elm, topAndBottom) {
		if (topAndBottom) {
			var width = parseNumber(elm.getStyle('borderTopWidth')) + parseNumber(elm.getStyle('borderBottomWidth'));
		} else {
			var width = parseNumber(elm.getStyle('borderLeftWidth')) + parseNumber(elm.getStyle('borderRightWidth'));
		}
		if (isNaN(width)) {
			return 0;
		}

		return width;
	},
	addEvents: function() {
		var titles = this.block.select('.itoris_pagetabs_title');
		for (var i = 0; i < titles.length; i++) {
			Event.observe(titles[i], this.config.switch_on, this.showTab.bind(this, i));
		}
		Event.observe(this.arrowLeft, 'mouseover', this.moveTitles.bind(this, 'left'));
		Event.observe(this.arrowLeft, 'mouseout', this.stopMoveTitles.bind(this));
		Event.observe(this.arrowRight, 'mouseover', this.moveTitles.bind(this, 'right'));
		Event.observe(this.arrowRight, 'mouseout', this.stopMoveTitles.bind(this));
		Event.observe(window, 'resize', this.updateResponsive.bind(this));
	},
	hideNotCurrentTabs : function() {
		var tabs = this.block.select('.itoris_pagetabs_tab');
		for (var i = 0; i < tabs.length; i++) {
			if (i != this.currentTab) {
				tabs[i].hide();
			}
		}
	},
	afterFadeEffect : function(num, effect) {
		if (num != this.currentTab) {
			effect.element.hide();
		}
		this.hideNotCurrentTabs();
		this.showCurrentTab();
	},
	showTab: function(num) {
		this.loader.hide();
		if (this.currentTab == num) {
			if (this.isResponsive) {
				this.switchTabsResponsive(this.block.select('.itoris_pagetabs_tab')[this.currentTab], null);
				this.block.select('.itoris_pagetabs_title_active')[0].removeClassName('itoris_pagetabs_title_active');
				this.currentTab = -1;
			}
			return;
		}
		var currentTab = this.block.select('.itoris_pagetabs_tab')[this.currentTab];
		var nextTab = this.block.select('.itoris_pagetabs_tab')[num];
		if (this.isResponsive) {
			if (this.config.use_ajax && this.loadedTabs.indexOf(num) == -1) {
				this.loader.show();
			}
			this.switchTabsResponsive(currentTab, nextTab);
		}
		if (this.config.use_ajax && this.loadedTabs.indexOf(num) == -1) {
			this.loadTabContent(num);
			return;
		}
		var tabs = this.block.select('.itoris_pagetabs_tab');
		var obj = this;
		if (!this.isResponsive) {
			switch (this.config.slide_effect) {
				case 'no_effect':
					currentTab.hide();
					nextTab.setStyle({left: 0});
					nextTab.show();
					break;
				case 'fade':
					nextTab.hide();
					nextTab.setStyle({left: 0});
					new Effect.Fade(currentTab, {duration: 0.5,
						afterFinishInternal: function(effect) {
							obj.afterFadeEffect(num, effect);
						}});
					new Effect.Appear(nextTab, {duration: 0.5,
						afterFinishInternal: function(effect) {
							obj.afterFadeEffect(num, effect);
						}});
					break;
				case 'horizontal':
					nextTab.setStyle({'display':'block'});
					if (num > this.currentTab) {
						nextTab.setStyle({left: this.tabWidth + 'px', zIndex: 3});
						var step = - this.tabWidth;
					} else {
						nextTab.setStyle({left: - this.tabWidth + 'px'});
						var step = this.tabWidth;
					}
					new Effect.Morph(currentTab, {
						style: 'left:' + step + 'px',
						duration: 0.5,
						afterFinishInternal: function(effect) {
							if (num != obj.currentTab) {
								effect.element.hide();
							}
							obj.showCurrentTab();
						}
					});
					new Effect.Morph(nextTab, {
						style: 'left:0px',
						duration: 0.5,
						afterFinishInternal: function(effect) {
							new Effect.Move(effect.element,{ x: -parseInt(effect.element.getWidth() * 0.1) * (step > 0 ? -1 : 1), y: 0, duration: 0.1, afterFinishInternal: function(effect) {
								new Effect.Move(effect.element,	{ x: parseInt(effect.element.getWidth() * 0.1) * (step > 0 ? -1 : 1), y: 0, duration: 0.2,
									afterFinishInternal: function(effect) {
										if (num != obj.currentTab) {
											effect.element.hide();
										}
										obj.showCurrentTab();
									}});
							}});
						}
					});
					break;
				case 'vertical':
				default:
					nextTab.setStyle({'display':'block', left: 0});
					var nextTabHeight = nextTab.getHeight();
					var currentTabHeight = currentTab.getHeight();
					var stepForCurrentTab = nextTabHeight > currentTabHeight ? nextTabHeight : currentTabHeight;
					if (num > this.currentTab) {
						nextTab.setStyle({top: nextTabHeight + 'px', zIndex: 3});
						var step = - nextTabHeight;
						stepForCurrentTab = - stepForCurrentTab;
					} else {
						nextTab.setStyle({top: - nextTabHeight + 'px'});
						var step = nextTabHeight;
					}

					new Effect.Morph(currentTab, {
						style: 'top:' + stepForCurrentTab + 'px',
						duration: 0.5,
						afterFinishInternal: function(effect) {
							if (num != obj.currentTab) {
								effect.element.hide();
							}
							obj.showCurrentTab();
						}
					});
					new Effect.Morph(nextTab, {
						style: 'top:0px',
						duration: 0.5,
						afterFinishInternal: function(effect) {
							if (num != obj.currentTab) {
								effect.element.hide();
							}
							obj.showCurrentTab();
						}
					});
			}
		}
		var titles = this.block.select('.itoris_pagetabs_title');
		this.changeClass(titles[num], 'itoris_pagetabs_title_active');
		this.changeClass(titles[this.currentTab], null, 'itoris_pagetabs_title_active');
		this.currentTab = num;
		if (!this.isResponsive) {
			this.setTabsBlockHeight(nextTab.getHeight());
		}
	},
	switchTabsResponsive: function(tabA, tabB) {
		if (tabA && tabA.visible()) {
			new Effect.Morph(tabA.up('.itoris_pagetabs_title_tab_content'), {
				style : {height: 0 + 'px'},
				duration : 0.3,
				afterFinish : function(ev) {
					ev.element.style.height = '';
					ev.element.hide();
				}
			});
		}
		if (tabB) {
			var showBlock = tabB.up('.itoris_pagetabs_title_tab_content');
			var currentHeight = tabB.getHeight();
			if (!showBlock.visible()) {
				showBlock.show();
				showBlock.insert({top: this.loader});
				tabB.show();
			}
			var targetHeight = showBlock.getHeight();
			showBlock.style.height = currentHeight;
			new Effect.Morph(showBlock, {
				style : {height: targetHeight + 'px'},
				duration : 0.3,
				afterFinish : function(ev) {
					ev.element.style.height = '';
					if (ev.element.viewportOffset().top < 0) {
						Effect.ScrollTo(ev.element);
					}
				}
			});
		}
		this.block.select('.itoris_productslider_slider').each(function(el){
			var v = "itorisProductslider"+el.id;
			if (window[v]) window[v].resizeMe();
		});
	},
	showCurrentTab: function() {
		var tab = this.block.select('.itoris_pagetabs_tab')[this.currentTab];
		tab.show();
		tab.setStyle({left:'0px', top: '0px'});
		this.block.select('.itoris_productslider_slider').each(function(el){
			var v = "itorisProductslider"+el.id;
			if (window[v]) window[v].resizeMe();
		});
		this.updateTabWidth();
		return this;
	},
	loadTabContent : function(num) {
		this.loader.show();
		this.nextTabNum = num;
		if (this.loadingTabs.indexOf(num) != -1) {
			return;
		}
		this.loadingTabs.push(num);
		var obj = this;
		new Ajax.Request(this.config.load_tab_content_url, {
			parameters: {id: obj.tabIds[num]},
			onComplete: function(response) {
				var responseObj = response.responseText.evalJSON();
				if (responseObj.error) {
					alert(responseObj.error);
				} else {
					obj.loadedTabs.push(num);
					obj.block.select('.itoris_pagetabs_tab_content')[num].innerHTML = responseObj.tab_content.stripScripts();
					var scripts = responseObj.tab_content.match(new RegExp(Prototype.ScriptFragment, 'img'));
					if (scripts) {
						for (var i = 0; i < scripts.length; i++) {
							eval(scripts[i].match(new RegExp(Prototype.ScriptFragment, 'im'))[1])
						}
					}
					if (Itoris.ProductSlider) {
						var tempSlider = new Itoris.ProductSlider();
						tempSlider.isInitialized = true;
						tempSlider.allSliders.each(function(slider) {slider.initializeSlider();});
					}
					obj.replaceUencInLinks(obj.block.select('.itoris_pagetabs_tab_content')[num].select('a'));
					if (obj.loader.visible() && obj.nextTabNum == num) {
						obj.showTab(obj.nextTabNum);
					}
				}
				obj.loadingTabs = obj.loadingTabs.without(num);
				if (!obj.loadingTabs.length) {
					obj.loader.hide();
				}
			}
		});
	},
	replaceUencInLinks : function(links) {
		try {
			if (links.length) {
				for (var i = 0; i < links.length; i++) {
					if (links[i].href.indexOf('uenc') != -1) {
						links[i].href = links[i].href.replace(/uenc\/[^\/]*/, 'uenc/' + this.config.uenc);
					}
				}
			}
		} catch (e) { /* tab should show any way */ }
	},
	moveTitles : function(direction) {
		if (!this.titleSlider) {
			this.titleSlider = new PeriodicalExecuter(this._moveTitles.bind(this, direction, false), 0.01);
		}
	},
	_moveTitles : function(direction, simulation) {
		var titlesBox = this.block.select('.itoris_pagetabs_titles')[0];
		var marginLeft = parseNumber(titlesBox.getStyle('marginLeft'));
		switch (direction) {
			case 'right':
				if (marginLeft == -this.titlesMaxMargin) {
					this.changeClass(this.arrowLeft, null, 'itoris_pagetabs_arrow_left_disabled');
					this.changeClass(this.arrowRight, 'itoris_pagetabs_arrow_right_disabled');
					this.stopMoveTitles();
				} else {
					this.changeClass(this.arrowRight, null, 'itoris_pagetabs_arrow_right_disabled');
					if (marginLeft == this.defaultTitlesPosition) {
						this.changeClass(this.arrowLeft, 'itoris_pagetabs_arrow_left_disabled');
					} else {
						this.changeClass(this.arrowLeft, null, 'itoris_pagetabs_arrow_left_disabled');
					}
					if (simulation) {
						return;
					}
					var nextMargin = marginLeft - 5;
					if (nextMargin < -this.titlesMaxMargin) {
						nextMargin = -this.titlesMaxMargin;
					}
					titlesBox.setStyle({marginLeft: nextMargin + 'px'})
				}
				break;
			case 'left':
				if (marginLeft == this.defaultTitlesPosition) {
					this.changeClass(this.arrowLeft, 'itoris_pagetabs_arrow_left_disabled');
					this.changeClass(this.arrowRight, null, 'itoris_pagetabs_arrow_right_disabled');
					this.stopMoveTitles();
				} else {
					this.changeClass(this.arrowRight, null, 'itoris_pagetabs_arrow_right_disabled');
					if (simulation) {
						return;
					}
					var nextMargin = marginLeft + 5;
					if (nextMargin > this.defaultTitlesPosition) {
						nextMargin = this.defaultTitlesPosition;
					}
					titlesBox.setStyle({marginLeft: nextMargin + 'px'})
				}
				break;
		}
	},
	stopMoveTitles : function() {
		if (this.titleSlider) {
			this.titleSlider.stop();
			this.titleSlider = null;
		}
	},
	changeClass : function(elm, addClass, removeClass) {
		if (elm) {
			if (addClass) {
				if (!elm.hasClassName(addClass)) {
					elm.addClassName(addClass);
				}
			}
			if (removeClass) {
				if (elm.hasClassName(removeClass)) {
					elm.removeClassName(removeClass);
				}
			}
		}
	},
	updateResponsive: function() {
		this.updateTabWidth();
		if (this.isMobile || !this.config.responsive_width || document.viewport.getWidth() < this.config.responsive_width) {
			this.changeClass(this.block, 'ipagetabs-responsive');
			this.isResponsive = true;
		} else {
			this.changeClass(this.block, null, 'ipagetabs-responsive');
			this.isResponsive = false;
		}
		this.updateTabContentPosition();
		this.updateScroll();
	},
	updateTabContentPosition: function() {
		var contents = this.block.select('.itoris_pagetabs_tab');
		var titleTabsContents = this.block.select('.itoris_pagetabs_title_tab_content');
		var tabsBox = this.block.select('.itoris_pagetabs_tabs_box')[titleTabsContents.length || 0];

		if (this.isResponsive) {
			var titles = this.block.select('.itoris_pagetabs_title');
			if (!titleTabsContents.length) {
				for (var i = 0; i < titles.length; i++) {
					var contentsBlock = document.createElement('li');
					contentsBlock.className = 'itoris_pagetabs_title_tab_content itoris_pagetabs_tabs_box';
					var contentsBlock2 = document.createElement('div');
					contentsBlock2.className = 'itoris_pagetabs_tabs';
					titles[i].insert({after: contentsBlock});
					contentsBlock.appendChild(contentsBlock2);
					contentsBlock2.appendChild(contents[i]);
					if (titles[i].hasClassName('itoris_pagetabs_title_active')) {
						contentsBlock.show();
					} else {
						contentsBlock.hide();
					}
				}
				tabsBox.hide();
			}
		} else {
			if (titleTabsContents.length) {
				tabsBox.appendChild(this.loader);
				tabsBox.show();
				var tabsContentsBox = tabsBox.select('.itoris_pagetabs_tabs')[0];
				for (var i = 0; i < contents.length; i++) {
					tabsContentsBox.appendChild(contents[i]);
					contents[i].hide();
				}
				titleTabsContents.each(function(elm){elm.remove();});
				if (!this.block.select('.itoris_pagetabs_title_active').length) {
					this.currentTab = 0;
					this.block.select('.itoris_pagetabs_title')[0].addClassName('itoris_pagetabs_title_active');
				}
				contents[this.currentTab].style.left = 0;
				contents[this.currentTab].style.top = 0;
				contents[this.currentTab].show();
				if (this.config.auto_height) {
					this.block.select('.itoris_pagetabs_tabs')[0].setStyle({height: contents[this.currentTab].getHeight() + 'px'});
				}
			}
		}
	}
});