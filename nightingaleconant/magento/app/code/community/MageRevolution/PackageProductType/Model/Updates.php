<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Model_Updates {

    public function getVersions($moduleCode){
        return $this->getData($moduleCode);
    }

    private function getHeaders() {

        $check = base64_decode('aHR0cDovL21hZ2VyZXZvbHV0aW9uLmNvbS9NUi1WRVJTSU9OUy92ZXJzaW9ucy5waHA=');
        $ch = curl_init($check);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_TIMEOUT,10);
        $output = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if($httpcode == 200) {
            return TRUE;
        }

        else {
            return FALSE;
        }

    }

    private function getData($moduleCode) {

        $headers = $this->getHeaders();

        if($headers == 200) {

            $fields = array(
                'module' => $moduleCode
            );

            $ch1 = base64_decode('aHR0cDovL21hZ2VyZXZvbHV0aW9uLmNvbS9NUi1WRVJTSU9OUy92ZXJzaW9ucy5waHA=');
            $ch = curl_init($ch1);

            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($ch);
            curl_close($ch);

            return $response;

        }

        else { return FALSE; }

    }
}