<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Block_Adminhtml_Sales_Items_Column_Qty extends Mage_Adminhtml_Block_Sales_Items_Column_Qty {

    public function getPackageShipped($_item) {
        $product = Mage::getModel('sales/order_item')->load($_item->getItemId());

        if($product && $product->getProductType()){
            if($product->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE){
                return true;
            } elseif($product->getParentItemId()){
                $product = Mage::getModel('sales/order_item')->load($product->getParentItemId());
                if($product->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE){
                    return false;
                }
            }
        }
        return true;
    }
}

