<?php

class WP_AdvancedSliderAddonExportImport_Block_Adminhtml_Widget_Grid_Column_Renderer_Export
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $formatLink = '';
        $id = trim($row->getData($this->getColumn()->getIndex()));
        $link = Mage::helper('adminhtml')->getUrl('advancedslideraddonexportimport/export', array('id' => $id));
        if ($link)
            $formatLink = '<a title="' . $this->__('Export') . '" href="' . $link . '">' . $this->__('Export') . '</a>';
        return $formatLink;
    }
}
