<?php

class WP_AdvancedSliderAddonExportImport_Block_Adminhtml_Import
    extends Mage_Adminhtml_Block_Widget_Container
{
    public function __construct()
    {
        $this->_headerText = $this->__('Import Sliders');
        $this->_controller = 'adminhtml_sliders_import';

        $this->_addButton('back', array(
            'label'     => $this->__('Back to Manage Sliders'),
            'onclick'   => 'setLocation(\'' . Mage::helper('adminhtml')->getUrl('advancedslider/sliders') . '\')',
            'class'     => 'back',
        ));

        $action = $this->getRequest()->getActionName();
        switch ($action) {

            case 'index' :
                $this->_addButton('import', array(
                    'label'     => $this->__('Import'),
                    'onclick'   => 'uploadForm.submit();',
                    'class'     => 'add',
                ));
                break;

            case 'process' :
                $this->_addButton('back_upload', array(
                    'label'     => $this->__('Back to Upload'),
                    'onclick'   => 'setLocation(\'' . Mage::helper('adminhtml')->getUrl('advancedslideraddonexportimport/import/index') . '\')',
                    'class'     => 'back',
                ));
                break;
        }

        parent::__construct();
    }

    public function getHeaderHtml()
    {
        return '<h3 class="icon-head ' . $this->getHeaderCssClass() . '">' . $this->getHeaderText() . '</h3>';
    }

    public function getPostMaxSize()
    {
        return ini_get('post_max_size');
    }

    public function getUploadMaxSize()
    {
        return ini_get('upload_max_filesize');
    }

    public function getDataMaxSize()
    {
        return min($this->getPostMaxSize(), $this->getUploadMaxSize());
    }

    public function getFormKey()
    {
        return Mage::getSingleton('core/session')->getFormKey();
    }

    public function getFormAction()
    {
        return Mage::helper('adminhtml')->getUrl('advancedslideraddonexportimport/import/process');
    }

    public function getListOfSliders()
    {
        return Mage::getSingleton('core/session')->getListOfSliders();
    }
}
