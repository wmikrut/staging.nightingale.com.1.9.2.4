//TODO refactor
var AWIsliderImagesAjaxForm = Class.create({
    initialize: function(name) {
        this.imageFormSubmitId = 'awis_imagesavebutton';
        this.imageFormErrorsId = 'awis_image_error';
        this.imageFormId = 'awis_imageform';
        this.imageFormContainerId = 'awis_imageformcontainer';
        this.generalFieldsetContainterId = 'general_settings';

        this.videoContentType = 2;
        this.contentType = {
            selector: 'content_type',
            url: '',
            pid: null,
            id: null
        };

        this.pid = null;

        this.selectors = {
            dateFromButton: 'active_from_trig',
            dateToButton: 'active_to_trig'
        };

        this.varienForm = null;

        this.window = null;
        this.global = window;
        this.selfObjectName = name;
        if(typeof name != 'undefined')
            this.global[name] = this;

        this.messages = {
            newTitle: 'Add Image or Video',
            editTitle: 'Edit Image or Video'
        };
        var me = this
        Event.observe(window, 'load', me.prepareSelf.bind(me));
    },

    prepareSelf: function() {
        if(awISSettings) {
            if(awISSettings.getOption('imagesAjaxFormUrl')) {
                this.updateAjaxUrl(awISSettings.getOption('imagesAjaxFormUrl'));
            } else {
                awISSettings.getOption('imagesAjaxFormUrl', this.updateAjaxUrl);
            }

            if(awISSettings.getOption('videoAjaxFieldsetUrl')) {
                this.videoAjaxFieldsetUrl = awISSettings.getOption('videoAjaxFieldsetUrl');
            }

            if(awISSettings.getOption('imageAjaxFieldsetUrl')) {
                this.imageAjaxFieldsetUrl = awISSettings.getOption('imageAjaxFieldsetUrl');
            }
        }
        this.translateMessages();
    },

    prepareGeneralFieldset: function(){
        this.contentType.url = this.imageAjaxFieldsetUrl;
        if ($(this.contentType.selector).value == this.videoContentType) {
            this.contentType.url = this.videoAjaxFieldsetUrl;
        }
        this.prepareContentTypeUrl();
        var me = this;
        new Ajax.Request(this.contentType.url, {
            method: 'post',
            parameters: {},
            onSuccess: function(transport) {
                var resp = transport.responseText.evalJSON(true);
                if(typeof resp == 'object') {
                    $(me.generalFieldsetContainterId).innerHTML = resp.text;
                    $(me.generalFieldsetContainterId).innerHTML.evalScripts();
                }
                me._resizeWindow();
                me.prepareForm();
            }
        });
    },

    prepareContentTypeUrl: function() {
        this.contentType.url += 'pid/' + this.contentType.pid;
        this.contentType.url += '/id/' + this.contentType.id;
    },

    prepareCalendar: function() {
        var pos = $(this.selectors.dateFromButton).cumulativeOffset();
        Calendar._TT["TT_DATE_FORMAT"] = Calendar._TT.DEF_DATE_FORMAT;
        Calendar.setup({
            inputField: "active_from",
            ifFormat: Calendar._TT.DEF_DATE_FORMAT,
            showsTime: false,
            button: "active_from_trig",
            align: "Bl",
            singleClick : true,
            position: pos
        });
        pos = $(this.selectors.dateToButton).cumulativeOffset();
        Calendar.setup({
            inputField: "active_to",
            ifFormat: Calendar._TT.DEF_DATE_FORMAT,
            showsTime: false,
            button: "active_to_trig",
            align: "Bl",
            singleClick : true,
            position: pos
        });
    },
    prepareForm: function() {
        this.varienForm = new varienForm(this.imageFormId);
        this._pe = new PeriodicalExecuter(this._resizeWindow.bind(this), 0.1);
        /*observe iframe onload and form onsubmit events*/
        $(this.imageFormId).observe('submit', this.global[this.selfObjectName].formBeforePost.bind(this));
        setTimeout(this.prepareCalendar.bind(this), 1000);
    },
    _resizeWindow: function() {
        if(this.imageFormContainerId && $(this.imageFormContainerId) && $(this.imageFormContainerId).getWidth() && $(this.imageFormContainerId).getHeight()) {
            if(this._pe) {
                this._pe.stop();
                this._pe = null;
            }
            if(this.window)
                this.window.setSize(Math.max(550, $(this.imageFormContainerId).getWidth()), $(this.imageFormContainerId).getHeight() + 30);
        }
    },
    updateAjaxUrl: function(ajaxUrl) {
        this.ajaxUrl = typeof ajaxUrl != 'undefined' ? ajaxUrl : '';
        this.ajaxUrl =  this.ajaxUrl.replace(/^http[s]{0,1}/, window.location.href.replace(/:[^:].*$/i, ''));
    },
    translateMessages: function() {
        if(typeof Translator != 'undefined' && Translator) {
            for(var line in this.messages)
                this.messages[line] = Translator.translate(this.messages[line]);
        }
    },
    showForm: function(pid, id) {
        this.window = new Window({
            className: 'magento',
            width: 550,
            height: 500,
            resizable: false,
            minimizable: false,
            maximizable: false,
            destroyOnClose: true,
            recenterAuto:false,
            zIndex: 101
        });
        /* showing form for new entry */
        this.window.setTitle(typeof id == 'undefined' || id === null ? this.messages.newTitle : this.messages.editTitle);
        var me = this;
        this.contentType.pid = pid;
        this.contentType.id = id;
        this.window.setAjaxContent(this.ajaxUrl, {
            parameters: {id: id, pid: pid},
            onComplete: function(){
                me.prepareGeneralFieldset();
            }
        }, true, true);
    },
    formAfterPost: function(resp) {
        $(this.imageFormSubmitId).removeClassName('disabled').writeAttribute('disabled', null);
        if(resp.s) {
            this.window.close();
            if(awislider_imagesJsObject)
                awislider_imagesJsObject.reload();
        } else {
            a = resp;
            $(this.imageFormErrorsId).innerHTML = resp.errors;
            this._resizeWindow();
        }
    },

    formBeforePost: function() {
        this._pe = new PeriodicalExecuter(this._resizeWindow.bind(this), 1);
        if(this.varienForm && this.varienForm.validate() == false) {
            return false;
        }
        $(this.imageFormSubmitId).addClassName('disabled').writeAttribute('disabled', 'disabled');
        return true;
    }
});
new AWIsliderImagesAjaxForm('awISAjaxForm');
