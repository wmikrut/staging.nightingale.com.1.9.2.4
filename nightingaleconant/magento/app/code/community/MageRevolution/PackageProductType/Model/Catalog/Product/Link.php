<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Model_Catalog_Product_Link extends Mage_Catalog_Model_Product_Link
{
    const LINK_TYPE_PACKAGE   = 6;

    /**
     * @return Mage_Catalog_Model_Product_Link
     */
    public function usePackageLinks()
    {
        $this->setLinkTypeId(self::LINK_TYPE_PACKAGE);
        return $this;
    }

    /**
     * Save data for product relations
     *
     * @param   Mage_Catalog_Model_Product $product
     * @return  Mage_Catalog_Model_Product_Link
     */
    public function saveProductRelations($product)
    {
        parent::saveProductRelations($product);

        $data = $product->getPackageLinkData();
        if (!is_null($data)) {
            $this->_getResource()->saveProductLinks($product, $data, self::LINK_TYPE_PACKAGE);
        }

    }
}