<?php
class LinkstureDCCL_ApplyCoupon_Block_Adminhtml_Applycoupon_Renderer_Website extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
	public function render(Varien_Object $row)
	{
		$_website =array();
		foreach (Mage::app()->getWebsites() as $website) {
			$_website[] = $website->getWebsiteId();
		}
		$value =  $row->getData($this->getColumn()->getIndex());
		if (in_array($value, $_website)) {
			$website = Mage::app()->getWebsite($value);
			return $website->getName();
		}
		return;
	}
}
?>