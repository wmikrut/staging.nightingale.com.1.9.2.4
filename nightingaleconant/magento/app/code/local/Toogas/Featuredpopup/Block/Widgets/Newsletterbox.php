<?php
/**
 * Toogas Lda.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA (End-User License Agreement)
 * that is bundled with this package in the file licence_toogas_community.txt.
 * It is also available at this URL:
 * http://www.toogas.com/licences/toogas_community.txt
 *
 * @category   Toogas
 * @package    Toogas_Featuredpopup
 * @copyright  Copyright (c) 2013 Toogas Lda. (http://www.toogas.com)
 * @license    http://www.toogas.com/licences/toogas_community.txt
 */
 
class Toogas_Featuredpopup_Block_Widgets_Newsletterbox
    extends Mage_Newsletter_Block_Subscribe
    implements Mage_Widget_Block_Interface
{
	
	const DEFAULT_ENTER_EMAIL_TEXT = "Please enter your email.";
	
	public function obtainText() {
		return $this->hasData('text') ? 
		$this->_getData('text') : 
		self::DEFAULT_ENTER_EMAIL_TEXT;
	}	

	public function obtainHeaderTitle() {
		return $this->hasData('header_title') ? 
		$this->_getData('header_title') : 
		'';
	} 
	
	public function obtainHeaderText() {
		return $this->hasData('header_text') ? 
		$this->_getData('header_text') : 
		'';
	}
	
	public function obtainContentTitle() {
		return $this->hasData('content_title') ? 
		$this->_getData('content_title') : 
		'';
	}
	
	public function obtainContentText() {
		return $this->hasData('content_text') ? 
		$this->_getData('content_text') : 
		'';
	}
	
	public function obtainInfoOne() {
		return $this->hasData('info_one') ? 
		$this->_getData('info_one') : 
		'';
	}				  
    
	public function obtainInfoTwo() {
		return $this->hasData('info_two') ? 
		$this->_getData('info_two') : 
		'';
	}
	
	public function obtainInfoThree() {
		return $this->hasData('info_three') ? 
		$this->_getData('info_three') : 
		'';
	}
	
	public function obtainInputText() {
		return $this->hasData('input_text') ? 
		$this->_getData('input_text') : 
		'';
	}	

	public function obtainButton() {
		return $this->hasData('button') ? 
		$this->_getData('button') : 
		'';
	}	
	//      
}
