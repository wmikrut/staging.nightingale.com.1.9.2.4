<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Model_Resource_Test_Collection_Grid extends Amasty_Abtesting_Model_Resource_Test_Collection
{
    public function getSelectCountSql($getClean = false)
    {
        if ($getClean) {
            return parent::getSelectCountSql();
        }
        else {
            $cleanCollection = Mage::getResourceModel(
                $this->getResourceModelName().'_collection'
            );

            return $cleanCollection->getSelectCountSql(true);
        }
    }

    protected function _beforeLoad()
    {
        $this->getSelect()
            ->joinLeft(
                array('tv' => $this->getResource()->getTable('amabtesting/test_variation')),
                'tv.test_id = main_table.id',
                array(
                    'total_visits' => 'SUM(tv.visits)',
                    'total_conversions' => 'SUM(tv.conversions)'
                )
            )
            ->group('main_table.id')
        ;

        return parent::_beforeLoad();
    }
}
