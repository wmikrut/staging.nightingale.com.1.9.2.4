<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGETABS
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

$this->run("

create table {$this->getTable('itoris_pagetabs_tab_instance')} (
	`instance_id` int unsigned not null auto_increment primary key,
	`title` text not null,
	`is_main` bool
) engine = InnoDB default charset = utf8;

insert into {$this->getTable('itoris_pagetabs_tab_instance')} (`title`, `is_main`) values ('Main', 1);

alter table {$this->getTable('itoris_pagetabs_tab')} add column `instance` int unsigned not null;

update {$this->getTable('itoris_pagetabs_tab')} set instance = (select instance_id from {$this->getTable('itoris_pagetabs_tab_instance')} where is_main=1);

alter table {$this->getTable('itoris_pagetabs_tab')} add foreign key (`instance`) references {$this->getTable('itoris_pagetabs_tab_instance')} (`instance_id`) on delete cascade on update cascade;

");

?>
