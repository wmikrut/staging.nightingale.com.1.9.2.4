<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Model_Visitor
{
    const TESTS_COOKIE_NAME = 'amabtest';
    const VISITOR_COOKIE_NAME = 'amabtest_visitor';
    const COOKIE_LIFETIME = 31536000;
    const VARIATION_GET_PARAM = 'amabtest_variation';

    public function getCurrentVariation($productId, $variationIndex = false)
    {
        $test = Mage::getSingleton('amabtesting/test')->findByProductId($productId);

        if (!$test || !$test->getId())
            return false;

        if ($variationIndex) {
            $variation = Mage::getResourceModel('amabtesting/test_variation_collection')
                ->setPageSize(1)
                ->setCurPage($variationIndex)
                ->getFirstItem()
            ;

            return $variation;
        }

        $this->getVisitorId(true);

        if ($currentTests = $this->getInvolvedTests()) {
            if (isset($currentTests[$test->getId()])) {
                $variationId = $currentTests[$test->getId()];

                $variation = Mage::getModel('amabtesting/test_variation')->load($variationId);

                if ($variation->getId())
                    return $variation;
            }
        }

        $variation = $test->fetchVariation();

        $currentTests[$test->getId()] = $variation->getId();

        $this->_setCookie(
            self::TESTS_COOKIE_NAME,
            json_encode($currentTests)
        );

        return $variation;
    }

    public function getInvolvedTests()
    {
        if (isset($_COOKIE[self::TESTS_COOKIE_NAME])) {
            $currentTests = json_decode(
                $_COOKIE[self::TESTS_COOKIE_NAME], true
            );

            if (!is_array($currentTests)) {
                $currentTests = array();
            }

            return $currentTests;
        }
        else
            return array();
    }

    public function getVisitorId($assign = false)
    {
        if (isset($_COOKIE[self::VISITOR_COOKIE_NAME])) {
            $visitorId = $_COOKIE[self::VISITOR_COOKIE_NAME];
        }
        else if ($assign) {
            $visitorId = rand();

            $this->_setCookie(
                self::VISITOR_COOKIE_NAME,
                $visitorId
            );
        }
        else
            return false;

        return $visitorId;
    }

    protected function _setCookie($name, $value)
    {
        setcookie(
            $name,
            $value,
            time() + self::COOKIE_LIFETIME,
            '/', '', false, true
        );

        $_COOKIE[$name] = $value;
    }

    public function isIgnored()
    {
        if (!isset($_SERVER['REMOTE_ADDR']) || !isset($_SERVER['HTTP_USER_AGENT']))
            return true;

        if (false !== strpos($_SERVER['HTTP_USER_AGENT'], 'crawler'))
            return true;

        $config = Mage::getResourceSingleton('amabtesting/config');

        if (!$config->getDbConfig('amabtesting/ignore/enable'))
            return false;

        $ips = $config->getDbConfig('amabtesting/ignore/ip');
        $ips = preg_split('/[,\s]+/', $ips, -1, PREG_SPLIT_NO_EMPTY);

        if (!empty($ips) && in_array($_SERVER['REMOTE_ADDR'], $ips))
            return true;

        $agents = $config->getDbConfig('amabtesting/ignore/agents');
        if (!$agents)
            return false;

        return @preg_match('@' . $agents . '@', $_SERVER['HTTP_USER_AGENT']);
    }
}
