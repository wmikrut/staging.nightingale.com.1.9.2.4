<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
$installer = $this;
$installer->startSetup();

$tables = array(
    $installer->getTable('catalog/eav_attribute') => array(
        'columns' => array(
            'is_package' => array(
                'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
                'unsigned'  => true,
                'nullable'  => false,
                'default'   => '1',
                'comment'   => 'Is Package'
            ),
        )
    )
);

$installer->getConnection()->modifyTables($tables);

/**
 * Install product link types
 */
$data = array(
    array(
        'link_type_id'  => MageRevolution_PackageProductType_Model_Catalog_Product_Link::LINK_TYPE_PACKAGE,
        'code'          => 'package'
    )
);

foreach ($data as $bind) {
    $installer->getConnection()->insertForce($installer->getTable('catalog/product_link_type'), $bind);
}

/**
 * install product link attributes
 */
$data = array(
    array(
        'link_type_id'                  => MageRevolution_PackageProductType_Model_Catalog_Product_Link::LINK_TYPE_PACKAGE,
        'product_link_attribute_code'   => 'position',
        'data_type'                     => 'int'
    ),
    array(
        'link_type_id'                  => MageRevolution_PackageProductType_Model_Catalog_Product_Link::LINK_TYPE_PACKAGE,
        'product_link_attribute_code'   => 'package_qty',
        'data_type'                     => 'int'
    )
);

$installer->getConnection()->insertMultiple($installer->getTable('catalog/product_link_attribute'), $data);

$catalogInstaller = new Mage_Catalog_Model_Resource_Setup($installer->_resourceName);

// Apply all simple attributes to products
$additionalTable = $catalogInstaller->getEntityType('catalog_product', 'additional_attribute_table');

$installer->run("
UPDATE `{$installer->getTable($additionalTable)}`
    SET apply_to = CONCAT_WS(',', apply_to, 'package')
    WHERE apply_to LIKE '%configurable%' AND apply_to NOT LIKE '%package%';
");


$installer->endSetup();