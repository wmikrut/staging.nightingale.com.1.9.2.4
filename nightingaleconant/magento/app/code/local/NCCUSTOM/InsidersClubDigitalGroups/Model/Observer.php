<?php

class NCCUSTOM_InsidersClubDigitalGroups_Model_Observer 
{    
    const INSIDERS_CLUB_DIGITAL = 6;
    const INSIDERS_CLUB_DIGITAL_PRICING = 7;

    public function processCart(Varien_Event_Observer $observer) 
    {
		$session = Mage::getSingleton('checkout/session');
		$_quote = Mage::getSingleton('checkout/session')->getQuote();
		
		if ( $_quote->getCustomerGroupId() == NCCUSTOM_InsidersClubDigitalGroups_Model_Observer::INSIDERS_CLUB_DIGITAL_PRICING ) 
		{
		    $idx = 0;
		    $cartItems = $_quote->getAllVisibleItems();
		    foreach ($cartItems as $item) 
		    {
				$idx++;
				if ( $idx == 1 ) 
				{		    
				    $sav_item = $item;
				}
		    }
		    if ( $idx > 0 ) {
				$_customer = Mage::getModel('customer/customer')->load($_quote->getCustomerId());
				$_customer->setGroupId(NCCUSTOM_InsidersClubDigitalGroups_Model_Observer::INSIDERS_CLUB_DIGITAL);
		        $_customer->save();
		        
				$original_price = $sav_item->getBasePrice();
				$sav_item->setCustomPrice($original_price);
				$sav_item->setOriginalCustomPrice($original_price);
				$_quote->save();
		    }
		}
		
		if ( $_quote->getCustomerGroupId() == NCCUSTOM_InsidersClubDigitalGroups_Model_Observer::INSIDERS_CLUB_DIGITAL ) 
		{
		    $idx = 0;
		    $cartItems = $_quote->getAllVisibleItems();
		    foreach ($cartItems as $item) 
		    {
				$idx++;
		    }
		    if ( $idx == 0 ) {
		    	$customer_id = $_quote->getCustomerId();
		    	
		    	$date = new DateTime('now');
		    	$date->modify('first day of this month');
		    	$fromDate = (string) $date->format('Y-m-d'). " 23:59:59";
		    	
		    	$date = new DateTime('now');
		    	$date->modify('last day of this month');
		    	$toDate = (string) $date->format('Y-m-d'). " 23:59:59";
		    	
		    	$_customer_orders = Mage::getModel('sales/order')
		    		->getCollection()
		    		->addFieldToFilter('customer_id', array('eq' => $customer_id))
		    		->addAttributeToFilter('created_at', array('from'=>$fromDate, 'to'=>$toDate))
		    		->addAttributeToFilter( 'status', array( 'neq' => "canceled" ));
		    	if ($_customer_orders->getSize() > 0) 
		    	{
		    		foreach ( $_customer_orders as $_customer_order ) 
		    		{		 
		    		    $_ordered_items = $_customer_order->getAllItems();
		    		    foreach ( $_ordered_items as $item ) 
		    		    {
		    			$base_price = $item->getBasePrice();
		    			$sku = $item->getSku();
					if ( $base_price > 0 ) 
					{
					    if (( $sku != '7002ICDR' ) and ( $sku != '7002ICDR'))
					    {
						$idx++;
					    }
					}
		    		    }
		    		}
		    	}
		    	
		    	if ( $idx == 0 ) 
		    	{
				$_customer = Mage::getModel('customer/customer')->load($_quote->getCustomerId());
				$_customer->setGroupId(NCCUSTOM_InsidersClubDigitalGroups_Model_Observer::INSIDERS_CLUB_DIGITAL_PRICING);
		        	$_customer->save();
		        
		    	}
		    }
		}
		
		
    }
}
?>