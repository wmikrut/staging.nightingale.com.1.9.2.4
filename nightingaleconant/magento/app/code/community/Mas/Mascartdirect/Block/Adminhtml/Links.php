<?php 
class Mas_Mascartdirect_Block_Adminhtml_Links extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract 
{
	public function render(Varien_Object $row)
	{
		$productId = $row->getId();
		$wrap = '';
		
		$type = $row->getTypeId();
		
		if (!Mage::registry('mascartdirect-popup')) {
			$wrap = $this->getPopupHtml($type);
		}
		
		$help = $this->__('Use Link Below');
		if (!$this->isPlainProduct($type)) {
			$help = $this->__('Click on link below to configure product and get direct link to cart or checkout');
		}
		
		if ($this->isPlainProduct($type)) {
			$url = $this->getUrl('mascartdirect/index/add', array('product' => $productId));
		} else {
			$url = $this->getUrl('catalog/product/view', array('id' => $productId, 'mascartdirect' => 1));
		}
		
		
		$wrap .= sprintf('<a href="%s" onclick="return mascartdirectPopupOpen(%s, %s);" target="_blank" title="%s">%s</a><br />', $url, "'" . $url . "'", "'" . $help . "'", Mage::helper('mascartdirect')->__('Right Mouse Click on link and choose Copy Link Location to save link in clipboard'), Mage::helper('mascartdirect')->__('Add To Cart'));
			
		if ($this->isPlainProduct($type)) {
			$url = $this->getUrl('mascartdirect/index/add', array('product' => $productId, 'bypass' => true));
		} else {
			$url = $this->getUrl('catalog/product/view', array('id' => $productId, 'mascartdirect' => 1));
		}
		
		$wrap .= sprintf('<a href="%s" onclick="return mascartdirectPopupOpen(%s, %s);" target="_blank" title="%s">%s</a>', $url, "'" . $url . "'", "'" . $help . "'", Mage::helper('mascartdirect')->__('Right Mouse Click on link and choose Copy Link Location to save link in clipboard'), Mage::helper('mascartdirect')->__('To Checkout'));	
		
		return $wrap;
	}
	
	public function isPlainProduct($type) {
		return !in_array($type, array('grouped', 'configurable', 'bundle'));
	}
	
	public function getPopupHtml($type)
	{
		$close = $this->__('Close');
		
		$eod = <<<EOD
		<style>
		#mascartdirect-popup {
			width: 600px;
			position: fixed;
			top: -1100px;
			left: 50%;
			margin-left: -300px;
			padding: 20px;
			background: #efefef;
			border: 2px solid #EA7601;
			cursor: default;
		}
		#mascartdirect-link {
			padding: 10px 0px;
			display: block;
		}
		</style>
		<script>
			function mascartdirectPopupOpen(url, hlp) {
				$('mascartdirect-link').innerHTML = url;
				$('mascartdirect-help').innerHTML = hlp;
				$('mascartdirect-link').writeAttribute('href', url);
				$('mascartdirect-popup').style.top = '200px';
				return false;
			}
			function mascartdirectPopupClose()
			{
				$('mascartdirect-popup').style.top = '-1100px';
				return false;
			}
		</script>
		<div id="mascartdirect-popup">
			<b id="mascartdirect-help"></b>
			<a id="mascartdirect-link" href="#" target="_blank" style="word-wrap: break-word;overflow: hidden;"></a>
			<a href="#" onclick="return mascartdirectPopupClose();">{$close}</a>
		</div>		
EOD;

		Mage::register('mascartdirect-popup', true);
		return $eod;

	}
}