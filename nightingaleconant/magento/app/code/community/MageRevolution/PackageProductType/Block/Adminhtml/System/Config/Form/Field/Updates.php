<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Block_Adminhtml_System_Config_Form_Field_Updates extends Mage_Adminhtml_Block_System_Config_Form_Fieldset
{
    protected $_dummyElement;
    protected $_fieldRenderer;
    protected $_values;

    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $html = '';
        $modules = array_keys((array)Mage::getConfig()->getNode('modules')->children());
        sort($modules);

        foreach ($modules as $moduleName) {
            if (strstr($moduleName, 'MageRevolution') === false) {
                continue;
            }

            $html.= $this->_getFieldHtml($element, $moduleName);
        }
        $html .= '';

        return $html;
    }

    protected function _getFieldRenderer()
    {
        if (empty($this->_fieldRenderer)) {
            $this->_fieldRenderer = Mage::getBlockSingleton('adminhtml/system_config_form_field');
        }
        return $this->_fieldRenderer;
    }

    protected function _getFieldHtml($fieldset, $moduleCode)
    {
        $currentVer = Mage::getConfig()->getModuleConfig($moduleCode)->version;
        if (!$currentVer)
            return '';

        $moduleName = substr($moduleCode, strpos($moduleCode, '_') + 1);
        $status = '<a href="https://www.sslsecurestore.com/mr/mr/downloadable/customer/products/" target="_blank" style="text-decoration:none">
                        <span style="color:green">'.$moduleName.'</span>
                    </a>';
        $info = "You are currently running version ".$currentVer." of this module which is the latest version.";
        $latestVer = Mage::getModel('package/updates')->getVersions($moduleCode);

        if ($this->_convertVersion($currentVer) < $this->_convertVersion($latestVer)){
            $status = '<a href="https://www.sslsecurestore.com/mr/mr/downloadable/customer/products/" target="_blank" style="text-decoration:underline">
                            <span style="color:red">'.$moduleName.'</span>
                        </a>';
            $info = 'There is a new update available for this module. You are currently running '.$currentVer.' while the latest version is '.$latestVer.'. Please click on the module
            name to visit your MageRevolution Dashboard to download the latest version available.';
        }

        $moduleName = $status;

        $field = $fieldset->addField($moduleCode, 'label', array(
            'name'  => 'dummy',
            'label' => '<img src="http://www.magerevolution.com/skin/adminhtml/default/default/images/magerevolution/favicon.png" alt="" /> ' . $moduleName,
            'value' => $info,
        ))->setRenderer($this->_getFieldRenderer());

        return $field->toHtml();
    }

    protected function _convertVersion($v)
    {
        $digits = @explode(".", $v);
        $version = 0;
        if (is_array($digits)){
            foreach ($digits as $k=>$v){
                $version += ($v * pow(10, max(0, (3-$k))));
            }

        }
        return $version;
    }
}