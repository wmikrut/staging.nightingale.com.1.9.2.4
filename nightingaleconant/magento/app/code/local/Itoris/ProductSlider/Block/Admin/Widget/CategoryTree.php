<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_ProductSlider_Block_Admin_Widget_CategoryTree extends Mage_Adminhtml_Block_Catalog_Category_Widget_Chooser {

	protected $categoriesValue = '';

	public function __construct() {
		parent::__construct();
		$this->setTemplate('itoris/productslider/widget/tree.phtml');
		$this->_withProductCount = true;
		$this->setId('itoris_categorychooser');
	}

	public function prepareElementHtml(Varien_Data_Form_Element_Abstract $element) {
		if ($element->getValue()) {
			$this->categoriesValue = $element->getValue();
			$values = str_replace('{', '', $element->getValue());
			$values = explode('}', $values);

			foreach ($values as $value) {
				if ($value) {
					$this->_selectedCategories[] = (int)$value;
				}
			}
		}

		$element->setData('after_element_html', $this->toHtml());
		return $element;
	}

	public function getCategoriesValue() {
		return $this->categoriesValue;
	}

	public function getCategoryCollection() {
		$storeId = $this->getRequest()->getParam('store', $this->_getDefaultStoreId());
		$collection = $this->getData('category_collection');
		if (is_null($collection)) {
			$collection = Mage::getModel('catalog/category')->getCollection();

			/* @var $collection Mage_Catalog_Model_Resource_Eav_Mysql4_Category_Collection */
			$collection->addAttributeToSelect('name')
				->addAttributeToSelect('is_active')
				->addAttributeToSelect('is_anchor')
				->setProductStoreId($storeId)
				->setLoadProductCount($this->_withProductCount)
				->setStoreId($storeId);

			$this->setData('category_collection', $collection);
		}
		return $collection;
	}

	protected function _getNodeJson($node, $level = 0) {
		$item = parent::_getNodeJson($node, $level);
		if (in_array($node->getId(), $this->getSelectedCategories())) {
			$item['checked'] = true;
		}
		$item['is_anchor'] = (int)$node->getIsAnchor();
		$item['url_key'] = $node->getData('url_key');
		return $item;
	}

	protected function _isParentSelectedCategory($node) {
		if ($node && count($this->_selectedCategories)) {
			foreach ($this->_selectedCategories as $categoryId) {
				$category = Mage::getModel('catalog/category')->load($categoryId);
				if ($category->getId()) {
					$pathIds = $category->getPathIds();
					if (in_array($node->getId(), $pathIds)) {
						return true;
					}
				}
			}
		}

		return false;
	}


	/**
	 * Tree JSON source URL
	 *
	 * @return string
	 */
	public function getLoadTreeUrl($expanded=null) {
		return $this->getUrl('adminhtml/catalog_category/categoriesJson', array(
			'_current'=>true,
			'uniq_id' => $this->getId(),
			'use_massaction' => $this->getUseMassaction()
		));
	}
}
?>