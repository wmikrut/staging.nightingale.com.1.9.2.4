<?php
/**
 * @category   Apptrian
 * @package    Apptrian_ProductSlider
 * @author     Apptrian
 * @copyright  Copyright (c) 2015 Apptrian (http://www.apptrian.com)
 * @license    http://www.apptrian.com/license    Proprietary Software License (EULA)
 */
class Apptrian_ProductSlider_Model_Config_Descriptionsource
{
    
    protected $_options;
    const DESCRIPTION       = 'description';
    const META_DESCRIPTION  = 'meta_description';
    const SHORT_DESCRIPTION = 'short_description';
    
    public function toOptionArray()
    {
        if (!$this->_options) {
			$this->_options[] = array(
			    'value'=>self::DESCRIPTION,
			    'label'=>Mage::helper('apptrian_productslider')->__('Description')
			);
            $this->_options[] = array(
			    'value'=>self::META_DESCRIPTION,
			    'label'=>Mage::helper('apptrian_productslider')->__('Meta Description')
			);
            $this->_options[] = array(
                'value'=>self::SHORT_DESCRIPTION,
            	'label'=>Mage::helper('apptrian_productslider')->__('Short Description')
            );
		}
		return $this->_options;
	}
    
}