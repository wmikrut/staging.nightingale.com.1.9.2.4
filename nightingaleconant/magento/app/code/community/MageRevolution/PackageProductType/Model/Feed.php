<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Model_Feed extends Mage_AdminNotification_Model_Feed {

    public function getFeedUrl() {
        return Mage::getStoreConfigFlag(self::XML_USE_HTTPS_PATH) ? 'https://' : 'http://'
                . 'www.magerevolution.com/update-feed.xml';
    }

    public function observeFeed() {
        if(!Mage::getStoreConfig('notifications/updates/enable')){
            return;
        }

        return Mage::getModel('package/feed')->checkUpdate();
    }

    /**
     * Check feed for modification
     *
     * @return Mage_AdminNotification_Model_Feed
     */
    public function checkUpdate()
    {
        if (($this->getFrequency() + $this->getLastUpdate()) > time()) {
            return $this;
        }

        $modules = array_keys((array)Mage::getConfig()->getNode('modules')->children());
        sort($modules);

        foreach ($modules as $moduleName) {
            if (strstr($moduleName, 'MageRevolution') === false) {
                continue;
            }

            $module[$moduleName] = $moduleName;
        }

        $feedData = array();

        $feedXml = $this->getFeedData();

        if ($feedXml && $feedXml->channel && $feedXml->channel->item) {
            foreach ($feedXml->channel->item as $item) {

                if(array_key_exists((string)$item->code, $module)){
                    $feedData[] = array(
                        'severity'      => (int)$item->severity,
                        'date_added'    => $this->getDate((string)$item->pubDate),
                        'title'         => (string)$item->title,
                        'description'   => (string)$item->description,
                        'url'           => (string)$item->link,
                    );
                }
            }

            if ($feedData) {
                Mage::getModel('adminnotification/inbox')->parse(array_reverse($feedData));
            }

        }
        $this->setLastUpdate();

        return $this;
    }
}