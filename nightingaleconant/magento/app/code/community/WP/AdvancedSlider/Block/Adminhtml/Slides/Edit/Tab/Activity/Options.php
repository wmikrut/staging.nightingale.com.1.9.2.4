<?php

class WP_AdvancedSlider_Block_Adminhtml_Slides_Edit_Tab_Activity_Options extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('advancedslider/form.phtml');
    }

    protected function _getSlide()
    {
        return Mage::registry('slide');
    }

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('advancedslider_slide_activity', array());
        $fieldset->setRenderer(Mage::getBlockSingleton('advancedslider/adminhtml_widget_form_renderer_fieldset'));

        $dateFormat = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        $fieldset->addField('start_datetime', 'date', array(
            'name'          => 'style_options[start_datetime]',
            'label'         => 'Start Date/Time',
            'format'        => $dateFormat,
            'time'          => true,
            'image'         => $this->getSkinUrl('images/grid-cal.gif'),
            'required'      => false,
        ));

        $fieldset->addField('end_datetime', 'date', array(
            'name'          => 'style_options[end_datetime]',
            'label'         => 'End Date/Time',
            'format'        => $dateFormat,
            'time'          => true,
            'image'         => $this->getSkinUrl('images/grid-cal.gif'),
            'required'      => false,
        ));

        $days = Mage::app()->getLocale()->getOptionWeekdays();
        $fieldset->addField('active_days', 'multiselect', array(
            'name'      => 'style_options[active_days]',
            'label'     => $this->__('Active Days'),
            'required'  => false,
            'values'    => $days,
        ));

        $fieldset->addField('start_active_time', 'time', array(
            'name'      => 'style_options[start_active_time]',
            'label'     => 'Start of an Active Time',
            'required'  => false,
        ));

        $fieldset->addField('end_active_time', 'time', array(
            'name'      => 'style_options[end_active_time]',
            'label'     => 'End of an Active Time',
            'required'  => false,
        ));

        if ($this->_getSlide()->getId())
        {
            $options = unserialize($this->_getSlide()->getStyleOptions());
            $form->setValues($options);
        }

        return parent::_prepareForm();
    }
}
