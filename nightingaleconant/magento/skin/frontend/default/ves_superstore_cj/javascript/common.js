(function ($) {
  $(window).ready(function () {
    /* automatic keep header always displaying on top */
    if ($("body").hasClass("layout-boxed-md") || $("body").hasClass("layout-boxed-lg")) {

    }
    else if ($("body").hasClass("keep-header")) {
      var mb = parseInt($("#header-main").css("margin-bottom"));
      var hideheight = $("#topbar").height() + mb + mb;
      var hh = $("#header").height() + mb;
      var updateTopbar = function () {
        var pos = $(window).scrollTop();
        if (pos >= hideheight) {
          $("#page").css("padding-top", hh);
          $("#header").addClass('hide-bar');
          $("#header").addClass("navbar navbar-fixed-top");

        }
        else {
          $("#header").removeClass('hide-bar');
        }
      }
      $(window).scroll(function () {
        updateTopbar();
      });
    }

    var $nav = $('.navbar-collapse .nav');
    $nav.find('.row').each(function(){
      var $this = $(this),
        $columns = $this.find('ul.level0').length;

      $this.css('width', ($columns * 200) + 'px');
      $this.find('ul:first-child').attr('class',$this.find('ul:last-child').attr('class'));
    });

    var $offcanvasNav = $('#menu-offcanvas');
    $offcanvasNav.find('.active > a').append('&nbsp;&nbsp;<i class="fa fa-check"></i>')

  });
})(jQuery);