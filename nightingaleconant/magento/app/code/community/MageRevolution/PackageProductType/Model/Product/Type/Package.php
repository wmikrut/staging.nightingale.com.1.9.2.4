<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Model_Product_Type_Package extends Mage_Catalog_Model_Product_Type_Abstract {
    const TYPE_CODE = 'package';

    /**
     * Product is composite
     *
     * @var bool
     */
    protected $_isComposite             = true;
    protected $_isStrictProcessMode = true;


    /**
     * Check if product is composite (grouped, configurable, etc)
     *
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     */
    public function isComposite($product = null)
    {
        return $this->_isComposite;
    }


    protected function _prepareProduct(Varien_Object $buyRequest, $product, $processMode)
    {
        $qty = $buyRequest['qty'];
        if($qty >= 1) {
            $result = parent::_prepareProduct($buyRequest, $product, $processMode);
            if (is_array($result)) {
                $product = $this->getProduct($product);


                $collection = $product->getPackageProductCollection()
                    ->addAttributeToSelect('required_options')
                    ->setPositionOrder()
                    ->addStoreFilter();

                foreach($collection as $col) {
                    $associatedQty[$col->getId()] = $col->getPackageQty();
                    $subProducts[] = Mage::getModel('catalog/product')->load($col->getId());
                }

                $i = 0;
                foreach($subProducts as $subProduct){
                    if($subProduct){
                        $product->setCartQty($qty);
                        $product->addCustomOption('product_qty_'.$subProduct->getId(), $associatedQty[$subProduct->getId()], $subProduct);
                        $product->addCustomOption('simple_product_'.$subProduct->getId(), $associatedQty[$subProduct->getId()], $subProduct);


                        $_result = $subProduct->getTypeInstance(true)->_prepareProduct(
                            $buyRequest,
                            $subProduct,
                            $processMode
                        );

                        $_result[0]->setParentProductId($product->getId())
                            ->addCustomOption('parent_product_id', $product->getId());

                        if ($this->_isStrictProcessMode) {
                            $_result[0]->setCartQty($associatedQty[$subProduct->getId()]);
                        }

                        $result[] = $_result[0];

                        $i++;
                    }
                }


                if (!isset($_result[0])) {
                    return Mage::helper('package')->__('Cannot add the item to shopping cart');
                }


                return $result;

            } else {
                return $this->getSpecifyOptionMessage();
            }
        } else {
            return $this->getQtyMessage();
        }
    }


    public function getQtyMessage() {
        return Mage::helper('package')->__('Please specify the product\'s qty.');
    }

}