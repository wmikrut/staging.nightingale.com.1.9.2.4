<?php
/**
 * WMD_Html5audio_Block_Adminhtml_Mapper_Main_Grid
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Block_Adminhtml_Mapper_Main_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    
    /**
     * Construct
     *
     * @return void
     */	
    public function __construct()
    {
        parent::__construct();
        $this->setId('mainGrid');
        $this->setDefaultSort('mapper_id');
        $this->setDefaultDir('desc');
        $this->setUseAjax(false);  
        $this->setSaveParametersInSession(true); 
    }

    /**
     * Prepare Collection
     *
     * @return object
     */	
    protected function _prepareCollection()
    {
		    $collection = Mage::getModel('html5audio/mapper')->getCollection();
        $this->setCollection($collection);      
        return parent::_prepareCollection();
    }
	
    /**
     * Prepare Massaction
     *
     * @return object
     */	
    protected function _prepareMassaction()
    {
        
		    $this->setMassactionIdField('mapper_id');
		    
        $this->getMassactionBlock()->setFormFieldName('id');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('html5audio')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('html5audio')->__('Are you sure?')
        ));
                   
        return $this;
    }	

    /**
     * Prepare Columns
     *
     * @return object
     */	
    protected function _prepareColumns()
    {
    	  $store = Mage::app()->getStore(); 

        $this->addColumn('mapper_id', array(
            'header'        => Mage::helper('html5audio')->__('Id'),
            'align'         => 'left',
            'filter_index'  => 'mapper_id',
            'index'         => 'mapper_id', 
            'width'         => '30px',
        ));

        $this->addColumn('link_id', array(
            'header'        => Mage::helper('html5audio')->__('Link Id'),
            'align'         => 'left',
            'filter_index'  => 'link_id',
            'index'         => 'link_id', 
        ));

        $this->addColumn('sample_id', array(
            'header'        => Mage::helper('html5audio')->__('Sample Id'),
            'align'         => 'left',
            'filter_index'  => 'sample_id',
            'index'         => 'sample_id', 
        ));

        $this->addColumn('product_id', array(
            'header'        => Mage::helper('html5audio')->__('Product Id'),
            'align'         => 'left',
            'filter_index'  => 'product_id',
            'index'         => 'product_id',
        ));

        $this->addColumn('playlist_id', array(
            'header'        => Mage::helper('html5audio')->__('Playlist Id'),
            'align'         => 'left',
            'filter_index'  => 'playlist_id',
            'index'         => 'playlist_id',
        ));
        
        $this->addColumn('page_actions', array(
            'header'    => Mage::helper('html5audio')->__('Action'),
            'width'         => '150px',
            'sortable'  => false,
            'filter'    => false,
            'renderer'  => 'html5audio/adminhtml_sample_main_grid_renderer_action',
        ));
        
        return parent::_prepareColumns();
        
    }  

//     protected function _filterStoreCondition($collection, $column)
//     {
//         if (!$value = $column->getFilter()->getValue()) {
//             return;
//         }
// 
//         $this->getCollection()->addStoreFilter($value);
//     }
    
    /**
     * Disable Return Row-Edit-Url
     *
     * @return boolean
     */	
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $row->getMapperId(),
        ));
    }  
    
    	
}
