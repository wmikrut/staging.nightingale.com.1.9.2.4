<?php 
/**
 * Mas_Mascartdirect extension by Makarovsoft.com
 * 
 * @category   	Mas
 * @package		Mas_Mascartdirect
 * @copyright  	Copyright (c) 2014
 * @license		http://makarovsoft.com/license.txt
 * @author		makarovsoft.com
 */
/**
 * Mascartdirect setup
 *
 * @category	Mas
 * @package		Mas_Mascartdirect
 * 
 */
class Mas_Mascartdirect_Model_Resource_Setup extends Mage_Core_Model_Resource_Setup{
	
}