<?php
/*
 *      RequestToken.inc
 *
 *      Copyright 2015 wmikrut <wmikrut72@gmail.com>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 *
 *
 */

function RequestToken($xml) {                                                                                                                                     
	include_once('mysql-credentials-production.php');
    include_once('XmlWriter.class.php');

    $xml_out = new XmlWriter2();

    $mysqli = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_db);
    $xml_data = new SimpleXMLElement($xml);

    $result = '';
    $status = '';

    $area = (string) $xml_data->XMLData->ApplicationArea;
    $user = (string) $xml_data->XMLData->UserName;
    $pass = (string) $xml_data->XMLData->Password;

    if ( !$mysqli->connect_errno) {
        $mysqlstm = <<<STRING
select count(*) as rcds
from nccustom_web_service_security
where service = '$area'
and user = '$user'
and password = '$pass'
and active = 1;
STRING;
        $f001 = $mysqli->query($mysqlstm);
        $db1 = $f001->fetch_assoc();
if ( $db1['rcds'] == 1 ) {
            if ( isset($_SERVER['REMOTE_ADDR'])) {
                $ip = $_SERVER['REMOTE_ADDR'];
            } else {
                $ip = "localhost";
            }                                                                                                                                                             $_SESSION[$ip] = "Y";
            $token = session_id();

            $mysqlstm = <<<STRING
delete from nccustom_web_service_token
where token = '$token';
STRING;
            $fnull = $mysqli->query($mysqlstm);

            $mysqlstm = <<<STRING
insert into nccustom_web_service_token
values('$token','$ip',CURRENT_TIMESTAMP);
STRING;
            $fnull = $mysqli->query($mysqlstm);
        } else {
            $result = stripslashes($xml);
            $status = "RequestToken Failed: Invalid credentials.";
        }
   } else {
        $status = "RequestToken Failed:  Database Server Unavailable.";
   }
$xml_out->push("XMLResponse");
        $xml_out->push("XMLData");
            $xml_out->element("ServiceName", "RequestToken");
            $xml_out->element("Token", $token);
            $xml_out->element("Status", $status);
        $xml_out->pop();
    $xml_out->pop();

    $result = $xml_out->getXml();

    return $result;
}

?>