<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Block_Adminhtml_Sales_Items_Renderer_Default extends Mage_Adminhtml_Block_Sales_Items_Renderer_Default {

    public function getChildPackageQtys()
    {
        $parent = $this->_getData('item');//->getOrderItem();
        $itemId = $parent->getOrderItemId();

        $collection = Mage::getModel('sales/order_item')->load($itemId);

        if($collection && is_numeric($collection->getQtyOrdered())){
            if($collection->getQtyShipped() != 0){
                if($collection->getQtyInvoiced() != 0){
                    return $collection->getQtyInvoiced() - $collection->getQtyShipped();
                } elseif($collection->getQtyOrdered() != 0){
                    return $collection->getQtyOrdered() - $collection->getQtyShipped();
                }
            }
            return $collection->getQtyOrdered();
        } else {
            return false;
        }
    }

    public function getIsPackage($_item) {
        $itemId = $_item->getOrderItemId();
        $product = Mage::getModel('sales/order_item')->load($itemId);

        if($product->getParentItemId()){
            $subProduct = Mage::getModel('sales/order_item')->load($product->getParentItemId());
            if($subProduct->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
}