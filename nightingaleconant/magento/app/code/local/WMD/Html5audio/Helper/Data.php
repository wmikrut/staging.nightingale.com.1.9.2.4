<?php
/**
 * WMD_Html5audio_Helper_Data
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Helper_Data extends Mage_Core_Helper_Abstract
{      
    
    protected $_playerInstances = array();
    protected $_playlist;
    protected $_playlistHash;
    
    protected function _getConfig($item = 'skin', $group = 'player')
    {
        return Mage::getStoreConfig('html5audio/' . $group . '/' . $item, Mage::app()->getStore()->getId());
    }
    
    protected function _prepareIds($playlistIds)
    {
        if (!is_array($playlistIds))
        {
            $idsArray = explode(',', $playlistIds);
        }
        else
        {
            $idsArray = $playlistIds;
        }
        return $idsArray; 
    }
    /**
     * Return a random string 0-9, a-z, A-Z
     * 
     * @param int $length
     * @return string
     */                        
    public function getRandomString($length = 24) {
        $original_string = implode("", 
            array_merge(range(0,9), range('a','z'), range('A', 'Z'))
        );
        return substr(str_shuffle($original_string), 0, $length);
    }
    
        
    protected function _addPlaylistEntry($sample, $i)
    {
        $this->_playlist[] =  array(            
            'mp3' => $sample->getMp3Path(),
            'oga' => $sample->getOggPath(),
            'title' => $sample->getTitle(),
            'hash' => $sample->getHash(),
        );       
    }
    
    protected function _returnPlaylist()
    {     
            
        $str = '';
                  
        foreach ($this->_playlist as $item)
        {            
            if ($this->_getConfig('stream', 'sample'))
            { 
                $mp3Url = Mage::getUrl('html5audio/index/stream', array(
                    'file' => $item['hash'],
                    'mime' => 'mp3',
                )); 
                $oggUrl = Mage::getUrl('html5audio/index/stream', array(
                    'file' => $item['hash'],
                    'mime' => 'oga',
                ));               
            } 
            else
            {
                $mp3Url = $item['mp3'];
                $oggUrl = $item['oga'];   
            }
            $str .= '
                {                                            
                  title:\'' . $item['title'] . '\',
                  mp3:\'' . $mp3Url . '\',
                  oga:\'' . $oggUrl . '\'
                },';   
        }
        
        return substr($str, 0, -1);
        
    }
    
    public function addButtonPlayerInstance($sampleId, $prepend = '')
    {          
        $this->_playlist = array();
        $sample = Mage::getModel('html5audio/sample')->load($sampleId);            
        $this->_addPlaylistEntry($sample, 0);  
        if ($playlistEntries = $this->_returnPlaylist())
        {             
            $this->_playerInstances[] = array(
                'id' => $sampleId,
                'prepend' => $prepend,
                'playlist'  => $playlistEntries,
            );
            return true;
        }          
        return false;
    }
     
    public function addPlayerInstance($playlistIds, $prepend = '')
    { 
              
        $this->_playlist = array();
        $idsArray = $this->_prepareIds($playlistIds);
        
        $i = 0;
        foreach ($idsArray as $playlistId)
        {               
            $playlist = Mage::getModel('html5audio/playlist')->load($playlistId);
              
            foreach ($playlist->getSamples() as $key => $value)
            {
                $sample = Mage::getModel('html5audio/sample')->load($key);            
                $this->_addPlaylistEntry($sample, $i);  
                $i++;
            }
        }
        if ($playlistEntries = $this->_returnPlaylist())
        {  
        
            $this->_playerInstances[] = array(
                'id' => str_replace(',', '_', $playlistIds),
                'prepend' => $prepend,
                'playlist'  => $playlistEntries,
            );
            return true;
        }
         
        return false;
        
    } 
      
    public function returnAllButtonPlayersJs()
    {
        
        $js = '';
        foreach ($this->_playerInstances as $player)
        {                                   
            $prepend = $player['prepend'];
            $id = $player['id'];
            $playlist = $player['playlist'];
            $js .= '
            jList = new jPlayerPlaylist({
          		jPlayer: "#jquery_jplayer_' . $prepend.$id . '",
          		cssSelectorAncestor: "#jp_container_' . $prepend.$id . '"
          	}, [' . $playlist . '
          	], {
          		swfPath: "js",
          		supplied: "mp3, oga",
          		wmode: "window"
          	});
            ';
        }          
        return $js; 
    }
      
    public function returnAllPlayersJs($wrapper = 'product-shop', $mode = 'grid')
    {
        
        $js = '';
        foreach ($this->_playerInstances as $player)
        {                                   
            $prepend = $player['prepend'];
            $id = $player['id'];
            $playlist = $player['playlist'];
            $js .= '
            jList = new jPlayerPlaylist({
          		jPlayer: "#jquery_jplayer_' . $prepend.$id . '",
          		cssSelectorAncestor: "#jp_container_' . $prepend.$id . '"
          	}, [' . $playlist . '
          	], {
          		swfPath: "js",
          		supplied: "mp3, oga",
          		wmode: "window"
          	});
            ';
        }
        
        if ('flex' == $this->_getConfig('skin'))
        {       
        
            switch ($wrapper)
            {
                case $this->_getConfig('listwrapper'):
                    $width = ('grid' == $mode) ? $this->_getConfig('gridwidth') : $this->_getConfig('listwidth');
                    break;
                case $this->_getConfig('relatedwrapper'):
                    $width = $this->_getConfig('relatedwidth');
                    break;
                case $this->_getConfig('upsellwrapper'):
                    $width = $this->_getConfig('upsellwidth');
                    break; 
                case $this->_getConfig('crosssellwrapper'):
                    $width = $this->_getConfig('crosssellwidth');
                    break; 
                case $this->_getConfig('newwrapper'):
                    $width = $this->_getConfig('newwidth');
                    break; 
                case $this->_getConfig('widgetgridwrapper'):              
                    $width = $this->_getConfig('widgetgridwidth');
                    break; 
                case $this->_getConfig('widgetlistwrapper'):  
                    $width = $this->_getConfig('widgetlistwidth');
                    break; 
                case $this->_getConfig('widgetwrapper'):  
                    $width = $this->_getConfig('widgetwidth');
                    break; 
                case $this->_getConfig('playerwrapper'):
                    $width = $this->_getConfig('playerwidth'); 
                    break; 
            }
            
            $location = (!$wrapper) ? 'product-shop' : $wrapper ;
            if (0 < $width)
            {
                $js .= '
            var barWidth = ' . $width . ';
            var offsetWidth = barWidth + 18;
            var wrapper = jQuery(\'.' . $location . '\');
            wrapper.find(\'div.jp-audio\').css("width", "+="+offsetWidth);
            wrapper.find(\'div.jp-audio .jp-type-single\').css("width", "+="+offsetWidth); 
            wrapper.find(\'div.jp-audio div.jp-type-playlist a.jp-previous\').css("left", "+="+offsetWidth);
            wrapper.find(\'div.jp-audio div.jp-type-playlist a.jp-next\').css("left", "+="+offsetWidth);
            wrapper.find(\'div.jp-progress-container\').css("width", "+="+barWidth);
            wrapper.find(\'div.jp-audio div.jp-type-single div.jp-progress\').css("width", "+="+barWidth);
            wrapper.find(\'div.jp-audio div.jp-type-playlist div.jp-progress\').css("width", "+="+barWidth);
                ';
            }
            else
            {
                $js .= '  
            var wrapper = jQuery(\'.' . $location . '\');
            wrapper.find(\'div.jp-progress-container\').hide();
                ';
            }
            
        }
        
        return $js; 
        
    } 
    
    public function returnPlayerPlaylist()
    {   
        foreach ($this->_playerInstances as $player)
        {               
            return trim( preg_replace( '/\s+/', ' ', $player['playlist'] ) );
        }     
    }
    
    public function returnAllPlayersPlaylists()
    {   
        $playlists = array();
        foreach ($this->_playerInstances as $player)
        {               
//             $playlists[] = trim( preg_replace( '/\s+/', '', $player['playlist'] ) );           
            $playlists[] = $player['playlist'];
        } 
        return $playlists;    
    }
    
    public function isPlaylistMode($playlistId)
    {      
        if ((is_array($playlistId) && (1 < count($playlistId))) || strpos((string)$playlistId, ','))
        {
            return true;
        }  
        if ($playlist = Mage::getModel('html5audio/playlist')->load($playlistId))
        {
            return (1 < count($playlist->getSamples())) ? true : false ;
        }
        return false;
    } 
    
    public function getWrap($item = 'skin')
    {
        return $this->_getConfig($item);
    }
    
    public function returnSkinPath()
    {
Mage::log(__METHOD__);
        $skinPath = 'jquery/plugin/skin/anthracite.mini/jplayer.anthracite.mini.css';
        switch ($this->_getConfig()) {
            case 'pink':
                $skinPath = 'jquery/plugin/skin/pink.flag/jplayer.pink.flag.css';
                break;
            case 'blue':
                $skinPath = 'jquery/plugin/skin/blue.monday/jplayer.blue.monday.css';
                break;
            case 'minimal':
                $skinPath = 'jquery/plugin/skin/minimal.skin/jplayer.minimal.skin.css';
                break;
            case 'anthracite':
                $skinPath = 'jquery/plugin/skin/anthracite.mini/jplayer.anthracite.mini.css';
                break;
            case 'stream':
                $skinPath = 'jquery/plugin/skin/anthracite.stream/jplayer.anthracite.stream.css';
                break;  
            case 'flex':
                $skinPath = 'jquery/plugin/skin/anthracite.flex/jplayer.anthracite.flex.css'; 
                break;      
            case 'button':
                $skinPath = 'jquery/plugin/skin/anthracite.button/jplayer.anthracite.button.css'; 
                break;        
        }
        return $skinPath;        
    }
    
    public function returnButtonHtml($simpleId)
    {
        $html = '<button class="button btn-cart right" onclick="setLocation(\'';
        $model = Mage::getModel('catalog/product');
        if ($simpleProduct = $model->load($simpleId))
        {
            if ($this->_getConfig('usebuy'))
            {
                $html .= Mage::helper('checkout/cart')->getAddUrl($simpleProduct);
                $caption = $this->__('Add CD to cart'); ;
            }
            else
            {
                $category = Mage::getModel('catalog/category')->load(array_pop($simpleProduct->getCategoryIds()));
                $html .= Mage::getBaseUrl() . $category->getUrlKey() . '/';                
                $html .= $simpleProduct->getUrlKey(). '.html';
                $caption = $this->__('Buy CD'); ;
            }  
            $html .= '\')" title="' . $caption . '" type="button"><span><span>' 
                . $caption . '</span></span></button>';
            
        }    
               
        return $html;
                           
            
    }
    
    public function returnLinksHtml($downloadableId, $wrapper)
    {
        
        $downloadableProduct = Mage::getModel('catalog/product')->load($downloadableId);
        $links = Mage::getModel('downloadable/product_type')->getLinks($downloadableProduct);
            
        $iPlayer = 0;
        $html = '<ul>';
        
        foreach ($links as $_link): 
            $html .= '<li style="clear:left;">';                    
            if ($sampleId = Mage::getModel('html5audio/mapper')->loadByLinkId($_link->getId())->getSampleId()): 
                $html .= '<div class="left">'
                    . Mage::helper('html5audio/player_interface')->getButtonPlayer($sampleId, $_link->getId(), false, $iPlayer)
                    . '</div>';
                $iPlayer++;
            endif;
                
            $html .= '<span class="label" style="display:inline-block; padding:2px 10px;">'
                      . '<label for="links_' . $_link->getId() . '">'
                      . $this->escapeHtml($_link->getTitle())
                      . '</label></span></li>';
        endforeach; 
        
        $html .= '</ul>        
    <script type="text/javascript">
    //<![CDATA[    
                                 
      jQuery(document).ready(function(){
           ' . Mage::helper('html5audio')->returnAllButtonPlayersJs($wrapper) . '                                                     
      });
      
    //]]>
    </script>    
        ';
        
        return $html;
        
    }
    
    public function getTitleFromId($id)
    {
        return Mage::getModel('html5audio/sample')->load($id)->getTitle();
    }
}