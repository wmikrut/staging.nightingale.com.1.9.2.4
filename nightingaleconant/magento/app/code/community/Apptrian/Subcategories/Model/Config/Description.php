<?php
/**
 * @category  Apptrian
 * @package   Apptrian_Subcategories
 * @author    Apptrian
 * @copyright Copyright (c) 2016 Apptrian (http://www.apptrian.com)
 * @license   http://www.apptrian.com/license Proprietary Software License EULA
 */
class Apptrian_Subcategories_Model_Config_Description
{
    
    protected $_options;
    const DESCRIPTION      = 'description';
    const META_DESCRIPTION = 'meta_description';
    
    public function toOptionArray()
    {
        if (!$this->_options) {
            $this->_options[] = array(
                'value' => self::DESCRIPTION,
                'label' => Mage::helper('apptrian_subcategories')
                    ->__('Description')
            );
            $this->_options[] = array(
                'value' => self::META_DESCRIPTION,
                'label' => Mage::helper('apptrian_subcategories')
                    ->__('Meta Description')
            );
        }
        return $this->_options;
    }
    
}
