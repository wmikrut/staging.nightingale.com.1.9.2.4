<?php
/*
 *      TestService.inc
 *
 *      Copyright 2015 wmikrut <wmikrut72@gmail.com>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 *
 *
 */

function TestService($xml) {                                                                                                                                     
	include('mysql-credentials-production.php');
    include_once('XmlWriter.class.php');
    include_once('soap/ValidateCredentials.inc');

    $result = '';
    $status = 'success';
    $status_message = '';  
    
    $ip_address = '0.0.0.0';
    if ( isset($_SERVER['REMOTE_ADDR'])) {
    	$ip_address = $_SERVER['REMOTE_ADDR'];
    }
    
    $xml_out = new XmlWriter2();
    $mysqli = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_db);
    $xml_data = new SimpleXMLElement($xml);
            
    $xml_application_area = (string) $xml_data->XMLData->ApplicationArea;
    $xml_user_name = (string) $xml_data->XMLData->UserName;
    $xml_password = (string) $xml_data->XMLData->Password;
    $xml_service_name = (string) $xml_data->XMLData->ServiceName;
    $xml_version = (string) $xml_data->XMLData->Version;
    
	$credentials_valid = ValidateCredentials($xml_application_area, $xml_user_name, $xml_password);
    $xml_email_address = "";
    $xml_first_name = "";
    $xml_last_name = "";
	
    if ( $credentials_valid == 'Y') {
    	$xml_email_address = (string) $xml_data->XMLData->EmailAddress;
    	$xml_first_name = (string) $xml_data->XMLData->FirstName;
    	$xml_last_name = (string) $xml_data->XMLData->LastName;
    } else {
    	$status = "fault";
    	$status_message = "You are not authorized to use this service.";
    }
    
    $xml_out->push("XMLResponse");
    	$xml_out->push("XMLData");
    		$xml_out->element("ServiceName", $xml_service_name);
    		$xml_out->element("Version", $xml_version);
    		$xml_out->element("EmailAddress", $xml_email_address);
    		$xml_out->element("FirstName", $xml_first_name);
    		$xml_out->element("LastName", $xml_last_name);
    		$xml_out->element("ServiceStatus", $status);
    		$xml_out->element("ServiceStatusMessage", $status_message);
    	$xml_out->pop();
    $xml_out->pop();
    
    $result = $xml_out->getXml();
    $mysqlstm = <<<STRING
insert into nccustom_webservice_log
values(0, '$xml_service_name', '$xml_user_name','$ip_address','$xml','$result', CURRENT_TIMESTAMP);
STRING;
    $fnull = $mysqli->query($mysqlstm);
    mysqli_close($mysqli);
    
    return $result;
}
?>