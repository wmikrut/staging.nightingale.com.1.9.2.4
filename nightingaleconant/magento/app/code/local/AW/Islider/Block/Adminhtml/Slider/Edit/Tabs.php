<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Block_Adminhtml_Slider_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('awislider_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Slider Information'));
    }
    
    protected function _beforeToHtml()
    {
        $this->addTab('general', array(
                'label'   => $this->__('General'),
                'title'   => $this->__('General'),
                'content' => $this->getLayout()->createBlock('awislider/adminhtml_slider_edit_tabs_general')->toHtml()
        ));
        $slider = Mage::registry('current_slider');
        $_imagesTabContent = $this->__('You can manage images right after saving this slider');
        if($slider->getId()) {
            $_imagesTabContent = $this->getLayout()->createBlock('awislider/adminhtml_slider_edit_tabs_images')
                ->setData('awis_pid', $slider->getId())
                ->toHtml();
        }
        $this->addTab('images', array(
                'label'   => $this->__('Content'),
                'title'   => $this->__('Content'),
                'content' => $_imagesTabContent
        ));
        if($this->getRequest()->getParam('continue_tab')) {
            $this->setActiveTab($this->getRequest()->getParam('continue_tab'));
        }
        return parent::_beforeToHtml();
    }
}