<?php
/**
 * Toogas Lda.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA (End-User License Agreement)
 * that is bundled with this package in the file licence_toogas_community.txt.
 * It is also available at this URL:
 * http://www.toogas.com/licences/toogas_community.txt
 *
 * @category   Toogas
 * @package    Toogas_Featuredpopup
 * @copyright  Copyright (c) 2010 Toogas Lda. (http://www.toogas.com)
 * @license    http://www.toogas.com/licences/toogas_community.txt
 */
$installer = $this;
$installer->startSetup();

$pageTable = $installer->getTable('toogas_featuredpopup');

$installer->getConnection()->addColumn($pageTable, 'pages',
    "tinyint(3) NOT NULL default '1' AFTER `url_link`");

$installer->getConnection()->addColumn($pageTable, 'pages_url',
    "varchar(255) NOT NULL default '' AFTER `pages`");

$installer->endSetup(); 
?>