<?php
/**
 * WMD_Html5audio_Block_Adminhtml_Playlist_Edit_Tab_Sample
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://eklatant.ch/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@ e k l a t a n t .ch>
 * @copyright 2013 Dominik Wyss | WMD Extensions (http:// e k l a t a n t .ch)
 * @link      http:// e k l a t a n t .ch
 * @license   http://eklatant.ch/WMD-License-Community.txt
*/

class WMD_Html5audio_Block_Adminhtml_Playlist_Edit_Tab_Sample extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('playlist_samples');
        $this->setDefaultSort('sample_id');
        $this->setUseAjax(true);
    }

    public function getPlaylist()
    {
        return Mage::registry('playlist');
    }

    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in playlist flag
        if ($column->getId() == 'in_playlist') {
            $sampleIds = $this->_getSelectedSamples();
            if (empty($sampleIds)) {
                $sampleIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('sample_id', array('in'=>$sampleIds));
            }
            elseif(!empty($sampleIds)) {
                $this->getCollection()->addFieldToFilter('sample_id', array('nin'=>$sampleIds));
            }
        }
        else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('html5audio/sample')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('in_playlist', array(
            'header_css_class' => 'a-center',
            'type'      => 'checkbox',
            'name'      => 'in_playlist',
            'values'    => $this->_getSelectedSamples(),
            'align'     => 'center',
            'index'     => 'sample_id'
        ));
        
        $this->addColumn('sample_id', array(
            'header'    => Mage::helper('html5audio')->__('ID'),
            'sortable'  => true,
            'width'     => '40',
            'index'     => 'sample_id'
        ));
        
        $this->addColumn('title', array(
            'header'    => Mage::helper('catalog')->__('Title'),
            'index'     => 'title'
        ));
        
        $this->addColumn('mp3_path', array(
            'header'    => Mage::helper('catalog')->__('mp3 Path'),
            'sortable'  => true,
            'index'     => 'mp3_path'
        ));
        
        $this->addColumn('ogg_path', array(
            'header'    => Mage::helper('catalog')->__('ogg Path'),
            'sortable'  => true,
            'index'     => 'ogg_path'
        ));
        
        $this->addColumn('sample_position', array(
            'header'    => Mage::helper('catalog')->__('Position'),
            'width'     => '1',
            'type'      => 'number',
            'index'     => 'position',
//             'editable'  => !$this->getPlaylist()->getProductsReadonly(),
            'renderer'  => 'adminhtml/widget_grid_column_renderer_input'
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    protected function _getSelectedSamples()
    {
        $samples = $this->getRequest()->getPost('selected_samples');
        if (is_null($samples)) {
            $samples = $this->getPlaylist()->getSamples();
            return array_keys($samples);
        }
        return $samples;
    }
    
    /**
     * Retrive entry js object name
     *
     * @return string
     */
    public function getJsObjectName()
    {        
        return $this->getId() . 'JsObject';
    }

}

