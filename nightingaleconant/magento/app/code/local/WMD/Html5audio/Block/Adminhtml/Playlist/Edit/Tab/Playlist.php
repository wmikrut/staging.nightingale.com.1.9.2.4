<?php
/**
 * WMD_Html5audio_Block_Adminhtml_Playlist_Edit_Tab_Playlist
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2013 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Block_Adminhtml_Playlist_Edit_Tab_Playlist extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare Form
     *
     * @return object
     */	
    protected function _prepareForm()
    {
                
        /** @var $playlist WMD_Html5audio_Model_Playlist */
        $playlist = Mage::registry('playlist');
        
        $form = new Varien_Data_Form();
        
		    $fieldset = $form->addFieldset('edit_playlist', array('legend' => Mage::helper('html5audio')->__('Playlist')));
		    		
        $fieldset->addField('in_playlist_samples', 'hidden', array(
            'name' => 'playlist_samples',
        ));
        
        if ($playlist->getPlaylistId()) {
            $fieldset->addField('playlist_id', 'hidden', array(
                'name' => 'playlist_id',
            ));
        }
        
        $fieldset->addField('playlist_title', 'text', array(
            'name'      => 'playlist_title',
            'title'     => Mage::helper('html5audio')->__('Title'),
            'label'     => Mage::helper('html5audio')->__('Title'),
        		'required'	=> true,	
        ));	
       
        $form->setValues($playlist->getData());
        $this->setForm($form);
        return parent::_prepareForm();	
    }
    
}