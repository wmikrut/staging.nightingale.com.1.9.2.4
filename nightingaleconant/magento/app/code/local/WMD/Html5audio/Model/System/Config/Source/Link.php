<?php     
/**
 * WMD_Html5audio_Model_System_Config_Source_Link
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.com>
 * @copyright 2013 Dominik Wyss | Digiswiss (http://www.digiswiss.ch)
 * @link      http://wmdextensions.com/
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Model_System_Config_Source_Link
{
    /**
     * Actions
     * @var array
     */		
    protected $_options;
	
    protected function _getCollection()
    {
        return Mage::getModel('downloadable/link')->getCollection();
    }
    
    /**
     * Return the avaiable job status
     * 
     * @param boolean $isMultiselect if Multiselect for the order status selection is allowed 
     *
     * @return array
     */	      
	  public function toOptionArray($isMultiselect=false)
    {		
		    if (!$this->_options) {
            foreach ($this->_getCollection() as $link)
            {
                $this->_options[] = array('value' => $link->getLinkId(), 'label' => $link->getLinkFile());
            } 	
            array_unshift($this->_options, array('value'=>'', 'label'=> Mage::helper('adminhtml')->__('-- Please Select --')));	
            return $this->_options;
        }		
    }   
    
//     public function toOptionHash()
//     {                           
//         $data = array();
//         foreach ($this->_getCollection() as $link)
//         {
//             $data[$link->getLinkId()] = $link->getLinkFile();
//         } 
//     		return $data;
//     }

}