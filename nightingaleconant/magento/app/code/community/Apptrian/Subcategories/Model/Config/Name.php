<?php
/**
 * @category  Apptrian
 * @package   Apptrian_Subcategories
 * @author    Apptrian
 * @copyright Copyright (c) 2016 Apptrian (http://www.apptrian.com)
 * @license   http://www.apptrian.com/license Proprietary Software License EULA
 */
class Apptrian_Subcategories_Model_Config_Name
{
    
    protected $_options;
    const NAME       = 'name';
    const META_TITLE = 'meta_title';
    
    public function toOptionArray()
    {
        if (!$this->_options) {
            $this->_options[] = array(
                'value' => self::NAME,
                'label' => Mage::helper('apptrian_subcategories')
                    ->__('Name')
            );
            $this->_options[] = array(
                'value' => self::META_TITLE,
                'label' => Mage::helper('apptrian_subcategories')
                    ->__('Page Title')
            );
        }
        return $this->_options;
    }
    
}
