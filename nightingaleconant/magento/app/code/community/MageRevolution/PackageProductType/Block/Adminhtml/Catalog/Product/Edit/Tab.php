<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Block_Adminhtml_Catalog_Product_Edit_Tab
extends Mage_Adminhtml_Block_Widget
implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    public function canShowTab() 
    {
        return true;
    }

    public function getTabLabel() 
    {
        return Mage::helper('package')->__('Associated Products');
    }

    public function getTabTitle()        
    {
        return Mage::helper('package')->__('Associated Products');
    }

    public function isHidden()
    {
        return false;
    }
    
    public function getTabUrl() 
    {
        return $this->getUrl('*/*/packageProduct', array('_current' => true));
    }
    
    public function getTabClass()
    {
        return 'ajax';
    }

}
