<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Block_Adminhtml_Slider_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_slider';
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'awislider';
        $slider = Mage::registry('current_slider');

        if ($slider && $slider->getId()) {
            $this->_addButton('addimage', array(
                    'label' => $this->__('Add Image or Video'),
                    'onclick' => 'awisAddImage()',
                    'class' => 'add',
                    'id' => 'awis-add-image'
                ), 0);
        }

        $this->_addButton('saveandcontinueedit', array(
            'label' => $this->__('Save And Continue Edit'),
            'onclick' => 'awisSaveAndContinueEdit()',
            'class' => 'save',
            'id' => 'awis-save-and-continue'
        ), -200);
        
        $this->_formScripts[] = "
            function awisAddImage() {
                awislider_tabsJsTabs.tabs[1].show();
                awISAjaxForm.showForm(" . $slider->getId() . ");
            }
            function awisSaveAndContinueEdit() {
                if($('edit_form').action.indexOf('continue/1/')<0)
                    $('edit_form').action += 'continue/1/';
                if($('edit_form').action.indexOf('continue_tab/')<0)
                    $('edit_form').action += 'continue_tab/'+awislider_tabsJsTabs.activeTab.name+'/';
                editForm.submit();
            }
            if(awISSettings) {
                awISSettings.setOption('imagesAjaxFormUrl', '{$this->getUrl('adminhtml/awislider_image/ajaxform')}');
                awISSettings.setOption('videoAjaxFieldsetUrl', '{$this->getUrl('adminhtml/awislider_image/videoOptions')}');
                awISSettings.setOption('imageAjaxFieldsetUrl', '{$this->getUrl('adminhtml/awislider_image/imageOptions')}');
            }"
        ;
    }
    
    public function getHeaderText()
    {
        return $this->__('Slider');
    }
}