<?php

require_once 'AdminController.php';

class WP_AdvancedSliderAddonExportImport_ImportController
    extends WP_AdvancedSliderAddonExportImport_AdminController
{
    protected function _init()
    {
        $this->_title($this->__('CMS'))
             ->_title($this->__('WP: Total Slider Manager'))
             ->_title($this->__('Import Sliders'));
        $this->loadLayout();
        $this->_setActiveMenu('cms/advancedslider/sliders');
        $this->renderLayout();
        return $this;
    }

    public function indexAction()
    {
        $list = array();
        Mage::getSingleton('core/session')->setListOfSliders($list);
        $this->_init();
    }

    public function processAction()
    {
        if (isset($_FILES['file1']['name']) && $_FILES['file1']['name'] != '') {
            try {
                $uploader = new Varien_File_Uploader('file1');
                $uploader->setAllowedExtensions(array('tgz'));
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $path = Mage::getBaseDir('media') . DS . 'advancedslider' . DS . 'import' . DS;
                $fileName = 'import_' . uniqid() . '_' . $_FILES['file1']['name'];
                $uploader->save($path, $fileName);
                $sliderId = Mage::getModel('advancedslideraddonexportimport/process')->import($path . $fileName);
                $list = array();
                $name = Mage::helper('advancedslideraddonexportimport')->getSliderLongName($sliderId);
                $link = Mage::helper('adminhtml')->getUrl('advancedslider/sliders/edit', array('id' => $sliderId));
                $info = array(
                    'sliderName'    => $name,
                    'statusName'    => 'done',
                    'statusMessage' => '<a href="' . $link . '">' . $this->__('Edit') . '</a>',
                );
                $list[$sliderId] = $info;
                Mage::getSingleton('core/session')->setListOfSliders($list);
                Mage::getSingleton('core/session')->addSuccess($this->__('The slider was successfully imported.'));
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError($e->getMessage());
            }
        }
        $this->_init();
    }
}
