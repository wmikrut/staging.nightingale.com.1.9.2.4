<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Model_Source_Autoposition extends AW_Islider_Model_Source_Abstract
{
    const NONE = 0;
    const NONE_LABEL = 'None';
    const LEFTCOLUMN = 1;
    const LEFTCOLUMN_LABEL = 'Left Column';
    const RIGHTCOLUMN = 2;
    const RIGHTCOLUMN_LABEL = 'Right Column';
    const BEFORECONTENT = 3;
    const BEFORECONTENT_LABEL = 'Before Content';
    
    public function toOptionArray()
    {
        return array(
            array('value' => self::NONE, 'label' => Mage::helper('awislider')->__(self::NONE_LABEL)),
            array('value' => self::LEFTCOLUMN, 'label' => Mage::helper('awislider')->__(self::LEFTCOLUMN_LABEL)),
            array('value' => self::RIGHTCOLUMN, 'label' => Mage::helper('awislider')->__(self::RIGHTCOLUMN_LABEL)),
            array('value' => self::BEFORECONTENT, 'label' => Mage::helper('awislider')->__(self::BEFORECONTENT_LABEL)),
        );
    }
}