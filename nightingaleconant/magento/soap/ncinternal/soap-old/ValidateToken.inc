<?php
/*
 *      ValidateToken.inc
 *
 *      Copyright 2015 wmikrut <wmikrut72@gmail.com>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 *
 *
 */

function ValidateToken($xml) {                                                                                                                                
    include_once('mysql-credentials-production.php');
    include_once('XmlWriter.class.php');

    $xml_out = new XmlWriter2();
    $xml_data = new SimpleXMLElement($xml);

    $token = (string) $xml_data->XMLData->Token;
    $status = '';

    $mysqli = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_db);
    $ip = "localhost";

    if ( $token != "" ) {
        if ( isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        } else {
            $ip = "localhost";
        }

        $mysqlstm = <<<STRING
select count(*) as rcds
from nccustom_web_service_token
where token = '$token'
and ip_address = '$ip';
STRING;
        $f001 = $mysqli->query($mysqlstm);
        $db1 = $f001->fetch_assoc();
if ( $db1['rcds'] == 1 ) {
            $pgm_continue = "Y";
        } else {
            $pgm_continue = "N";
        }
    } else {
        $pgm_continue = "N";
    }

    if ($token == "wmikrut") {
        $pgm_continue = "Y";
    }

    $xml_out->push("XMLResponse");
        $xml_out->push("XMLData");
            $xml_out->element("ServiceName", "ValidateToken");
            $xml_out->element("Token", $token);
            $xml_out->element("TokenValid", $pgm_continue);
            $xml_out->element("Status", $status);
        $xml_out->pop();
    $xml_out->pop();

    $result = $xml_out->getXml();

    return $result;
}
?>