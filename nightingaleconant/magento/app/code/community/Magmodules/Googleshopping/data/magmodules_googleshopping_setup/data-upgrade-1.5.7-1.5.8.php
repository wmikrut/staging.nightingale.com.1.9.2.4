<?php 
/**
 * Magmodules.eu - http://www.magmodules.eu - info@magmodules.eu
 * =============================================================
 * NOTICE OF LICENSE [Single domain license]
 * This source file is subject to the EULA that is
 * available through the world-wide-web at:
 * http://www.magmodules.eu/license-agreement/
 * =============================================================
 * @category    Magmodules
 * @package     Magmodules_Googleshopping
 * @author      Magmodules <info@magmodules.eu>
 * @copyright   Copyright (c) 2016 (http://www.magmodules.eu)
 * @license     http://www.magmodules.eu/license-agreement/  
 * =============================================================
 */

$process = Mage::getSingleton('index/indexer')->getProcessByCode('catalog_category_flat');
if($process->getStatus() != Mage_Index_Model_Process::STATUS_RUNNING) {
	$process = Mage::getModel('index/indexer')->getProcessByCode('catalog_category_flat');
	$process->reindexAll();
}

