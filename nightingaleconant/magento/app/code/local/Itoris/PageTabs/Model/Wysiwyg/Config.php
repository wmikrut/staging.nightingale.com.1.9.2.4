<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGETABS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */


class Itoris_PageTabs_Model_Wysiwyg_Config extends Mage_Cms_Model_Wysiwyg_Config {

	/**
	 * Get config for wysiwyg
	 *
	 * @param array $data
	 * @return Varien_Object
	 */
	public function getConfig($data = array()) {
		$config = new Varien_Object();

		$config->setData(array(
			'enabled'                       => $this->isEnabled(),
			'hidden'                        => $this->isHidden(),
			'use_container'                 => false,
			'add_variables'                 => false,
			'add_variables_itoris'          => true,
			'add_widgets'                   => true,
			'no_display'                    => false,
			'translator'                    => Mage::helper('cms'),
			'encode_directives'             => true,
			'directives_url'                => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg/directive'),
			'popup_css'                     => Mage::getBaseUrl('js').'mage/adminhtml/wysiwyg/tiny_mce/themes/advanced/skins/default/dialog.css',
			'content_css'                   => Mage::getBaseUrl('js').'mage/adminhtml/wysiwyg/tiny_mce/themes/advanced/skins/default/content.css',
			'width'                         => '100%',
			'plugins'                       => array()
		));

		$config->setData('directives_url_quoted', preg_quote($config->getData('directives_url')));

		if (Mage::getSingleton('admin/session')->isAllowed('cms/media_gallery')) {
			$config->addData(array(
				'add_images'                  => true,
				'files_browser_window_url'    => Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/cms_wysiwyg_images/index'),
				'files_browser_window_width'  => (int) Mage::getConfig()->getNode('adminhtml/cms/browser/window_width'),
				'files_browser_window_height' => (int) Mage::getConfig()->getNode('adminhtml/cms/browser/window_height'),
			));
		}

		if (is_array($data)) {
			$config->addData($data);
		}

		Mage::dispatchEvent('cms_wysiwyg_config_prepare', array('config' => $config));
		Mage::dispatchEvent('itoris_pagetabs_cms_wysiwyg_config_prepare_after', array('config' => $config));

		return $config;
	}
}
?>