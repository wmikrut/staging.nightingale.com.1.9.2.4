<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGECONTENTSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PageContentSlider_Adminhtml_Itorispagecontentslider_SlideController extends Itoris_PageContentSlider_Controller_Admin_Controller {

	public function indexAction() {
		$this->loadLayout();
		$this->renderLayout();
	}

	public function newAction() {
		$this->_redirect('*/*/edit');
	}

	public function editAction() {
		$slide = Mage::getModel('itoris_pagecontentslider/slide')->load($this->getRequest()->getParam('id'));
		Mage::register('current_slide', $slide);

		$this->loadLayout();
		$this->renderLayout();
	}

	public function saveAction() {
		$params = $this->getRequest()->getPost('slide', array());
		if (!is_array($params)) {
			$params = array();
		}
		try{
			/** @var $slide Itoris_PageContentSlider_Model_Slide */
			$slide = Mage::getModel('itoris_pagecontentslider/slide')->load($this->getRequest()->getParam('id'));
			//start for magento 1.4
			if (isset($params['start_publish_date']) && empty($params['start_publish_date'])) {
				unset($params['start_publish_date']);
				if ($slide->getStartPublishDate()) {
					$slide->setStartPublishDate(null);
				}
			}
			if (isset($params['end_publish_date']) && empty($params['end_publish_date'])) {
				unset($params['end_publish_date']);
				if ($slide->getEndPublishDate()) {
					$slide->setEndPublishDate(null);
				}
			}
			//end for magento 1.4
			$slide->addData($params);
			if (!isset($params['enabled'])) {
				$slide->setEnabled(0);
			}
			$orderSlide = Mage::getModel('itoris_pagecontentslider/slide')->load($slide->getSlideOrder(), 'slide_order');
			if ($orderSlide->getId() && $orderSlide->getId() != $slide->getId()) {
				$maxSlideOrder = $slide->calculateSlideOrder();
				$slide->setSlideOrder($maxSlideOrder);
			}
			$slide->save();

			$this->_getSession()->addSuccess($this->__('Slide have been saved'));
		} catch (Exception $e) {
			Mage::logException($e);
			$this->_getSession()->addError($this->__('Slide have not been saved'));
		}

		if ($this->getRequest()->getParam('back')) {
			$this->_redirect('*/*/edit', array('id' => $slide->getId(), '_current'=>true));
		} else {
			$this->_redirect('*/*');
		}
	}

	public function massDeleteAction() {
		$slideIds = $this->getRequest()->getParam('slide');
		if (!is_array($slideIds)) {
			$this->_getSession()->addError($this->__('Please select slide(s).'));
		} else {
			if (!empty($slideIds)) {
				try {
					foreach ($slideIds as $slideId) {
						$slide = Mage::getModel('itoris_pagecontentslider/slide')->load($slideId);
						$slide->delete();
					}
					$this->_getSession()->addSuccess(
						$this->__('Total of %d record(s) have been deleted.', count($slideIds))
					);
				} catch (Exception $e) {
					Mage::logException($e);
					$this->_getSession()->addError($this->__('An error occurred while deleted the slide(s).'));
				}
			}
		}
		$this->_redirect('*/*/index');
	}

	public function massStatusAction() {
		$slideIds = $this->getRequest()->getParam('slide');
		if (!is_array($slideIds)) {
			$this->_getSession()->addError($this->__('Please select slide(s).'));
		} else {
			if (!empty($slideIds)) {
				try {
					$status = $this->getRequest()->getParam('status');
					foreach ($slideIds as $slideId) {
						$slideModel = Mage::getModel('itoris_pagecontentslider/slide')->load($slideId);
						if ($slideModel->getEnabled() != $status) {
							$slideModel->setEnabled($status);
							$slideModel->save();
						}
					}
					$this->_getSession()->addSuccess(
						$this->__('Total of %d record(s) have been updated.', count($slideIds))
					);

				} catch (Exception $e) {
					Mage::logException($e);
					$this->_getSession()->addError($this->__('An error occurred while updating the slide(s) status.'));
				}
			}
		}

		$this->_redirect('*/*/index');
	}

	public function deleteAction() {
		$slideId = $this->getRequest()->getParam('id');

		try {
			if (!empty($slideId)) {
				$slide = Mage::getModel('itoris_pagecontentslider/slide')->load($slideId);
				if ($slide->getId()) {
					$slide->delete();
					$this->_getSession()->addSuccess($this->__('Record has been deleted.'));
				}
			}
		} catch (Exception $e) {
			Mage::logException($e);
			$this->_getSession()->addError($this->__('An error occurred while deleted the slide.'));
		}
		$this->_redirect('*/*/index');
	}

	public function changeOrderAction() {
		$id = $this->getRequest()->getParam('id');
		try {
			$slide = Mage::getModel('itoris_pagecontentslider/slide')->load($id);
			if ($slide->getId()) {
				$orderDir = $this->getRequest()->getParam('order');
				$collection = $slide->getCollection()->setPageSize(1);
				switch ($orderDir) {
					case 'next':
						$collection->addFieldToFilter('slide_order', array('gt' => $slide->getSlideOrder()))
							->setOrder('slide_order', 'ASC');
						break;
					case 'prev':
						default:
						$collection->addFieldToFilter('slide_order', array('lt' => $slide->getSlideOrder()))
							->setOrder('slide_order', 'DESC');
				}
				if ($collection->getSize()) {
					$item = Mage::getModel('itoris_pagecontentslider/slide')->load($collection->getFirstItem()->getId());
					$orderToSet = $item->getSlideOrder();
					$item->setSlideOrder($slide->getSlideOrder());
					$item->save();
					$slide->setSlideOrder($orderToSet);
					$slide->save();
					$this->_getSession()->addSuccess($this->__('Order has been changed.'));
				}
			}
		} catch (Exception $e) {
			Mage::logException($e);
			$this->_getSession()->addSuccess($this->__('Order has not been changed.'));
		}
		$this->_redirect('*/*/index');
	}

	protected function _isAllowed() {
		return Mage::getSingleton('admin/session')->isAllowed('admin/system/itoris/pagecontentslider/slide');
	}
}
?>