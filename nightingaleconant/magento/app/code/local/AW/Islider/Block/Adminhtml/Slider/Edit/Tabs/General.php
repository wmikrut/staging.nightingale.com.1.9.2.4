<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Block_Adminhtml_Slider_Edit_Tabs_General extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $slider = Mage::registry('current_slider');

        $this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence'));

        $this
            ->_addGeneralFieldsetToForm($form)
            ->_addRepresentationFieldsetToForm($form)
            ->_addGoogleAnalyticsFieldsetToForm($form);

        $form->setValues($slider);
        return parent::_prepareForm();
    }

    protected function _addGeneralFieldsetToForm($form)
    {
        $_fieldset = $form->addFieldset('general_fieldset', array(
                'legend' => $this->__('General')
        ));
        $_fieldset->addField('name', 'text', array(
                'name'     => 'name',
                'label'    => $this->__('Name'),
                'required' => true
        ));
        $_fieldset->addField('block_id', 'text', array(
                'name'     => 'block_id',
                'label'    => $this->__('Block Code ID'),
                'required' => true
        ));
        $_fieldset->addField('is_active', 'select', array(
                'name'     => 'is_active',
                'label'    => $this->__('Status'),
                'required' => true,
                'values'   => Mage::getModel('awislider/source_status')->toOptionArray()
        ));
        $_fieldset->addField('autoposition', 'select', array(
                'name'   => 'autoposition',
                'label'  => $this->__('Automatic layout position'),
                'values' => Mage::getModel('awislider/source_autoposition')->toOptionArray()
        ));
        if (!Mage::app()->isSingleStoreMode()) {
            $_fieldset->addField('store', 'multiselect', array(
                    'name'      => 'store[]',
                    'label'     => $this->__('Store View'),
                    'required'  => TRUE,
                    'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(FALSE, TRUE),
            ));
        }

        return $this;
    }

    protected function _addRepresentationFieldsetToForm($form)
    {
        $_fieldset = $form->addFieldset('representation_fieldset', array(
                'legend' => $this->__('Representation')
        ));
        $_fieldset->addField('nav_autohide', 'select', array(
                'name'   => 'nav_autohide',
                'label'  => $this->__('Auto hide navigation'),
                'values' => Mage::getModel('awislider/source_navigation')->toOptionArray()
        ));
        $_fieldset->addField('switch_effect', 'select', array(
                'name'   => 'switch_effect',
                'label'  => $this->__('Switch effect'),
                'values' => Mage::getSingleton('awislider/source_switcheffects')->toOptionArray()
        ));
        $_fieldset->addField('representation', 'select', array(
                'name'   => 'representation',
                'label'  => $this->__('Representation'),
                'values' => Mage::getSingleton('awislider/source_representation')->toOptionArray()
        ));
        $_fieldset->addField('width', 'text', array(
                'name'     => 'width',
                'required' => true,
                'label'    => $this->__('Width, px'),
                'class'    => 'validate-number validate-zero-or-greater'
        ));
        $_fieldset->addField('height', 'text', array(
                'name'     => 'height',
                'required' => true,
                'label'    => $this->__('Height, px'),
                'class'    => 'validate-number validate-zero-or-greater'
        ));
        $_fieldset->addField('animation_speed', 'text', array(
                'name'     => 'animation_speed',
                'label'    => $this->__('Animation speed, seconds'),
                'required' => true,
                'note'     => $this->__('0 disables animation'),
                'class'    => 'validate-number validate-zero-or-greater'
        ));
        $_fieldset->addField('transition_speed', 'text', array(
                'name'  => 'transition_speed',
                'label' => $this->__('Transition speed, seconds'),
                'class' => 'validate-number validate-greater-than-zero'
        ));
        $_fieldset->addField('first_timeout', 'text', array(
                'name'     => 'first_timeout',
                'label'    => $this->__('First frame timeout, seconds'),
                'required' => true,
                'note'     => $this->__('0 means same as others'),
                'class'    => 'validate-number validate-zero-or-greater'
        ));
        $_fieldset->addField('random_order', 'select', array(
                'name'     => 'random_order',
                'label'    => $this->__('Display Images in Random Order'),
                'values'   => Mage::getSingleton('adminhtml/system_config_source_yesno')->toOptionArray(),
        ));

        $this->getChild('form_after')
            ->addFieldMap('representation', 'representation')
            ->addFieldMap('width', 'width')
            ->addFieldMap('height', 'height')
            ->addFieldDependence('width', 'representation', AW_Islider_Model_Source_Representation::FIXED)
            ->addFieldDependence('height', 'representation', AW_Islider_Model_Source_Representation::FIXED);

        return $this;
    }

    protected function _addGoogleAnalyticsFieldsetToForm($form)
    {
        /** @var AW_Islider_Helper_Ga $gaHelper */
        $gaHelper = Mage::helper('awislider/ga');
        if (!$gaHelper->isInstalled()) {
            return $this;
        }

        /** @var Varien_Data_Form_Element_Fieldset $fieldset */
        $fieldset = $form->addFieldset('ga_fieldset', array(
            'legend' => $this->__('Google Analytics')
        ));

        if ($gaHelper->canUseGa()) {
            $fieldset->addField('ga_category', 'text', array(
                'name' => 'ga_category',
                'label' => $this->__('Category'),
            ));
        } else {
            $fieldset->addField('ga_notavailable', 'note', array(
                'text' => $this->__(
                    'Setup <a target="_blank" href="%s">Google Analytics</a> to use this feature',
                    Mage::helper('adminhtml')->getUrl('adminhtml/system_config/edit', array('section' => 'google'))
                ),
            ));
        }

        return $this;
    }
}