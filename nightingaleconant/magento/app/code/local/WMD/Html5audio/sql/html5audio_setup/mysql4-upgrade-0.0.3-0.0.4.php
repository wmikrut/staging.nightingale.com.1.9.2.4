<?php
/**
 * WMD_Html5audio Installer 0.0.4
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/

// $installer = $this;
$installer = new Mage_Catalog_Model_Resource_Eav_Mysql4_Setup('core_setup');
$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup'); 

$setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'playlist_id', array(
    'group'        => 'HTML5 Audio',
    'label'        => 'Playlist',
    'visible'      => true,
    'required'     => false,
    'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'backend'      => 'html5audio/attribute_backend_playlist',
    'type'         => 'int',
    'input'        => 'select',
    'source'       => 'html5audio/attribute_source_playlist',
    'user_defined' => true, 
    'searchable'   => false,
    'filterable'   => false,
    'comparable'   => false,
    'is_visible_on_front'     => false,
    'visible_on_front'        => false,
    'used_in_product_listing' => true,  
    'is_used_in_product_listing' => true,   
));

$installer->endSetup(); 

