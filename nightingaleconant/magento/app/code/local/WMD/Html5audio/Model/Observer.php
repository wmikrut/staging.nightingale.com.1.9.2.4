<?php
/**
 * WMD_Html5audio_Model_Observer
 *
 * WMD Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2014 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Model_Observer extends Mage_Core_Model_Abstract
{
    public function insertBlock($observer)
    {
        /** @var $_block Mage_Core_Block_Abstract */
        /*Get block instance*/
        $_block = $observer->getBlock();
        /*get Block type*/
        $_type = $_block->getType();
       /*Check block type*/
        if ($_type == 'html5audio/adminhtml_playlist_edit_tab_sample') {
            if (!Mage::app()->getRequest()->isXmlHttpRequest()) 
            {
                /*Clone block instance*/
                $_files = clone $_block;
                /*set another type for block*/
                $_files->setType('html5audio/adminhtml_playlist_edit_tab_sample_form');
                /*set child for block*/
                $_block->setChild('files', $_files);
                /*set our template*/
                $_block->setTemplate('html5audio/playlist/files.phtml');
            }
        }
                      
//         if ($_type == 'catalog/product_view') {
//             if ('catalog/product/view/addtocart.phtml' == $_block->getTemplate())
//             {           
//                 if ('downloadable' == Mage::registry('current_product')->getTypeId())
//                 {
//                     $_block->setTemplate('catalog/product/view/dladdtocart.phtml');
//                 }
//             }  
//         }
              
    }    
}
