<?php
/**
 * WMD_Html5audio_Block_Adminhtml_Sample_New
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Block_Adminhtml_Sample_New extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Construct
     *
     * @return void
     */	
    public function __construct()
    {
        parent::__construct();
		
        $this->_blockGroup = 'html5audio';
        $this->_mode = 'new';
        $this->_controller = 'adminhtml_sample';
		
    		$this->_updateButton('save', 'label', Mage::helper('html5audio')->__('Save'));
    		
    }

    /**
     * Return Header Text
     *
     * @return string
     */	 
    public function getHeaderText()
    {
        return Mage::helper('html5audio')->__('New Sample');
    }
}
