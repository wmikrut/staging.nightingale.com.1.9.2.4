<?php
/**
 * WMD_Html5audio_Model_Sample
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2013 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/

class WMD_Html5audio_IndexController extends Mage_Core_Controller_Front_Action
{   
    
    
	  /**
     * 
     *
     * @return void
     */	
    public function indexAction()
    {
  		  	
    }    
    
	  /**
     * Stream file 
     *
     * @return void
     */	
    public function streamAction()
    {
        $data = $this->getRequest()->getParams();         
        $sample = Mage::getModel('html5audio/sample')->loadByHash($data['file']);                      
        if ($sample->getSampleId())
        {
            $path = $mime = '';
            switch ($data['mime'])
            {
                case 'oga':
                    $path = $sample->getOggPath();
                    $mime = 'audio/ogg';
                    break;
                case 'mp3':
                    $path = $sample->getMp3Path();
                    $mime = 'audio/mpeg';
            }
            
            $path_parts = @pathinfo($path);
                        
            $file = Mage::getBaseDir('media') . DS . 'html5audio' . $path;
            if (file_exists($file))
            {
                $size = filesize($file);                
              	$begin	= 0;
              	$end	= $size - 1;
                $responseCode = 200;
                if (isset($_SERVER['HTTP_RANGE']))
              	{
              		if (preg_match('/bytes=\h*(\d+)-(\d*)[\D.*]?/i', $_SERVER['HTTP_RANGE'], $matches))
              		{
              			$begin	= intval($matches[1]);
              			if (!empty($matches[2]))
              			{
              				$end	= intval($matches[2]);
              			}
              		}
              		$responseCode = 206;             		
              	}
                
                
                $this->getResponse()
                    ->setHttpResponseCode($responseCode)
                    ->setHeader('Pragma', 'public', true)
                    ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
                    ->setHeader('Accept-Ranges', 'Bytes', true)
                    ->setHeader('Content-type', $mime, true)
                    ->setHeader('Content-Length', $size);
//                     ->setHeader('Content-Disposition', 'inline; filename='.$path_parts['basename']);
            
              	if (isset($_SERVER['HTTP_RANGE']))
              	{
                		$this->getResponse()
                        ->setHeader('Content-Range', 'bytes ' . $begin . '-' . $end . '/' . $size, true);
              	}
            
                $this->getResponse()
                    ->clearBody();
                $this->getResponse()
                    ->sendHeaders();
                    
                $handle = new Varien_Io_File();
                $handle->streamOpen($file, 'r');
                
                while ($buffer = $handle->streamRead()) {
                    print $buffer;
                }
                exit;
            } 
            else
            {
                Mage::getSingleton('customer/session')->addError(
                    Mage::helper('html5audio')->__('The file to was not found.')
                ); 
                $this->_redirectReferer();
            }
        } 
        else
        {
            Mage::getSingleton('customer/session')->addError(
                Mage::helper('html5audio')->__('The file reference was not found.')
            );
            $this->_redirectReferer(); 
        }
       
    }    
    
    
}
