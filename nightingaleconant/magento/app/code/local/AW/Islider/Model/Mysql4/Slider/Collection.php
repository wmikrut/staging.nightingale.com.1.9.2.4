<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Model_Mysql4_Slider_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('awislider/slider');
    }
    
    protected function _afterLoad()
    {
        foreach($this->getItems() as $_item) {
            if(is_string($_item->getData('store'))) {
                $_item->setData('store', @explode(',', $_item->getData('store')));
            }
        }
        return parent::_afterLoad();
    }

    public function addStoreFilter($storeIds)
    {
        $query = '(FIND_IN_SET(0, store) ';
        foreach ($storeIds as $storeId) {
            $query .= 'OR FIND_IN_SET(' . $storeId . ', store) ';
        }
        $query .= ')';
        $this
            ->getSelect()
            ->where($query)
        ;
        return $this;
    }
    
    public function addIdFilter($id)
    {
        if (null === $id) {
            $id = -1;
        }
        return $this->addFieldToFilter('id', $id);
    }

    public function addFilterByNonId($id)
    {
        if (null === $id) {
            $id = -1;
        }
        return $this->addFieldToFilter('id', array('neq' => $id));
    }

    public function addBlockIdFilter($blockId)
    {
        if (null === $blockId) {
            $blockId = -1;
        }
        return $this->addFieldToFilter('block_id', $blockId);
    }

    public function addEnabledFilter()
    {
        return $this->addFieldToFilter('is_active', AW_Islider_Model_Source_Status::ENABLED);
    }

    public function addPositionFilter($position)
    {
        return $this->addFieldToFilter('autoposition', $position);
    }
    
    /**
     * Covers bug in Magento function
     * @return Varien_Db_Select
     */
    /**public function getSelectCountSql(){
        //todo
        $this->_renderFilters();
        $countSelect = clone $this->getSelect();
        return $countSelect->reset()->from($this->getSelect(), array())->columns('COUNT(*)');
    }**/
}
