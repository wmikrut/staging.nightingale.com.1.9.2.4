function itoris_toogleFieldEditMode(toogleIdentifier, fieldContainer) {
	if ($(toogleIdentifier).checked) {
		$(fieldContainer).disabled = true;
    } else {
        $(fieldContainer).disabled = false;
    }
}