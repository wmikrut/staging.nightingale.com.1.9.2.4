<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGETABS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */


class Itoris_PageTabs_Model_Observer {

	/**
	 * Add variable plugin config and additional settings for Widgets Insertion Plugin to wysiwyg config
	 *
	 * @param Varien_Event_Observer $observer
	 * @return Itoris_PageContentSlider_Model_Observer
	 */
	public function wysiwygConfigPrepare(Varien_Event_Observer $observer) {
		$config = $observer->getEvent()->getConfig();

		if ($config->getData('add_variables_itoris')) {
			$settings = Mage::getModel('itoris_pagetabs/wysiwyg_variable')->getWysiwygPluginSettings($config);
			$config->addData($settings);
		}
		if ($config->getData('add_widgets')) {
			$settings = Mage::getModel('itoris_pagetabs/wysiwyg_widget')->getPluginSettings($config);
			$config->addData($settings);
		}
		return $this;
	}
}
?>