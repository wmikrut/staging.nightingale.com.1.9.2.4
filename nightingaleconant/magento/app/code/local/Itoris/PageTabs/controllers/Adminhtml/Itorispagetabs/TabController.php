<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGETABS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */


class Itoris_PageTabs_Adminhtml_Itorispagetabs_TabController extends Itoris_PageTabs_Controller_Admin_Controller {

	public function indexAction() {
		$this->loadLayout();
		$this->renderLayout();
	}

	public function newAction() {
		$this->_redirect('*/*/edit');
	}

	public function editAction() {
		$tab = Mage::getModel('itoris_pagetabs/tab')->load($this->getRequest()->getParam('id'));
		Mage::register('current_tab', $tab);

		$this->loadLayout();
		$this->renderLayout();
	}

	public function saveAction() {
		$params = $this->getRequest()->getPost('tab', array());
		if (!is_array($params)) {
			$params = array();
		}
		try{
			/** @var $tab Itoris_PageTabs_Model_Tab */
			$tab = Mage::getModel('itoris_pagetabs/tab')->load($this->getRequest()->getParam('id'));
			//start for magento 1.4
			if (isset($params['start_publish_date']) && empty($params['start_publish_date'])) {
				unset($params['start_publish_date']);
				if ($tab->getStartPublishDate()) {
					$tab->setStartPublishDate(null);
				}
			}
			if (isset($params['end_publish_date']) && empty($params['end_publish_date'])) {
				unset($params['end_publish_date']);
				if ($tab->getEndPublishDate()) {
					$tab->setEndPublishDate(null);
				}
			}
			//end for magento 1.4
			$tab->addData($params);
			if (!isset($params['enabled'])) {
				$tab->setEnabled(0);
			}
			$orderTab = Mage::getModel('itoris_pagetabs/tab')->load($tab->getTabOrder(), 'tab_order');
			if ($orderTab->getId() && $orderTab->getId() != $tab->getId()) {
				$maxTabOrder = $tab->calculateTabOrder();
				$tab->setTabOrder($maxTabOrder);
			}
			$tab->save();

			$this->_getSession()->addSuccess($this->__('Tab have been saved'));
		} catch (Exception $e) {
			Mage::logException($e);
			$this->_getSession()->addError($this->__('Tab have not been saved'));
		}

		if ($this->getRequest()->getParam('back')) {
			$this->_redirect('*/*/edit', array('id' => $tab->getId(), '_current'=>true));
		} else {
			$this->_redirect('*/*');
		}
	}

	public function massDeleteAction() {
		$tabIds = $this->getRequest()->getParam('tab');
		if (!is_array($tabIds)) {
			$this->_getSession()->addError($this->__('Please select tab(s).'));
		} else {
			if (!empty($tabIds)) {
				try {
					foreach ($tabIds as $tabId) {
						$tab = Mage::getModel('itoris_pagetabs/tab')->load($tabId);
						$tab->delete();
					}
					$this->_getSession()->addSuccess(
						$this->__('Total of %d record(s) have been deleted.', count($tabIds))
					);
				} catch (Exception $e) {
					Mage::logException($e);
					$this->_getSession()->addError($this->__('An error occurred while deleted the tab(s).'));
				}
			}
		}
		$this->_redirect('*/*/index');
	}

	public function massStatusAction() {
		$tabIds = $this->getRequest()->getParam('tab');
		if (!is_array($tabIds)) {
			$this->_getSession()->addError($this->__('Please select tab(s).'));
		} else {
			if (!empty($tabIds)) {
				try {
					$status = $this->getRequest()->getParam('status');
					foreach ($tabIds as $tabId) {
						$tabModel = Mage::getModel('itoris_pagetabs/tab')->load($tabId);
						if ($tabModel->getEnabled() != $status) {
							$tabModel->setEnabled($status);
							$tabModel->save();
						}
					}
					$this->_getSession()->addSuccess(
						$this->__('Total of %d record(s) have been updated.', count($tabIds))
					);

				} catch (Exception $e) {
					Mage::logException($e);
					$this->_getSession()->addError($this->__('An error occurred while updating the tab(s) status.'));
				}
			}
		}

		$this->_redirect('*/*/index');
	}

	public function deleteAction() {
		$tabId = $this->getRequest()->getParam('id');

		try {
			if (!empty($tabId)) {
				$tab = Mage::getModel('itoris_pagetabs/tab')->load($tabId);
				if ($tab->getId()) {
					$tab->delete();
					$this->_getSession()->addSuccess($this->__('Record has been deleted.'));
				}
			}
		} catch (Exception $e) {
			$this->_getSession()->addError($this->__('An error occurred while deleted the tab.'));
		}
		$this->_redirect('*/*/index');
	}

	public function changeOrderAction() {
		$id = $this->getRequest()->getParam('id');
		try {
			$tab = Mage::getModel('itoris_pagetabs/tab')->load($id);
			if ($tab->getId()) {
				$orderDir = $this->getRequest()->getParam('order');
				$collection = $tab->getCollection()->setPageSize(1);
				switch ($orderDir) {
					case 'next':
						$collection->addFieldToFilter('tab_order', array('gt' => $tab->getTabOrder()))
							->setOrder('tab_order', 'ASC');
						break;
					case 'prev':
					default:
						$collection->addFieldToFilter('tab_order', array('lt' => $tab->getTabOrder()))
							->setOrder('tab_order', 'DESC');
				}
				if ($collection->getSize()) {
					$item = Mage::getModel('itoris_pagetabs/tab')->load($collection->getFirstItem()->getId());
					$orderToSet = $item->getTabOrder();
					$item->setTabOrder($tab->getTabOrder());
					$item->save();
					$tab->setTabOrder($orderToSet);
					$tab->save();
					$this->_getSession()->addSuccess($this->__('Order has been changed.'));
				}
			}
		} catch (Exception $e) {
			Mage::logException($e);
			$this->_getSession()->addSuccess($this->__('Order has not been changed.'));
		}
		$this->_redirect('*/*/index');
	}

	protected function _isAllowed() {
		return Mage::getSingleton('admin/session')->isAllowed('admin/system/itoris_extensions/pagetabs/tab');
	}
}
?>