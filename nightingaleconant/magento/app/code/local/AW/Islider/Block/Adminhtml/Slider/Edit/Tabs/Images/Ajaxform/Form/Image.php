<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Block_Adminhtml_Slider_Edit_Tabs_Images_Ajaxform_Form_Image extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $this->setChild('form_after', $this->getLayout()->createBlock('adminhtml/widget_form_element_dependence'));

        $this
            ->_addGeneralFieldsetToForm($form)
            ->_addUrlSettingsFieldsetToForm($form)
            ->_addGoogleAnalyticsFieldsetToForm($form);

        $formData = Mage::registry('current_image_slider');
        $formData->setData('image_type', $formData->getData('type'));
        $formData->setData('image_remote',
            $formData->getType() == AW_Islider_Model_Source_Images_Type::REMOTEURL ? $formData->getLocation() : '');
        $form->setValues($formData);

        return parent::_prepareForm();
    }

    protected function _addGeneralFieldsetToForm($form)
    {
        $formData = Mage::registry('current_image_slider');
        $fieldset = $form->addFieldset('image_general_fieldset', array(
            'legend' => $this->__('General Information')
        ));
        $fieldset->addField('pid', 'hidden', array(
            'name' => 'pid',
        ));
        $fieldset->addField('image_type', 'select', array(
            'name' => 'type',
            'label' => $this->__('Image Type'),
            'values' => Mage::getModel('awislider/source_images_type')->toOptionArray()
        ));
        $fieldset->addField('image_file', 'file', array(
            'name' => 'image_file',
            'label' => $this->__('Image'),
            'required' => ($formData->getType() == AW_Islider_Model_Source_Images_Type::FILE && !$formData->getLocation() ?true:false),
            'note' => $formData->getType() == AW_Islider_Model_Source_Images_Type::FILE ? $this->__('Current file: %s', $formData->getLocation()) : ''
        ));
        $fieldset->addField('image_remote', 'text', array(
            'name' => 'image_remote',
            'label' => $this->__('Image'),
            'required' => true,
        ));
        $fieldset->addField('title', 'text', array(
            'name' => 'title',
            'label' => $this->__('Image Title'),
            'required' => false
        ));
        $fieldset->addField('status', 'select', array(
            'name' => 'status',
            'label' => $this->__('Status'),
            'values' => Mage::getModel('awislider/source_status')->toOptionArray()
        ));

        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
        $fieldset->addField('active_from', 'date', array(
            'name' => 'active_from',
            'label' => $this->__('Date From'),
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format' => $dateFormatIso,
            'locale' => Mage::app()->getLocale()->getLocaleCode(),
            'required' => false
        ));
        $fieldset->addField('active_to', 'date', array(
            'name' => 'active_to',
            'label' => $this->__('Date To'),
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format' => $dateFormatIso,
            'locale' => Mage::app()->getLocale()->getLocaleCode(),
            'required' => false
        ));
        $fieldset->addField('sort_order', 'text', array(
            'name' => 'sort_order',
            'label' => $this->__('Sort Order'),
            'required' => true
        ));

        $this->getChild('form_after')
            ->addFieldMap('image_type', 'image_type')
            ->addFieldMap('image_file', 'image_file')
            ->addFieldMap('image_remote', 'image_remote')
            ->addFieldDependence('image_file', 'image_type', AW_Islider_Model_Source_Images_Type::FILE)
            ->addFieldDependence('image_remote', 'image_type', AW_Islider_Model_Source_Images_Type::REMOTEURL);

        return $this;
    }

    protected function _addUrlSettingsFieldsetToForm($form)
    {
        $fieldset = $form->addFieldset('url_settings_fieldset', array(
            'legend' => $this->__('URL Settings')
        ));
        $fieldset->addField('url', 'text', array(
            'name' => 'url',
            'label' => $this->__('URL')
        ));
        $fieldset->addField('new_window', 'select', array(
            'name' => 'new_window',
            'label' => $this->__('Open URL in new window'),
            'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray()
        ));
        $fieldset->addField('nofollow', 'select', array(
            'name' => 'nofollow',
            'label' => $this->__('Add \'No follow\' to URL'),
            'values' => Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray()
        ));

        return $this;
    }

    protected function _addGoogleAnalyticsFieldsetToForm($form)
    {
        /** @var AW_Islider_Helper_Ga $gaHelper */
        $gaHelper = Mage::helper('awislider/ga');
        if (!$gaHelper->isInstalled()) {
            return $this;
        }

        /** @var Varien_Data_Form_Element_Fieldset $fieldset */
        $fieldset = $form->addFieldset('ga_fieldset', array(
            'legend' => $this->__('Google Analytics')
        ));

        if ($gaHelper->canUseGa()) {
            $fieldset->addField('ga_value', 'text', array(
                'name' => 'ga_value',
                'label' => $this->__('Value'),
                'note' => $this->__('Please enter a number 0 or greater in this field.')
            ));
        } else {
            $fieldset->addField('ga_notavailable', 'note', array(
                'text' => $this->__(
                    'Setup <a target="_blank" href="%s">Google Analytics</a> to use this feature',
                    Mage::helper('adminhtml')->getUrl('adminhtml/system_config/edit', array('section' => 'google'))
                ),
            ));
        }

        return $this;
    }
}