<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Model_Mysql4_Image_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    const DATE_PHP_FORMAT           = 'Y-m-d';

    public function _construct()
    {
        parent::_construct();
        $this->_init('awislider/image');
    }

    public function sortBySortOrder($sort = Zend_Db_Select::SQL_ASC)
    {
        return $this->setOrder('sort_order', $sort);
    }

    public function sortByRandom()
    {
        return $this->setOrder('rand()');
    }

    public function addActualDateFilter()
    {

        $this->addFieldToFilter('active_from',
            array(
                array('lteq' => Mage::getModel('core/date')->gmtDate(self::DATE_PHP_FORMAT)),
                array('null' => '')))
            ->addFieldToFilter('active_to',
                array(
                    array('gteq' => Mage::getModel('core/date')->gmtDate(self::DATE_PHP_FORMAT)),
                    array('null' => ''))
            );
        return $this;
    }
    
    public function addSliderFilter($sliderId)
    {
        if (null === $sliderId) {
            $sliderId = -1;
        }
        return $this->addFieldToFilter('pid', $sliderId);
    }

    public function addStoreFilter($storeIds)
    {
        $query = '(FIND_IN_SET(0, store) ';
        foreach ($storeIds as $storeId) {
            $query .= 'OR FIND_IN_SET(' . $storeId . ', store) ';
        }
        $query .= ')';
        $this
            ->getSelect()
            ->where($query)
        ;
        return $this;
    }

    public function addActiveFilter()
    {
        return $this->addFieldToFilter('status', AW_Islider_Model_Source_Status::ENABLED);
    }
}