<?php
/**
 * Pulsar Plugins
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.pulsarplugins.com/pulsarplugins-license.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento community edition
 * Pulsar Plugins does not guarantee correct work of this extension
 * on any other Magento edition except Magento community edition.
 * Pulsar Plugins does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   PP
 * @package    PP_EmailByShipping
 * @version    1.1
 * @copyright  Copyright (c) 2010-2014 Pulsar Plugins (http://www.pulsarplugins.com/)
 * @license    http://www.pulsarplugins.com/pulsarplugins-license.txt
 *
*/
class PP_EmailByShipping_Block_Dataarray extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    protected $magentoAttributes;
 
    public function __construct()
    {
		$this->addColumn('customer_type', array(
            'label' => Mage::helper('adminhtml')->__('Customer Type'),
            'size'  => 5,
        ));
        $this->addColumn('shipping_code', array(
            'label' => Mage::helper('adminhtml')->__('Shipping Method'),
            'size'  => 22,
        ));
        $this->addColumn('template_id', array(
            'label' => Mage::helper('adminhtml')->__('Order Confirmation E-mail Template'),
            'size'  => 22
        ));
		$this->addColumn('shipping_template_id', array(
            'label' => Mage::helper('adminhtml')->__('Shipping Confirmation E-mail Template'),
            'size'  => 22
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('adminhtml')->__('Add new template');
 
        parent::__construct();
        $this->setTemplate('emailbyshipping/array_dropdown.phtml');
    }
 
    protected function _renderCellTemplate($columnName)
    {
        if (empty($this->_columns[$columnName])) {
            throw new Exception('Wrong column name specified.');
        }
        $column     = $this->_columns[$columnName];
        $inputName  = $this->getElement()->getName() . '[#{_id}][' . $columnName . ']';
 
 
        $rendered = '<select name="'.$inputName.'">';
        if($columnName == 'shipping_code')
        {
			$options	= Mage::helper('ordermailtable')->getAllShippingMethods();
			foreach ($options as $junk=>$shippingData) {
				$label	= $shippingData['label'];
				$code	= $shippingData['value'][0]['value'];
				$rendered .= '<option value="'.$code.'">'.$label.'</option>';
			}
        }
        else if ($columnName == 'template_id') 
        {
			$options	= Mage::helper('ordermailtable')->getAllEmailTemplates();
			foreach ($options as $key=>$data) {
				$rendered .= '<option value="'.$data['value'].'">'.$data['label'].'</option>';
            }
        } else if ($columnName == 'shipping_template_id') {
			$options	= Mage::helper('ordermailtable')->getAllEmailTemplates();
			foreach ($options as $key=>$data) {
				$rendered .= '<option value="'.$data['value'].'">'.$data['label'].'</option>';
            }
		} else {
			$rendered .= '<option value="Registered">'.__('Registered').'</option>';
			$rendered .= '<option value="Guest">'.__('Guest').'</option>';
		}
        $rendered .= '</select>';
 
        return $rendered;
    }
}
?>