<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Block_Catalog_Product_List_Package extends Mage_Catalog_Block_Product_Abstract
{
    protected $_mapRenderer = 'msrp_noform';

    protected $_itemCollection;

    protected $_itemPackageCollection;

    protected $_packageQty;

    protected function _prepareData()
    {
        $product = Mage::registry('product');

        $this->_itemCollection = $product->getPackageProductCollection()
            ->addAttributeToSelect('required_options')
            ->setPositionOrder()
            ->addStoreFilter();

        foreach ($this->_itemCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }

        return $this;
    }

    protected function _beforeToHtml()
    {
        $this->_prepareData();
        return parent::_beforeToHtml();
    }

    public function getItemPackageQty($id, $item) {

    }

    //Get Associated Products
    public function getItems()
    {
        $data = array();
        $productId = array();

        foreach($this->_itemCollection as $col) {
            if(!in_array($col->getId(), $productId)) {
                $data[] = array(
                    'package_qty' => $col->getPackageQty(),
                    'product_id' => $col->getId()
                );
                $productId[] = $col->getId();
            }
        }

        $collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('entity_id', array('in' => $productId));
        $collection->setDataToAll('package_qty', $data);


        if($collection->getSize()){
            return $collection;
        }


        return null;
    }

    public function getTest() {
        return true;
    }

    public function isGridEnabled() {
        if(Mage::getStoreConfig('package/settings/enabled')){
            return true;
        } else {
            return false;
        }
    }
}