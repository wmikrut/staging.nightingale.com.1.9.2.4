<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_CATEGORYBANNERSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_CategoryBannerSlider_Helper_Data extends Mage_Core_Helper_Abstract {

	protected $alias = 'categorybannerslider';

	/**
	 * Check is magento version lower than 1.4.0.0
	 *
	 * @param string $compareVersion
	 * @return bool
	 */
	public function isOldVersion($compareVersion = '1.4.0') {
		if (version_compare(Mage::getVersion(),$compareVersion, '<')) {
			return true;
		} else {
			return false;
		}
	}

	public function isAdminRegistered() {
		try {
			return Itoris_Installer_Client::isAdminRegistered($this->getAlias());
		} catch (Exception $e) {
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			return false;
		}
	}

	public function isRegisteredAutonomous($website = null) {
		return Itoris_Installer_Client::isRegisteredAutonomous($this->getAlias(), $website);
	}

	public function registerCurrentStoreHost($sn) {
		return Itoris_Installer_Client::registerCurrentStoreHost($this->getAlias(), $sn);
	}

	public function isRegistered($website) {
		return Itoris_Installer_Client::isRegistered($this->getAlias(), $website);
	}

	public function getAlias() {
		return $this->alias;
	}

	/**
	 * Retrieve category children for the store
	 *
	 * @param $storeId
	 * @param $categoryId
	 * @return array
	 */
	public function getWebsiteStoreCategories($storeId, $categoryId) {
		$dataRow = array();
		/** @var $store Mage_Core_Model_Store */
		$store = Mage::getModel('core/store')->load($storeId);
		$dataRow['website'] = $store->getWebsite()->getName();
		$dataRow['website_id'] = $store->getWebsite()->getId();
		$dataRow['store'] = $store->getName();
		$dataRow['store_id'] = $storeId;
		$rootCategoryId = $store->getRootCategoryId();
		$categories = array();

		/** @var $categoryEntity Mage_Catalog_Model_Category */
		$categoryEntity = Mage::getModel('catalog/category')->load($categoryId);
		$dataRow['current_category']['id'] = $categoryEntity->getId();
		if ($categoryEntity->getId() == 1) {
			$dataRow['current_category']['url'] = $store->getUrl();
			$dataRow['current_category']['name'] = $this->__('Home Page');
		} else {
			$url = $categoryEntity->setStoreId($store->getId())->getUrl();
			if (strpos($url, '?') === false) {
				$url .= '?';
			} else {
				$url .= '&';
			}
			$url .= '___store=' . $store->getCode();
			$dataRow['current_category']['name'] = $categoryEntity->getName();
			$dataRow['current_category']['url'] = $url;
		}

		$parentCategories = array();
		$categoryModel = $categoryEntity->getParentCategory();
		while ($categoryModel->getId() && $categoryModel->getParentCategory()->getId()) {
			array_unshift($parentCategories, $categoryModel->getName());
			$categoryModel = $categoryModel->getParentCategory();
		}
		$parentCategories[] = $categoryEntity->getName();
		if (isset($parentCategories[0])) {
			unset($parentCategories[0]);
		}

		$dataRow['categories'] = implode(': ', $parentCategories);

		return $dataRow;
	}

	/**
	 * Convert string 'store_id-category_id,store_id-category_id,...' to array
	 *
	 * @param $categories
	 * @return array|null
	 */
	public function prepareCategories($categories) {
		if (empty($categories)) {
			return null;
		}
		$categories = explode(',', $categories);
		$data = array();
		foreach ($categories as $category) {
			if (empty($category)) {
				continue;
			}
			$category = explode('-', $category);
			$storeId = $category[0];
			$categoryId = $category[1];

			$data[] = $this->getWebsiteStoreCategories($storeId, $categoryId);
		}
		$data = $this->sortCategoriesArray($data);
		return $data;
	}

	/**
	 * Sort categories array by website name, store name, category name
	 *
	 * @param $data
	 * @return mixed
	 */
	public function sortCategoriesArray($data){
		foreach ($data as $key => $row) {
			$website[$key] = $row['website'];
			$store[$key] = $row['store'];
			$categories[$key] = $row['categories'];
		}
		array_multisort($website, SORT_ASC,SORT_STRING,
				$store,SORT_ASC,SORT_STRING,
				$categories,SORT_ASC,SORT_STRING,
				$data
		);
		return $data;
	}
}
 
?>