<?php

class WP_AdvancedSliderAddonExportImport_Helper_Data extends Mage_Core_Helper_Abstract
{
    private $_sliderStyles = null;

    public function isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('cms/advancedslider/sliders');
    }

    public function getSliderStyles()
    {
        if (is_null($this->_sliderStyles)) {
            $this->_sliderStyles = Mage::getModel('advancedslider/source_style')->getOptionArray();
        }
        return $this->_sliderStyles;
    }

    public function getSliderLongName($sliderId)
    {
        $styles = $this->getSliderStyles();
        $slider = Mage::getModel('advancedslider/sliders')->load($sliderId);
        $name   = array();
        $name[] = $slider->getName();
        $name[] = $slider->getId();
        $name[] = $styles[$slider->getStyle()];
        $name[] = $slider->getWidth() . 'x' . $slider->getHeight();
        $fw = $slider->getFullWidth();
        if ($fw) $name[] = Mage::helper('advancedslider')->__('FW');
        return implode(' / ', $name);
    }
}
