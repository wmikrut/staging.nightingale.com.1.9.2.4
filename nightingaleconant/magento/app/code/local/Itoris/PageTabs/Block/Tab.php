<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGETABS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */


class Itoris_PageTabs_Block_Tab extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface {

	static protected $widgetCount = 0;
	static protected $cssJsLoaded = false;
	protected $blockId = 'itoris_pagetabs';
	protected $tabs = null;

	public function _construct() {
		parent::_construct();
		if ($this->getDataHelper()->getSettings()->getEnabled() && $this->getDataHelper()->isRegisteredAutonomous()) {
			self::$widgetCount++;
			$this->blockId .= self::$widgetCount;
			$this->setTemplate('itoris/pagetabs/tabs.phtml');
		}
	}

	public function getBlockId() {
		return $this->blockId;
	}

	public function setCssJsLoaded() {
		self::$cssJsLoaded = true;
	}

	public function isCssJsLoaded() {
		return self::$cssJsLoaded;
	}

	public function getTabs() {
		if (is_null($this->tabs)) {
			$this->prepareTabs();
		}

		return $this->tabs;
 	}

	protected function prepareTabs() {
		$instanceId = $this->getInstanceId();
		if (!$instanceId) {
			$instanceId = Mage::getModel('itoris_pagetabs/instance')->getMainInstanceId();
		}
		$this->tabs = Mage::getModel('itoris_pagetabs/tab')->getCollection()
			->addFieldToFilter('enabled', array('eq' => 1))
			->addFieldToFilter('instance', array('eq' => $instanceId))
			->addDateFilter($this->getDataHelper()->getCurrentDate()->toString('Y-MM-dd'))
			->addStoreFilter(Mage::app()->getStore()->getId())
			->addGroupFilter(Mage::getSingleton('customer/session')->getCustomerGroupId())
			->addOrder('tab_order', 'ASC');
	}

	public function getBlockHeight() {
		return $this->getDataHelper()->getSettings()->getHeight();
	}

	public function getBlockConfigJson() {
		$settings = $this->getDataHelper()->getSettings();
		$config = array(
			'slide_effect'         => $settings->getSlideEffect(),
			'switch_on'            => $settings->getSwitchOn(),
			'auto_height'          => (bool)$settings->getAutoHeight(),
			'use_ajax'             => $settings->getLoadMethod() == Itoris_PageTabs_Model_Settings::LOAD_METHOD_AJAX,
			'load_tab_content_url' => $this->getUrl('itoris_pagetabs/tab/load'),
			'uenc'                 => Mage::helper('core/url')->getEncodedUrl(),
			'responsive_width'     => (int)$settings->getResponsiveWidth(),
		);

		return Zend_Json::encode($config);
	}

	public function canLoadProductSliderJs() {
		return Mage::getConfig()->getModuleConfig('Itoris_ProductSlider')->active;
	}

	/**
	 * Prepare and return tab content
	 *
	 * @param $tab
	 * @return string
	 */
	public function getTabContent($tab) {
		return Mage::getModel('widget/template_filter')->filter($tab->getContent());
	}

	/**
	 * Retrieve tab ids sorted by order
	 *
	 * @return array
	 */
	public function getTabIds() {
		$tabIds = array();

		foreach ($this->getTabs() as $tab) {
			$tabIds[] = $tab->getId();
		}

		return $tabIds;
	}

	/**
	 * @return Itoris_PageTabs_Helper_Data
	 */
	public function getDataHelper() {
		return Mage::helper('itoris_pagetabs');
	}
}
?>