<?php

class NC_OrderFulfillment_IndexController extends Mage_Adminhtml_Controller_Action {

    private $orderFulfillment;

    public function _construct() {
        $this->orderFulfillment = Mage::getModel( 'orderfulfillment/orderFulfillment' );
    }

    /**
     * Return some checking result
     *
     * @return void
     */
    public function runexportAction() {

        $this->orderFulfillment->exportData();

        Mage::app()->getResponse()->setBody( '' );
    }

    /**
     * Return some checking result
     *
     * @return void
     */
    public function runimportAction() {

        $this->orderFulfillment->importData();

        Mage::app()->getResponse()->setBody( '' );
    }

    /**
     * Return some checking result
     *
     * @return void
     */
    public function downloadAction() {
        $path = $this->orderFulfillment->getExportDirectory() . DS . 'upload';

        $latest_ctime    = 0;
        $latest_filename = '';
        try {

            $filepath  = '';
            $directory = dir( $path );
            while ( false !== ( $entry = $directory->read() ) ) {
                $filepath = $path . DS . $entry;

                // could do also other checks than just checking whether the entry is a file
                if ( is_file( $filepath ) && filectime( $filepath ) > $latest_ctime ) {
                    $latest_ctime    = filectime( $filepath );
                    $latest_filename = $entry;
                }
            }
            $this->getResponse()
                 ->setHttpResponseCode( 200 )
                 ->setHeader( 'Cache-Control', 'public, must-revalidate, post-check=0, pre-check=0', true )
                 ->setHeader( 'Pragma', 'public', true )
                 ->setHeader( 'Content-type', 'text/csv' )
                 ->setHeader( 'Content-Length', filesize( $filepath ) )
                 ->setHeader( 'Content-Description', "File Transfer" )
                 ->setHeader( 'Content-Disposition', 'attachment; filename="' . basename( $filepath ) . '"' );
            $this->getResponse()->clearBody();
            $this->getResponse()->sendHeaders();
            ob_clean();
            flush();
            readfile( $filepath );
        } catch ( Exception $e ) {
            Mage::app()->getResponse()->setBody( $e );
        }
    }
}