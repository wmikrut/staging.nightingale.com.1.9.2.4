<?php
/**
 * @category  Apptrian
 * @package   Apptrian_Subcategories
 * @author    Apptrian
 * @copyright Copyright (c) 2016 Apptrian (http://www.apptrian.com)
 * @license   http://www.apptrian.com/license Proprietary Software License EULA
 */
class Apptrian_Subcategories_Model_Config_Categoryids 
    extends Mage_Core_Model_Config_Data
{
    
    public function _beforeSave()
    {
    
        $result = $this->validate();
        
        if ($result !== true) {
            
            Mage::throwException(implode("\n", $result));
            
        }
        
        return parent::_beforeSave();
        
    }
    
    public function validate()
    {
        
        $errors    = array();
        $helper    = Mage::helper('apptrian_subcategories');
        $value     = $this->getValue();
        $validator = Zend_Validate::is(
            $value, 'Regex', array('pattern' => '/^[0-9,]*$/')
        );
        
        if (!$validator) {
            $errors[] = $helper->__(
                'Category IDs field is not valid. (Only numbers and commas.)'
            );
        }
        
        if (empty($errors)) {
            return true;
        }
        
        return $errors;
        
    }
    
}
