function getCategoriesDropdown(storeId, catId, emptyRow){
	var first = true;
	if(!storeId){
		return;
	}
	$('category_dropdown').update();
	if(emptyRow){
		var emptyOption = document.createElement('option');
		$('category_dropdown').appendChild(emptyOption);
	}
  	for(var i = 0; i < categories[storeId].length; i++) {
		var categoryId = categories[storeId][i].id;
  		var option = document.createElement('option');
  		Element.extend(option);
		option.value = categoryId;
		if(categoryId == catId){
			option.selected = true;
		}
  		option.update(categories[storeId][i].name);
  		$('category_dropdown').appendChild(option);
		var level = categories[storeId][i].level;
		var margin = (level - 1) * 15 + 'px';
  		option.setStyle({'marginLeft' : margin});
		if(first){
			option.setStyle({fontWeight : 'bold'});
			first = false;
		}
  	}
}