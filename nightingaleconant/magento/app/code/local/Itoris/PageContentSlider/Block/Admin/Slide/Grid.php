<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGECONTENTSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PageContentSlider_Block_Admin_Slide_Grid extends Mage_Adminhtml_Block_Widget_Grid {

	public function __construct() {
		parent::__construct();
		$this->setId('slideGrid');
		$this->setDefaultSort('slide_order');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection() {
		$collection = Mage::getModel('itoris_pagecontentslider/slide')->getCollection();
		$this->setCollection($collection);

		return parent::_prepareCollection();
	}

	protected function _prepareColumns() {
		$this->addColumn('title', array(
			'header' => $this->__('Title'),
			'index'  => 'title',
		));
		$minMaxOrder = Mage::getModel('itoris_pagecontentslider/slide')->getMinMaxOrder();
		$this->addColumn('slide_order', array(
			'header'    => $this->__('Order'),
			'index'     => 'slide_order',
			'type'      => 'number',
			'filter'    => false,
			'min_order' => $minMaxOrder ? $minMaxOrder['min_order'] : null,
			'max_order' => $minMaxOrder ? $minMaxOrder['max_order'] : null,
			'can_order' => $this->getParam($this->getVarNameSort(), $this->_defaultSort) == 'slide_order',
			'order_dir' => $this->getParam($this->getVarNameDir(), $this->_defaultDir),
			'renderer'  => 'itoris_pagecontentslider/admin_grid_column_renderer_order',
		));

		$this->addColumn('status', array(
			'header'  => $this->__('Status'),
			'index'   => 'enabled',
			'type'    => 'options',
			'options' => $this->getStatusOptions(),
		));

		$this->addColumn('date_from', array(
			'header'   => $this->__('Date from'),
			'type'     => 'date',
			'index'    => 'start_publish_date',
		));

		$this->addColumn('date_to', array(
			'header'   => $this->__('Date to'),
			'type'     => 'date',
			'index'    => 'end_publish_date',
		));

		$groups = Mage::getResourceModel('customer/group_collection')
			->load()
			->toOptionHash();
		$groups = array(null => $this->__('All Groups')) + $groups;

		$this->addColumn('slide_customergroup', array(
			'header'   => $this->__('User Groups'),
			'type'     => 'options',
			'index'    => 'user_groups',
			'options'  => $groups,
			'sortable' => false,
			'renderer' => 'itoris_pagecontentslider/admin_grid_column_renderer_options',
			'filter'   => 'itoris_pagecontentslider/admin_grid_column_filter_select',
		));

		$this->addColumn('slide_store', array(
			'header' => $this->__('Store Views'),
			'type'   => 'store',
			'index'  => 'store_views',
			'store_all'     => true,
			'store_view'    => true,
			'sortable'      => false,
			'renderer' => 'itoris_pagecontentslider/admin_grid_column_renderer_store',
			'filter'   => 'itoris_pagecontentslider/admin_grid_column_filter_store',
		));

		$this->addColumn('action',
			array(
				'header'    =>  $this->__('Action'),
				'width'     => '100',
				'type'      => 'action',
				'getter'    => 'getId',
				'actions'   => array(
					array(
						'caption'   => $this->__('Edit'),
						'url'       => array('base'=> '*/*/edit'),
						'field'     => 'id'
					)
				),
				'filter'    => false,
				'sortable'  => false,
				'is_system' => true,
			));

		return parent::_prepareColumns();
	}

	protected function _prepareMassaction() {
		$this->setMassactionIdField('slide_id');
		$this->getMassactionBlock()->setFormFieldName('slide');

		$this->getMassactionBlock()->addItem('delete', array(
			'label'    => $this->__('Delete'),
			'url'      => $this->getUrl('*/*/massDelete'),
			'confirm'  => $this->__('Do you really want to remove selected slides?')
		));

		$statuses = array(
			array('label'=> '', 'value'=> ''),
			array(
				'label' => $this->__('Disabled'),
				'value' => 0,
			),
			array(
				'label' => $this->__('Enabled'),
				'value' => 1,
			)
		);
		$this->getMassactionBlock()->addItem('change_status', array(
			'label'        => Mage::helper('customer')->__('Change status'),
			'url'          => $this->getUrl('*/*/massStatus'),
			'additional'   => array(
				'visibility'    => array(
					'name'     => 'status',
					'type'     => 'select',
					'class'    => 'required-entry',
					'label'    => $this->__('Status'),
					'values'   => $statuses,
				)
			)
		));

		return $this;
	}

	protected function getStatusOptions() {
		return array($this->__('Disabled'), $this->__('Enabled'));
	}

	public function getRowUrl($row) {
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}
}
?>