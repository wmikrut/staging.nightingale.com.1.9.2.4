<?php
class NCCUSTOM_AutoInvoiceDownloadable_Model_Observer {
	public function exportFeed($observer) {		
		
	//$line = "NCCUSTOM Auto Invoice Downloadable Fired";
	//Mage::log($line);
	$order = Mage::getModel('sales/order')->load(Mage::getSingleton('checkout/session')->getLastRealOrderId(), 'increment_id');
        //$order = $observer->getEvent()->getOrder();
        $order_id = $order->getOrderId();
        
        $_items = $order->getAllVisibleItems();
        
        $downloadable = 1;
        
        foreach ( $_items as $item ) {
        	$product_type = $item->getProductType();
        	if ( $product_type != 'downloadable' ) {
        		$downloadable = 0;
        	}
        }
        
        if ( $downloadable == 1 ) {
        	try {
        		if($order->canInvoice()) {
        			$_invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
        			if ($_invoice->getTotalQty()) {
        				$_invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
        				$_invoice->register();
        				$transactionSave = Mage::getModel('core/resource_transaction')
        				->addObject($_invoice)
        				->addObject($_invoice->getOrder());
        				$transactionSave->save();
        				$line = 'Auto Invoice/Capture of '. $order_id. ' successful.';
        				Mage::log($line);                                                                                                                                                  } else {
        					$line = 'NCCUSTOM Cannot Invoice Downloadable Order '. $order_id. '. No items found.';
        					Mage::log($line);
        				}
        		} else {
        			$line = 'NCCUSTOM Cannot Invoice Downloadable Order '. $order_id;
        			Mage::log($line);
        		}
        	} catch (Exception $e) {
        		$line = 'NCCUSTOM Caught exception: '. $e->getMessage();
        		Mage::log($line);
        	}
        } 
    }
}
?>
