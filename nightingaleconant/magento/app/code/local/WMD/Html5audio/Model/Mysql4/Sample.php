<?php
/**
 * WMD_Html5audio_Model_Mysql4_Sample
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Model_Mysql4_Sample extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Construct
     *
     * @return void
     */	
    protected function _construct()
    {
        $this->_init('html5audio/sample', 'sample_id');
    }	
    
    /**
     * Handle random string
     * 
     * @param Mage_Core_Model_Abstract $object
     * @return WMD_Html5audio_Model_Mysql4_Sample
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        $object->setData('hash', Mage::helper('html5audio')->getRandomString());
    }
    
    /**
     * Load link by hash
     *
     * @param WMD_Html5audio_Model_Sample $sample
     * @param string $hash
     * @return WMD_Html5audio_Model_Mysql4_Sample
     */
    public function loadByHash(WMD_Html5audio_Model_Sample $sample, $hash)
    {
        $adapter = $this->_getWriteAdapter();
        $bind    = array('sample_hash' => (string)$hash);
        $select  = $adapter->select()
            ->from($this->getMainTable())
            ->where('hash = :sample_hash');

        $sampleId = $adapter->fetchOne($select, $bind);
        
        if ($sampleId) {
            $this->load($sample, $sampleId);
        } else {        
            $sample->setData(array());
        }
//         $select = $adapter->select()
//             ->from($this->getMainTable(), array('sample_id', 'hash'))
//             ->where('hash = :sample_hash');
//         $bind = array('sample_hash' => (string)$hash);
//         
//         $sample = $adapter->fetchOne($select, $bind);        
// //         return $this->_getWriteAdapter()->fetchPairs($select, $bind);
        return $this;
    }   
    
    public function saveToPlaylist($sampleId, $playlistId)
    {             
        $adapter = $this->_getWriteAdapter();
        $data = array();
        $data[] = array(
            'sample_id' => (int)$sampleId,
            'playlist_id'  => (int)$playlistId,
        );
        $adapter->insertMultiple($this->getTable('html5audio/playlist_sample'), $data);
        return true;
    }
}
