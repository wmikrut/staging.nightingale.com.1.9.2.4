<?php
/**
 * WMD_Html5audio Installer 0.0.1
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/

$installer = $this;
$installer->startSetup();

$installer->run("

CREATE TABLE IF NOT EXISTS {$installer->getTable('html5audio/sample')} (
  `sample_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Sample ID', 
  `hash` varchar(255) DEFAULT NULL COMMENT 'Hash',   
  `mp3_path` varchar(255) DEFAULT NULL COMMENT 'mp3 Path',  
  `ogg_path` varchar(255) DEFAULT NULL COMMENT 'ogg Path',  
  `title` varchar(255) DEFAULT NULL COMMENT 'Title', 
  `date` timestamp NULL DEFAULT NULL COMMENT 'Last Update Time', 
  `position` int(10) DEFAULT NULL COMMENT 'Position',
  `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
  PRIMARY KEY (`sample_id`),
  KEY `IDX_HA_SAMPLE_SAMPLE_ID` (`sample_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='HTML5 Audio Sample Backend Table' ;

");
 
$installer->endSetup(); 

