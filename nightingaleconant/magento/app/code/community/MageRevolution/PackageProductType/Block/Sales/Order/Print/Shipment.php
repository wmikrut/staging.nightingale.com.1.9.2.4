<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Block_Sales_Order_Print_Shipment extends Mage_Sales_Block_Order_Print_Shipment {

    /**
     * Getter for billing address of order by format
     *
     * @param Mage_Sales_Model_Order_Shipment $shipment
     * @return array
     */
    public function getShipmentItems($shipment)
    {
        $res = array();
        $package = false;
        foreach ($shipment->getItemsCollection() as $item) {
            if ($item->getOrderItem()->getParentItem()) {
                $orderItemId = $item->getOrderItemId();
                $order = Mage::getModel('sales/order_item')->load($orderItemId);
                if($order->getParentItemId()){
                    $parentItemId = $order->getParentItemId();
                    $parentProduct = Mage::getModel('sales/order_item')->load($parentItemId);
                    if($parentProduct->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE){
                            $res[] = $item;
                    }
                }
            }
            elseif (!$item->getOrderItem()->getParentItem()) {
                $order = Mage::getModel('sales/order_item')->load($item->getOrderItemId());
                if($order->getProductType() != MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE){
                    $res[] = $item;
                }
            }
        }
        return $res;
    }
}