<?php

require_once 'AdminController.php';

class WP_AdvancedSliderAddonExportImport_ExportController
    extends WP_AdvancedSliderAddonExportImport_AdminController
{
    protected function _init()
    {
        $this->_title($this->__('CMS'))
             ->_title($this->__('WP: Total Slider Manager'))
             ->_title($this->__('Export Sliders'));
        $this->loadLayout();
        $this->_setActiveMenu('cms/advancedslider/sliders');
        $this->renderLayout();
        return $this;
    }

    public function indexAction()
    {
        $sliderId   = $this->getRequest()->getParam('id');
        $ids        = array($sliderId);
        $list       = array();
        foreach ($ids as $id) {
            $name = Mage::helper('advancedslideraddonexportimport')->getSliderLongName($id);
            $info = array(
                'sliderName'    => $name,
                'statusName'    => 'ready',
                'statusMessage' => $this->__('Ready to export'),
            );
            $list[$id] = $info;
        }
        Mage::getSingleton('core/session')->setListOfSliders($list);
        $this->_init();
    }

    public function processAction()
    {
        $list = Mage::getSingleton('core/session')->getListOfSliders();
        foreach ($list as $sliderId => $sliderInfo) {
            if ($list[$sliderId]['statusName'] == 'done') continue;
            try {
                $fileUrl = Mage::getModel('advancedslideraddonexportimport/process')->export($sliderId);
                $list[$sliderId]['statusName']      = 'done';
                $list[$sliderId]['statusMessage']   = '<a href="' . $fileUrl . '">' . $this->__('Download') . '</a>';
                Mage::getSingleton('core/session')->addSuccess($this->__('The slider was successfully exported.'));
            } catch (Exception $e) {
                $list[$sliderId]['statusName']      = 'failed';
                $list[$sliderId]['statusMessage']   = $this->__('Failed');
                Mage::getSingleton('core/session')->addError($e->getMessage());
            }
        }
        Mage::getSingleton('core/session')->setListOfSliders($list);
        $this->_init();
    }
}
