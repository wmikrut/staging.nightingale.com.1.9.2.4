<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGETABS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */


$this->startSetup();

$this->run("

create table {$this->getTable('itoris_pagetabs_view')} (
	`view_id` int unsigned not null auto_increment primary key,
	`scope` enum('default', 'website', 'store') not null,
	`scope_id` int unsigned not null,
	unique(`scope`, `scope_id`)
) engine = InnoDB default charset = utf8;

create table {$this->getTable('itoris_pagetabs_settings')} (
	`setting_id` int unsigned not null auto_increment primary key,
	`view_id` int unsigned not null,
	`product_id` int(10) unsigned null,
	`key` varchar(255) not null,
	`value` int unsigned not null,
	`type` enum('text', 'default') null,
	unique(`view_id`, `key`, `product_id`),
	foreign key (`view_id`) references {$this->getTable('itoris_pagetabs_view')} (`view_id`) on delete cascade on update cascade,
	foreign key (`product_id`) references {$this->getTable('catalog_product_entity')} (`entity_id`) on delete cascade on update cascade
) engine = InnoDB default charset = utf8;

create table {$this->getTable('itoris_pagetabs_settings_text')} (
	`setting_id` int unsigned not null,
	`value` text not null,
	index(`setting_id`),
	foreign key (`setting_id`) references {$this->getTable('itoris_pagetabs_settings')} (`setting_id`) on delete cascade on update cascade
) engine = InnoDB default charset = utf8;

create table {$this->getTable('itoris_pagetabs_tab')} (
	`tab_id` int unsigned not null auto_increment primary key,
	`title` text not null,
	`enabled` bool,
	`start_publish_date` date null,
	`end_publish_date` date null,
	`content` text not null,
	`tab_order` smallint not null
) engine = InnoDB default charset = utf8;

create table {$this->getTable('itoris_pagetabs_tab_store')} (
	`tab_id` int unsigned not null,
	`store_id` smallint(5) unsigned not null,
	foreign key (`tab_id`) references {$this->getTable('itoris_pagetabs_tab')} (`tab_id`) on delete cascade on update cascade,
	foreign key (`store_id`) references {$this->getTable('core_store')} (`store_id`) on delete cascade on update cascade
)engine = InnoDB default charset = utf8;

create table {$this->getTable('itoris_pagetabs_tab_customergroup')} (
	`tab_id` int unsigned not null,
	`group_id` smallint(5) unsigned not null,
	foreign key (`tab_id`) references {$this->getTable('itoris_pagetabs_tab')} (`tab_id`) on delete cascade on update cascade,
	foreign key (`group_id`) references {$this->getTable('customer_group')} (`customer_group_id`) on delete cascade on update cascade
)engine = InnoDB default charset = utf8;
");

$this->endSetup();
?>