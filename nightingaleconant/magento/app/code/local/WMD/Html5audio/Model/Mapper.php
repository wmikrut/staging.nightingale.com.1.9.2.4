<?php
/**
 * WMD_Html5audio_Model_Mapper
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2013 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Model_Mapper extends Mage_Core_Model_Abstract
{
      
    public function _construct()
    {
        parent::_construct();
        $this->_init('html5audio/mapper');
    }  
    
    public function loadByLinkId($id)
    {
        return $this->_getResource()->loadByLinkId($this, $id);
    } 	  
    
    public function generateMappers($postData = array())
    {
        $links = $samples = array();
        $collection = Mage::getModel('downloadable/link')->getCollection()
            ->addFieldToFilter('product_id', $postData['product_id']);
            
        foreach ($collection as $link)
        {
            $links[] = (int) $link->getLinkId();
        }
        
        $playlist = Mage::getModel('html5audio/playlist')->load($postData['playlist_id']);
        $playlistSamples = $playlist->getSamples($playlist);
            
        foreach ($playlistSamples as $key => $value)
        {
            $samples[] = (int) $key;
        }
        
        for ($i = 0; $i < count($links); $i++)
        {
            if (isset($samples[$i]))
            {          
                try {
                    $model = Mage::getModel('html5audio/mapper');
                    $model->setData(array(
                        'link_id' => (int) $links[$i],
                        'sample_id' => (int) $samples[$i], 
                        'product_id' => (int) $postData['product_id'], 
                        'playlist_id' => (int) $postData['playlist_id'], 
                    ));
                    $model->save(); 
                }
                catch (Exception $e)
                {
                
                }
            }
        }
                   
    }
}
