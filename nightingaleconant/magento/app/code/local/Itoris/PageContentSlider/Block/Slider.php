<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGECONTENTSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PageContentSlider_Block_Slider extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface {

	static protected $widgetCount = 0;
	static protected $cssJsLoaded = false;
	protected $blockId = 'itoris_pagecontentslider';
	protected $slides = null;

	public function _construct() {
		parent::_construct();
		if ($this->getDataHelper()->getSettings()->getEnabled() && $this->getDataHelper()->isRegisteredAutonomous()) {
			self::$widgetCount++;
			$this->blockId .= self::$widgetCount;
			$this->setTemplate('itoris/pagecontentslider/slider.phtml');
		}
	}

	public function getBlockId() {
		return $this->blockId;
	}

	public function setCssJsLoaded() {
		self::$cssJsLoaded = true;
	}

	public function isCssJsLoaded() {
		return self::$cssJsLoaded;
	}

	public function getSlides() {
		if (is_null($this->slides)) {
			$this->prepareSlides();
		}

		return $this->slides;
 	}

	protected function prepareSlides() {
		$this->slides = Mage::getModel('itoris_pagecontentslider/slide')->getCollection()
			->addFieldToFilter('enabled', array('eq' => 1))
			->addDateFilter($this->getDataHelper()->getCurrentDate()->toString('Y-MM-dd'))
			->addStoreFilter(Mage::app()->getStore()->getId())
			->addGroupFilter(Mage::getSingleton('customer/session')->getCustomerGroupId())
			->addOrder('slide_order', 'ASC');
	}

	public function getBlockHeight() {
		return $this->getDataHelper()->getSettings()->getHeight();
	}

	public function getBlockConfigJson() {
		$settings = $this->getDataHelper()->getSettings();
		$config = array(
			'mode'                => $settings->getMode(),
			'slide_effect'        => $settings->getSlideEffect(),
			'slide_automatically' => $settings->getSlideAutomatically(),
			'delay'               => $settings->getDelay(),
		);

		return Zend_Json::encode($config);
	}

	/**
	 * Prepare and return slide content
	 *
	 * @param $slide
	 * @return string
	 */
	public function getSlideContent($slide) {
		return Mage::getModel('widget/template_filter')->filter($slide->getContent());
	}

	/**
	 * Retrieve theme code
	 *
	 * @return string
	 */
	public function getTheme() {
		return $this->getDataHelper()->getSettings()->getColorScheme();
	}

	/**
	 * @return Itoris_PageContentSlider_Helper_Data
	 */
	public function getDataHelper() {
		return Mage::helper('itoris_pagecontentslider');
	}
}
?>