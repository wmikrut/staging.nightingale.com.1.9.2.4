<?php
/**
 * Product_import.php
 * CommerceThemes @ InterSEC Solutions LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.commercethemes.com/LICENSE-M1.txt
 *
 * @category   Product
 * @package    Productimport
 * @copyright  Copyright (c) 2003-2009 CommerceThemes @ InterSEC Solutions LLC. (http://www.commercethemes.com)
 * @license    http://www.commercethemes.com/LICENSE-M1.txt
 */ 

class CommerceExtensions_Productimportexport_Model_Convert_Adapter_Productimportskupricegrouppriceonly
extends Mage_Catalog_Model_Convert_Adapter_Product
{
	
	/**
	* Save product (import)
	* 
	* @param array $importData 
	* @throws Mage_Core_Exception
	* @return bool 
	*/
	public function saveRow( array $importData )
	{
		#$product = $this -> getProductModel();
		$product = $this->getProductModel()
            ->reset();
		#$product -> setData( array() );

		$productIDcheckifnew = $product->getIdBySku($importData['sku']);
		if ($productIDcheckifnew) {
			
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			$prefix = Mage::getConfig()->getNode('global/resources/db/table_prefix');
			$sql = "select * from ".$prefix."catalog_product_entity where sku='".strval(trim($importData['sku']))."'";
			
			$rs = $write->fetchAll($sql);
			if($rs)
			{
				//UPDATE FOR PRICE
				if(isset($importData['price']))
				{
					$priceforupdate = $importData['price'];
					
					if(isset($importData['store_id'])) {
						$store_id = $importData['store_id'];
					} else {
						$store_id = '0';
					}
					$prefix = Mage::getConfig()->getNode('global/resources/db/table_prefix');
					$read = Mage::getSingleton('core/resource')->getConnection('core_read');
					$write = Mage::getSingleton('core/resource')->getConnection('core_write');
					$entity_type_id = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
					
					$select_qry = "SELECT ".$prefix."catalog_product_entity.sku, ".$prefix."catalog_product_entity_decimal.value FROM ".$prefix."catalog_product_entity INNER JOIN ".$prefix."catalog_product_entity_decimal ON ".$prefix."catalog_product_entity_decimal.entity_id = ".$prefix."catalog_product_entity.entity_id WHERE ".$prefix."catalog_product_entity_decimal.store_id = '".$store_id."' AND ".$prefix."catalog_product_entity_decimal.attribute_id = (
						 SELECT attribute_id FROM ".$prefix."eav_attribute eav
						 WHERE eav.attribute_code = 'price' AND eav.entity_type_id = '".$entity_type_id."'
						) AND ".$prefix."catalog_product_entity.sku = '".strval(trim($importData['sku']))."'";
			 		
					$rs2 = $read->fetchAll($select_qry);
					
					if($rs2) {	
						#echo "WE HAVE TO UPDATE";
						$write = Mage::getSingleton('core/resource')->getConnection('core_write');
						$write->query("
						  UPDATE ".$prefix."catalog_product_entity_decimal val, ".$prefix."catalog_product_entity prod
						  SET  val.value = '".$priceforupdate."'
						  WHERE val.entity_id = prod.entity_id
					      AND val.store_id = '".$store_id."'
						  AND val.attribute_id = (
							 SELECT attribute_id FROM ".$prefix."eav_attribute eav
							 WHERE eav.entity_type_id = '".$entity_type_id."' 
							   AND eav.attribute_code = 'price'
							)
						  AND prod.sku = '".strval(trim($importData['sku']))."'
						");
						
					} else {		
						#echo "WE HAVE TO INSERT";			
						$entity_type_id = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
						$select_qry =$read->query("SELECT attribute_id FROM `".$prefix."eav_attribute` WHERE `attribute_code`='price' and entity_type_id ='".$entity_type_id."'");
						$row = $select_qry->fetch();
						$attribute_id = $row['attribute_id'];
						
						$write->query("INSERT INTO ".$prefix."catalog_product_entity_decimal (entity_type_id,attribute_id,store_id,entity_id,value) VALUES ('".$entity_type_id."','".$attribute_id."','".$store_id."','".$productIDcheckifnew."','".$priceforupdate."')");
					}
				}//UPDATE FOR SPECIAL PRICE
				if(isset($importData['special_price']) && $importData['special_price'] != "")
				{
					#$specialpriceforupdate = $importData['special_price'];
					$priceforupdate = $importData['special_price'];
					
					if(isset($importData['store_id'])) {
						$store_id = $importData['store_id'];
					} else {
						$store_id = '0';
					}
					$prefix = Mage::getConfig()->getNode('global/resources/db/table_prefix');
					$read = Mage::getSingleton('core/resource')->getConnection('core_read');
					$write = Mage::getSingleton('core/resource')->getConnection('core_write');
					$entity_type_id = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
					
					$select_qry = "SELECT ".$prefix."catalog_product_entity.sku, ".$prefix."catalog_product_entity_decimal.value FROM ".$prefix."catalog_product_entity INNER JOIN ".$prefix."catalog_product_entity_decimal ON ".$prefix."catalog_product_entity_decimal.entity_id = ".$prefix."catalog_product_entity.entity_id WHERE ".$prefix."catalog_product_entity_decimal.store_id = '".$store_id."' AND ".$prefix."catalog_product_entity_decimal.attribute_id = (
						 SELECT attribute_id FROM ".$prefix."eav_attribute eav
						 WHERE eav.attribute_code = 'special_price' AND eav.entity_type_id = '".$entity_type_id."'
						) AND ".$prefix."catalog_product_entity.sku = '".strval(trim($importData['sku']))."'";
			 		
					$rs2 = $read->fetchAll($select_qry);
					
					if($rs2) {	
						#echo "WE HAVE TO UPDATE";
						$write = Mage::getSingleton('core/resource')->getConnection('core_write');
						$write->query("
						  UPDATE ".$prefix."catalog_product_entity_decimal val, ".$prefix."catalog_product_entity prod
						  SET  val.value = '".$priceforupdate."'
						  WHERE val.entity_id = prod.entity_id
					      AND val.store_id = '".$store_id."'
						  AND val.attribute_id = (
							 SELECT attribute_id FROM ".$prefix."eav_attribute eav
							 WHERE eav.entity_type_id = '".$entity_type_id."' 
							   AND eav.attribute_code = 'special_price'
							)
						  AND prod.sku = '".strval(trim($importData['sku']))."'
						");
						
					} else {		
						#echo "WE HAVE TO INSERT";			
						$entity_type_id = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
						$select_qry =$read->query("SELECT attribute_id FROM `".$prefix."eav_attribute` WHERE `attribute_code`='special_price' and entity_type_id ='".$entity_type_id."'");
						$row = $select_qry->fetch();
						$attribute_id = $row['attribute_id'];
						
						$write->query("INSERT INTO ".$prefix."catalog_product_entity_decimal (entity_type_id,attribute_id,store_id,entity_id,value) VALUES ('".$entity_type_id."','".$attribute_id."','".$store_id."','".$productIDcheckifnew."','".$priceforupdate."')");
					}
				
				}
				//UPDATE FOR COST
				if(isset($importData['cost']) && $importData['cost'] != "")
				{
					#$costforupdate = $importData['cost'];
					$priceforupdate = $importData['cost'];
					
					if(isset($importData['store_id'])) {
						$store_id = $importData['store_id'];
					} else {
						$store_id = '0';
					}
					$prefix = Mage::getConfig()->getNode('global/resources/db/table_prefix');
					$read = Mage::getSingleton('core/resource')->getConnection('core_read');
					$write = Mage::getSingleton('core/resource')->getConnection('core_write');
					$entity_type_id = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
					
					$select_qry = "SELECT ".$prefix."catalog_product_entity.sku, ".$prefix."catalog_product_entity_decimal.value FROM ".$prefix."catalog_product_entity INNER JOIN ".$prefix."catalog_product_entity_decimal ON ".$prefix."catalog_product_entity_decimal.entity_id = ".$prefix."catalog_product_entity.entity_id WHERE ".$prefix."catalog_product_entity_decimal.store_id = '".$store_id."' AND ".$prefix."catalog_product_entity_decimal.attribute_id = (
						 SELECT attribute_id FROM ".$prefix."eav_attribute eav
						 WHERE eav.attribute_code = 'cost' AND eav.entity_type_id = '".$entity_type_id."'
						) AND ".$prefix."catalog_product_entity.sku = '".strval(trim($importData['sku']))."'";
			 		
					$rs2 = $read->fetchAll($select_qry);
					
					if($rs2) {	
						#echo "WE HAVE TO UPDATE";
						$write = Mage::getSingleton('core/resource')->getConnection('core_write');
						$write->query("
						  UPDATE ".$prefix."catalog_product_entity_decimal val, ".$prefix."catalog_product_entity prod
						  SET  val.value = '".$priceforupdate."'
						  WHERE val.entity_id = prod.entity_id
					      AND val.store_id = '".$store_id."'
						  AND val.attribute_id = (
							 SELECT attribute_id FROM ".$prefix."eav_attribute eav
							 WHERE eav.entity_type_id = '".$entity_type_id."' 
							   AND eav.attribute_code = 'cost'
							)
						  AND prod.sku = '".strval(trim($importData['sku']))."'
						");
						
					} else {		
						#echo "WE HAVE TO INSERT";			
						$entity_type_id = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
						$select_qry =$read->query("SELECT attribute_id FROM `".$prefix."eav_attribute` WHERE `attribute_code`='cost' and entity_type_id ='".$entity_type_id."'");
						$row = $select_qry->fetch();
						$attribute_id = $row['attribute_id'];
						
						$write->query("INSERT INTO ".$prefix."catalog_product_entity_decimal (entity_type_id,attribute_id,store_id,entity_id,value) VALUES ('".$entity_type_id."','".$attribute_id."','".$store_id."','".$productIDcheckifnew."','".$priceforupdate."')");
					}
				
				}
				//UPDATE FOR GROUP PRICING
				if(isset($importData['group_price_price']) && $importData['group_price_price'] != "") {
    						
    						$finalgroup_price_price = $importData['group_price_price'];
							$resource = Mage::getSingleton('core/resource');
							$adapter = Mage::getSingleton('core/resource')->getConnection('core_write');
							$read = Mage::getSingleton('core/resource')->getConnection('core_read');
							$prefix = Mage::getConfig()->getNode('global/resources/db/table_prefix'); 
							
							
							if($this->getBatchParams('append_group_prices') != "true") { 
							 //delete existing group prices
							 $select_qry2 = $read->query("DELETE FROM ".$resource->getTableName('catalog/product_attribute_group_price')." WHERE entity_id = '".$rs[0]['entity_id']."'");
							}
							if($finalgroup_price_price != "delete") {
								$group_price_priceData = explode('|',$finalgroup_price_price);
										
								foreach($group_price_priceData as $singleattributeData) {
									$FinalGroupedPriceData = explode('=',$singleattributeData);
									if(isset($FinalGroupedPriceData[2])) {
										$websiteInternalId = $FinalGroupedPriceData[2];
									} else {
										$websiteInternalId = "0";
										#$websiteInternalId = $website -> getId();
									}
									
									if($FinalGroupedPriceData[0] != "") {
										if($new) {
											#echo "SQL2: ";
											$insertPrice='INSERT into '.$resource->getTableName('catalog/product_attribute_group_price').' (entity_id,all_groups,customer_group_id,value,website_id) VALUES ("'.$rs[0]['entity_id'].'","0","'.$FinalGroupedPriceData[0].'","'.$FinalGroupedPriceData[1].'","'.$websiteInternalId.'");';
											$adapter->query($insertPrice);
											
										} else {
											#echo "SQL UPDATE: " . $updatePrice;
											$select_qry2 = $read->query("SELECT value_id FROM ".$resource->getTableName('catalog/product_attribute_group_price')." WHERE value = '".$FinalGroupedPriceData[1]."' AND customer_group_id = '".$FinalGroupedPriceData[0]."' AND website_id = '".$websiteInternalId."'  AND entity_id = '".$rs[0]['entity_id']."'");
											$newrowItemId2 = $select_qry2->fetch();
											$db_product_id = $newrowItemId2['value_id'];
											if($db_product_id == "") {
												$insertPrice='INSERT into '.$resource->getTableName('catalog/product_attribute_group_price').' (entity_id,all_groups,customer_group_id,value,website_id) VALUES ("'.$rs[0]['entity_id'].'","0","'.$FinalGroupedPriceData[0].'","'.$FinalGroupedPriceData[1].'","'.$websiteInternalId.'");';
												#echo "INSERT SQL2: " . $insertPrice;
												$adapter->query($insertPrice);
											
											} else {
											#echo "VALUE ID: " . $db_product_id;
											#echo "SQL UPDATE2: ";
											
											$updatePrice="UPDATE ".$resource->getTableName('catalog/product_attribute_group_price')." SET customer_group_id = '".$FinalGroupedPriceData[0]."', value = '".$FinalGroupedPriceData[1]."', website_id = '".$websiteInternalId."' WHERE value_id = '".$db_product_id."'";
											$adapter->query($updatePrice);
											}
										}
									}
								}
							}
				}
			} //if $rs is true
				
		} else {
		
		
		}// else if end for if new or existing
		
		return true;
	} 
	
	protected function userCSVDataAsArray( $data )
	{
		return explode( ',', str_replace( " ", "", $data ) );
	} 
	
	protected function skusToIds( $userData, $product )
	{
		$productIds = array();
		foreach ( $this -> userCSVDataAsArray( $userData ) as $oneSku ) {
			if ( ( $a_sku = ( int )$product -> getIdBySku( $oneSku ) ) > 0 ) {
				parse_str( "position=", $productIds[$a_sku] );
			} 
		} 
		return $productIds;
	} 
	
	protected function _removeFile( $file )
	{
		if ( file_exists( $file ) ) {
		$ext = substr(strrchr($file, '.'), 1);
			if( strlen( $ext ) == 4 ) {
				if ( unlink( $file ) ) {
					return true;
				} 
			}
		} 
		return false;
	} 
}