<?php
/**
 * WMD_Html5audio_Block_Adminhtml_Mapper_Main
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2014 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Block_Adminhtml_Mapper_Main extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	
    /**
     * Construct
     *
     * @return void
     */		
    public function __construct() 
    {
        parent::__construct();
        $this->_blockGroup = 'html5audio';
        $this->_controller = 'adminhtml_mapper_main';         
		    $this->setTemplate('html5audio/mapper/list.phtml');  
    }
    
    /**
     * Before HTML
     *
     * @return object
     */		
    public function _beforeToHtml()
    {
        $this->setChild('new_mapper',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('html5audio')->__('Add Mapper'),
                    'onclick'   => 'setLocation(\''
                        . $this->getUrl('html5audio/adminhtml_mapper_main/edit')
                    . '\')'
                  ))
        );	
        
        $this->setChild('generate',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('html5audio')->__('Generate Mappers'),
                    'onclick'   => 'setLocation(\''
                         . $this->getUrl('html5audio/adminhtml_mapper_main/edit', array('generate'=>1))
                    . '\')'
                  ))
        );	

        return parent::_beforeToHtml();
    }		
    
    /**
     * Return Header Text
     *
     * @return string
     */	
    public function getHeaderText()
    {
		    return Mage::helper('html5audio')->__('Mapper List');
    }
}
