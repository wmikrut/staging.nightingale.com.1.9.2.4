<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 

class Itoris_ProductSlider_Block_Admin_Settings_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {
		$form = new Varien_Data_Form();

		$fieldset = $form->addFieldset('general_fields', array(
			'legend' => $this->__('Homepage Product Slider Configuration'),
		));

		$fieldset->addField('enabled', 'select', array(
			'name'   => 'settings[enabled][value]',
			'label'  => $this->__('Extension Enabled'),
			'title'  => $this->__('Extension Enabled'),
			'values' => array(
				array('label' => $this->__('Yes'),
					'value' => 1),
				array('label' => $this->__('No'),
					'value' => 0),
			),
		))->getRenderer()->setTemplate('itoris/productslider/configuration/form/element.phtml');

		$fieldset->addField('is_saleable_check', 'select', array(
			'name'   => 'settings[is_saleable_check][value]',
			'label'  => $this->__('Check products if saleable'),
			'title'  => $this->__('Check products if saleable'),
			'values' => array(
				array(
					'label' => $this->__('Magento default'),
					'value' => Itoris_ProductSlider_Model_Settings::IS_SALEABLE_CHECK_DEFAULT,
				),
				array(
					'label' => $this->__('Simple check'),
					'value' => Itoris_ProductSlider_Model_Settings::IS_SALEABLE_CHECK_SIMPLE,
				),
			),
		));

		$form->addValues($this->getFormHelper()->prepareElementsValues($form));
		$form->setUseContainer(true);
		$form->setId('edit_form');
		$form->setAction($this->getUrl('adminhtml/itorisproductslider_configuration/save', array('_current' => true)));
		$form->setMethod('post');
		$this->setForm($form);
	}

	/**
	 * @return Itoris_ProductSlider_Helper_Form
	 */
	public function getFormHelper() {
		return Mage::helper('itoris_productslider/form');
	}

}
?>