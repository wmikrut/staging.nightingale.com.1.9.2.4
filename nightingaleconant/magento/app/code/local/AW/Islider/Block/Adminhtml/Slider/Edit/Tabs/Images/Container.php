<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Block_Adminhtml_Slider_Edit_Tabs_Images_Container extends Mage_Adminhtml_Block_Widget_Container
{
    protected function _beforeToHtml()
    {
        $image = Mage::registry('current_image_slider');
        $this->_headerText = $this->__('Add Image or Video');
        if($image->getId()) {
            $this->_headerText = $this->__('Edit Image');
        }
        return parent::_beforeToHtml();
    }

    protected function _prepareLayout()
    {
        $this->_addButton('save', array(
            'label' => $this->__('Save'),
            'type'  => 'submit',
            'class' => 'save',
            'id'    => 'awis_imagesavebutton'
        ));
        return parent::_prepareLayout();
    }

    public function getImageId()
    {
        $formData = Mage::registry('current_image_slider');
        return $formData->getId();
    }
}