<?php
/**
 * WMD_Html5audio Installer 0.0.1
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/

$installer = $this;
$installer->startSetup();

$installer->run("

CREATE TABLE IF NOT EXISTS {$installer->getTable('html5audio/playlist')} (
  `playlist_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Sample ID', 
  `hash` varchar(255) DEFAULT NULL COMMENT 'Hash',    
  `playlist_title` varchar(255) DEFAULT NULL COMMENT 'Title',
  PRIMARY KEY (`playlist_id`),
  KEY `IDX_HA_PLAYLIST_PLAYLIST_ID` (`playlist_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='HTML5 Audio Playlist Backend Table' ;

");
 
$installer->endSetup(); 

