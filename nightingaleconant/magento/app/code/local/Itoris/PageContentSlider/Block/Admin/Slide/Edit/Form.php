<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGECONTENTSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PageContentSlider_Block_Admin_Slide_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

	protected function _prepareForm() {
		$form = new Varien_Data_Form(array(
			'id'        => 'edit_form',
			'action'    => $this->getData('action'),
			'method'    => 'post',
			'enctype'   => 'multipart/form-data'
		));

		/** @var $slide Itoris_PageContentSlider_Model_Slide */
		$slide = Mage::registry('current_slide');

		$fieldset = $form->addFieldset('details_fieldset', array(
			'legend' => $this->__('Slide Details'),
		));

		$fieldset->addField('id', 'hidden', array(
			'name'     => 'id',
			'value'    => $slide->getId(),
		));

		$fieldset->addField('title', 'textarea', array(
			'label'    => $this->__('Title'),
			'title'    => $this->__('Title'),
			'name'     => 'slide[title]',
			'required' => true,
			'value'    => $slide->getTitle(),
			'note'     => $this->__('HTML tags allowed'),
		));

		$fieldset->addField('slide_order', 'text', array(
			'label'    => $this->__('Order'),
			'title'    => $this->__('Order'),
			'name'     => 'slide[slide_order]',
			'required' => true,
			'value'    => $slide->getSlideOrder() ? $slide->getSlideOrder() : $slide->calculateSlideOrder(),
			'class'    => 'validate-number-range number-range-1-1000',
		));

		$fieldset->addField('enabled', 'checkbox', array(
			'label'   => $this->__('Enabled'),
			'title'   => $this->__('Enabled'),
			'name'    => 'slide[enabled]',
			'checked' => $slide->getEnabled(),
			'value'   => 1,
		));

		$fieldset->addField('start_publish_date', 'date', array(
			'label'        => $this->__('Start publish on'),
			'title'        => $this->__('Start publish on'),
			'name'         => 'slide[start_publish_date]',
			'value'        => $slide->getStartPublishDate(),
			'image'        => $this->getSkinUrl('images/grid-cal.gif'),
			'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
			'format'       => Varien_Date::DATE_INTERNAL_FORMAT,
			'class'        => 'validate-date',
		));

		$fieldset->addField('end_publish_date', 'date', array(
			'label'        => $this->__('End publish on'),
			'title'        => $this->__('End publish on'),
			'name'         => 'slide[end_publish_date]',
			'value'        => $slide->getEndPublishDate(),
			'image'        => $this->getSkinUrl('images/grid-cal.gif'),
			'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
			'format'       => Varien_Date::DATE_INTERNAL_FORMAT,
			'class'        => 'validate-date',
		));

		$fieldset->addField('store_views', 'multiselect', array(
			'label'    => $this->__('Store Views'),
			'title'    => $this->__('Store Views'),
			'name'     => 'slide[store_views]',
			'values'   => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
			'value'    => $slide->getStoreViews(),
		));

		$userGroups = Mage::getResourceModel('customer/group_collection')->toOptionArray();
		array_unshift($userGroups, array(
			'label' => $this->__('All Groups'),
			'value' => '',
		));

		$fieldset->addField('user_groups', 'multiselect', array(
			'label'  => $this->__('User Groups'),
			'title'  => $this->__('User Groups'),
			'name'   => 'slide[user_groups]',
			'values' => $userGroups,
			'value'  => $slide->getUserGroups(),
		));

		$fieldset->addField('slide_content', 'editor', array(
			'label'    => $this->__('Content'),
			'title'    => $this->__('Content'),
			'name'     => 'slide[content]',
			'required' => true,
			'value'    => $slide->getContent(),
			'wysiwyg'  => true,
			'config'   => Mage::getSingleton('itoris_pagecontentslider/wysiwyg_config')->getConfig(),
		));

		$fieldset->addField('redirect_url', 'text', array(
			'label' => $this->__('Onclick Redirect URL'),
			'title' => $this->__('Onclick Redirect URL'),
			'name'  => 'slide[redirect_url]',
			'value' => $slide->getRedirectUrl(),
			'style' => 'float:left;',
			'after_element_html' => '<div style="float:left;margin-left:20px;"><span>'
									. $this->__('Open in a new window')
									. ' </span><input type="checkbox" name="slide[open_new_window]" value="1" '
									. ($slide->getOpenNewWindow() ? 'checked="checked"' : '')
									.' /></div>',
		));

		$form->setUseContainer(true);
		$this->setForm($form);
		return parent::_prepareForm();
	}

	protected function _prepareLayout() {
		parent::_prepareLayout();
		if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
			$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
		}
	}
}
?>