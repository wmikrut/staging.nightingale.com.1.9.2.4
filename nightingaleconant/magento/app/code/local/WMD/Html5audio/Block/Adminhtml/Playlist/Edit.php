<?php
/**
 * WMD_Html5audio_Block_Adminhtml_Playlist_Edit
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2013 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Block_Adminhtml_Playlist_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
     
    /**
     * Playlist model
     * 
     * @var WMD_Html5audio_Model_Playlist $_playlist     
     */              
    protected $_playlist;
    
    /**
     * Construct
     *
     * @return void
     */	
    public function __construct()
    {
        parent::__construct();
		
        $this->_blockGroup = 'html5audio';
        $this->_mode = 'edit';
        $this->_controller = 'adminhtml_playlist';
                   
        if (!$this->getPlaylist()->isObjectNew())
        {
    		    $this->_removeButton('reset');
    		}
        $this->_addButton(
            'save_and_edit_button', 
            array(
                'label'     => Mage::helper('catalog')->__('Save and Continue Edit'),
                'onclick'   => 'saveAndContinueEdit(\''.$this->getSaveAndContinueUrl().'\')',
                'class' => 'save'
            )
        );  
		
    		$this->_updateButton('save', 'label', Mage::helper('html5audio')->__('Save'));
               
        $this->_formScripts[] = '
            //<![CDATA[  
                
            var productTemplateSyntax = /(^|.|\r|\n)({{(\w+)}})/;
            function saveAndContinueEdit(urlTemplate) { 
                var template = new Template(urlTemplate, productTemplateSyntax);
                var url = template.evaluate({tab_id:html5audio_playlist_edit_tabsJsTabs.activeTab.id});
                editForm.submit(url);    
            }
            
            var playlistSamples = $H(' . $this->getSamplesJson() . ');
            $(\'in_playlist_samples\').value = playlistSamples.toQueryString();
            
            function registerPlaylistSample(grid, element, checked){
                if(checked){
                    if(element.positionElement){
                        element.positionElement.disabled = false;
                        playlistSamples.set(element.value, element.positionElement.value);
                    }
                }
                else{
                    if(element.positionElement){
                        element.positionElement.disabled = true;
                    }
                    playlistSamples.unset(element.value);
                }
                $(\'in_playlist_samples\').value = playlistSamples.toQueryString();
                grid.reloadParams = {\'selected_samples[]\':playlistSamples.keys()};
            }
            
            function playlistSampleRowClick(grid, event){
                var trElement = Event.findElement(event, \'tr\');
                var isInput   = Event.element(event).tagName == \'INPUT\';
                if(trElement){
                    var checkbox = Element.getElementsBySelector(trElement, \'input\');
                    if(checkbox[0]){
                        var checked = isInput ? checkbox[0].checked : !checkbox[0].checked;
                        playlist_samplesJsObject.setCheckboxChecked(checkbox[0], checked);
                    }
                } 
            }
            
            function positionChange(event){
                var element = Event.element(event);
                if(element && element.checkboxElement && element.checkboxElement.checked){
                    playlistSamples.set(element.checkboxElement.value, element.value);
                    $(\'in_playlist_samples\').value = playlistSamples.toQueryString();
                } 
            }
            
            var tabIndex = 1000;
            function playlistSampleRowInit(grid, row){
                var checkbox = $(row).getElementsByClassName(\'checkbox\')[0];
                var position = $(row).getElementsByClassName(\'input-text\')[0];
                if(checkbox && position){
                    checkbox.positionElement = position;
                    position.checkboxElement = checkbox;
                    position.disabled = !checkbox.checked;
                    position.tabIndex = tabIndex++;
                    Event.observe(position,\'keyup\',positionChange);
                }    
            }
       
            playlist_samplesJsObject.rowClickCallback = playlistSampleRowClick;
            playlist_samplesJsObject.initRowCallback = playlistSampleRowInit;
            playlist_samplesJsObject.checkboxCheckCallback = registerPlaylistSample;
            playlist_samplesJsObject.rows.each(function(row){playlistSampleRowInit(playlist_samplesJsObject, row)}); 
           //]]>
        ';
    		
    } 
   
    /**
     * Playlist model from registry
     * 
     * @return WMD_Html5audio_Model_Playlist 
     */                  
    public function getPlaylist()
    {
        if (!$this->_playlist)
        {
            $this->_playlist = Mage::registry('playlist');
        }         
        return $this->_playlist;
    }             
    
    public function getSamplesJson()
    {
        $samples = $this->getPlaylist()->getSamples();                
        if (!empty($samples)) {
            return Mage::helper('core')->jsonEncode($samples);
        }
        return '{}';
    }   
    
    
    /**
     * Return Header Text
     *
     * @return string
     */	 
    public function getHeaderText()
    {
        if ($title = $this->getPlaylist()->getPlaylistTitle())
        {
            return Mage::helper('html5audio')->__('Edit Playlist ' . $title);
        }
        return Mage::helper('html5audio')->__('New Playlist'); 
    }   
       
    public function getSaveAndContinueUrl()
    {
        return $this->getUrl('*/*/save', array(
            '_current'   => true,
            'back'       => 'edit',
            'tab'        => '{{tab_id}}',
            'active_tab' => null
        ));
    } 
}
