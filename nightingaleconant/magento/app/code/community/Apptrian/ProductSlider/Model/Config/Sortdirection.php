<?php
/**
 * @category   Apptrian
 * @package    Apptrian_ProductSlider
 * @author     Apptrian
 * @copyright  Copyright (c) 2015 Apptrian (http://www.apptrian.com)
 * @license    http://www.apptrian.com/license    Proprietary Software License (EULA)
 */
class Apptrian_ProductSlider_Model_Config_Sortdirection
{
    
    protected $_options;
	const SORT_DIRECTION_ASC  = 'asc';
    const SORT_DIRECTION_DESC = 'desc';
    
    public function toOptionArray()
    {
        if (!$this->_options) {
			$this->_options[] = array(
			   'value'=>self::SORT_DIRECTION_ASC,
			   'label'=>Mage::helper('apptrian_productslider')->__('Ascending')
			);
			$this->_options[] = array(
			   'value'=>self::SORT_DIRECTION_DESC,
			   'label'=>Mage::helper('apptrian_productslider')->__('Descending')
			);
		}
		return $this->_options;
	}
    
}