<?php
/**
 * WMD_Html5audio_Adminhtml_Mapper_MainController
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2014 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Adminhtml_Mapper_MainController extends Mage_Adminhtml_Controller_Action
{   
    /**
     * load layout, activate menu and add breadcrumbs
     *
     * @return object
     */                         
    protected function _initAction()
    {
        $this->loadLayout()
  		      ->_setActiveMenu('catalog/html5audio')
            ->_addBreadcrumb(
                Mage::helper('html5audio')->__('Mapper'), 
                Mage::helper('html5audio')->__('Mapper')
            );
        return $this;
    }
    	
	  /**
     * Show download links
     *
     * @return void
     */	
    public function indexAction()
    {
  		  $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('html5audio/adminhtml_mapper_main'))
            ->renderLayout();	
    } 
    
    /**
     * Show mapper form
     *
     * @return void
     */	
    public function newAction()
    {
    		$this->editAction();
    }
    
    /**
     * Show mapper form
     *
     * @return void
     */	
    public function editAction()
    {
		    $mapper = Mage::getModel('html5audio/mapper');
  	    if ($id = $this->getRequest()->getParam('id')) 
        {
            $mapper->load($id);
        } else {
            $mapper->isObjectNew(true);
        }
        if ($this->getRequest()->getParam('generate'))
        {
            $mapper->setGenerateMappers(true);
        }
  	    Mage::register('mapper', $mapper);
        $this->loadLayout()
            ->_setActiveMenu('html5audio/mapper')
            ->_addBreadcrumb(
                Mage::helper('html5audio')->__('New Mapper'), 
                Mage::helper('html5audio')->__('New Mapper')
            )
            ->_addContent($this->getLayout()->createBlock('html5audio/adminhtml_mapper_edit'))
    				->_addLeft($this->getLayout()->createBlock('html5audio/adminhtml_mapper_edit_tabs'))
    		    ->renderLayout();	
    }	
      
    /**
     * Save mapper form
     *
     * @return void
     */	
    public function saveAction()
    {  
        if ( $this->getRequest()->getPost() ) {
            try {
                $postData = $this->getRequest()->getPost();
                $model = Mage::getModel('html5audio/mapper');
                         
                if (!isset($postData['generate_mappers']))
                {
                    if ($id = $postData['link_id'])
                    {
                        $model->load($id);
                    }
                    $model->setData($postData);
                    $model->save();
                }
                else
                {
                    $model->generateMappers($postData);
        
                    Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('html5audio')->__('Mappers where successfully created and saved.')
                    );
                }
    
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/'); 
    }	

    /**
     * Delete sample 
     *
     * @return void
     */
    public function deleteAction()
    {
        
        $id = $this->getRequest()->getParam('id');
        try
        {
    				Mage::getModel('html5audio/mapper')->setId($id)->delete();					
            
            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('html5audio')->__('Entry was deleted successfully.')
            );
        } 
  			catch (Exception $e)
  			{
//                   Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
		
        $this->indexAction();	
    		
    }                   
    
    /**
     * Mass delete samples
     *
     * @return void
     */
    public function massDeleteAction()
    {
        
        $ids = $this->getRequest()->getParam('id');
        if(!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('html5audio')->__('Please select at least one item.')
          	);
        }
    		else
    		{
            try 
      			{
                foreach ($ids as $id)
        				{
        					   Mage::getModel('html5audio/mapper')->setId($id)->delete();					
                }
				
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('html5audio')->__('Entries were deleted successfully.')
                );
            } 
      			catch (Exception $e)
      			{
//                 Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
		
        $this->indexAction();	
    		
    }           
   
         
    /**
     * Associations grid-load for the ajax request
     * 
     * @return void          
     */
    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('html5audio/adminhtml_mapper_main_grid')->toHtml()
        );
    }         
}

