<?php

class NC_DataInstaller_Model_Resource_Eav_Mysql4_Setup extends Mage_Catalog_Model_Resource_Setup {

    var $cmsPages = array(
        array(
            'title' => 'About Us',
            'identifier' => 'about',
            'content_heading' => 'About ATP',
            'root_template' => 'two_columns_left',
            'content' => '<h1>About Us</h1>
<div>
<h3>A Message from Vic Conant</h3>
</div>
<p><img style="float: left;" src="http://www.nightingale.com/media/Vic1.jpg" alt="Vic Conant">I have always believed that thoughts and ideas are our most powerful possessions. For an idea can change the way we look at the world and the way we think. And, it can also improve every area of our life – career, relationships, personal wealth, and self-confidence.</p>
<p>Powerful ideas are at the very heart of success and are the driving force behind everything we do at the Nightingale-Conant Corporation. We are the world leader in self-development and it is our aim to help you reach your highest and most desired destination.</p>
<p><em>Thank you for over 40 wonderful years and many more to come.</em></p>
<h3>Company History</h3>
<p><strong>Nightingale-Conant is a special company … a company that has changed the lives of millions of people worldwide.</strong></p>
<p>Nightingale-Conant has a simple yet marvelous purpose: to help people realize their full potential … to make them successful and happy. The goal is self-improvement … the vehicles are audio and video programs. Nightingale-Conant is the world’s largest producer of audio programs.</p>
<p>Our message goes out to hundreds of thousands of people every year, people who tell us that Nightingale-Conant has transformed their lives for the better.</p>
<p>We are an active, vibrant company operating out of Chicago and Devon, England. But our roots … our fiber … our ethic … goes back to the mid-1950s.</p>
<p><em><strong>It all started with…</strong></em></p>
<p>It all started with two unique men … Earl Nightingale and Lloyd Conant.</p>
<p><a href="/authors/earl-nightingale/">Earl Nightingale’s</a> life began simply. He grew up in Long Beach, California. His parents had little money, and his father disappeared when he was 12. But even as a boy, Earl was always asking questions, always reading in his local public library, trying to understand the way life works.</p>
<p><img style="float: left;" src="http://www.nightingale.com/media/History_EarlNightingale1.jpg" alt="Earl Nightingale">On December 7, 1941, Earl was at Pearl Harbor … in a lookout tower on the Battleship <em>Arizona</em>. A thousand men died on the <em>Arizona</em> … Earl was one of only a hundred who survived. He was literally blown off the ship, unconscious, and another sailor pulled him to safety.</p>
<p>Earl said that he felt that he had been spared for some reason. After the war Earl became obsessed with why everyone in his neighborhood was poor and confused. He wondered why one person was able to create wealth and happiness for his family, while another with a similar background stayed ignorant and penniless. Answering these questions became Earl’s life work.</p>
<p>Earl worked in radio in Phoenix and later in Chicago. He also owned his own insurance agency.</p>
<p>In 1956, he was about to take a trip but wanted to leave a message for his sales force. He got up in the middle of the night inspired to write a message that he recorded the next morning. That message became one of the most important and famous motivational recordings ever made.</p>
<p><strong><em>The Strangest Secret</em></strong></p>
<p>It’s called <em><a href="/products/strangest-secret/">The Strangest Secret</a></em>. It sold over a million copies and is the only gold record ever achieved for the spoken word. Its message is simple, yet powerful … <em>You become what you think about …</em> and it became the basis for the Nightingale-Conant Corporation. Many thousands of people have attributed this recording with turning their lives around and helping them make their fortunes in the world.</p>
<p>One of the people <em>The Strangest Secret</em> inspired was Lloyd Conant, the owner of a Chicago printing and direct response company. Lloyd contacted Earl, and the two were immediately attracted by their shared values. In 1960, they formed the Nightingale-Conant Corporation.</p>
<p><strong><em>Lloyd Conant</em></strong></p>
<p>Lloyd Conant was one of those special people whom everyone respected. He cared deeply for people and, in turn, received tremendous loyalty and affection from everyone who worked for him.<img style="float: left;" src="http://www.nightingale.com/media/History_LloydConant.jpg" alt="Lloyd Conant"></p>
<p>Like Earl Nightingale, Lloyd Conant was a self-made man. He grew up in St. Joseph, Missouri, the son of a candy maker. During the Depression, Lloyd’s father lost his job. To help support the family, Lloyd moved to Kansas City, Missouri, to sell typewriters. During this time, he met many successful business and professional leaders. He also attended a businessmen’s Bible class. This period had a tremendous influence on Lloyd. He developed the business and ethical principles that later became the cornerstone of Nightingale-Conant.</p>
<p>In World War II, Lloyd served as a glider pilot. After the war, he moved to Chicago, where he started the printing company that later became part of Nightingale-Conant.</p>
<p>From the beginning, Lloyd and Earl were almost a perfect match. Earl, the creative force … the one who conceptualized, wrote, and delivered almost all the early programs. Lloyd, the solid rock … the man behind the scenes who kept the company running through all kinds of problems and changing conditions, the super administrator and entrepreneur, the man who worked the endless hours each day to make Nightingale-Conant what it is today.</p>
<p>In 1959, Earl and Lloyd brought out Earl’s great radio program <em>Our Changing World</em>. It aired on more than a thousand radio stations, and at the time was the longest-running, most widely syndicated show in radio history. For five minutes a day, five days a week, people throughout the world learned something valuable from <em>Our Changing World</em>. Earl eventually recorded 5,000 radio programs and was elected into the Radio Hall of Fame.</p>
<p><strong><em>Lead the Field</em></strong></p>
<p>In 1960, Earl was writing their first major full-sized program, <em><a href="/products/lead-field/">Lead the Field</a></em>. It also became a bestseller, eventually touching the lives of over a million people. It is one of the classics of our industry.</p>
<p>Throughout the 1960s while Earl was doing the creating, Lloyd was doing the shaping. He molded a company ethic that still exists today. Simply put, customer service is more important than anything else.</p>
<p>In 1978, Nightingale-Conant made a strategic decision to begin publishing and marketing new authors, and added Denis Waitley as its second author. His recording <em><a href="/products/psychology-winning/">The Psychology of Winning</a></em> reached gold status when it sold 1.5 million copies, making it the bestselling audio self-improvement program in history. Because of the success of Waitley’s program, the Company began to publish a variety of authors.</p>
<p>Since then, the size of the self-improvement market has grown significantly, and Nightingale-Conant has been successful in promoting an increasing number of authors. As the number of titles grew, the Company expanded its mail order marketing and distributor efforts. The Company has benefited as the mail order industry became increasingly popular.</p>
<p>During this same period, Nightingale-Conant’s Author, Distributorship, and Seminar program sales grew into a significant business. Authors and seminar companies found that providing N-C’s programs at the back of the seminar room significantly increased their per head profit. Also, independent distributors around the country and around the world all sell Nightingale-Conant programs in a wide variety of ways.</p>
<p>Nightingale-Conant has come a long way. It has improved so many lives, helped so many careers, helped so many people. There’s really no measuring what it’s meant to millions of people throughout the world.</p>
<p>It started with two men from humble beginnings. Their spirit and their standards are the core of the company. That will never change … despite new technologies and new messages. For this company’s mission will always be clear: to help people become the best they can be.</p>
<p>Nightingale-Conant is a special place. Its people feel something many others never know … a sense that they’re doing something exciting and important. And that will only continue. For in the next decade, and the rest of this century, Nightingale-Conant will remain in the forefront … committed to helping people everywhere.</p>
<p><em><strong>Nightingale-Conant … an example and an inspiration to the entire world!</strong></em></p>',
        ),
        array(
            'title' => 'Home Page',
            'identifier' => 'home',
            'content_heading' => '',
            'content' => '<p>&nbsp;</p>',
            'root_template' => 'home',
            'layout_update_xml' => '<reference name="root">
            <action method="setTemplate">
                <template>page/homepage.phtml</template>
            </action>
        </reference>'
        ),
        array(
            'title' => 'Motivational Quotes',
            'identifier' => 'motivational-quotes',
            'content_heading' => 'Motivational Quotes',
            'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vestibulum dui vel nisi tempor lobortis nec non velit. Nam nec mauris id tellus vulputate iaculis et id diam. Proin at est non nunc rhoncus mattis. Maecenas iaculis mattis libero, in euismod risus accumsan in. Proin suscipit laoreet metus, et bibendum lacus scelerisque in. Sed molestie mauris lectus, ac convallis libero porta eget. Vivamus dui ligula, semper quis sapien id, condimentum vestibulum mi.</p>',
            'root_template' => 'two_columns_left'
        ),
        array(
            'title' => 'Personal Mission Statement',
            'identifier' => 'personal-mission',
            'content_heading' => 'Personal Mission Statement',
            'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vestibulum dui vel nisi tempor lobortis nec non velit. Nam nec mauris id tellus vulputate iaculis et id diam. Proin at est non nunc rhoncus mattis. Maecenas iaculis mattis libero, in euismod risus accumsan in. Proin suscipit laoreet metus, et bibendum lacus scelerisque in. Sed molestie mauris lectus, ac convallis libero porta eget. Vivamus dui ligula, semper quis sapien id, condimentum vestibulum mi.</p>',
            'root_template' => 'two_columns_left'
        ),
        array(
            'title' => 'Goal Setting Guide',
            'identifier' => 'goal-setting-guide',
            'content_heading' => 'Goal Setting Guide',
            'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vestibulum dui vel nisi tempor lobortis nec non velit. Nam nec mauris id tellus vulputate iaculis et id diam. Proin at est non nunc rhoncus mattis. Maecenas iaculis mattis libero, in euismod risus accumsan in. Proin suscipit laoreet metus, et bibendum lacus scelerisque in. Sed molestie mauris lectus, ac convallis libero porta eget. Vivamus dui ligula, semper quis sapien id, condimentum vestibulum mi.</p>',
            'root_template' => 'two_columns_left'
        ),
        array(
            'title' => 'Author Insights',
            'identifier' => 'author-insights',
            'content_heading' => 'Author Insights',
            'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vestibulum dui vel nisi tempor lobortis nec non velit. Nam nec mauris id tellus vulputate iaculis et id diam. Proin at est non nunc rhoncus mattis. Maecenas iaculis mattis libero, in euismod risus accumsan in. Proin suscipit laoreet metus, et bibendum lacus scelerisque in. Sed molestie mauris lectus, ac convallis libero porta eget. Vivamus dui ligula, semper quis sapien id, condimentum vestibulum mi.</p>',
            'root_template' => 'two_columns_left'
        ),
        array(
            'title' => 'MP3 Downloads',
            'identifier' => 'mp3-downloads',
            'content_heading' => 'MP3 Downloads',
            'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vestibulum dui vel nisi tempor lobortis nec non velit. Nam nec mauris id tellus vulputate iaculis et id diam. Proin at est non nunc rhoncus mattis. Maecenas iaculis mattis libero, in euismod risus accumsan in. Proin suscipit laoreet metus, et bibendum lacus scelerisque in. Sed molestie mauris lectus, ac convallis libero porta eget. Vivamus dui ligula, semper quis sapien id, condimentum vestibulum mi.</p>',
            'root_template' => 'two_columns_left'
        ),
        array(
            'title' => "Insiders Club",
            'identifier' => 'insiders-club',
            'content_heading' => "Insiders Club",
            'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vestibulum dui vel nisi tempor lobortis nec non velit. Nam nec mauris id tellus vulputate iaculis et id diam. Proin at est non nunc rhoncus mattis. Maecenas iaculis mattis libero, in euismod risus accumsan in. Proin suscipit laoreet metus, et bibendum lacus scelerisque in. Sed molestie mauris lectus, ac convallis libero porta eget. Vivamus dui ligula, semper quis sapien id, condimentum vestibulum mi.</p>',
            'root_template' => 'two_columns_left'
        ),
    );

    public function setupStaticBlocks() {

        $content = array(
            array(
                'title'            => 'Footer Links Left',
                'block_identifier' => 'footer_links_left',
                'content'          => '<h4>About Us</h4>
                    <ul>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">Our Location</a></li>
                        <li><a href="#">Our Company</a></li>
                        <li><a href="#">Our Staff</a></li>
                        <li><a href="#">Support</a></li>
                    </ul>',
            ),
            array(
                'title'            => 'Footer Links Middle',
                'block_identifier' => 'footer_links_middle',
                'content'          => '<h4>Latest From Us</h4>
                    <ul>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">Our Location</a></li>
                        <li><a href="#">Our Company</a></li>
                        <li><a href="#">Our Staff</a></li>
                        <li><a href="#">Support</a></li>
                    </ul>',
            ),
            array(
                'title'            => 'Footer Links Right',
                'block_identifier' => 'footer_links_right',
                'content'          => '<h4>About Us</h4>
                    <ul>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">Our Location</a></li>
                        <li><a href="#">Our Company</a></li>
                        <li><a href="#">Our Staff</a></li>
                        <li><a href="#">Support</a></li>
                    </ul>',
            ),
            array(
                'title'            => 'Footer About',
                'block_identifier' => 'footer_about',
                'content'          => '<h4>About Us</h4>
                <p>Donec scelerisque mi vitae interdum porta. Aliquam erat volutpat.
                Vestibulum ultrices mi sit amet elit consequat, ac molestie augue ultrices.</p>
                <p><span class="fa fa-phone">&nbsp;</span>+1 (555) 555-0001</p>
                <p><span class="fa fa-envelope-o">&nbsp;</span> Email: <a href="info@nightingale.com">info@nightingale.com</a></p>',
            ),
            array(
                'title'            => 'Homepage Pre-Form Content',
                'block_identifier' => 'promo_form_content',
                'content'          => '<blockquote>Sometimes when you innovate, you make mistakes. It is best to admit them quickly, and get on with improving your other innovations.</blockquote>
                <p>&mdash;Steve Jobs</p>
                <p class="signup">Sign Up NOW to receive a daily dose of insight and inspriation with our <span>FREE Motivational Quote of the Day!</span></p>',
            ),
        );

        Mage::app()->setCurrentStore( Mage_Core_Model_App::ADMIN_STORE_ID );

        $cmsBlocks = Mage::getResourceModel( 'cms/block_collection' )->getColumnValues( 'identifier' );
        foreach ( $content as $c ) {
            $add = true;
            foreach ( $cmsBlocks as $block ) {
                if ( $c['block_identifier'] == $block ) {
                    $add = false;
                }
            }
            Mage::getModel( 'cms/block' )->load( $c['block_identifier'], 'identifier' )->delete();
            if ( $add ) {
                $staticBlock = array(
                    'title'      => ( $c['title'] ),
                    'identifier' => $c['block_identifier'],
                    'content'    => $c['content'],
                    'is_active'  => 1,
                    'stores'     => array( 0 )
                );

                Mage::getModel( 'cms/block' )->setData( $staticBlock )->save();
            }
        }
    }

    public function setupAttributes( $filename = "categories.csv" ) {

    }

    public function setupCategories( $filename = "categories.csv" ) {

        // open the csv
        if ( ! $handle = fopen( $filename, "r", 1 ) ) {
            die( 'Failed to open file' );
        }
        // process categories
        $headings = array();
        for ( $i = 0; ( $line = fgets( $handle ) ) !== false; $i ++ ) {
            $values = array_map( 'trim', explode( ',', str_replace( '"', "", $line ) ) );
            if ( $i < 1 ) {
                $headings = $values;
            } else {
                $categoryData        = array_combine( $headings, $values );
                $category_collection = Mage::getModel( 'catalog/category' )->getCollection()
                                           ->addFieldToFilter( 'name', $categoryData['name'] )
                                           ->setPageSize( 1 );

                if ( $category_collection->count() ) { // item exists, move on to next tree item
                    continue;
                } else {
                    $result = $this->_createCategory( $categoryData );
                }
            }
        }

        fclose( $handle );

        $this->_createCategory( array( 'name' => 'Featured', 'description' => '' ), 0 );
    }

    private function _createCategory( $categoryData, $layeredNav = 1 ) {
        $category = Mage::getModel( 'catalog/category' );
        $category->setStoreId( 0 );

        $cat['name']            = $categoryData['name'];
        $cat['path']            = "1/2"; //parent relationship..
        $cat['description']     = $categoryData['description'];
        $cat['is_active']       = 1;
        $cat['is_anchor']       = 1; //for layered navigation
        $cat['page_layout']     = 'one_column';
        $cat['include_in_menu'] = $layeredNav;
        $cat['url_key']         = strtolower( str_replace( " ", "-", $categoryData['name'] ) ); //url to access this category


        $category->addData( $cat );
        $category->save();

        return $category;
    }

    public function updateCategories() {
        $categories = Mage::getModel( 'catalog/category' )->getCollection()
                          ->addAttributeToSelect( '*' );

        foreach ( $categories as $category ) {
            $name        = $category->getName();
            $id          = $category->getId();
            $c           = Mage::getModel( 'catalog/category' )->load( $id );
            $description = $c->getDescription();

            if ( $description ) {
                $url = preg_replace('~[^\\pL0-9_]+~u', '-', $description);
                $url = trim($url, "-");
                $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
                $url = strtolower($url);
                $url = preg_replace('~[^-a-z0-9_]+~', '', $url);
                $category->addData( array( 'name' => $description, 'description' => $name, 'url_key' => $url ) );
                $category->save();
            }
        }
    }

    public function addCmsPages() {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        $sortOrder = -1;
        $cmsPages = Mage::getResourceModel('cms/page_collection')->getColumnValues('identifier');
        foreach ($this->cmsPages as $page) {
            $add = true;
            foreach ($cmsPages as $p) {
                if ($page['identifier'] == $p) {
                    $add = false;
                }
            }

            if ($add) {
                $cms = array(
                    'title'         => $page['title'],
                    'identifier'    => $page['identifier'],
                    'content'       => $page['content'],
                    'is_active'     => 1,
                    'sort_order'    => ( $sortOrder ++ ),
                    'stores'        => array( 0 ),
                    'root_template' => $page['root_template']
                );
                Mage::getModel( 'cms/page' )->setData( $cms )->save();
            }
        }
    }
}