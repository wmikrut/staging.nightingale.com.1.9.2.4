<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Model_Resource_Test_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('amabtesting/test');
    }

    public function addProductFilter($productId)
    {
        $this->getSelect()
            ->join(
                array('tp' => $this->getResource()->getTable('amabtesting/test_product')),
                'tp.test_id = main_table.id',
                array()
            )
        ;

        if (is_array($productId)) {
            $this->getSelect()->where('tp.product_id IN (?)', $productId);
        }
        else {
            $this->getSelect()->where('tp.product_id=?', $productId);
        }

        return $this;
    }

    public function addActiveFilter()
    {
        $this->addFieldToFilter(
            'status', Amasty_Abtesting_Model_Test::STATUS_RUNNING
        );

        return $this;
    }

    public function getFirstTest()
    {
        $this
            ->setPageSize(1)
            ->setOrder('test_id', 'ASC')
        ;

        return $this->getFirstItem();
    }
}