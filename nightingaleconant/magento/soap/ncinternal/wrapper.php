<?php

include_once('XmlWriter.class.php');
include_once('soap/CreateXMLRequest.inc');

function XML2Array(SimpleXMLElement $parent) {
    $array = array();
    foreach ($parent as $name => $element) {
	($node = & $array[$name])
	&& (1 === count($node) ? $node = array($node) : 1)
	&& $node = & $node[];
                            
	$node = $element->count() ? XML2Array($element) : trim($element);
    }
    return $array;
}

$ssl = "off";
if ( isset($_SERVER['HTTPS'])) {
    $ssl = $_SERVER['HTTPS'];
}

$result = '';
$wdsl = '';

if ( $ssl == "on" ) {
    $jsonRequest = isset($_GET['jsonrequest']) ? $_GET['jsonrequest'] : "";
    if ( $jsonRequest != '' ) {
	$ary_data = json_decode($jsonRequest, true);
	$XMLRequest = CreateXMLRequest($ary_data);
	
	$wsdl = "https://www.nightingale.com/soap/ncinternal/server.php?wsdl";
	$wsdl_out = "www.nightingale.com/soap/ncinternal/server.php?wsdl";

	//$client = new nusoap_client($wsdl, 'wsdl');
	$client = new SoapClient(null, array(
	    'location' => "https://www.nightingale.com/soap/ncinternal/server2.php",
	    'uri'      => "urn://www.nightingale.com",
	    'trace'    => 1 ));
	    
	//$response = $client->call('XMLRequest', array('XMLRequest' => $XMLRequest));
	$response = $client->__soapCall("XMLRequest",array($XMLRequest));
	$xml   = simplexml_load_string($response);
	
	$array = XML2Array($xml);
	$array_xmldata = array();
	$array_xmldata = $array['XMLData'];
	$ary_response = array("jsonResponse" => $array_xmldata);
	//$ary_response = array($xml->getName() => $array);
	$result = json_encode($ary_response);	

    } else {
	$ary_data = array("jsonResponse" =>  array("ServiceName" => "TestService", "Version" => "", "ServiceStatus" => "fault", "ServiceStatusMessage" => "No Request Data Received."));
	$result = json_encode($ary_data);
    }
} else {
    $ary_data = array("jsonResponse" => array("ServiceName" => "TestService", "Version" => "", "ServiceStatus" => "fault", "ServiceStatusMessage" => "SSL is required."));
    $result = json_encode($ary_data);
}

//$result_out = addslashes($result);
$result_out = $result;

$resp = <<<STRING
{
    "callstate": "Success",
    "result": $result_out
}
STRING;

header( 'Content-type: application/x-javascript');
print "jsonCallback($resp)";

?>