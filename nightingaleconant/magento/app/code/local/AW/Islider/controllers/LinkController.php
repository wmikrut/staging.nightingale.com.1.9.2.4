<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_LinkController extends Mage_Core_Controller_Front_Action
{
    protected function outAction()
    {
        $request = $this->getRequest();
        if (!Mage::helper('awislider')->isModuleOutputEnabled() || !$request->getParam('sid')) {
            return $this;
        }

        $image = Mage::getModel('awislider/image')->load($request->getParam('sid'));
        if (null !== $image->getId()) {
            $image->setClicksTotal($image->getClicksTotal() + 1);
            $cookie = Mage::getSingleton('core/cookie')->get(AW_Islider_Helper_Data::AWIS_COOKIE_PREFIX . $image->getId());
            if (!$cookie) {
                Mage::getSingleton('core/cookie')->set(AW_Islider_Helper_Data::AWIS_COOKIE_PREFIX . $image->getId(), 1, true, '/');
                $image->setClicksUnique($image->getClicksUnique() + 1);
            }
            try {
                $image->save();
            } catch (Exception $e) {
                Mage::logException($e);
            }
            $request->setParam('sid', null);
            return $this->getResponse()->setRedirect($image->getData('url'));
        }
        return $this->_redirectReferer();
    }
}
