<?php

class NCCUSTOM_AddToCart_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
	$formKey = Mage::getSingleton('core/session')->getFormKey();
	
	$p = $this->getRequest()->getParam('p');
        $q = $this->getRequest()->getParam('q');
        $c = $this->getRequest()->getParam('c');
        
        $error = 0;
        
        if ( $c == 1 ) 
        {
    	    echo "Clear the cart before proceeding...</br>";
        }
        else
        {
    	    echo "Do not clear the cart...</br>";
        }
        
	$_product = Mage::getModel('catalog/product')->load($p);
	$product_name = $_product->getName();
	
	if ( $product_name == null ) 
	{
	    $error = 1;
	}
	
	if (( $q < 1 ) or ( $q > 10 ) )
	{
	    $error = 2;
	}
	
	if ( $error == 0 ) 
	{
	    echo "Add ". $product_name. "(". $p. ") to the cart for a quantity of ". $q. "</br>";
	}
	else
	{
	    echo "Error:  Inalid part or quantity < 0 or > 10</br>";
	}
    }
}

?>
