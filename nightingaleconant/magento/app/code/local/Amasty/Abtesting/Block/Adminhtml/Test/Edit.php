<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Block_Adminhtml_Test_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'amabtesting';
        $this->_controller = 'adminhtml_test';

        $test = Mage::registry('current_test');

        if ($test->isReadonly())
        {
            $this
                ->_removeButton('save')
                ->_removeButton('reset')
            ;

            $this->_formScripts[]="
$('testTabs').select('.entry-edit input[type=text],.entry-edit textarea,.entry-edit select').each(function(e){
    e.setAttribute('disabled', 'disabled');
});
$('testTabs').select('.btn-wysiwyg').each(function(e){
    e.remove();
});
            ";
        }
        else {
            $this->_addButton(
                'save_and_edit_button',
                array(
                    'label'   => $this->__('Save and Continue Edit'),
                    'onclick' => 'saveAndContinueEdit()',
                    'class'   => 'save'
                ),
                100
            );
        }

        if ($test->getStatus() == Amasty_Abtesting_Model_Test::STATUS_RUNNING) {
            $this->_removeButton('delete');
        }
    }

    public function getHeaderText()
    {
        $header = $this->__('Edit Test');

        return $header;
    }
}
