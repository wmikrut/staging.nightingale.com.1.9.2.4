<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Block_Adminhtml_Test_Edit_Tab_Variation extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _fieldName($name)
    {
        return "variation[{$this->getVariation()->getId()}][$name]";
    }

    protected function _fieldId($attribute)
    {
        return "variation_{$this->getVariation()->getId()}_{$attribute->getAttributeCode()}";
    }

    protected function _prepareForm()
    {
        $test = Mage::registry('current_test');

        $variation = $this->getVariation();

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getData('action'),
            'method' => 'post'
        ));

        $commonFieldset = $form->addFieldset('common_fieldset',
            array(
                'legend' => $this->__('General'),
            )
        );

        $commonFieldset->addField('id', 'hidden', array(
            'name' => $this->_fieldName($variation->getIdFieldName()),
            'value' => $variation->getId(),
        ));

        $commonFieldset->addField('abvariation_name', 'text', array(
            'name' => $this->_fieldName('abvariation_name'),
            'label' => $this->__('Variation Name'),
            'title' => $this->__('Variation Name'),
            'value' => $variation->getName(),
        ));

        $dataFieldset = $form->addFieldset('data_fieldset',
            array(
                'legend' => $this->__('Test Data'),
                'class'  => 'fieldset-wide'
            )
        );

        $this->_setFieldset($test->getAttributes(), $dataFieldset);

        $metaproduct = $variation->getMetaProduct();

        foreach ($test->getAttributes() as $attribute) {
            if ($element = $form->getElement($this->_fieldId($attribute))) {
                $value = $metaproduct->getData($attribute->getAttributeCode());
                $element->setValue($value);
            }
        }

        $this->setForm($form);

        return parent::_prepareForm();
    }

    protected function _setFieldset($attributes, $fieldset, $exclude=array())
    {
        $this->_addElementTypes($fieldset);
        foreach ($attributes as $attribute) {
            if (($inputType = $attribute->getFrontend()->getInputType())) {

                $fieldType      = $inputType;
                $rendererClass  = $attribute->getFrontend()->getInputRendererClass();
                if (!empty($rendererClass)) {
                    $fieldType  = $inputType . '_' . $attribute->getAttributeCode();
                    $fieldset->addType($fieldType, $rendererClass);
                }

                $attribute->getFrontend()->getAttribute()->setIsRequired(false);
                $element = $fieldset->addField(
                    $this->_fieldId($attribute),
                    $fieldType,
                    array(
                        'name'      => $this->_fieldName($attribute->getAttributeCode()),
                        'label'     => $attribute->getTitle(),
                        'class'     => $attribute->getFrontend()->getClass(),
                        'required'  => false,
                        'note'      => $attribute->getNote(),
                    )
                )
                    ->setEntityAttribute($attribute);

                $element->setAfterElementHtml($this->_getAdditionalElementHtml($element));

                if ($inputType == 'select') {
                    $element->setValues($attribute->getSource()->getAllOptions(true, true));
                } else if ($inputType == 'multiselect') {
                    $element->setValues($attribute->getSource()->getAllOptions(false, true));
                    $element->setCanBeEmpty(true);
                } else if ($inputType == 'date') {
                    $element->setImage($this->getSkinUrl('images/grid-cal.gif'));
                    $element->setFormat(Mage::app()->getLocale()->getDateFormatWithLongYear());
                } else if ($inputType == 'datetime') {
                    $element->setImage($this->getSkinUrl('images/grid-cal.gif'));
                    $element->setTime(true);
                    $element->setStyle('width:50%;');
                    $element->setFormat(
                        Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT)
                    );
                } else if ($inputType == 'multiline') {
                    $element->setLineCount($attribute->getMultilineCount());
                }
            }
        }
    }

    protected function _getAdditionalElementTypes()
    {
        $result = array(
            'weight'   => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_weight'),
            'boolean'  => Mage::getConfig()->getBlockClassName('adminhtml/catalog_product_helper_form_boolean'),
            'textarea' => Mage::getConfig()->getBlockClassName('adminhtml/catalog_helper_form_wysiwyg')
        );

        return $result;
    }
}
