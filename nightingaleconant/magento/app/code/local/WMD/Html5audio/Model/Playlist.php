<?php
/**
 * WMD_Html5audio_Model_Playlist
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2013 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Model_Playlist extends Mage_Core_Model_Abstract
{
            
    public function _construct()
    {
        parent::_construct();
        $this->_init('html5audio/playlist');
    }
	     	  
    /**
     * Retrieve array of sample id's for playlist
     *
     *
     * @return array
     */
    public function getSamples()
    {
                
        if (!$this->getId()) {
            return array();
        }

        return $this->getResource()->getSamples($this);
    }
          
    /**
     * Load playlist by hash
     * 
     * @param string $hash
     * @return object WMD_Html5audio_Model_Playlist
     */                       
    public function loadByHash($hash)
    {
        $this->_getResource()->loadByHash($this, $hash);
        return $this;
    }
    
}
