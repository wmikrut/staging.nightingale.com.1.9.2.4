<?php
/**
 * Toogas Lda.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA (End-User License Agreement)
 * that is bundled with this package in the file licence_toogas_community.txt.
 * It is also available at this URL:
 * http://www.toogas.com/licences/toogas_community.txt
 *
 * @category   Toogas
 * @package    Toogas_Featuredpopup
 * @copyright  Copyright (c) 2010 Toogas Lda. (http://www.toogas.com)
 * @license    http://www.toogas.com/licences/toogas_community.txt
 */
class Toogas_Featuredpopup_Helper_Data extends Mage_Core_Helper_Abstract
{

	const XML_FEATUREDPOPUPENABLED = 'toogas_featuredpopup/configfeaturedpopup/featured_popup_enabled';
	const XML_FEATUREDPOPUPSERIAL = 'toogas_featuredpopup/configfeaturedpopup/featured_popup_serial';	
	const XML_FEATUREDPOPUPBLOCKAGENT = 'toogas_featuredpopup/configfeaturedpopup/featured_block_agent';
	
	public function sacaStatus() {
		return array('0' => Mage::helper('featuredpopup')->__('Disabled'), 
		'1' => Mage::helper('featuredpopup')->__('Enabled'));		
	}
	
	public function getSystemStatus() {
		return (bool) Mage::getStoreConfigFlag(self::XML_FEATUREDPOPUPENABLED);
	}
	
	public function getSerial($store = null) {
		return Mage::getStoreConfig(self::XML_FEATUREDPOPUPSERIAL, $store);		
	}
	
	public function blockUserAgent() {
		return Mage::getStoreConfig(self::XML_FEATUREDPOPUPBLOCKAGENT);
	}	
	
	public function getTipoOptions($grid = false) {
		if (!$grid) {
			$value[] = array('value' => 1, 'label' => $this->__('Image'));
			$value[] = array('value' => 2, 'label' => $this->__('Editor'));
			return $value;
		}
		else {
			return array(1 => $this->__('Image'),  2 => $this->__('Editor'));			
		}
	}
	
	public function getPageOptions($grid = false) {
		if (!$grid) {
			$value[] = array('value' => 1, 'label' => $this->__('All Pages'));
			$value[] = array('value' => 2, 'label' => $this->__('Category Pages'));
			$value[] = array('value' => 3, 'label' => $this->__('Product Pages'));
			$value[] = array('value' => 4, 'label' => $this->__('Homepage'));
			$value[] = array('value' => 5, 'label' => $this->__('Specified Url'));
		}
		else {
			return array(1 => $this->__('All Pages'), 2 => $this->__('Category Pages'),
			3 => $this->__('Product Pages'), 4 => $this->__('Homepage'), 
			5 => $this->__('Specified Url'));
		}
		return $value;								
	}
	
	public function getShowEffectsOptions() {
		$value[] = array('value' => 'Effect.Appear', 'label' => $this->__('Appear'));
		$value[] = array('value' => 'Effect.BlindDown', 'label' => $this->__('BlindDown'));
		$value[] = array('value' => 'Effect.SlideDown', 'label' => $this->__('SlideDown'));
		$value[] = array('value' => 'Effect.Grow', 'label' => $this->__('Grow'));						
		return $value;		
	}
	
	public function getHideEffectsOptions() {
		$value[] = array('value' => 'Effect.Fade', 'label' => $this->__('Fade'));
		$value[] = array('value' => 'Effect.Puff', 'label' => $this->__('Puff'));
		$value[] = array('value' => 'Effect.DropOut', 'label' => $this->__('DropOut'));
		//$value[] = array('value' => 'Effect.Shake', 'label' => $this->__('Shake')); //n fecha
		$value[] = array('value' => 'Effect.SwitchOff', 'label' => $this->__('SwitchOff'));
		$value[] = array('value' => 'Effect.BlindUp', 'label' => $this->__('BlindUp'));
		$value[] = array('value' => 'Effect.SlideUp', 'label' => $this->__('SlideUp'));
		//$value[] = array('value' => 'Effect.Pulsate', 'label' => $this->__('Pulsate')); // n fecha
		$value[] = array('value' => 'Effect.Squish', 'label' => $this->__('Squish'));
		$value[] = array('value' => 'Effect.Fold', 'label' => $this->__('Fold'));
		$value[] = array('value' => 'Effect.Shrink', 'label' => $this->__('Shrink'));						
		return $value;		
	}	
	
	public function seCatalogo() {
		return Mage::registry('current_category') && !Mage::registry('current_product') 
		? true : false; 
	}

	public function seProduto() {
		return Mage::registry('current_product') ? true : false; 		
	}
	
	public function seHomepage() {
		return Mage::getUrl('') == Mage::getUrl('*/*/*', array('_current'=>true, '_use_rewrite'=>true));		
	}
	
	public function urlActual() {
		return Mage::helper('core/url')->getCurrentUrl();
	}
	
    public function recursiveReplace($search, $replace, $subject)
    {
        if(!is_array($subject))
        return $subject;
    
        foreach($subject as $key => $value)
        if(is_string($value))
        $subject[$key] = str_replace($search, $replace, $value);
        elseif(is_array($value))
        $subject[$key] = self::recursiveReplace($search, $replace, $value);
        return $subject;
    }
    
    public function processaTemplate($template) {
    	if (!$template) {return '';}
		//$_myHelper = Mage::helper('cms');
		return Mage::helper('cms')->getBlockTemplateProcessor()->filter($template);
    }    		
	
}