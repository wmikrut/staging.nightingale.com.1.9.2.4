<?php

class NC_FormWidget_Block_Email extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface {


    /**
     * Set the template
     */
    protected function _beforeToHtml() {
        $this->setTemplate( $this->_getData( 'template' ) );

        return $this;
    }

    /**
     * Retrieve the title
     *
     * @return string
     */
    public function getTitle() {
        if ( ( $title = $this->_getData( 'title' ) ) !== false ) {
            return $title ? $title : '';
        }

        return false;
    }

    /**
     * Retrieve the GUID
     *
     * @return string
     */
    public function getGuid() {
        if ( ( $guid = $this->_getData( 'guid' ) ) !== false ) {
            return $guid ? $guid : 'null';
        }

        return false;
    }
}