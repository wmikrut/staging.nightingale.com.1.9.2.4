<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 


class Itoris_ProductSlider_Model_Widget_Options_OrderBy {

	public function toOptionArray() {
		return array(
			array(
				'label' => 'Randomly',
				'value' => Itoris_ProductSlider_Block_ProductSlider::ORDER_BY_RANDOMLY
			),
			array(
				'label' => 'Alphabetically ',
				'value' => Itoris_ProductSlider_Block_ProductSlider::ORDER_BY_ALPHABETICALLY
			),
			array(
				'label' => 'Price: Low to High',
				'value' => Itoris_ProductSlider_Block_ProductSlider::ORDER_BY_PRICE_LOW_TO_HIGH
			),
			array(
				'label' => 'Price: High to Low',
				'value' => Itoris_ProductSlider_Block_ProductSlider::ORDER_BY_PRICE_HIGH_TO_LOW
			),
			array(
				'label' => 'Most Reviewed First',
				'value' => Itoris_ProductSlider_Block_ProductSlider::ORDER_BY_MOST_REVIEWED_FIRST
			),
			array(
				'label' => 'Top Rated First',
				'value' => Itoris_ProductSlider_Block_ProductSlider::ORDER_BY_TOP_RATED_FIRST
			),
			array(
				'label' => 'Top Sellers First',
				'value' => Itoris_ProductSlider_Block_ProductSlider::ORDER_BY_TOP_SELLERS_FIRST
			),
			array(
				'label' => 'Most Discounted First',
				'value' => Itoris_ProductSlider_Block_ProductSlider::ORDER_BY_MOST_DISCOUNTED_FIRST
			),
			array(
				'label' => 'Newest First',
				'value' => Itoris_ProductSlider_Block_ProductSlider::ORDER_BY_NEWEST_FIRST
			)
		);
	}
}
?>