<?php
class NCCUSTOM_ProductsToHubspot_Model_Observer {
	public function exportFeed($observer) {
		//*********************************************************************************
		//* At times Hubspot can be slow... and we do not want to interfere with the
		//* customer's experience.  So records will be dumped into a table and a  cron
		//* job will add the Hubspot information later
		//*********************************************************************************
		include('mysql-credentials-production.php');

		$_order = $observer->getEvent()->getOrder();
		$order_id = $_order->getId();
		$increment_id = $_order->getIncrementId();
		
		$_orderDetails = $_order->getData();
		

		$lcl_date = date("Y-m-d H:i:s");
		
		$first_time_buyer_hubspot_list = 722;
		$first_time_buyer = 0;
		$save_name = '';
		$save_price = 0;

		$customerId = $_order->getCustomerId();
		
		$couponCode = strtolower(trim($_orderDetails['coupon_code']));
		$email = $_order->getCustomerEmail();
		$first = ucfirst(strtolower($_order->getcustomerFirstname()));
		$last = ucfirst(strtolower($_order->getCustomerLastname()));
		
		$line = "Process fired: ". $lcl_date. " - $customerId/$order_id - ($email, $first, $last / $couponCode) \r\n";
		Mage::log($line, null, "NCCUSTOM_ProductsToHubspot.log");
		//Mage::log($_order, null, "NCCUSTOM_ProductsToHubspot.log");
	
		if ( $email == '' ) 
		{
		    Mage::log($_order, null, "NCCUSTOM_ProductsToHubspot.log");
		}
	
        //*********************************************************************************
        //* Check to see if this customer is a first time buyer 
        //*********************************************************************************
        $customer_id = $_order->getCustomerId();
        $ix = 0;
        $first_time_customer = 0;
        
        $_customer_orders = Mage::getResourceModel('sales/order_collection')
			->addFieldToSelect('*')
			->addFieldToFilter('customer_id', $customer_id);
			
		foreach ( $_customer_orders as $customer_order )
		{
			$ignore = 0;
			$save_price = 0;
			$save_name = '';
			
			foreach ($customer_order->getAllItems() as $customer_item)
			{
				$product_type = $customer_item->getProductType();
				$name = $customer_item->getName();
				$price = $customer_item->getPrice();
				$discount_amount = $customer_item->getDiscountAmount();
				$base_price = ( $price - $discount_amount);
				
				if (( $product_type != 'simple' ) and ( $product_type != 'downloadable' ))
				{
					$ignore = 1;
				}
				else
				{
					if ( $base_price > $save_price ) 
					{
						$save_price = $base_price;
						$save_name = $name;
					}
				}
			}
			if ( $ignore == 0 )
			{
				$ix++;
			}
		}
		
		//*********************************************************************************
        //* This is a first time buyer
        //*********************************************************************************
		if ( $ix == 1 ) 
		{
			$first_time_buyer = 1;
		}
		else
		{
		    $save_name = '';
		    $save_price = 0;
		}
			  
		foreach ($_order->getAllItems() as $item) 
		{

			$item_id = $item->getProductId();
			$product = Mage::getModel( 'catalog/product' )->load( $item->getProductId());
			$hubspot_list = trim($product->getnccustom_hubspot());
			

			if ( $hubspot_list != '' ) 
			{
				if ( $first_time_buyer == 1 ) 
				{
					$hubspot_list = $first_time_buyer_hubspot_list;
				}
				try 
				{
					if ( $email != '' ) 
					{
						$mysqli = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_db);
						$mysqlstm = <<<STRING
insert into $mysql_db.nccustom_hubspot_product
values(0,'$email','$first','$last',$item_id,$hubspot_list,CURRENT_TIMESTAMP,0,0, '$couponCode','$save_name', $order_id);
STRING;
						Mage::log($mysqlstm. "\r\n", null, 'nc_20160121a.log');
						$fnull = $mysqli->query($mysqlstm);
						mysqli_close($mysqli);
					}
				} 
				catch (Exception $e) 
				{
					$line = 'NCCUSTOM Caught exception: '. $e->getMessage();
					Mage::log($line);
				}
			} 
			else 
			{
				
				//*************************************************************************
				//* If this customer is not being added to a HubSpot list by product,
				//* Let's determine what list the customer should go to
				//*************************************************************************
				$customerGroupId = $_order->getCustomerGroupId();
				if ( 	($customerGroupId != 4) 
					and ($customerGroupId != 6) 
					and	($customerGroupId != 7)             	
					) 
				{ 
					//Exclude Insiders Club Members
					$hubspot_list_id = 450; //Magento - Buyers added by API
				
					try {
						$mysqli = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_db);
						$mysqlstm = <<<STRING
select *
from $mysql_db.nccustom_coupon_to_hubspot
where lcase(coupon_code) = '$couponCode'
and active = 1;
STRING;
						$f001 = $mysqli->query($mysqlstm);
						while ( $db1 = $f001->fetch_assoc()) 
						{
							$hubspot_list_id = $db1['hubspot_list_id'];
						}
						
						if ( $first_time_buyer == 1 ) 
						{
							$hubspot_list_id = $first_time_buyer_hubspot_list;
						}
						
						if ( $hubspot_list_id > 0 ) 
						{
							if ( $email != '' ) 
							{
								$mysqlstm = <<<STRING
insert into $mysql_db.nccustom_hubspot_product
values(0,'$email','$first','$last',0,$hubspot_list_id,CURRENT_TIMESTAMP,0,0, '$couponCode','$save_name',$order_id);
STRING;
								Mage::log($mysqlstm. "\r\n", null, 'nc_20160121b.log');
								$fnull = $mysqli->query($mysqlstm);
							}
						}
						mysqli_close($mysqli);
					} 
					catch (Exception $e) 
					{
						$line = 'NCCUSTOM Caught exception: '. $e->getMessage();
						Mage::log($line);
					}
				}
			}
        }              
    }
}
?>
