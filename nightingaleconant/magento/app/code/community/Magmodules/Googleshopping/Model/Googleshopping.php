<?php
/**
 * Magmodules.eu - http://www.magmodules.eu - info@magmodules.eu
 * =============================================================
 * NOTICE OF LICENSE [Single domain license]
 * This source file is subject to the EULA that is
 * available through the world-wide-web at:
 * http://www.magmodules.eu/license-agreement/
 * =============================================================
 * @category    Magmodules
 * @package     Magmodules_Googleshopping
 * @author      Magmodules <info@magmodules.eu>
 * @copyright   Copyright (c) 2016 (http://www.magmodules.eu)
 * @license     http://www.magmodules.eu/license-agreement/  
 * =============================================================
 */
 
class Magmodules_Googleshopping_Model_Googleshopping extends Magmodules_Googleshopping_Model_Common {
	
	public function generateFeed($store_id, $time_start) 
	{
        $limit = $this->setMemoryLimit($store_id);
        $config = $this->getFeedConfig($store_id);	
		$products = $this->getProducts($config, $config['limit']);	
		$prices = Mage::helper('googleshopping')->getTypePrices($config, $products);
		if($feed = $this->getFeedData($products, $config, $time_start, $prices)) {
			return $this->saveFeed($feed, $config, 'googleshopping', $feed['config']['products']);
		}	
	}

	public function getFeedData($products, $config, $time_start, $prices) 
	{		
		foreach($products as $product) {
			$parent_id = Mage::helper('googleshopping')->getParentData($product, $config);
			if($parent_id > 0) { $parent = $products->getItemById($parent_id); } else { $parent = ''; }
			if($product_data = Mage::helper('googleshopping')->getProductDataRow($product, $config, $parent)) {
				foreach($product_data as $key => $value) {
					if((!is_array($value)) && (!empty($value))) { $product_row[$key] = $value; }	
				}
				if($extra_data = $this->getExtraDataFields($product_data, $config, $product, $prices)) {
					$product_row = array_merge($product_row, $extra_data);
				}
				$feed['channel'][] = $product_row;				
				unset($product_row);
			}
		}	
		if(!empty($feed)) {
			$return_feed = array();
			$return_feed['config'] = $this->getFeedHeader($config, $time_start, count($feed['channel']));
			$return_feed['channel'] = $feed['channel'];			
			return $return_feed;
		} else {
			$return_feed = array();
			$return_feed['config'] = $this->getFeedHeader($config, $time_start);
			return $return_feed;	
		}
	}
	
	public function getFeedConfig($storeId, $type = 'xml')
	{
		$config							= array();
		$feed 							= Mage::helper('googleshopping'); 
        $filename 						= $this->getFileName('googleshopping', $storeId);
		$websiteId 						= Mage::app()->getStore($storeId)->getWebsiteId();

		// DEFAULTS
		$config['store_id'] 			= $storeId;
		$config['website_name']			= $feed->cleanData(Mage::getModel('core/website')->load($websiteId)->getName(), 'striptags');
		$config['website_url']			= Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
		$config['media_url']			= Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
		$config['media_image_url']		= $config['media_url'] . 'catalog' . DS . 'product';
		$config['media_gallery_id'] 	= Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product', 'media_gallery');
		$config['file_name']			= $filename;
		$config['limit']	 			= Mage::getStoreConfig('googleshopping/generate/limit', $storeId);
		$config['filters']				= @unserialize(Mage::getStoreConfig('googleshopping/filter/advanced', $storeId));	
		$config['version']				= (string)Mage::getConfig()->getNode()->modules->Magmodules_Googleshopping->version;

		// PRODUCT & CATEGORY 
		$config['filter_enabled']		= Mage::getStoreConfig('googleshopping/filter/category_enabled', $storeId);
		$config['filter_cat']			= Mage::getStoreConfig('googleshopping/filter/categories', $storeId);		
		$config['filter_type']			= Mage::getStoreConfig('googleshopping/filter/category_type', $storeId);
		$config['filter_status']		= Mage::getStoreConfig('googleshopping/filter/visibility_inc', $storeId);
		$config['category_default']		= Mage::getStoreConfig('googleshopping/data/category_fixed', $storeId);
		$config['producttype']	 		= Mage::getStoreConfig('googleshopping/advanced/producttype', $storeId);
		$config['identifier']	 		= Mage::getStoreConfig('googleshopping/advanced/identifier', $storeId);
		$config['stock'] 				= Mage::getStoreConfig('googleshopping/filter/stock', $storeId);					
		$config['conf_enabled']			= Mage::getStoreConfig('googleshopping/advanced/conf_enabled', $storeId);	
		$config['conf_fields']			= Mage::getStoreConfig('googleshopping/advanced/conf_fields', $storeId);	
		$config['conf_switch_urls']		= Mage::getStoreConfig('googleshopping/advanced/conf_switch_urls', $storeId);	
		$config['conf_exclude_parent']	= Mage::getStoreConfig('googleshopping/advanced/conf_enabled', $storeId);	
		$config['url_suffix']			= Mage::getStoreConfig('googleshopping/advanced/url_utm', $storeId);	
		$config['images'] 				= Mage::getStoreConfig('googleshopping/data/images', $storeId);
		$config['image1'] 				= Mage::getStoreConfig('googleshopping/data/image1', $storeId);
		$config['condition_default']	= Mage::getStoreConfig('googleshopping/data/condition_default', $storeId);	

		// STOCK & CONDITION
		$config['stock_manage']			= Mage::getStoreConfig('cataloginventory/item_options/manage_stock');
		$config['stock_instock']		= 'in stock';
		$config['stock_outofstock']		= 'out of stock';
		$config['condition_default'] 	= Mage::getStoreConfig('googleshopping/data/condition_default', $storeId);
		$config['hide_no_stock']		= Mage::getStoreConfig('googleshopping/filter/stock', $storeId);
		
		// WEIGHT
		$config['weight']				= Mage::getStoreConfig('googleshopping/advanced/weight', $storeId);
		$config['weight_units']			= Mage::getStoreConfig('googleshopping/advanced/weight_units', $storeId);

		// PRICE
		$config['price_scope']			= Mage::getStoreConfig('catalog/price/scope');
		$config['price_add_tax']	 	= Mage::getStoreConfig('googleshopping/advanced/add_tax', $storeId);
		$config['price_add_tax_perc']	= Mage::getStoreConfig('googleshopping/advanced/tax_percentage', $storeId);
		$config['price_grouped']		= Mage::getStoreConfig('googleshopping/advanced/grouped_price', $storeId);
		$config['force_tax']	 		= Mage::getStoreConfig('googleshopping/advanced/force_tax', $storeId);
		$config['currency'] 			= Mage::app()->getStore($storeId)->getCurrentCurrencyCode(); 
		$config['base_currency_code'] 	= Mage::app()->getStore($storeId)->getBaseCurrencyCode();
		$config['markup']				= Mage::helper('googleshopping')->getPriceMarkup($config);
		$config['use_tax']				= Mage::helper('googleshopping')->getTaxUsage($config);

		// SHIPPING
		$config['shipping']				= @unserialize(Mage::getStoreConfig('googleshopping/advanced/shipping', $storeId));
		
		// CHECK CUSTOM ATTRIBUTES
		$eavAttribute = new Mage_Eav_Model_Mysql4_Entity_Attribute();		
		if($eavAttribute->getIdByCode('catalog_category', 'googleshopping_category')) {
			$config['category_custom'] = 'googleshopping_category';
		}
		if($eavAttribute->getIdByCode('catalog_product', 'googleshopping_exclude')) {
			$config['filter_exclude'] = 'googleshopping_exclude';
		}		
		if(Mage::getStoreConfig('googleshopping/data/condition_type', $storeId) == 'attribute') {
			$config['condition_attribute']	= Mage::getStoreConfig('googleshopping/data/condition', $storeId);			
		}

		if(Mage::getStoreConfig('googleshopping/data/name', $storeId) == 'use_custom') {
			$config['custom_name'] = Mage::getStoreConfig('googleshopping/data/name_custom', $storeId);
		}

		// FIELD & CATEGORY DATA
		$config['field']			= $this->getFeedAttributes($storeId, $type, $config);
		$config['category_data']	= $feed->getCategoryData($config, $storeId);

		return $config;	
	}

	public function getFeedAttributes($storeId = 0, $type = 'xml', $config = '')
	{
		$attributes = array();
		$attributes['id']			= array('label' => 'g:id', 'source' => Mage::getStoreConfig('googleshopping/data/id', $storeId));
		$attributes['title']		= array('label' => 'g:title', 'source' => Mage::getStoreConfig('googleshopping/data/name', $storeId), 'action' => 'striptags_truncate150_uppercheck');
		$attributes['description']	= array('label' => 'g:description', 'source' => Mage::getStoreConfig('googleshopping/data/description', $storeId), 'action' => 'striptags_truncate');
		$attributes['gtin']			= array('label' => 'g:gtin', 'source' => Mage::getStoreConfig('googleshopping/data/gtin_attribute', $storeId), 'action' => 'striptags');
		$attributes['brand']		= array('label' => 'g:brand', 'source' => Mage::getStoreConfig('googleshopping/data/brand_attribute', $storeId), 'action' => 'striptags');
		$attributes['mpn']			= array('label' => 'g:mpn', 'source' => Mage::getStoreConfig('googleshopping/data/mpn_attribute', $storeId), 'action' => 'striptags');
		$attributes['color']		= array('label' => 'g:color', 'source' => Mage::getStoreConfig('googleshopping/data/color', $storeId), 'action' => 'striptags');
		$attributes['material']		= array('label' => 'g:material', 'source' => Mage::getStoreConfig('googleshopping/data/material', $storeId), 'action' => 'striptags');
		$attributes['pattern']		= array('label' => 'g:pattern', 'source' => Mage::getStoreConfig('googleshopping/data/pattern', $storeId), 'action' => 'striptags');
		$attributes['size']			= array('label' => 'g:size', 'source' => Mage::getStoreConfig('googleshopping/data/size', $storeId), 'action' => 'striptags');
		$attributes['size_type']	= array('label' => 'g:size_type', 'source' => Mage::getStoreConfig('googleshopping/data/size_type', $storeId), 'action' => 'striptags');
		$attributes['size_system']	= array('label' => 'g:size_system', 'source' => Mage::getStoreConfig('googleshopping/data/size_system', $storeId), 'action' => 'striptags');
		$attributes['gender']		= array('label' => 'g:gender', 'source' => Mage::getStoreConfig('googleshopping/data/gender', $storeId), 'action' => 'striptags');
		$attributes['product_url']	= array('label' => 'g:link', 'source' => '');
		$attributes['image_link']	= array('label' => 'g:image_link', 'source' => '');		
		$attributes['availability']	= array('label' => 'g:availability', 'source' => '');		
		$attributes['condition']	= array('label' => 'g:condition', 'source' => Mage::getStoreConfig('googleshopping/data/condition', $storeId));
		$attributes['price']		= array('label' => 'g:price', 'source' => '');		
		$attributes['weight']		= array('label' => 'g:weight', 'source' => '');		
		$attributes['categories']	= array('label' => 'categories', 'source' => '');				
		$attributes['bundle']		= array('label' => 'g:is_bundle', 'source' => '');				
		$attributes['parent_id']	= array('label' => 'g:item_group_id', 'source' => Mage::getStoreConfig('googleshopping/data/id', $storeId), 'parent' => 1);				

		if($extra_fields = @unserialize(Mage::getStoreConfig('googleshopping/advanced/extra', $storeId))) {
			foreach($extra_fields as $extra_field) {
				$attributes[$extra_field['attribute']] = array('label' => $extra_field['name'], 'source' => $extra_field['attribute'], 'action' => $extra_field['action']);		
			}
		}
		
		if($type == 'flatcheck') {
			if($filters = @unserialize(Mage::getStoreConfig('googleshopping/filter/advanced', $storeId))) {
				foreach($filters as $filter) {
					$attributes[$filter['attribute']] = array('label' => $filter['attribute'], 'source' => $filter['attribute']);
				}
			}
			if(Mage::getStoreConfig('googleshopping/data/name', $storeId) == 'use_custom') {
				$custom_values = Mage::getStoreConfig('googleshopping/data/name_custom', $storeId);
				$att = preg_match_all("/{{([^}]*)}}/", $custom_values, $found_atts);			
				if(!empty($found_atts)) {
					foreach($found_atts[1] as $att) {
						$attributes[$att] = array('label' => $att, 'source' => $att);
					}	
				}	
			}
		}
		
		return Mage::helper('googleshopping')->addAttributeData($attributes, $config);	
	}
	
	public function getFileName($type, $storeId, $refresh = 1) 
	{
        if(!$fileName = Mage::getStoreConfig($type . '/generate/filename', $storeId)) {
			$fileName = $type . '.xml';
		}
		
		if(substr($fileName, -3) != 'xml') {
			$fileName = $fileName . '-' . $storeId. '.xml';
		} else {
			$fileName = substr($fileName, 0, -4) . '-' . $storeId. '.xml';			
		}

        if(!file_exists(Mage::getBaseDir('media') . DS . $type)) {
        	mkdir(Mage::getBaseDir('media') . DS . $type);
        }
        
        $fileName = Mage::getBaseDir() . DS . 'media' . DS . $type . DS . $fileName;
		if(file_exists($fileName) && ($refresh)) {
            unlink($fileName);
        }
        return $fileName;
    }
   
    public function saveFeed($feed, $config, $type, $count) 
    {	
		$encoding = Mage::getStoreConfig('design/head/default_charset');
		$xml_data = new SimpleXMLElement("<rss xmlns:g=\"http://base.google.com/ns/1.0\" version=\"2.0\" encoding=\"" . $encoding . "\"></rss>");
		$this->getArray2Xml($feed, $xml_data);
		$dom = dom_import_simplexml($xml_data)->ownerDocument;
		$dom->encoding = $encoding;
		$dom->formatOutput = true;
		$xml_feed = $dom->saveXML();
        if (!file_put_contents($config['file_name'], $xml_feed)) {       
  			Mage::getSingleton('adminhtml/session')->addError(Mage::helper($type)->__('File writing not succeeded'));
        } else {
        	$filename = $config['file_name'];
        	$store_id = $config['store_id'];
	        $local_path = Mage::getBaseDir() . DS . 'media' . DS . $type . DS;
        	$filename = str_replace($local_path, '', $filename);
        	$websiteUrl = Mage::app()->getStore($store_id)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        	$feed_url = $websiteUrl . $type . '/' . $filename;			
			$result = array();
			$result['url'] = $feed_url;
			$result['shop'] = Mage::app()->getStore($store_id)->getCode();
			$result['date'] = date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time()));
			$result['qty'] = $count;
        	return $result;
        }
	}        

	protected function getPrices($data, $conf_prices, $id) 
	{			
		$prices = array();
		if(!empty($conf_prices[$id])) {
			$prices['g:price'] = $conf_prices[$id];	
		} else {
			if(isset($data['sales_price'])) {
				$prices['g:price'] = $data['regular_price'];
				$prices['g:sale_price'] = $data['sales_price'];
				if(isset($data['sales_date_start']) && isset($data['sales_date_end'])) {
					$prices['g:sale_price_effective_date'] = str_replace(' ', 'T', $data['sales_date_start']) . '/' . str_replace(' ', 'T', $data['sales_date_end']);
				}		
			} else {
				$prices['g:price'] = $data['price'];	
			}
		}
		return $prices;
	}
	
	protected function getShipping($data, $config) 
	{
		$shipping_array = array(); $i = 1;
		if(!empty($config['shipping'])) {
			foreach($config['shipping'] as $shipping) {
				$price = $data['g:price']['final_price_clean'];
				if(($price >= $shipping['price_from']) && ($price <= $shipping['price_to'])) {
					$shipping_price = $shipping['price'];
					$shipping_price = number_format($shipping_price, 2, '.', '') . ' ' . $config['currency'];					
					$shipping_array_r['g:country'] = $shipping['country'];
					$shipping_array_r['g:service'] = $shipping['service'];
					$shipping_array_r['g:price'] = $shipping_price;
					$shipping_array['g:shipping-' . $i] = $shipping_array_r;
					$i++;
				}				
			}
		}
		return $shipping_array;
	}

	protected function getIdentifierExists($product_data, $config) 
	{			
		if($config['identifier'] == 1) {
			$identifiers = '';
			if(!empty($product_data['g:gtin'])) { $identifiers++; }	
			if(!empty($product_data['g:brand'])) { $identifiers++; }	
			if(!empty($product_data['g:mpn'])) { $identifiers++; }									
			if($identifiers < 2) {
				$identifier['g:identifier_exists'] = 'FALSE';
				return $identifier;
			}
		}	
		if($config['identifier'] == 2) {
			$identifier['g:identifier_exists'] = 'FALSE';
			return $identifier;
		}		
	}

	protected function getCategoryData($product_data, $config) 
	{			
		$level_1 = $level_2 = '';
		$category = array();
		$category['g:google_product_category'] = Mage::helper('googleshopping')->cleanData($config['category_default'], 'stiptags');		
		if(!empty($product_data['categories'])) {
			foreach($product_data['categories'] as $cat) {
				if($cat['level'] > $level_1) {
					if(!empty($cat['custom'])) {
						$category['g:google_product_category'] = $cat['custom'];
						$level_1 = $cat['level'];
					}	
				}
			}
			if(!empty($config['producttype'])) {
				foreach($product_data['categories'] as $cat) {
					if($cat['level'] > $level_2) {
						$category['g:product_type'] = implode(' > ', $cat['path']);					
						$level_2 = $cat['level'];
					}
				}	
			}
		}
		return $category;		
	}
			
	public function getImages($product_data, $config) 
	{					
		$_images = array(); $i = 1; 
		if($config['images'] == 'all') {
			if(!empty($config['image1'])) {
				if(!empty($product_data['image'][$config['image1']])) {
					$_images['g:image_link'] = $product_data['image'][$config['image1']];
				}
			} else {
				if(!empty($product_data['image']['base'])) {
					$_images['g:image_link'] = $product_data['image']['base'];
				}						
			}
			if(empty($_images['g:image_link'])) {
				$_images['g:image_link'] = $product_data['image_link'];		
			}
			if(!empty($product_data['image']['all'])) {
				foreach($product_data['image']['all'] as $image) {
					if(!in_array($image, $_images)) {				
						$_images['g:additional_image_link' . $i] = $image;
						$i++;
					}	
				}		
			}
		}
		return $_images;
	}

	protected function getCustomData($product_data, $config, $product) 
	{			
		$custom = array();
		if(isset($config['custom_name'])) {
			$custom['g:title'] = $this->reformatString($config['custom_name'], $product, '');
		}
		return $custom;
	}	

	protected function reformatString($data, $product, $symbol = '') 
	{
		$att = preg_match_all("/{{([^}]*)}}/", $data, $attributes);			
		if(!empty($attributes)) {
			$i = 0;
			foreach($attributes[0] as $key => $value) {
				if(!empty($product[$attributes[1][$key]])) {				
					if($product->getAttributeText($attributes[1][$key])) {
						$data = str_replace($value, $product->getAttributeText($attributes[1][$key]), $data);
					} else {
						$data = str_replace($value, $product[$attributes[1][$key]], $data);						
					}						
				} else {
					$data = str_replace($value, '', $data);					
				}
				if($symbol) {
					$data = preg_replace('/' . $symbol . '+/', ' ' . $symbol .' ', rtrim(str_replace(' ' . $symbol . ' ', $symbol, $data), $symbol));
				}
			}
		}
		return trim($data);
	}	
	
	protected function getExtraDataFields($product_data, $config, $product, $prices) 
	{
		$_extra = array();
		if($_custom = $this->getCustomData($product_data, $config, $product)) {
			$_extra = array_merge($_extra, $_custom);
		}
		if($_identifier_exists = $this->getIdentifierExists($product_data, $config)) {
			$_extra = array_merge($_extra, $_identifier_exists);
		}
		if($_category_data = $this->getCategoryData($product_data, $config)) {
			$_extra = array_merge($_extra, $_category_data);
		}
		if($_prices = $this->getPrices($product_data['g:price'], $prices, $product->getEntityId())) {
			$_extra = array_merge($_extra, $_prices);
		}
		if($_images = $this->getImages($product_data, $config)) {
			$_extra = array_merge($_extra, $_images);
		}
		if($_shipping = $this->getShipping($product_data, $config)) {
			$_extra = array_merge($_extra, $_shipping);
		}
		return $_extra;
	}	

	function getArray2Xml($array, &$xml_user_info) 
	{
		foreach($array as $key => $value) {
			if(is_array($value)) {
				if(!is_numeric($key)) {
					if(substr($key,0,10) == 'g:shipping') {
						$key = 'g:shipping';
						$subnode = $xml_user_info->addChild("$key", "", "http://base.google.com/ns/1.0");
						$this->getArray2Xml($value, $subnode);
					} else {
						$subnode = $xml_user_info->addChild("$key");
						$this->getArray2Xml($value, $subnode);
					}	
				} else {				
					$subnode = $xml_user_info->addChild("item");
					$this->getArray2Xml($value, $subnode);
				}
			} else {
				if(substr($key,0,23) == 'g:additional_image_link') {
					$key = 'g:additional_image_link';
				}	
				$xml_user_info->addChild("$key", htmlspecialchars("$value"), "http://base.google.com/ns/1.0");
			}
		}
	}		

	protected function setMemoryLimit($storeId)
	{
		if(Mage::getStoreConfig('googleshopping/generate/overwrite', $storeId)) {
			if($memory_limit = Mage::getStoreConfig('googleshopping/generate/memory_limit', $storeId)) {
				ini_set('memory_limit', $memory_limit);
			}		
			if($max_execution_time = Mage::getStoreConfig('googleshopping/generate/max_execution_time', $storeId)) {
				ini_set('max_execution_time', $max_execution_time);
			}		
		}	
	} 

	protected function getFeedHeader($config, $time_start, $count = 0) 
	{
		$header = array();
		$header['system'] = 'Magento';
		$header['extension'] = 'Magmodules_Googleshopping';
		$header['extension_version'] = $config['version'];
		$header['store'] = $config['website_name'];
		$header['url'] = $config['website_url'];
		if($config['limit'] > 0) {
			$header['set_limit'] = $config['limit'];
		}
		$header['products'] = $count;
		$header['generated'] = Mage::getModel('core/date')->date('Y-m-d H:i:s');
		$header['processing_time'] = number_format((microtime(true) - $time_start), 4);
		return $header;
	}

}