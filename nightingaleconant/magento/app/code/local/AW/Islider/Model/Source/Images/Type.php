<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Model_Source_Images_Type extends AW_Islider_Model_Source_Abstract
{
    const FILE      = 1;
    const REMOTEURL = 2;

    const FILE_LABEL      = 'File';
    const REMOTEURL_LABEL = 'Remote URL';

    public function toOptionArray()
    {
        return array(
            array('value' => self::FILE, 'label' => Mage::helper('awislider')->__(self::FILE_LABEL)),
            array('value' => self::REMOTEURL, 'label' => Mage::helper('awislider')->__(self::REMOTEURL_LABEL))
        );
    }
}