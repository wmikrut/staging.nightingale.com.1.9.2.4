<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Block_Block extends Mage_Core_Block_Template
{
    protected $_sliders = null;

    public function getBlockPosition()
    {
        switch ($this->getNameInLayout()) {
            case 'awis.sidebar.left.bottom':
                return AW_Islider_Model_Source_Autoposition::LEFTCOLUMN;
                break;
            case 'awis.sidebar.right.bottom':
                return AW_Islider_Model_Source_Autoposition::RIGHTCOLUMN;
                break;
            case 'awis.content.top':
                return AW_Islider_Model_Source_Autoposition::BEFORECONTENT;
                break;
            default:
                return AW_Islider_Model_Source_Autoposition::NONE;
        }
    }

    public function getSliderCollection()
    {
        if ($this->_sliders === null) {
            $collection = $this->_getCollection();
            $this->_sliders = $collection->load();
        }
        return $this->_sliders;
    }

    protected function _getCollection()
    {
        /** @var AW_Islider_Model_Mysql4_Slider_Collection $collection */
        $collection = Mage::getModel('awislider/slider')->getCollection();
        $collection
            ->addStoreFilter(array(Mage::app()->getStore()->getId()))
            ->addEnabledFilter();
        if (null !== $this->getId()) {
            $collection->addBlockIdFilter($this->getId());
            $collection->addFilter('autoposition', AW_Islider_Model_Source_Autoposition::NONE);
            return $collection;
        }
        if (null !== $this->getIncrementId()) {
            $collection->addIdFilter($this->getIncrementId());
            $collection->addFilter('autoposition', AW_Islider_Model_Source_Autoposition::NONE);
            return $collection;
        }
        $collection->addPositionFilter($this->getBlockPosition());
        return $collection;
    }

    public function getSliderHtml($slider)
    {
        $sliderBlock = $this->getLayout()->createBlock(
            'awislider/representations_default_block',
            'awslider_' . $slider->getId()
        );
        $sliderBlock
            ->setTemplate('aw_islider/representations/default/block.phtml')
            ->setSlider($slider);
        return $sliderBlock->toHtml();
    }

    protected function _beforeToHtml()
    {
        $this->setData('cache_tags', Mage::helper('awislider')->getCacheTagsForMultipleBlock());
        if (!$this->getTemplate()
            && ($this->getData('id') || $this->getData('increment_id'))
        ) {
            $this->setTemplate('aw_islider/blocks.phtml');
        }
        return parent::_beforeToHtml();
    }
}
