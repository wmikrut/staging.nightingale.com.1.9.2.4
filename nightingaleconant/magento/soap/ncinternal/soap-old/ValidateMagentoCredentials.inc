<?php
/*
 *      ValidateMagentoCredentials.inc                                                                                                                                   *
 *      Copyright 2015 wmikrut <wmikrut72@gmail.com>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.                                                                                                                     *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 *
 *
 */

function ValidateMagentoCredentials($xml) {
    include('mysql-credentials-production.php');

    include_once('XmlWriter.class.php');
    include_once('soap/ValidateCredentials.inc');

    $result = '';
    $status = 'success';
    $status_message = '';

    $ip_address = '0.0.0.0';
    if ( isset($_SERVER['REMOTE_ADDR'])) {
        $ip_address = $_SERVER['REMOTE_ADDR'];
    }

    $xml_out = new XmlWriter2();
    $mysqli = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_db);
    $mysqli_web4 = new mysqli($mysql_hostname_web4, $mysql_username_web4, $mysql_password_web4, $mysql_db_web4);
    $xml_data = new SimpleXMLElement($xml);
    
    $xml_application_area = (string) $xml_data->XMLData->ApplicationArea;
    $xml_user_name = (string) $xml_data->XMLData->UserName;
    $xml_password = (string) $xml_data->XMLData->Password;
    $xml_version = (string) $xml_data->XMLData->Version;
    $xml_service_name = (string) $xml_data->XMLData->ServiceName;
    
    $credentials_valid = ValidateCredentials($xml_application_area, $xml_user_name, $xml_password);
    
    
    if ( $credentials_valid == 'Y') {
    	$xml_email_address = (string) $xml_data->XMLData->EmailAddress;
    	$xml_user_password = (string) $xml_data->XMLData->UserPassword;
    
    	require_once( '/var/www/vhosts/nightingale.com/httpdocs/app/Mage.php' );
    	Mage::app("default");
    	$id = 1;
    	$validate = (bool) true;
    
    	$xml_user_password = addslashes($xml_user_password);
    
    	try{
    		$validate = Mage::getModel('customer/customer')->setWebsiteId($id)->authenticate($xml_email_address, $xml_user_password);
    	} catch ( Exception $e ){
    		$validate = false;
    	}
    	if ( $validate == true ) {
    		$credentials_valid = '1';
    	} else {
    		$credentials_valid = '0';
    	}
    
    } else {
    	$status = "fault";
    	$status_message = "You are not authorized to use this service.";
    }

    $xml_out->push("XMLResponse");
    $xml_out->push("XMLData");
    $xml_out->element("ServiceName", "ValidateMagentoCredentials");
    $xml_out->element("Version", $xml_version);
    $xml_out->element("EmailAddress", $xml_email_address);
    $xml_out->element("UserPassword", $xml_user_password);
    $xml_out->element("ServiceStatus", $status);
    $xml_out->element("ServiceStatusMessage", $status_message);
    $xml_out->element("MagentoCredentialsValid", $credentials_valid);
    $xml_out->pop();
    $xml_out->pop();
    
    $result = $xml_out->getXml();
    
    $mysqlstm = <<<STRING
insert into nccustom_webservice_log
values(0, '$xml_service_name', '$xml_user_name','$ip_address','$xml','$result',CURRENT_TIMESTAMP);
STRING;
    $fnull = $mysqli->query($mysqlstm);
    
    mysqli_close($mysqli);
    mysqli_close($mysqli_web4);
    
    return $result;
    }
?>