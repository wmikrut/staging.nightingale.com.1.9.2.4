<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_CATEGORYBANNERSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_CategoryBannerSlider_Block_Slider extends Mage_Core_Block_Template {

	private $set = array();
	private $banners = array();
    private $jsCssHtml = '';

	protected function _prepareLayout() {
		if(!Mage::helper('itoris_categorybannerslider')->isRegisteredAutonomous(Mage::app()->getWebsite())){
			return;
		}
		/** @var $settings Itoris_CategoryBannerSlider_Model_Settings */
		$settings = Mage::getModel('itoris_categorybannerslider/settings');
		$settings->load(Mage::app()->getWebsite()->getId(), Mage::app()->getStore()->getId());
		if(!$settings->getEnable()){
			return;
		}
		if(!$this->getTemplate()){
			$this->setTemplate('itoris/categorybannerslider/slider.phtml');
		}
		/** @var $head Mage_Page_Block_Html_Head */
		$head = $this->getLayout()->getBlock('head');
		$head->addJs('itoris/categorybannerslider/js/slider.js');
        $this->jsCssHtml .= '<script type="text/javascript" src="' . Mage::getBaseUrl('js') . 'itoris/categorybannerslider/js/slider.js' . '"></script>';
		$head->addItem('js_css', 'itoris/categorybannerslider/css/main.css');
        $this->jsCssHtml .= '<link rel="stylesheet" type="text/css" href="' . Mage::getBaseUrl('js') . 'itoris/categorybannerslider/css/main.css' . '"/>';
		$store = Mage::app()->getStore();
		$category = Mage::registry('current_category');
		if($category){
			$categoryId = $category->getId();
		}else{
			$categoryId = 1;
		}
		try{
			$set = $this->getSetsModel()->loadCategoryBanner($store->getId(), $categoryId);
		}catch(Exception $e){
			return;
		}
		$this->set['width'] = $set['width'];
		$this->set['height'] = $set['height'];
		if($set){
			switch($set['effect']){
				case Itoris_CategoryBannerSlider_Model_Sets::EFFECT_SIMPLE:
							$this->set['effect'] = 'simple';
							break;
				case Itoris_CategoryBannerSlider_Model_Sets::EFFECT_FADE:
							$this->set['effect'] = 'fade';
							break;
				case Itoris_CategoryBannerSlider_Model_Sets::EFFECT_HORIZONTAL_SLIDING:
							$this->set['effect'] = 'sliding';
							break;
			}
			switch($set['skin']){
				case Itoris_CategoryBannerSlider_Model_Sets::SKIN_NUM_PAGER_ARROWS:
							$this->set['skin'] = 'num_arrows';
							$head->addItem('js_css', 'itoris/categorybannerslider/css/nums.css');
                            $this->jsCssHtml .= '<link rel="stylesheet" type="text/css" href="' . Mage::getBaseUrl('js') . 'itoris/categorybannerslider/css/nums.css' . '"/>';
							break;
				case Itoris_CategoryBannerSlider_Model_Sets::SKIN_TITLED_PAGER_ARROWS:
							$this->set['skin'] = 'titled_arrows';
							break;
				case Itoris_CategoryBannerSlider_Model_Sets::SKIN_NUM_PAGER:
							$this->set['skin'] = 'num';
							$head->addItem('js_css', 'itoris/categorybannerslider/css/nums.css');
                            $this->jsCssHtml .= '<link rel="stylesheet" type="text/css" href="' . Mage::getBaseUrl('js') . 'itoris/categorybannerslider/css/nums.css' . '"/>';
							break;
				case Itoris_CategoryBannerSlider_Model_Sets::SKIN_TITLED_PAGER:
							$this->set['skin'] = 'titled';
							break;
				case Itoris_CategoryBannerSlider_Model_Sets::SKIN_ARROWS:
							$this->set['skin'] = 'arrows';
							break;
				case Itoris_CategoryBannerSlider_Model_Sets::SKIN_NO:
							$this->set['skin'] = 'no';
							break;
			}
			$this->set['autorotation'] = $set['autorotation'];

			$banners = $set['banners'];
			$banners = explode('.-,', $banners);
			foreach($banners as $key => $banner){
				$notTitled = explode('._,', $banners[$key]);
				$titled = array('title' => $notTitled[0],
								'alt' => $notTitled[1],
								'delay' => $notTitled[2],
								'link_url' => $notTitled[3],
								'open_in' => ($notTitled[4] == Itoris_CategoryBannerSlider_Model_Sets::TARGET_BLANK) ? '_blank' : '_self',
								'file_name' => $notTitled[5],
								'url' => Mage::getBaseUrl('media') . 'catalog/category/banners' . $notTitled[5],
								'order' => $notTitled[6],
							);
				$banners[$key] = $titled;
			}
			$this->banners = $banners;
			if(count($banners) > 15){
				$head->addItem('js_css', 'itoris/categorybannerslider/css/nums_more.css');
                $this->jsCssHtml .= '<link rel="stylesheet" type="text/css" href="' . Mage::getBaseUrl('js') . 'itoris/categorybannerslider/css/nums.css' . '"/>';
			}
		}
	}

	/**
	 * Get json banners set
	 *
	 * @return string
	 */
	public function getOptionsJson() {
		return Zend_Json::encode($this->set);
	}

	/**
	 * Get json banners of the banners set
	 *
	 * @return string
	 */
	public function getBannersJson() {
		return Zend_Json::encode($this->banners);
	}

    public function getJsCssHtml() {
        return $this->jsCssHtml;
    }

	/**
	 * Get banners set model
	 *
	 * @return Itoris_CategoryBannerSlider_Model_Sets
	 */
	private function getSetsModel() {
		return Mage::getModel('itoris_categorybannerslider/sets');
	}
}
?>