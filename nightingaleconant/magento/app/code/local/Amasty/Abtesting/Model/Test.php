<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Model_Test extends Mage_Core_Model_Abstract
{
    const STATUS_SETUP      = 0;
    const STATUS_RUNNING    = 1;
    const STATUS_PAUSED     = 2;
    const STATUS_COMPLETE   = 3;

    const GOAL_PURCHASE = 0;
    const GOAL_CART     = 1;
    const GOAL_WISHLIST = 2;
    const GOAL_VISIT    = 3;

    const MAX_VARIANTS = 10;

    const DEFAULT_DAYS = 4;
    const DEFAULT_VISITORS = 50;
    const DEFAULT_DETECT_RATE = 20;

    const DEGRADE_CONFIDENCE = .80;
    const DEGRADE_CHANGE_THRESHOLD = .05;

    const ZERO_CONVERSION = .001;

    public function getStatuses()
    {
        $hlp = Mage::helper('amabtesting');

        return array(
            self::STATUS_SETUP      => $hlp->__('Setup'),
            self::STATUS_RUNNING    => $hlp->__('Running'),
            self::STATUS_PAUSED     => $hlp->__('Paused'),
            self::STATUS_COMPLETE   => $hlp->__('Complete'),
        );
    }

    public function getGoals()
    {
        $hlp = Mage::helper('amabtesting');

        return array(
            self::GOAL_PURCHASE => $hlp->__('Product Purchased'),
            self::GOAL_CART     => $hlp->__('Product Added To Cart'),
            self::GOAL_WISHLIST => $hlp->__('Product Added To Wishlist'),
            self::GOAL_VISIT    => $hlp->__('Page Visited'),
        );
    }

    protected function _construct()
    {
        $this->_init('amabtesting/test');
    }

    public function getCacheTags()
    {
        $tags = parent::getCacheTags();

        foreach ($this->getInvolvedProducts() as $product) {
            $tags []= Mage_Catalog_Model_Product::CACHE_TAG . '_' . $product->getId();
        }

        return $tags;
    }

    public function getInvolvedProducts()
    {
        return $this->getResource()->getInvolvedProducts($this);
    }

    public function getAttributes()
    {
        if (!$this->hasData('attributes')) {
            $this->setData(
                'attributes',
                $this->getResource()->getAttributes($this)
            );
        }

        return $this->getData('attributes');
    }

    public function isReadonly()
    {
        if (!$this->hasData('readonly')) {
            $readonly = in_array(
                $this->getStatus(),
                array(
                    Amasty_Abtesting_Model_Test::STATUS_RUNNING,
                    Amasty_Abtesting_Model_Test::STATUS_COMPLETE
                )
            );
            $this->setData('readonly', $readonly);
        }

        return $this->getData('readonly');
    }

    public function findByProductId($productId)
    {
        $tests = Mage::getResourceModel('amabtesting/test_collection')
            ->addActiveFilter()
            ->addProductFilter($productId)
        ;

        return $tests->getFirstTest();
    }

    public function fetchVariation()
    {
        $variations = Mage::getResourceModel('amabtesting/test_variation_collection')
            ->addFieldToFilter('test_id', $this->getId())
            ->setPageSize(1)
            ->setOrder('visits / rate', 'ASC')
        ;

        $variation = $variations->getFirstItem();

        if ($variation->getId()) {
            $variation
                ->setVisits($variation->getVisits() + 1)
                ->save();
        }

        return $variation;
    }

    public function getVariations()
    {
        $variations = Mage::getResourceModel('amabtesting/test_variation_collection')
            ->addFieldToFilter('test_id', $this->getId())
            ->setOrder('id', 'ASC')
        ;

        return $variations;
    }

    public function getTotalVisitors()
    {
        if (!$this->getData('total_visitors')) {
            $this->setData(
                'total_visitors',
                $this->getResource()->getTotalVisitors($this)
            );
        }

        return $this->getData('total_visitors');
    }

    public function canBeEstimated()
    {
        $estimateStamp = strtotime($this->getStartedAt()) + self::DEFAULT_DAYS * 24 * 3600;

        if ($estimateStamp > Mage::getModel('core/date')->gmtTimestamp())
            return false;

        if ($this->getTotalVisitors() < self::DEFAULT_VISITORS)
            return false;

        return true;
    }

    public function estimateVisitors()
    {
        $a = 1.96;
        $b = 0.84;

        $relativeDetect = +$this->getDetectRate() / 100;

        $pControl = max($this->getControlConversionRate(), self::ZERO_CONVERSION);
        $pDetect = max(min(1, $pControl + $relativeDetect + $pControl), .01);

        /* Formula 1 */
//        $visitors = (($a + $b) * ($a + $b) * ($pControl * (1 - $pControl) + $pDetect * (1 - $pDetect)))
//            /
//            (($pControl - $pDetect) * ($pControl - $pDetect))
//        ;


        /* Formula 2 */
        $stdDev = sqrt($pControl * (1 - $pControl));
        $effectSize = max($pControl * $pDetect, self::ZERO_CONVERSION);
        $visitors = pow(4 * ($stdDev / $effectSize), 2);


        $visitors = ceil($visitors * $this->getVariations()->getSize());

        return $visitors;
    }

    public function getControlConversionRate()
    {
        $controlVariation = $this->getVariations()->getFirstItem();

        return $controlVariation->getConversions() / $controlVariation->getVisits();
    }

    public function checkCompletion($complete = false)
    {
        if ($this->getAutocomplete() && $this->canBeEstimated()) {
            if ($this->getTotalVisitors() >= $this->estimateVisitors()) {
                if ($complete) {
                    $this->complete();
                }

                return true;
            }
        }

        return false;
    }

    public function complete()
    {
        $this->setStatus(self::STATUS_COMPLETE);
        $this->save();

        $this->_notifyCompletion();

        return $this;
    }

    protected function _notifyCompletion()
    {
        if (!Mage::getStoreConfigFlag('amabtesting/notifications/send'))
            return;

        $to = Mage::getStoreConfig('amabtesting/notifications/to');
        if (!$to)
            return;

        $from = Mage::getStoreConfig('amabtesting/notifications/from');
        $template = Mage::getStoreConfig('amabtesting/notifications/template');

        $mailTemplate = Mage::getModel('core/email_template');
        /* @var $mailTemplate Mage_Core_Model_Email_Template */
        $mailTemplate->setDesignConfig(array('area' => 'frontend'))
            ->sendTransactional(
                $template,
                $from,
                $to,
                null,
                array('test' => $this)
            )
        ;
    }

    public function updateRates()
    {
        if ($this->canBeEstimated()) {
            $variations = $this->getVariations();

            $control = $variations->getFirstItem();

            foreach ($this->getVariations() as $variation) {
                if ($variation->getId() == $control->getId())
                    continue;

                $rate = 1.0;

                list($change, $p) = $control->getResult($variation);

                if ($change < -self::DEGRADE_CHANGE_THRESHOLD && $p >= self::DEGRADE_CONFIDENCE) {
                    $rate = (1 - $p) / (1 - self::DEGRADE_CONFIDENCE);
                }

                $variation
                    ->setRate($rate)
                    ->save()
                ;
            }
        }

        return $this;
    }
}
