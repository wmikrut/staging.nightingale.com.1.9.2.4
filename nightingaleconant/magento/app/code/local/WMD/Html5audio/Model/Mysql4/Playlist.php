<?php
/**
 * WMD_Html5audio_Model_Mysql4_Playlist
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2013 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Model_Mysql4_Playlist extends Mage_Core_Model_Mysql4_Abstract
{
       
    /**
     * Entry playlist sample name
     *
     * @var string
     */
    protected $_playlistSampleTable;
    
    /**
     * Construct
     *
     * @return void
     */	
    protected function _construct()
    {
        $this->_init('html5audio/playlist', 'playlist_id');
        $this->_playlistSampleTable = $this->getTable('html5audio/playlist_sample'); 
        
    }	
    
    /**
     * Handle random string
     * 
     * @param Mage_Core_Model_Abstract $object
     * @return WMD_Html5audio_Model_Mysql4_Playlist
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        $object->setData('hash', Mage::helper('html5audio')->getRandomString());
    }
    
    
    /**
     * Perform operations after object save
     *
     * @param Mage_Core_Model_Abstract $object
     * @return WMD_Html5audio_Model_Mysql4_Playlist
     */
    protected function _afterSave(Mage_Core_Model_Abstract $object)
    {
        $this->_savePlaylistSamples($object);
        return parent::_afterSave($object);
    }     
     
    /**
     * Save playlist samples relation
     *
     * @param Mage_Core_Model_Abstract $object
     * @return WMD_Html5audio_Model_Mysql4_Playlist
     */
    protected function _savePlaylistSamples($playlist)
    {                
    
        $playlist->setIsChangedSamplesList(false);
        $id = $playlist->getId();    
        
        /**
         * new playlist-samples relationships
         */
        $samples = $playlist->getPostedSamples();
        
        /**
         * Example re-save entry
         */
        if ($samples === null) {
            return $this;
        }

        /**
         * old playlist-samples relationships
         */
        $oldSamples = $playlist->getSamples();
        
        $insert = array_diff_key($samples, $oldSamples);
        $delete = array_diff_key($oldSamples, $samples);

        /**
         * Find sample ids which are presented in both arrays
         * and saved before (check $oldSamples array)
         */
        $update = array_intersect_key($samples, $oldSamples);
        $update = array_diff_assoc($update, $oldSamples);

        $adapter = $this->_getWriteAdapter();

        /**
         * Delete samples from playlist
         */
        if (!empty($delete)) {
            $cond = array(
                'sample_id IN(?)' => array_keys($delete),
                'playlist_id=?' => $id
            );
            $adapter->delete($this->_playlistSampleTable, $cond);
        }

        /**
         * Add samples to playlist
         */
        if (!empty($insert)) {
            $data = array();
            foreach ($insert as $sampleId => $valueId) {
                $data[] = array(
                    'playlist_id' => (int)$id,
                    'sample_id'  => (int)$sampleId,
                );
            }
            $adapter->insertMultiple($this->_playlistSampleTable, $data);
        }    
        
        return $this;
    
    }
    
    /**
     * Load playlist by hash
     *
     * @param WMD_Html5audio_Model_Playlist $playlist
     * @param string $hash
     * @return WMD_Html5audio_Model_Mysql4_Playlist
     */
    public function loadByHash(WMD_Html5audio_Model_Playlist $playlist, $hash)
    {
        $adapter = $this->_getReadAdapter();
        $bind    = array('playlist_hash' => $hash);
        $select  = $adapter->select()
            ->from($this->getMainTable())
            ->where('hash = :playlist_hash');

        $playlistId = $adapter->fetchOne($select, $bind);
        
        if ($playlistId) {
            $this->load($playlist, $playlistId);
        } else {
            $playlist->setData(array());
        }

        return $this;
    }
    
        
    /**
     * Get playlist samples
     *
     * @param WMD_Html5audio_Model_Playlist $playlist
     * @return array
     */
    public function getSamples($playlist)
    {
        $select = $this->_getWriteAdapter()->select()
            ->from($this->_playlistSampleTable, array('sample_id', 'playlist_id'))
            ->where('playlist_id = :playlist_id');
        $bind = array('playlist_id' => (int)$playlist->getId());
        
        return $this->_getWriteAdapter()->fetchPairs($select, $bind);
    }
    
}
