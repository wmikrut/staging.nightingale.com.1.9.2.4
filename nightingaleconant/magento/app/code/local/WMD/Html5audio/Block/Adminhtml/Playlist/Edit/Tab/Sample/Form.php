<?php
/**
 * WMD_Html5audio_Block_Adminhtml_Playlist_Edit_Tab_Sample_Form
 *
 * WMD Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.com>
 * @copyright 2014 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/

class WMD_Html5audio_Block_Adminhtml_Playlist_Edit_Tab_Sample_Form extends Mage_Adminhtml_Block_Widget_Form
{

    public function __construct()
    {
    
    }
    
}

