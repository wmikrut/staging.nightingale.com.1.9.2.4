<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_CATEGORYBANNERSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_CategoryBannerSlider_Helper_Category extends Mage_Core_Helper_Abstract {

	/**
	 * Get array[storeId][categoryId][categoryName]
	 *								 [categoryUrl]
	 *								 [categoryLevel]
	 * @return array
	 * */
	public function getCategories() {
		$stores = Mage::app()->getStores();
		$categories = array();
		/** @var $store Mage_Core_Model_Store */
		foreach ($stores as $store) {
			$rootCategoryId = $store->getRootCategoryId();
			$storeId = $store->getId();
			/** @var $rootCategory Mage_Catalog_Model_Category */
			$rootCategory = Mage::getModel('catalog/category')->load($rootCategoryId);

			if ($rootCategory->getIsActive()) {
				$storeCategories = array();
				$categories[$storeId] = $this->getChildren($rootCategory, $storeCategories, $store);
			}
		}
		return $categories;
	}

	protected function getChildren(Mage_Catalog_Model_Category $category, array $categories, $store) {
		$categories[] = array(
			'name'  => $this->__('Home Page'),
			'url'   => $store->getUrl('', array('_store_to_url' => true)),
			'level' => 1,
			'id'    => 1,
		);

		if ($category->hasChildren()) {
			$allStoreCategories = $this->_getStoreCategories($category, $store->getId())->getItems();
			$categories = $this->_prepareChildCategories($category->getId(), $categories, $allStoreCategories, $store);
		}

		return $categories;
	}

	protected function _prepareChildCategories($parentId, $categories, $allCategories, $store) {
		foreach ($allCategories as $child) {
			if ($child->getParentId() == $parentId) {
				$child->setStoreId($store->getId());
				if ($child->getRequestPath()) {
					$url = $store->getUrl('', array(
						'_direct'       => $child->getRequestPath(),
						'_store_to_url'	=> 1,
					));
				} else {
					$url = $child->getCategoryIdUrl();
					if (strpos($url, '?') === false) {
						$url .= '?';
					} else {
						$url .= '&';
					}
					$url .= '___store=' . $store->getCode();
				}
				$categories[] = array(
					'name'  => $child->getName(),
					'url'   => $url,
					'level' => $child->getLevel(),
					'id'    => $child->getId(),
				);
				$categories = $this->_prepareChildCategories($child->getId(), $categories, $allCategories, $store);
			}
		}
		return $categories;
	}

	protected function _getStoreCategories($category, $storeId) {
		$category->setStoreId($storeId);
		$category->setLoadProductCount(false);
		$collection = $category->getCollection();
		/* @var $collection Mage_Catalog_Model_Resource_Category_Collection */
		$collection->addAttributeToSelect('url_key')
			->addAttributeToSelect('name')
			->addAttributeToFilter('is_active', 1)
			//->addAttributeToFilter('level', array('lt' => 5))
			->setOrder('position', Varien_Db_Select::SQL_ASC);

		$collection->joinTable(
			'core/url_rewrite',
			'category_id=entity_id',
			array('request_path'),
			"{{table}}.is_system=1"
			. " AND {{table}}.product_id IS NULL"
			. " AND {{table}}.store_id='{$storeId}'"
			. " AND id_path LIKE 'category/%'",
			'left'
		);

		$collection->load();

		return $collection;
	}

	public function getCategoriesJson() {
		return Zend_Json::encode($this->getCategories());
	}
}

?>
 
