<?php
/**
 * Pulsar Plugins
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.pulsarplugins.com/pulsarplugins-license.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento community edition
 * Pulsar Plugins does not guarantee correct work of this extension
 * on any other Magento edition except Magento community edition.
 * Pulsar Plugins does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   PP
 * @package    PP_EmailByShipping
 * @version    1.1
 * @copyright  Copyright (c) 2010-2014 Pulsar Plugins (http://www.pulsarplugins.com/)
 * @license    http://www.pulsarplugins.com/pulsarplugins-license.txt
 *
*/
class PP_EmailByShipping_Model_Dataarray extends Mage_Adminhtml_Model_System_Config_Backend_Serialized_Array {
	
	function _beforeSave() {	
		if (is_array($this->getValue())) {
			$data		= $this->getValue();
			$checker	= array();
			foreach ($data as $rowId=>$rowData) {
				$thisVal	= '';
				foreach ($rowData as $key=>$value) {
					$thisVal	.= $value.'x|x';
				}
				if (in_array($thisVal, $checker)) {
					unset($data[$rowId]); // remove duplicate
					$tmp	= explode('x|x', $thisVal);
					//Mage::getSingleton('core/session')->addError(__('Multiple combination:').' '.implode('-',$tmp));
					//$this->_dataSaveAllowed	= false;
				} else {
					$checker[]	= $thisVal;
				}
			}
			$this->setValue($data);
            //$this->setValue(serialize($this->getValue()));
        }
		
		parent::_beforeSave();
	}
}
?>