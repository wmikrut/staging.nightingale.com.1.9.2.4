<?php
class NCCUSTOM_CaptureCouponCode_Model_Observer 
{

    public function exportFeed($observer) 
    {		
	$log = "nccustom_CaptureCouponCode.log";
	
	try 
	{
	    //*****************************************************************
	    //* Only capture the first coupon used.  Subsequent changes
	    //* should not apply as the first coupon is what brought the
	    //* customer here.
	    //*****************************************************************
	    if ( !isset($_SESSION['nccustom_coupon_code']))
	    {
		$quote= $observer->getEvent()->getQuote();
		$id = $quote->getId();
	
		$remoteAddr = Mage::helper('core/http')->getRemoteAddr();
		$remoteAddr2 = Mage::helper('core/http')->getRemoteAddr(true);
	    
		$rule = $observer->getEvent()->getRule();
    		$couponCode = $rule->getCouponCode();
		if ( $couponCode != '' )
		{
		    $line = $id. ",". $remoteAddr. ",". $remoteAddr2. ",". $couponCode;
		    $_SESSION['nccustom_coupon_code'] = $couponCode;
		    //Mage::log($line, null, $log);
		    //Mage::log($_SESSION, null, $log);
		}
	    }
	}
	catch (Exception $e) 
	{
	    $line = 'NCCUSTOM Caught exception: '. $e->getMessage();
	    Mage::log($line, null, $log);
	}
    }
}
?>