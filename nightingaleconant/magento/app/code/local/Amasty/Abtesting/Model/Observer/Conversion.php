<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Model_Observer_Conversion
{
    public function onQuoteSubmitSuccess($observer)
    {
        Mage::getSingleton('amabtesting/conversion')->createConversion(
            Amasty_Abtesting_Model_Test::GOAL_PURCHASE,
            $observer->getOrder()->getItemsCollection()->getColumnValues('product_id')
        );
    }

    public function onProductAddToCart($observer)
    {
        Mage::getSingleton('amabtesting/conversion')->createConversion(
            Amasty_Abtesting_Model_Test::GOAL_CART,
            $observer->getProduct()->getId()
        );
    }

    public function onAddToWishlist($observer)
    {
        Mage::getSingleton('amabtesting/conversion')->createConversion(
            Amasty_Abtesting_Model_Test::GOAL_WISHLIST,
            $observer->getProduct()->getId()
        );
    }

    public function onActionPredispatch($observer)
    {
        if (Mage::app()->getRequest()->getMethod() !== 'GET')
            return;

        Mage::getSingleton('amabtesting/conversion')->createConversion(
            Amasty_Abtesting_Model_Test::GOAL_VISIT
        );
    }
}
