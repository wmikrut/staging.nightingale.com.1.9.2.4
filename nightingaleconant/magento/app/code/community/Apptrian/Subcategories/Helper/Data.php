<?php
/**
 * @category  Apptrian
 * @package   Apptrian_Subcategories
 * @author    Apptrian
 * @copyright Copyright (c) 2016 Apptrian (http://www.apptrian.com)
 * @license   http://www.apptrian.com/license Proprietary Software License EULA
 */
class Apptrian_Subcategories_Helper_Data extends Mage_Core_Helper_Abstract
{
    
    /**
     * @var array|null
     */
    protected $_options = null;
    
    /**
     * @var array
     */
    protected $_elements = array(
        'image'       => '0',
        'name'        => '1',
        'description' => '2'
    );
    
    /**
     * Returns extension version.
     *
     * @return string
     */
    public function getExtensionVersion()
    {
        return (string) Mage::getConfig()
            ->getNode()->modules->Apptrian_Subcategories->version;
    }
    
    /**
     * Based on provided configuration path returns configuration value.
     *
     * @param string $configPath
     * @return string
     */
    public function getConfig($configPath)
    {
        return Mage::getStoreConfig($configPath);
    }
    
    /**
     * Decodes provided options from "json-like" string to array.
     *
     * @param string $value
     * @return array|null
     */
    public function decode($value)
    {
        return json_decode(
            str_replace(
                array('[', ']', '`', '|'), array('{', '}', '"', '\\'), $value
            ), 
            true
        );
    }
    
    /**
     * Based on provided $sortOrderString returns array of sorted elements
     *
     * @param string $sortOrderString
     * @return array
     */
    public function getSortOrder($sortOrderString)
    {
    
        $elements = array();
        
        $ids = explode(',', trim(trim($sortOrderString), ','));
        
        foreach ($ids as $id) {
            $elements[] = array_search($id, $this->_elements);
        }
        
        return $elements;
    
    }
    
    /**
     * Based on provided $blockOptions that come from
     * Apptrian_Subcategories_Block_GridList class via method getCategories() 
     * in this class. Returns array of options.
     *
     * @param string $sortOrderString
     * @return array
     */
    public function determineOptions($blockOptions)
    {
        $options        = array();
        $result         = array();
        $nameInLayout   = $blockOptions['name_in_layout'];
        $fullActionName = $blockOptions['full_action_name'];
        $bOptions       = $blockOptions['options'];
        
        $catParam       = $this->filterAndValidateCategoryId(
            $blockOptions['cat_param']
        );
        
        $categoryPageXml = strpos(
            $nameInLayout, 'apptrian.subcategories.category.page'
        );
        
        if (0 === $categoryPageXml
            && $fullActionName = 'catalog_category_view'
        ) {
            
            $pageType = 'category_page';
            
            $options['category_ids'] = '';
            $options['mode']         = '';
            
            $options['exclude_ids']  = trim(
                $this->getConfig(
                    'apptrian_subcategories/category_page/exclude_ids'
                ), ','
            );
            
            $options['css_ident'] = 'category';
            
        } else {
            
            $pageType = 'home_page';
            
            $options['category_ids'] = trim(
                $this->getConfig(
                    'apptrian_subcategories/home_page/category_ids'
                ), ','
            );
            
            $options['mode']         = $this->getConfig(
                'apptrian_subcategories/home_page/mode'
            );
            
            $options['exclude_ids']  = '';
            
            if ($fullActionName == 'cms_index_index') {
                $options['css_ident'] = 'home';
            } else {
                $options['css_ident'] = 'cms';
            }
            
        }
        
        $options['enabled']          = $this->getConfig(
            'apptrian_subcategories/general/enabled'
        );
        
        $options['page_type']        = $pageType;
        $options['full_action_name'] = $fullActionName;
        $options['layout']           = $this->getConfig(
            'apptrian_subcategories/' . $pageType . '/layout'
        );
        
        $options['single_link']      = $this->getConfig(
            'apptrian_subcategories/' . $pageType . '/single_link'
        );
        
        $options['sort_attribute']   = $this->getConfig(
            'apptrian_subcategories/' . $pageType . '/sort_attribute'
        );
        
        $options['sort_direction']   = $this->getConfig(
            'apptrian_subcategories/' . $pageType . '/sort_direction'
        );
        
        $options['heading']          = $this->getConfig(
            'apptrian_subcategories/' . $pageType . '/heading'
        );
        
        $options['sort_order']       = $this->getConfig(
            'apptrian_subcategories/' . $pageType . '/data_sort_order'
        );
        
        $options['image']            = $this->getConfig(
            'apptrian_subcategories/' . $pageType . '/image'
        );
        
        $options['name']             = $this->getConfig(
            'apptrian_subcategories/' . $pageType . '/name'
        );
        
        $options['description']      = $this->getConfig(
            'apptrian_subcategories/' . $pageType . '/description'
        );
        
        // Array merge with xml config and then with block config
        if ($bOptions !== null) {
            $result = array_merge($options, $bOptions);
        } else {
            $result = $options;
        }
        
        $sortOrder            = $this->getSortOrder($result['sort_order']);
        $result['sort_order'] = $sortOrder;
        
        // If someone provides options that are not suitable
        if (0 === $categoryPageXml
            && $fullActionName = 'catalog_category_view'
        ) {
        
            $result['category_ids'] = '';
            $result['mode']         = '';
        
        } else {
        
            $result['exclude_ids']  = '';
        
        }
        
        // Add cat param to result
        $result['cat_param'] = $catParam;
        
        return $result;
        
    }
    
    /**
     * Based on provided $data that comes from
     * Apptrian_Subcategories_Block_GridList class returns
     * array of options and categories used in grid_list.phtml file.
     *
     * @param string $sortOrderString
     * @return array
     */
    public function getCategories($data)
    {
        
        $categoryModel         = Mage::getModel('catalog/category');
        $categoryResourceModel = Mage::getResourceModel(
            'catalog/category_collection'
        );
        $categories            = array();
        $result                = array();
        $o                     = $this->determineOptions($data);
        
        // Assign options property for other methods in this class
        $this->_options = $o;
        
        // Get vars for easier access
        $pageType    = $o['page_type'];
        $categoryIds = $o['category_ids'];
        $mode        = $o['mode'];
        $catParam    = $o['cat_param'];
        
        // Attribute options: "name", "meta_title", "position", and "created_at"
        // Direction options: "asc" and "desc"
        $sortAttribute = $o['sort_attribute'];
        $sortDirection = $o['sort_direction'];
        
        $attributesToSelect = array('name', 'url_key', 'url_path', 'image', 
            'thumbnail', 'description', 'meta_description', 'meta_title'
        );
        
        // For home page and other pages when category_ids is provided
        if ($pageType == 'home_page' && $categoryIds != '') {
            
            // "Random" mode
            if ($mode == 'random') {
                
                // Get random parent ID
                $id = $this->getRandomId($categoryIds);
                
                $category = $categoryModel->load($id);
                
                $childrenIds = $category->getChildren();
                
                $collection = $categoryResourceModel
                    ->addAttributeToSelect($attributesToSelect)
                    ->addAttributeToFilter('is_active', 1)
                    ->addAttributeToSort($sortAttribute, $sortDirection)
                    ->addIdFilter($childrenIds)
                    ->load();
                
                // Get categories array from collection
                $categories = $this->getCategoriesFromCollection($collection);
                
            // "Specific" mode
            } else {
                
                $collection = $categoryResourceModel
                    ->addAttributeToSelect($attributesToSelect)
                    ->addAttributeToFilter('is_active', 1)
                    ->addIdFilter($categoryIds);
                
                // In this context "position" is different and must be done 
                // programmatically so there is no need to sort it
                if ($sortAttribute != 'position') {
                    
                    $collection->addAttributeToSort(
                        $sortAttribute, $sortDirection
                    )->load();
                    
                    // Get categories array from collection
                    $categories = $this->getCategoriesFromCollection(
                        $collection
                    );
                    
                } else {
                    
                    $collection->load();
                    
                    // Get categories array from collection sorted by 
                    // $categoryIDs
                    $categories = $this->getCategoriesFromCollection(
                        $collection, $categoryIds
                    );
                    
                }
                
            }
            
        // For category pages and home page and any other page when 
        // category_ids field is empty
        } else {
            
            if ($pageType == 'category_page') {
                
                if ($catParam) {
                    $categoryId = $catParam;
                } else {
                    $categoryId = Mage::getSingleton('catalog/layer')
                        ->getCurrentCategory()->getId();
                }
                
                //Is in exclude list
                if ($this->isExcluded($categoryId)) {
                    
                    $result['categories'] = $categories;
                    $result['options']    = $o;
                    
                    return $result;
                }
                
            } else {
                
                $categoryId  = Mage::app()->getStore()->getRootCategoryId();
                
            }
            
            $category    = $categoryModel->load($categoryId);
            $childrenIds = $category->getChildren();
            
            $collection = $categoryResourceModel
                ->addAttributeToSelect($attributesToSelect)
                ->addAttributeToFilter('is_active', 1)
                ->addAttributeToSort($sortAttribute, $sortDirection)
                ->addIdFilter($childrenIds)
                ->load();
            
            // Get categories array from collection
            $categories = $this->getCategoriesFromCollection($collection);
            
        }
        
        $result['categories'] = $categories;
        $result['options']    = $o;
        
        return $result;
        
    }
    
    /**
     * Based on provided Category Collection and optionally sort order. 
     * Returns sorted array of categories.
     *
     * @param Mage_Catalog_Model_Resource_Category_Collection $collection
     * @param string $sortOrder
     * @return array
     */
    public function getCategoriesFromCollection($collection, $sortOrder = '')
    {
    
        $categories = array();
        
        if ($sortOrder != '') {
            
            $sort = explode(',', $sortOrder);
            
            foreach ($sort as $id) {
                
                $c = $collection->getItemById($id);
                
                if ($c != null) {
                    
                    $categories[$id] = $this->categoryToArray($c);
                    
                }
                
            }
            
        } else {
            
            foreach ($collection as $c) {
                
                $id = $c->getId();
                
                $categories[$id] = $this->categoryToArray($c);
                
            }
            
        }
        
        return $categories;
        
    }
    
    /**
     * Based on provided category object returns small category array with 
     * necessary data.
     *
     * @param Mage_Catalog_Model_Category $c
     * @return array
     */
    public function categoryToArray($c)
    {
        
        $category = array();
        
        $category['name']        = $this->getName($c);
        $category['url']         = Mage::helper('core')
            ->escapeUrl($c->getUrl());
        $category['image']       = $this->getImage($c);
        $category['description'] = $this->getDescription($c);
        
        return $category;
        
    }
    
    /**
     * Returns proper name text based on provided data.
     *
     * @param Mage_Catalog_Model_Category $category
     * @return string
     */
    public function getName($category)
    {
        $o             = $this->_options;
        $nameAttribute = $o['name'];
        
        if ($nameAttribute == 'name') {
            
            $categoryName = Mage::helper('core')
                ->escapeHtml($category->getName());
            
        } elseif ($nameAttribute == 'meta_title') {
            
            $categoryName = Mage::helper('core')
                ->escapeHtml($category->getMetaTitle());
            
        } else {
            
            $categoryName = '';
            
        }
        
        return trim($categoryName);
    
    }
    
    /**
     * Returns proper description text based on provided data.
     *
     * @param Mage_Catalog_Model_Category $category
     * @return string
     */
    public function getDescription($category)
    {
        
        $o                    = $this->_options;
        $descriptionAttribute = $o['description'];
        
        if ($descriptionAttribute == 'description') {
            
            $description = $category->getDescription();
            
            if ($description) {
                $categoryDescription = Mage::helper('catalog/output')
                    ->categoryAttribute($category, $description, 'description');
            } else {
                $categoryDescription = '';
            }
            
        } elseif ($descriptionAttribute == 'meta_description') {
            
            $categoryDescription = Mage::helper('core')
                ->escapeHtml($category->getMetaDescription());
            
        } else {
            
            $categoryDescription = '';
            
        }
        
        return trim($categoryDescription);
        
    }
    
    /**
     * Generates image url based on provided data.
     *
     * @param Mage_Catalog_Model_Category $category
     * @return string
     */
    public function getImage($category)
    {
        
        $o                   = $this->_options;
        $imageAttribute      = $o['image'];
        $placeholderImageUrl = Mage::getModel('catalog/product')
            ->getSmallImageUrl();
        
        if ($imageAttribute == 'image') {
            $image = $category->getImage();
        } elseif ($imageAttribute == 'thumbnail') {
            $image = $category->getThumbnail();
        } else {
            $image = '';
        }
        
        if ($image != null) {
            $url = $this->getImageUrl($image);
        } else {
            $url = $placeholderImageUrl;
        }
        
        return $url;
        
    }
    
    /**
     * Retrieve image URL based on provided file name.
     *
     * @return string
     */
    public function getImageUrl($image)
    {
        $url = false;
        if ($image) {
            $url = Mage::getBaseUrl('media') . 'catalog/category/' . $image;
        }
        return $url;
    }
    
    /**
     * Based on provided comma separated list of Ids, returns one random id.
     *
     * @param string $categoryIds
     * @return string
     */
    public function getRandomId($categoryIds)
    {
        
        $pool = explode(',', $categoryIds);
        
        $index = array_rand($pool, 1);
        
        return $pool[$index];
        
    }
    
    /**
     * Returns array of exclude Ids from config.
     *
     * @return array
     */
    public function getExcludedIds()
    {
        $o          = $this->_options;
        $excludeIds = $o['exclude_ids'];
        
        return explode(',', $excludeIds);
    }
    
    /**
     * Checks if category id is in excluded list.
     *
     * @param int $id
     * @return boolean
     */
    public function isExcluded($id = 0)
    {
    
        if ($id > 0) {
        
            $excluded = $this->getExcludedIds();
            
            if (count($excluded) > 0 && in_array($id, $excluded)) {
                
                return true;
                
            // Exclude list is empty
            } else {
                
                return false;
                
            }
            
        // Not a category page
        } else {
        
            return false;
            
        }
        
    }
    
    /**
     * Filters and validates "cat" url query param for layered category pages
     *
     * @param string $id
     * @return int
     */
    public function filterAndValidateCategoryId($id)
    {
        
        $filterChain = new Zend_Filter();
        $filterChain->addFilter(new Zend_Filter_StripTags())
                    ->addFilter(new Zend_Filter_StringTrim());
        
        $idFiltered = $filterChain->filter($id);
        
        if ($idFiltered != ''
            && Zend_Validate::is($idFiltered, 'Digits')
            && $idFiltered > 0
        ) {
            
            return (int) $idFiltered;
            
        } else {
            
            return 0;
            
        }
        
    }
    
}
