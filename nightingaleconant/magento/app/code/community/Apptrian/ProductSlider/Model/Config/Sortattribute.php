<?php
/**
 * @category   Apptrian
 * @package    Apptrian_ProductSlider
 * @author     Apptrian
 * @copyright  Copyright (c) 2015 Apptrian (http://www.apptrian.com)
 * @license    http://www.apptrian.com/license    Proprietary Software License (EULA)
 */
class Apptrian_ProductSlider_Model_Config_Sortattribute
{
    
    protected $_options;
    const SORT_ATTRIBUTE_CREATED_AT = 'created_at';
	const SORT_ATTRIBUTE_NAME       = 'name';
    const SORT_ATTRIBUTE_PRICE      = 'price';
    
    public function toOptionArray()
    {
        if (!$this->_options) {
        	$this->_options[] = array(
        			'value'=>self::SORT_ATTRIBUTE_CREATED_AT,
        			'label'=>Mage::helper('apptrian_productslider')->__('Date')
        	);
			$this->_options[] = array(
			   'value'=>self::SORT_ATTRIBUTE_NAME,
			   'label'=>Mage::helper('apptrian_productslider')->__('Name')
			);
			$this->_options[] = array(
			   'value'=>self::SORT_ATTRIBUTE_PRICE,
			   'label'=>Mage::helper('apptrian_productslider')->__('Price')
			);
		}
		return $this->_options;
	}
    
}