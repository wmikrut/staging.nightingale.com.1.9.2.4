<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGECONTENTSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PageContentSlider_Model_Mysql4_Slide_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

	protected $storeTable = 'itoris_pagecontentslider_slide_store';
	protected $groupTable = 'itoris_pagecontentslider_slide_customergroup';

	protected function _construct() {
		parent::_construct();
		$this->_init('itoris_pagecontentslider/slide');
		$this->storeTable = $this->getTable('slide_store');
		$this->groupTable = $this->getTable('slide_usergroup');
	}

	protected function _initSelect() {
		$this->getSelect()->reset(null)->from(array('main_table' => $this->getResource()->getMainTable()))
			->joinLeft(
			array('groups' => $this->groupTable),
			'groups.slide_id = main_table.slide_id',
			array('user_groups' => 'group_concat(DISTINCT groups.group_id SEPARATOR ",")')
		)
			->joinLeft(
			array('store' => $this->storeTable),
			'store.slide_id = main_table.slide_id',
			array('store_views' => 'group_concat(DISTINCT store.store_id SEPARATOR ",")')
		)
			->group('main_table.slide_id');

		return $this;
	}

	public function getMinMaxOrder() {
		$this->getSelect()->reset(null)->from(array('main_table' => $this->getResource()->getMainTable()));
		$this->addExpressionFieldToSelect('max_order', 'max({{slide_order}})', array('slide_order' => 'main_table.slide_order'))
			->addExpressionFieldToSelect('min_order', 'min({{slide_order}})', array('slide_order' => 'main_table.slide_order'));
		return $this;
	}

	public function getAllIds() {
		$idsSelect = clone $this->getSelect();
		$idsSelect->reset(Zend_Db_Select::ORDER);
		$idsSelect->reset(Zend_Db_Select::LIMIT_COUNT);
		$idsSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
		$idsSelect->reset(Zend_Db_Select::COLUMNS);
		$idsSelect->reset(Zend_Db_Select::HAVING);
		$idsSelect->columns(
			'main_table.' . $this->getResource()->getIdFieldName()
		);
		return $this->getConnection()->fetchCol($idsSelect, $this->_bindParams);
	}

	protected function setTotalRecords() {
		$countSelect = clone $this->getSelect();
		$countSelect->reset(Zend_Db_Select::LIMIT_COUNT);
		$countSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
		$this->_totalRecords = count($this->_fetchAll($countSelect));
	}

	public function getSize() {
		$this->setTotalRecords();
		return $this->_totalRecords;
	}

	public function addFieldToFilter($field, $condition=null) {
		$field = $this->_getMappedField($field);
		if ($field == 'user_groups' || $field == 'store_views') {
			$this->_select->having($this->_getConditionSql($field, $condition), null, 'TYPE_CONDITION');
		} else {
			$this->_select->where($this->_getConditionSql($field, $condition), null, 'TYPE_CONDITION');
		}

		return $this;
	}

	public function addFieldToFilterHaving($field, $condition = null) {
		$field = $this->_getMappedField($field);
		$this->_select->having($this->_getConditionSql($field, $condition), null, 'TYPE_CONDITION');

		return $this;
	}

	public function addFieldToFilterOr($field, $condition=null) {
		$field = $this->_getMappedField($field);
		if ($field == 'user_groups' || $field == 'store_views') {
			$this->_select->orHaving($this->_getConditionSql($field, $condition), null, 'TYPE_CONDITION');
		} else {
			$this->_select->orWhere($this->_getConditionSql($field, $condition), null, 'TYPE_CONDITION');
		}

		return $this;
	}

	public function addStoreFilter($storeId) {
		$this->_select->having("FIND_IN_SET(0, store_views) OR FIND_IN_SET('" . intval($storeId) . "', store_views)");
		return $this;
	}

	public function addGroupFilter($groupId) {
		$this->_select->having("user_groups IS NULL OR FIND_IN_SET('" . intval($groupId) . "', user_groups)");
		return $this;
	}

	public function addDateFilter($date) {
		$date = $this->getConnection()->quote($date);
		$this->_select->where("(start_publish_date IS NULL OR start_publish_date <= {$date}) and (end_publish_date IS NULL OR end_publish_date >= {$date})");
		return $this;
	}
}
?>