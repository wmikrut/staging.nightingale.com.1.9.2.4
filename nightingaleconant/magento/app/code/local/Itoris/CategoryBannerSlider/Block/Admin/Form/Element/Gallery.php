<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_CATEGORYBANNERSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_CategoryBannerSlider_Block_Admin_Form_Element_Gallery extends Varien_Data_Form_Element_Abstract {

    public function getElementHtml() {
        $html = $this->getContentHtml();
        return $html;
    }

    public function getContentHtml() {
        /* @var $content Itoris_CategoryBannerSlider_Block_Admin_Form_Element_Gallery_Content */
        $content = Mage::getSingleton('core/layout')
            ->createBlock('itoris_categorybannerslider/admin_form_element_gallery_content');

        $content->setId($this->getHtmlId() . '_content')
            ->setElement($this);
        return $content->toHtml();
    }

    public function getLabel() {
        return '';
    }

    public function toHtml() {
        return '<tr><td class="value" colspan="3">' . $this->getElementHtml() . '</td></tr>';
    }
	
}

?>