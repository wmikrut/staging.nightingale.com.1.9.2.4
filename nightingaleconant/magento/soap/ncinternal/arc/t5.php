<?php

include_once('XmlWriter.class.php');
include_once('soap/CreateXMLRequest.inc');

function XML2Array(SimpleXMLElement $parent) {
    $array = array();
    foreach ($parent as $name => $element) {
        ($node = & $array[$name])
        && (1 === count($node) ? $node = array($node) : 1)
        && $node = & $node[];

        $node = $element->count() ? XML2Array($element) : trim($element);
    }
    return $array;
}


$response = <<<STRING
<XMLResponse>
<XMLData>
<ServiceName>Test Service</ServiceName>
<FirstName>William</FirstName>
<LastName>Mikrut</LastName>
</XMLData>
</XMLResponse>
STRING;

$array_xmldata = array();

$xml   = simplexml_load_string($response);
$array = XML2Array($xml);
$array_xmldata = (array) $array['XMLData'];

//$ary_response = array($xml->getName() => $array_xmldata);
$ary_response = array('jsonResponse' => $array_xmldata);

$result = json_encode($ary_response);

echo $result. "\r\n";
                        
?>
