<?php
/**
 * @category   Apptrian
 * @package    Apptrian_ProductSlider
 * @author     Apptrian
 * @copyright  Copyright (c) 2015 Apptrian (http://www.apptrian.com)
 * @license    http://www.apptrian.com/license    Proprietary Software License (EULA)
 */
class Apptrian_ProductSlider_Model_Observer
{
	/**
	 * Adds custom handles based on config options.
	 * 
	 * @param Varien_Event_Observer $observer
	 * @return Apptrian_ProductSlider_Model_Observer
	 */
	public function addCustomHandles(Varien_Event_Observer $observer)
	{
	
		if (Mage::getStoreConfigFlag('apptrian_productslider/general/enabled')) {
	
			// Initialize customHandles array
			$customHandles = array();
				
			$pageType = Mage::helper('apptrian_productslider')->getPageType();
				
			if ($pageType !== null) {
					
				// Get slider handle if enabled
				if (Mage::getStoreConfigFlag('apptrian_productslider/' . $pageType . '_page/enabled')) {
						
					$parent   = Mage::getStoreConfig('apptrian_productslider/' . $pageType . '_page/parent');
					$position = Mage::getStoreConfig('apptrian_productslider/' . $pageType . '_page/position');
	
					$customHandles[] = 'apptrian_productslider_' . $parent . '_' . $position;
						
				}
					
			}
				
			if (count($customHandles) > 0) {
	
				$layout = $observer->getEvent()->getLayout();
	
				foreach ($customHandles as $customHandle) {
						
					$layout->getUpdate()->addHandle($customHandle);
						
				}
	
			}
	
	
		}
	
		return $this;
	
	}
	
}