<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_CATEGORYBANNERSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_CategoryBannerSlider_Model_Mysql4_Sets_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

	protected $tableSets = 'itoris_categorybannerslider_sets';
	protected $tableBanners = 'itoris_categorybannerslider_banners';
	protected $tableCategories = 'itoris_categorybannerslider_categories';

	protected function _construct() {
		$this->_init('itoris_categorybannerslider/sets');
		$this->tableSets = Mage::getSingleton('core/resource')->getTableName($this->tableSets);
		$this->tableBanners = Mage::getSingleton('core/resource')->getTableName($this->tableBanners);
		$this->tableCategories = Mage::getSingleton('core/resource')->getTableName($this->tableCategories);
	}

	protected function _initSelect() {
		 $this->getSelect()->from(array('main_table' => $this->tableSets))
					->joinLeft(array('banners' => $this->tableBanners),
							'banners.set_id = main_table.set_id',
							array('banners' => 'group_concat(DISTINCT concat_ws("._,", banner_id, title, alt) SEPARATOR ".-,")',
								'banners_count' => 'count(DISTINCT concat_ws("._,", title, alt, file_name))'
							)
		 				)
		 			->joinLeft(array('categories' => $this->tableCategories),
							"categories.set_id = main_table.set_id",
			 				array('category' => 'group_concat(DISTINCT concat_ws("-", store_id, category_id) SEPARATOR ",")',
							 	'category_count' => 'count(DISTINCT concat_ws("-", store_id, category_id))'
							 )
		 				)
					->group('main_table.set_id');

		 return $this;
	}

	protected function setTotalRecords() {
		$countSelect = clone $this->getSelect();
		$countSelect->reset(Zend_Db_Select::LIMIT_COUNT);
		$countSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
		$this->_totalRecords = count($this->_fetchAll($countSelect));
	}

	public function getSize() {
        $this->setTotalRecords();
        return $this->_totalRecords;
    }

}
 
?>