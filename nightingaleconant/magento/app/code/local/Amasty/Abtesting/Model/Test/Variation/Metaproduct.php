<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Model_Test_Variation_Metaproduct extends Mage_Catalog_Model_Product
{
    protected function _beforeSave()
    {
        return $this;
    }

    /**
     * Prevent index process
     * @return $this
     */
    protected function _afterSave()
    {
        return $this;
    }

    /**
     * Prevent index process
     * @return $this
     */
    public function afterCommitCallback()
    {
        return $this;
    }
}