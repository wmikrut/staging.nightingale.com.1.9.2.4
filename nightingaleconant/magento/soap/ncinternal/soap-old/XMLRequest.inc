<?php
/*
 *      XMLRequest.inc
 *
 *      Copyright 2015 wmikrut <wmikrut72@gmail.com>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 *
 *
 */

function XMLRequest ( $xml ) {
		
	$result = "";
    $response = "";
    $status = "";
    $pgm_continue = "N";

    $h1 = fopen('/tmp/20160711b.log', 'w+');
    fwrite($h1, $xml);
    fclose($h1);

    $xml_data = new SimpleXMLElement($xml);                                                                                                                       
    $lcl_service = (string) $xml_data->XMLData->ServiceName;
    
 	switch ( $lcl_service ) {
    	case "RequestToken":
        	include_once("soap/RequestToken.inc");
            $result = RequestToken($xml);
            break;
		case "ValidateToken":
			include_once("soap/ValidateToken.inc");
            $result = ValidateToken($xml);
            break;
		case "NCInsidersClub":
        	include_once("soap/NCInsidersClubV2.inc");
            $result = NCInsidersClub($xml);
            break;
        case "NCInsidersClubV2":
        	include_once("soap/NCInsidersClubV2.inc");
            $result = NCInsidersClub($xml);
            break;
		case "TestService":
			include_once("soap/TestService.inc");
            $result = TestService($xml);
            break;
		case "ValidateMagentoCredentials":
			include_once("soap/ValidateMagentoCredentials.inc");
            $result = ValidateMagentoCredentials($xml);
            break;
		default:
			include_once("soap/NoService.inc");
			$result = NoService($xml);
 	}
 	
 	
    return $result;
}                                                                                                                                                             
?>