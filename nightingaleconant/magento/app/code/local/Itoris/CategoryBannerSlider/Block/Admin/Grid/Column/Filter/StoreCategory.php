<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_CATEGORYBANNERSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */
class Itoris_CategoryBannerSlider_Block_Admin_Grid_Column_Filter_StoreCategory extends Mage_Adminhtml_Block_Widget_Grid_Column_Filter_Abstract {

	protected function _getOptions() {
        $emptyOption = array('value' => null, 'label' => '');

        $optionGroups = $this->getColumn()->getOptionGroups();
        if ($optionGroups) {
            array_unshift($optionGroups, $emptyOption);
            return $optionGroups;
        }

        $colOptions = $this->getColumn()->getOptions();
        if (!empty($colOptions) && is_array($colOptions) ) {
            $options = array($emptyOption);
            foreach ($colOptions as $value => $label) {
                $options[] = array('value' => $value, 'label' => $label);
            }
            return $options;
        }
        return array();
    }

    /**
     * Render an option with selected value
     *
     * @param array $option
     * @param string $value
     * @return string
     */
    protected function _renderOption($option, $value, $margin = 0) {
        $selected = (($option['value'] == $value && (!is_null($value))) ? ' selected="selected"' : '' );
        return '<option style="margin-left:'. $margin .'px;" value="'. $option['value'] .'"'.$selected.'>'. $option['label'] .'</option>';
    }

	/**
	 * Get filter html
	 *
	 * @return string
	 */
    public function getHtml() {
		$html = $this->__('Store:');
        $html .= ' <select onchange="getCategoriesDropdown(this.value, null, true);" name="'.$this->_getHtmlName().'[store]" id="'.$this->_getHtmlId().'" class="dropdown"><option></option>';
        $value = $this->getValue();
		$firstStoreId = 0;
		$categoryId = 0;
		if(isset($value['category'])){
			$categoryId = $value['category'];
		}
		if(isset($value['store'])){
			$value = $value['store'];
			$firstStoreId = $value;
		}
		$websites = Mage::app()->getWebsites();
		/** @var $website Mage_Core_Model_Website */
        foreach ($websites as $website){
			$html .= '<optgroup label="' . $website->getName() . '">';
			$stores = $website->getStores();
            foreach ($stores as $store){
				if(!$firstStoreId){
					$firstStoreId = $store->getId();
				}
                $html .= $this->_renderOption(array('value' => $store->getId(),
											  		'label' => $store->getName()
											  		), $value);
            }
            $html .= '</optgroup>';
        }
        $html.='</select> ';
		$html .= '<script type="text/javascript">';
		$html .= 'var categories = ' . Mage::helper('itoris_categorybannerslider/category')->getCategoriesJson() . ';';
		$html .= '</script>';
		$html .= $this->__('Category:');
        $html .= ' <select name="'.$this->_getHtmlName().'[category]" id="category_dropdown" class="dropdown">';
        $html.='</select> ';
		$html .= '<script type="text/javascript">getCategoriesDropdown('. $firstStoreId .','. $categoryId .', true)</script>';
        return $html;
    }

	/**
	 * Retrieve the filter condition for the collection
	 *
	 * @return array|null
	 */
    public function getCondition() {
        if (is_null($this->getValue())) {
            return null;
        }
		$value = $this->getValue();
		$store = isset($value['store']) ? $value['store'] : '';
        $cond['store'] = array('like'=>'%'. $store .'%');
		$category = isset($value['category']) ? $value['category'] : '';
        $cond['category'] = array('like'=>'%'. $category .'%');
		return $cond;
    }

}
?>
 
