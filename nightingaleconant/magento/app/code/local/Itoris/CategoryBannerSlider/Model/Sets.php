<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_CATEGORYBANNERSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_CategoryBannerSlider_Model_Sets extends Mage_Core_Model_Abstract {

	const STATUS_DISABLED = 0;
	const STATUS_ENABLED = 1;
	const EFFECT_SIMPLE = 2;
	const EFFECT_FADE = 3;
	const EFFECT_HORIZONTAL_SLIDING = 4;
	const SKIN_NO = 5;
	const SKIN_NUM_PAGER = 6;
	const SKIN_TITLED_PAGER = 7;
	const SKIN_ARROWS = 8;
	const SKIN_NUM_PAGER_ARROWS = 9;
	const SKIN_TITLED_PAGER_ARROWS = 10;
	const TARGET_BLANK = 11;
	const TARGET_SELF = 12;
	const DEFAULT_WIDTH = 300;
	const DEFAULT_HEIGHT = 200;

	/** @var $_resource Varien_Db_Adapter_Pdo_Mysql */
	protected $_resource;
	protected $tableSets = 'itoris_categorybannerslider_sets';
	protected $tableBanners = 'itoris_categorybannerslider_banners';
	protected $tableCategories = 'itoris_categorybannerslider_categories';

	public function __construct() {
		$this->_resource = Mage::getSingleton('core/resource')->getConnection('core_write');
		$this->_init('itoris_categorybannerslider/sets');
		$this->tableSets = Mage::getSingleton('core/resource')->getTableName($this->tableSets);
		$this->tableBanners = Mage::getSingleton('core/resource')->getTableName($this->tableBanners);
		$this->tableCategories = Mage::getSingleton('core/resource')->getTableName($this->tableCategories);
	}

	public function loadSet($id) {
		$set = $this->_resource->fetchRow("SELECT e.*, group_concat(DISTINCT concat_ws('._,', b.title, b.alt, b.delay, b.link_url, b.open_in, b.file_name, b.enabled, b.order) SEPARATOR '.-,') as banners,
											group_concat(DISTINCT concat_ws('-', c.store_id, c.category_id) SEPARATOR ',') as categories
		  							FROM $this->tableSets as e
									left join $this->tableBanners as b on b.set_id = e.set_id
									left join $this->tableCategories as c on c.set_id = e.set_id
									where e.set_id = $id");
		return $set;
	}

	public function saveSet(array $set) {
		if (is_array($set)) {
			$effect = (int)$set['effect'];
			$skin = (int)$set['skin'];
			$autorotation = isset($set['autorotation']) ? 1 : 0;
			$width = (int)$set['width'];
			$height = (int)$set['height'];
			$setId = 0;
			if (isset($set['set_id']) && !empty($set['set_id'])) {
				$setId = (int)$set['set_id'];
				$this->_resource->query("UPDATE {$this->tableSets} set `effect` = {$effect}, `skin` = {$skin}, `autorotation` = {$autorotation}, `width` = {$width}, `height` = {$height}
										where `set_id` = {$setId}
				");
				$this->_resource->query("delete t1,t2 from {$this->tableBanners} as t1, {$this->tableCategories} as t2 where t1.set_id = {$setId} and t2.set_id = {$setId}");
				$this->saveCategoriesBanners($set, $setId);
			} else {
				$status = Itoris_CategoryBannerSlider_Model_Sets::STATUS_ENABLED;
				$this->_resource->query("INSERT into {$this->tableSets} (`status`, `effect`, `skin`, `autorotation`, `width`, `height`)
										values ({$status}, {$effect}, {$skin}, {$autorotation}, {$width}, {$height})
				");
				$setId = $this->_resource->lastInsertId();
				$this->saveCategoriesBanners($set, $setId);
			}
			return $setId;
		} else {
			throw new Exception();
		}
	}

	/**
	 * Load banners for the category for the store
	 *
	 * @param $storeId
	 * @param $categoryId
	 * @return array
	 */
	public function loadCategoryBanner($storeId, $categoryId) {
		$status = Itoris_CategoryBannerSlider_Model_Sets::STATUS_ENABLED;
		return $this->_resource->fetchRow("SELECT s.effect, s.skin, s.autorotation, s.width, s.height,
										 group_concat(DISTINCT concat_ws('._,', b.title, b.alt, b.delay, b.link_url, b.open_in, b.file_name, b.order) SEPARATOR '.-,') as banners
										FROM {$this->tableCategories} as e
										inner join {$this->tableSets} as s
										on s.set_id = e.set_id and s.status = {$status}
										inner join {$this->tableBanners} as b
										on b.set_id = s.set_id and b.enabled = 1
										where e.store_id = {$storeId}
											and e.category_id = {$categoryId}
										group by e.set_id
										limit 1
		");
	}

	private function saveCategoriesBanners($set, $setId) {
		if (isset($set['categories'])) {
			$this->saveCategories($set['categories'], $setId);
		}
		if (isset($set['banners'])) {
			$this->saveBanners($set['banners'], $setId);
		}
	}

	private function saveCategories($categoriesSet, $setId) {
		$categories = $this->prepareForCategoriesTable($categoriesSet, $setId);
		$this->_resource->query("INSERT into {$this->tableCategories} (`set_id`, `store_id`, `category_id`)
								values {$categories}
		");
	}

	/**
     * Prepare (setId, storeId, categoryId)
     *
     * @return string
     */
	private function prepareForCategoriesTable($categoriesIds, $setId) {
		$categories = array();
		foreach ($categoriesIds as $category => $empty) {
			$ids = explode('_', $category);
			$categories[] = '(' . $setId . ',' . (int)$ids[0] . ',' . (int)$ids[1] . ')';
		}
		return implode(',', $categories);
	}

	private function saveBanners($bannersSet, $setId) {
		$banners = $this->prepareForBannersTable($bannersSet, $setId);
		$this->_resource->query("INSERT into {$this->tableBanners} (`set_id`, `title`, `alt`, `delay`, `link_url`, `open_in`, `file_name`, `enabled`, `order`)
								values {$banners}
		");
	}

	/**
     * Prepare banner data (setId, title, alt, delay, link_url, open_in, file_name, enabled, order)
     *
     * @return string
     */
	private function prepareForBannersTable($bannersSet, $setId) {
		$banners = array();
		foreach ($bannersSet as $banner) {
			$title = $this->_resource->quote($banner['title']);
			$alt = $this->_resource->quote($banner['alt']);
			$delay = (int)$banner['delay'];
			$link_url = $this->_resource->quote($banner['link_url']);
			$open_in = (int)$banner['open_in'];
			$file_name = $this->_resource->quote($banner['file_name']);
			$enabled = isset($banner['enabled']) ? 1 : 0;
			$order = (int)$banner['order'];
			$banners[] = '(' . implode(',', array($setId, $title, $alt, $delay, $link_url, $open_in, $file_name, $enabled, $order)) . ')';
		}
		return implode(',', $banners);
	}

	public function deleteSets($ids) {
		$keys = $this->idsForKeyIn($ids);
		$this->_resource->query("DELETE from {$this->tableSets} where `set_id` IN ({$keys})");
	}

	/**
	 * Change banners set status
	 *
	 * @param $ids
	 * @param $status
	 */
	public function changeSetsStatus($ids, $status) {
		$keys = $this->idsForKeyIn($ids);
		$this->_resource->query("UPDATE {$this->tableSets} SET `status` = {$status} WHERE `set_id` IN ({$keys})");
	}

	/**
	 * Convert array values to int then convert array to string 'value1,value2,...'
	 *
	 * @param $ids
	 * @return array|string
	 */
	private function idsForKeyIn($ids) {
		$keys = array_map('intval', $ids);
		$keys = implode(',', $keys);
		return $keys;
	}

	/**
	 * Retrive file name of the banner by banner id
	 *
	 * @param $id
	 * @return array
	 */
	public function loadBanner($id) {
		return $this->_resource->fetchRow("SELECT `file_name` FROM {$this->tableBanners} WHERE `banner_id` = {$id}");
	}

}
?>