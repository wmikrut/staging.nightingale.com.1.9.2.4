<?php

class NCCUSTOM_Ncreporting_Block_Adminhtml_Adminblock extends Mage_Core_Block_Template{
    
    //here we can put some logic if we want to call it in our adminblock.phtml
    public function myfunction()
    {
	return "Hello World!";
    }
    
    public function showcmspage($pageIdentifier)
    {    
    
	$page = Mage::getModel('cms/page');    
	$page->setStoreId(Mage::app()->getStore()->getId());    
	$page->load($pageIdentifier,'identifier');
	
	$helper = Mage::helper('cms');
	$processor = $helper->getPageTemplateProcessor();	
	$html = $processor->filter($page->getContent());
	
	if ( !$html ) 
	{
	    $html = "<h2>Your CMS Page named ". $pageIdentifier. " is missing.</h2>";
	}
		
	return $html;
    }
}