<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Helper_Image extends Mage_Core_Helper_Abstract
{
    const FOLDER = 'aw_islider';

    static function getImageUrl($imageName)
    {
        $isSecure = Mage::app()->getRequest()->isSecure();
        $baseMediaUrl = Mage::getBaseUrl('media', $isSecure);
        return $baseMediaUrl . self::FOLDER . '/' . $imageName;
    }

    /**
    * Returns full path to uploads storage
    * @return string
    */
    public static function getImagePath()
    {
        $path = Mage::getBaseDir('media') . DS . self::FOLDER . DS;
        if (!file_exists($path)) {
            mkdir($path);
        }
        return $path;
    }

    public static function imageResize($imageName, $width = 100, $height = 100)
    {
        $newName = $width . 'x' . $height . '_' . $imageName;
        $basePath = self::getImagePath();
        if (file_exists($basePath . $newName)) {
            return $newName;
        }
        if (class_exists('Varien_Image_Adapter_Gd2') && file_exists($basePath . $imageName)) {
            try {
                $image = new Varien_Image_Adapter_Gd2();
                $image->open($basePath . $imageName);
                $image->keepAspectRatio(true);
                $image->resize($width, $height);
                $image->save($basePath, $newName);
            } catch(Exception $e) {
                Mage::logException($e);
                return false;
            }
            return $newName;
        }
        return false;
    }

    public function imageResizeRemote($imagePath, $width = 100, $height = 100)
    {
        $newName = md5($imagePath) . '.' . self::getExtension($imagePath);
        if(self::getExtension($imagePath) && self::curlDownload($imagePath, self::getImagePath() . $newName)) {
            $result = self::imageResize($newName, $width, $height);
            @unlink(self::getImagePath() . $newName);
            return $result;
        }
        return false;
    }

    /**
    * Returns file extension
    * @param string $fname
    * @return string
    */
    public static function getExtension($fname)
    {
        $fileInfo = pathinfo($fname);
        return isset($fileInfo['extension']) ? $fileInfo['extension'] : '';
    }

    public function isAllowedImage($type) {
        $_allowedMimeTypes = array(
            'image/jpeg',
            'image/png',
            'image/gif'
        );
        return (in_array($type, $_allowedMimeTypes));
    }

    public static function curlDownload($from, $dest)
    {
        if (!self::curlCheckFunctions()) {
            return false;
        }
        $_ch = curl_init();
        curl_setopt($_ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($_ch, CURLOPT_SSL_VERIFYHOST, 0);
        if (!$_ch) {
            return false;
        }
        $df = fopen($dest, "w");
        if (!$df) {
            return false;
        }
        if (!curl_setopt($_ch, CURLOPT_URL, $from)) {
            fclose($df);
            curl_close($_ch);
            return false;
        }
        if (curl_setopt($_ch, CURLOPT_FILE, $df) && curl_setopt($_ch, CURLOPT_HEADER, 0) && curl_exec($_ch)) {
            curl_close($_ch);
            fclose($df);
            return true;
        }
        return false;
    }

    protected static function curlCheckFunctions()
    {
        return function_exists("curl_init") && function_exists("curl_setopt") &&
            function_exists("curl_exec") && function_exists("curl_close")
        ;
    }
}