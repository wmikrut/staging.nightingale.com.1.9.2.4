<?php

/**
 * Class NC_Forms_Block_Form
 */
class NC_Forms_Block_Form extends Mage_Directory_Block_Data {

	public function getBackUrl()
	{
		if ($this->getData('back_url')) {
			return $this->getData('back_url');
		}
		return $this->getUrl('/');
	}
}