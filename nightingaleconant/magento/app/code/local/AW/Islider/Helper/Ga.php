<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Helper_Ga
{
    protected function _getXmlAccountActive()
    {
        return defined('Mage_GoogleAnalytics_Helper_Data::XML_PATH_ACTIVE') // 1411 issue
            ? Mage_GoogleAnalytics_Helper_Data::XML_PATH_ACTIVE
            : 'google/analytics/active';
    }

    protected function _getXmlAccountPath()
    {
        return defined('Mage_GoogleAnalytics_Helper_Data::XML_PATH_ACCOUNT') // 1411 issue
            ? Mage_GoogleAnalytics_Helper_Data::XML_PATH_ACCOUNT
            : 'google/analytics/account';
    }

    public function getGaActive($store = null)
    {
        return Mage::getStoreConfig($this->_getXmlAccountActive(), $store);
    }

    public function getGaAccount($store = null)
    {
        return Mage::getStoreConfig($this->_getXmlAccountPath(), $store);
    }

    public function isInstalled()
    {
        return Mage::helper('awislider')->isExtensionInstalled('Mage_GoogleAnalytics');
    }

    public function canUseGa($store = null)
    {
        return ($this->isInstalled()
            && $this->getGaActive($store)
            && $this->getGaAccount($store));
    }
}
