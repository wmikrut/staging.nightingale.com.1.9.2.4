<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Helper_Data extends Mage_Core_Helper_Abstract
{
    const AW_IS_FORM_DATA_KEY = 'awislider_formdata';
    const AW_IS_FORM_DATA_IMAGES_KEY = 'awislider_formdata_images';
    const DEFAULT_DATE_VALUE = '0000-00-00';
    const AWIS_COOKIE_PREFIX = 'awislider-images-';
    const CACHE_BLOCK_PREFIX = 'awislider-frontend-block-';

    const README_URL = 'http://confluence.aheadworks.com/display/EUDOC/Images+Slider';

    public function isClickStatisticsEnabled($store = null)
    {
        return Mage::getStoreConfig('awislider/general/directurls', $store);
    }

    public function getReadmeUrl()
    {
        return self::README_URL;
    }

    public function isExtensionInstalled($name)
    {
        $modules = (array)Mage::getConfig()->getNode('modules')->children();
        return (array_key_exists($name, $modules)
            && ('true' == (string)$modules[$name]->active)
            && !(bool)Mage::getStoreConfig('advanced/modules_disable_output/' . $name));
    }

    /**
     * @return Mage_Core_Model_Cache|null
     */
    protected function _getFPCInstance()
    {
        if (method_exists($this, 'isModuleOutputEnabled')
            && $this->isModuleOutputEnabled('Enterprise_PageCache')
        ) {
            return Enterprise_PageCache_Model_Cache::getCacheInstance();
        }
    }

    /**
     * @param AW_Islider_Model_Slider $slider
     * @return array
     */
    public function getCacheTagsForSlider(AW_Islider_Model_Slider $slider)
    {
        return array(self::CACHE_BLOCK_PREFIX . $slider->getId());
    }

    public function getCacheTagsForMultipleBlock()
    {
        return array(self::CACHE_BLOCK_PREFIX . 'common');
    }

    public function cleanFPCForSlider(AW_Islider_Model_Slider $slider)
    {
        if (!$fpcInstance = $this->_getFPCInstance()) {
            return;
        }
        $cacheTags = array_merge(
            $this->getCacheTagsForMultipleBlock(),
            $this->getCacheTagsForSlider($slider)
        );
        $fpcInstance->clean($cacheTags);
        return $this;
    }
}
