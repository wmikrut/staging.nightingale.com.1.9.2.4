<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Block_Adminhtml_Sales_Order_Shipment_Create_Items extends Mage_Adminhtml_Block_Sales_Order_Shipment_Create_Items {

    public function getIsPackage($_item) {
        $itemId = $_item->getOrderItemId();
        $product = Mage::getModel('sales/order_item')->load($itemId);
        $subProduct = Mage::getModel('sales/order_item')->load($product->getParentItemId());

        if($product->getParentItemId()){
            if($subProduct->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE) {
                if($product->getQtyInvoiced()>$product->getQtyShipped() || ($product->getQtyInvoiced() == 0 && $product->getQtyOrdered()>$product->getQtyShipped())){
                    return true;
                }
            } else {
                return false;
            }
        }

        return false;
    }

    public function getParentQtys($_item){
        $itemId = $_item->getOrderItemId();
        $product = Mage::getModel('sales/order_item')->load($itemId);

        if($product->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE) {
            if($product->getQtyInvoiced()==$product->getQtyShipped() || ($product->getQtyInvoiced() == 0 && $product->getQtyOrdered()==$product->getQtyShipped())){
                return false;
            }
        }
    }

}