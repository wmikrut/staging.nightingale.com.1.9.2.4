<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Model_Service_Order extends Mage_Sales_Model_Service_Order {

    /**
     * Prepare order shipment based on order items and requested items qty
     *
     * @param array $qtys
     * @return Mage_Sales_Model_Order_Shipment
     */
    public function prepareShipment($qtys = array())
    {
        $this->updateLocaleNumbers($qtys);
        $totalQty = 0;
        $shipment = $this->_convertor->toShipment($this->_order);
        $data = Mage::app()->getRequest()->getParams();

        foreach ($this->_order->getAllItems() as $orderItem) {

            if (!$this->_canShipItem($orderItem, $qtys)) {
                continue;
            }

            $item = $this->_convertor->itemToShipmentItem($orderItem);

            if ($orderItem->isDummy(true)) {
                $qty = 0;
                if (isset($qtys[$orderItem->getParentItemId()])) {

                    $productOptions = $orderItem->getProductOptions();
                    if (isset($productOptions['bundle_selection_attributes'])) {
                        $bundleSelectionAttributes = unserialize($productOptions['bundle_selection_attributes']);

                        if ($bundleSelectionAttributes) {
                            $qty = $bundleSelectionAttributes['qty'] * $qtys[$orderItem->getParentItemId()];
                            $qty = min($qty, $orderItem->getSimpleQtyToShip());

                            $item->setQty($qty);
                            $shipment->addItem($item);
                            continue;
                        } else {
                            $qty = 1;
                        }
                    }
                } else {
                    $qty = 1;
                }
            } else {
                if (isset($qtys[$orderItem->getId()])) {
                    $qty = min($qtys[$orderItem->getId()], $orderItem->getQtyToShip());
                } elseif (!count($qtys)) {
                    $qty = $orderItem->getQtyToShip();
                } else {
                    continue;
                }
            }


            if(is_array($data)){
                //Let's check to make sure the shipment data has been fully
                //and not just loaded.
                if(array_key_exists('shipment', $data)){
                    $check = $data['shipment']['items'];
                    if(array_key_exists($orderItem->getItemId(), $check)){
                        //Let's load the order
                        $collection = Mage::getModel('sales/order_item')->load($orderItem->getItemId());
                        $order = Mage::getModel('sales/order')->load($collection->getOrderId());
                        //Let's load all the order items
                        $orderItems = $order->getAllItems();
                        foreach($orderItems as $product){
                            //Let's make sure the submitted product is equal to
                            //the current product
                            if($product->getProductId() == $orderItem->getProductId() && $orderItem->getProductType() != MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE){
                                //Lets make sure the parent product is a package product
                                //before setting the qty
                                $parentProduct = Mage::getModel('sales/order_item')->load($product->getParentItemId());
                                if($parentProduct->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE){
                                    $qty = $data['shipment']['items'][$orderItem->getItemId()];
                                    if($qty > $product->getQtyOrdered()){
                                        $qty = $product->getQtyOrdered();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $totalQty += $qty;
            $item->setQty($qty);
            $item->setQtyShipped($qty);
            $shipment->addItem($item);
        }

        $shipment->setTotalQty($totalQty);
        return $shipment;
    }

}