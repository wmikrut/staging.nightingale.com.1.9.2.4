<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Model_Resource_Test_Variation_Metaproduct_Collection extends Mage_Catalog_Model_Resource_Product_Collection
{
    /**
     * Prevent catalog_product_collection_load_before event
     * to keep meta products in collection
     */
    protected function _beforeLoad()
    {
        return $this;
    }
}