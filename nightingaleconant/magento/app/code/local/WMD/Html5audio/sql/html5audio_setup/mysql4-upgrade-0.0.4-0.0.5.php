<?php
/**
 * WMD_Html5audio Installer 0.0.5
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/

// $installer = $this;
$installer = new Mage_Catalog_Model_Resource_Eav_Mysql4_Setup('core_setup');
// $installer = $this;      
// $installer->startSetup();

$entityTypeId = $installer->getEntityTypeId(Mage_Catalog_Model_Product::ENTITY);
$idAttributeOldSelect = $installer->getAttribute($entityTypeId, 'playlist_id', 'attribute_id');
$installer->updateAttribute($entityTypeId, $idAttributeOldSelect, array(
    'backend_type' => 'text',
    'frontend_input' => 'multiselect'
));
  
// $installer->endSetup();


