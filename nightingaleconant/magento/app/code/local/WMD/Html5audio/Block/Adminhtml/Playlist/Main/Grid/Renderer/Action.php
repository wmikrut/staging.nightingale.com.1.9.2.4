<?php
/**
 * WMD_Html5audio_Block_Adminhtml_Playlist_Main_Grid_Renderer_Action
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Block_Adminhtml_Playlist_Main_Grid_Renderer_Action extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
 
    /**
     * Grid renderer for delete action
     *
     * @return string     
     */              
    public function render(Varien_Object $row)
    {
        $link = $this->getUrl('*/*/delete', array('_current' => true,'id' => $row->getId()));
        $rendered_link = '<a href="' . $link . '">'
            . Mage::helper('html5audio')->__('Delete') . '</a>';
        
        return $rendered_link;     
    }

} 