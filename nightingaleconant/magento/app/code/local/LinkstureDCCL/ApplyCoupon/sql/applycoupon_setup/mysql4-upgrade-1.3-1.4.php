<?php

$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE `" . $this->getTable('ltpl_applycoupon') . "`
ADD `coupon_id` int(10) NOT NULL default '0' AFTER `sid`;
");

$installer->endSetup();
