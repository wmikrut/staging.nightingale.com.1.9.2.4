<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


$installer = $this;
$installer->startSetup();

$sql ="
    ALTER TABLE {$this->getTable('awislider/images')} CHANGE `active_from` `active_from` DATE NOT NULL DEFAULT '" . AW_Islider_Helper_Data::DEFAULT_DATE_VALUE . "';
    ALTER TABLE {$this->getTable('awislider/images')} CHANGE `active_to` `active_to` DATE NOT NULL DEFAULT '" . AW_Islider_Helper_Data::DEFAULT_DATE_VALUE . "';
    ALTER TABLE {$this->getTable('awislider/images')} CHANGE `url` `url` TEXT NULL;
    ALTER TABLE {$this->getTable('awislider/images')} CHANGE `title` `title` TEXT NULL;
    ALTER TABLE {$this->getTable('awislider/images')} CHANGE `type` `type` TINYINT(4) NOT NULL DEFAULT 1;
    ALTER TABLE {$this->getTable('awislider/images')} CHANGE `is_active` `status` TINYINT(4) NOT NULL DEFAULT 1;
    ALTER TABLE {$this->getTable('awislider/images')} ADD `content_type` INT(3) NOT NULL DEFAULT 1;
    ALTER TABLE {$this->getTable('awislider/images')} ADD `ga_value` INT(3) NULL;
    ALTER TABLE {$this->getTable('awislider/sliders')} ADD `random_order` INT(3) NOT NULL DEFAULT 0;
    ALTER TABLE {$this->getTable('awislider/sliders')} ADD `representation` INT(3) NOT NULL DEFAULT 1;
    ALTER TABLE {$this->getTable('awislider/sliders')} ADD `transition_speed` DECIMAL(11,2) NOT NULL DEFAULT 1;
    ALTER TABLE {$this->getTable('awislider/sliders')} ADD `ga_category` VARCHAR(255) NULL;
    UPDATE {$this->getTable('awislider/sliders')} SET `representation` = ".AW_Islider_Model_Source_Representation::FIXED." WHERE (`height` > 0 OR `width` > 0)
";
$installer->run($sql);

$installer->endSetup();