<?php

class WP_AdvancedSliderAddonExportImport_AdminController extends Mage_Adminhtml_Controller_Action
{
    public function preDispatch()
    {
        parent::preDispatch();
        $this->_checkAjaxSession();
    }

    public function _checkAjaxSession()
    {
        if ($this->getRequest()->getParam('isAjax')) {
            if (!Mage::getSingleton('admin/session')->isLoggedIn() || !$this->_isAllowed()) {
                die($this->getResponse()->setBody($this->_getDeniedJson()));
            }
        }
    }

    protected function _getDeniedJson()
    {
        return Mage::helper('core')->jsonEncode(
            array(
                'ajaxExpired'  => 1,
                'ajaxRedirect' => $this->getUrl('adminhtml/index/login')
            )
        );
    }

    protected function _isAllowed()
    {
        return Mage::helper('advancedslideraddonexportimport')->isAllowed();
    }
}
