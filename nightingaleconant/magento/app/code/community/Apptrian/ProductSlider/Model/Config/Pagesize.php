<?php
/**
 * @category   Apptrian
 * @package    Apptrian_ProductSlider
 * @author     Apptrian
 * @copyright  Copyright (c) 2015 Apptrian (http://www.apptrian.com)
 * @license    http://www.apptrian.com/license    Proprietary Software License (EULA)
 */
class Apptrian_ProductSlider_Model_Config_Pagesize extends Mage_Core_Model_Config_Data
{
    
    public function _beforeSave()
    {
    
        $result = $this->validate();
        
        if ($result !== true) {
            
            Mage::throwException(implode("\n", $result));
            
        }
        
        return parent::_beforeSave();
        
    }
    
    public function validate()
    {
        
        $errors = array();
        $helper = Mage::helper('apptrian_productslider');
        $value  = $this->getValue();
        
        if (!Zend_Validate::is($value, 'Digits')) {
            $errors[] = $helper->__('Page size must be an integer.');
        }
        
        if (!Zend_Validate::is($value, 'GreaterThan', array(9))) {
            $errors[] = $helper->__('Page size must be greater than (or equal) 10.');
        }
        
        if (empty($errors)) {
            return true;
        }
        
        return $errors;
        
    }
    
}