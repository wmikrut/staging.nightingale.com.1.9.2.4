<?php

function isValidEmail($email) {
	return filter_var($email, FILTER_VALIDATE_EMAIL)
	&& preg_match('/@.+\./', $email)
	&& !preg_match('/@\[/', $email)
	&& !preg_match('/".+@/', $email)
	&& !preg_match('/=.+@/', $email);
}

function isDomainVald($email){
	$exp = "^[a-z\'0-9]+([._-][a-z\'0-9]+)*@([a-z0-9]+([._-][a-z0-9]+))+$";
	if(eregi($exp,$email)){
		if(checkdnsrr(array_pop(explode("@",$email)),"MX")){
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}

function ValidateEmail($email) {
	$isValid = 0;
	
	if ( isValidEmail($email) ) {
		if ( isDomainVald($email)) {
			$isValid = 1;
		} else {
			$isValid = 0;
		}		
	} else {
		$isValid = 0;
	}
	return $isValid;
}
?>