<?php
class NCCUSTOM_InsidersClubDigital99_Model_Observer 
{
    const INSIDERS_CLUB_DIGITAL_ITEM = 921;
    const INSIDERS_CLUB_DIGITAL_PRICING = 7;
    
    public function exportFeed($observer) 
    {		
    	include('mysql-credentials-production.php');
    	$mysqli = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_db);
    	$log = "nccustom_InsidersClubDigital99.log";
    	//Mage::log("InsidersClubDigital99", null, $log);
	    	
	    	$order = Mage::getModel('sales/order')->load(Mage::getSingleton('checkout/session')->getLastRealOrderId(), 'increment_id');
		//$order = $observer->getEvent()->getOrder();
		
		$customer_id = $order->getCustomerId();
		$customer_email = $order->getCustomerEmail();
		
		$cartItems = $order->getAllItems();
		foreach ( $cartItems as $item )
		{	
		    $product_id = $item->getProductId();
		    if ( $product_id == NCCUSTOM_InsidersClubDigital99_Model_Observer::INSIDERS_CLUB_DIGITAL_ITEM )
		    {
				$order_id = $order->getId();
				$line = $order_id. "-". $customer_id. "-". $customer_email;
				//mage::log($line, null, $log);		
				
				$_customer = Mage::getModel('customer/customer')->load($customer_id);
				$_customer->setGroupId(NCCUSTOM_InsidersClubDigital99_Model_Observer::INSIDERS_CLUB_DIGITAL_PRICING);
				$_customer->save();
				
				$mysqlstm = <<<STRING
select count(*) as rcds
from $mysql_db.nccustom_insiders_membership_state
where email_address = '$customer_email';
STRING;
				$f003 = $mysqli->query($mysqlstm);
				$db3 = $f003->fetch_assoc();
				$db3_rcds = $db3['rcds'];
				
				if ( $db3_rcds == 0 )
				{
					$start_date = date("Y-m-d H:i:s");
					$mysqlstm = "insert ignore into $mysql_db.nccustom_insiders_membership_state ";
					$mysqlstm.= "values(0, '$customer_email', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 0, '$order_id', '$start_date', '', 0);";
				} 
				else 
				{
					$mysqlstm = "update $mysql_db.nccustom_insiders_membership_state ";
					$mysqlstm.= "set active_member = 1 ";
					$mysqlstm.= "where email_address = '$customer_email';";
				}
						
				$fnull = $mysqli->query($mysqlstm);
				
				if ( isset($_SESSION['nccustom_coupon_code']))
				{
					$nccustom_coupon_code = $_SESSION['nccustom_coupon_code'];
					$order->setCouponCode($nccustom_coupon_code)->save();
					unset($_SESSION['nccustom_coupon_code']);
				}			
	    	}
		}		
		mysqli_close($mysqli);
    }
}
?>