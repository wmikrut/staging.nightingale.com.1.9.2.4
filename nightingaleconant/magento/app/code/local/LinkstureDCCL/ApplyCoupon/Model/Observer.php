<?php
class LinkstureDCCL_ApplyCoupon_Model_Observer 
{
	public function applyCouponEvent($observer)
	{
		$run_event = Mage::getSingleton('core/session')->getData("applyCouponEvent");
		if (Mage::getStoreConfig('applycoupon_section/applycoupon_group/active') && $run_event) {
			$coupon_code = trim(Mage::getSingleton("checkout/session")->getData("coupon_code"));
			Mage::getSingleton('core/session')->setData("applyCouponEvent",0);
			if ($coupon_code != '')
			{
				Mage::getSingleton('checkout/cart')->getQuote()->setCouponCode($coupon_code)->save();
			}
		}
	}
	public function addCouponEvent($observer)
	{
		if (Mage::getStoreConfig('applycoupon_section/applycoupon_group/active')) {
			$rule = $observer->getEvent()->getRule();
			$id=$rule->getRuleId();
			$rulename = $rule->getName();
			// $couponcode = $rule->getCouponCode();
			$website_ids = $rule->getWebsiteIds();
			$fromDate = $rule->getFromDate();
          	$toDate = $rule->getToDate();
          	$toDate = ($toDate) ? $toDate : date('Y-m-d');
          	$fromDate = ($fromDate) ? $fromDate : date('Y-m-d');
			if(strtotime(date('Y-m-d')) <= strtotime($toDate) && strtotime(date('Y-m-d')) >= strtotime($fromDate) && $rule->getIsActive()){
				$status = 1;
			} else {
				$status = 0;
			}
			//$status = $rule->getIsActive();
		
		  	$load_rule = $this->_loadSalesRule($id);
	        $collection = Mage::getResourceModel('salesrule/coupon_collection');
	        $collection->addRuleToFilter($load_rule);
	        // $this->_applyCollectionModifiers($collection);
	        $datas = $collection->load()->toArray();
	        $data_collection = $datas['items'];

            $applycoupon = Mage::getModel('applycoupon/applycoupon');
			$collections = $applycoupon->getCollection();

			foreach ($data_collection as $data) {
		    	$couponcode = $data['code'];
		    	$coupon_id =  $data['coupon_id'];
		    	$arr_coupon_id[] = $data['coupon_id'];
		    	
		    	$websites=array();
		    	$nid=array();
				$i = 0;
		    	
		    	foreach ($collections as $collection) {
			    	if ($collection->getSid() == $id && $collection->getCouponId() == $coupon_id ) {
						$nid[$i] = $collection->getId();
						$websites[$i] = $collection->getWebsites();
						$i += 1;
					}
				}
				
				if (count($website_ids) == count($nid)) {
			    	// echo "Update Rule" .'</br>';
					for ($i=0; $i < count($nid); $i++) {
						if (isset($nid[$i])) {
							$base_url = Mage::app()->getWebsite($website_ids[$i])->getDefaultStore()->getBaseUrl();
							$link_without_redirection  = $base_url.'applycoupon/index/?code='.$couponcode.'&return_url=no';
						 	$applycoupon->setData(array('id' => $nid[$i],'rule_name' => $rulename,'coupon_code' => $couponcode,'websites' => $website_ids[$i], 'link_without_redirection' => $link_without_redirection, 'status' => $status,'sid' => $id,'coupon_id' => $coupon_id));
							$applycoupon->save();
						 } 
					}
				}elseif (count($website_ids) > count($nid)) {
					for ($i=0; $i < count($website_ids); $i++) { 
						if (!in_array($website_ids[$i], $websites)) {
							// echo "Add new Rules" .'</br>';
							$base_url = Mage::app()->getWebsite($website_ids[$i])->getDefaultStore()->getBaseUrl();
							$link_without_redirection  = $base_url.'applycoupon/index/?code='.$couponcode.'&return_url=no';
							$applycoupon->setData(array('coupon_id' => $coupon_id,'sid' => $id,'rule_name' => $rulename, 'coupon_code' => $couponcode,'websites' => $website_ids[$i], 'link_without_redirection' => $link_without_redirection, 'status' => $status));
							$applycoupon->save();
						}
						else{
							// echo "old Rule + New Rule".'<br>';
								$_collection_for_id = Mage::getModel('applycoupon/applycoupon')->getCollection()->addFieldToFilter('coupon_id', $coupon_id)->addFieldToFilter('websites', $website_ids[$i]);
								$array_id = $_collection_for_id->getData();
								$myid= $array_id[0]['id'];
								$base_url = Mage::app()->getWebsite($website_ids[$i])->getDefaultStore()->getBaseUrl();
								$link_without_redirection  = $base_url.'applycoupon/index/?code='.$couponcode.'&return_url=no';
								if (isset($myid)) {
									$applycoupon->setData(array('id' => $myid,'rule_name' => $rulename,'coupon_code' => $couponcode,'websites' => $website_ids[$i], 'link_without_redirection' => $link_without_redirection,'status' => $status,'sid' => $id,'coupon_id' => $coupon_id));
									$applycoupon->save();
								}
								
							}
					}
				}elseif (count($website_ids) < count($nid)) {
					//update & delete
					for ($i=0; $i < count($websites); $i++) { 
						 if (!in_array($websites[$i], $website_ids)) {
							if($nid[$i] > 0){
					    		$applycoupon->load($nid[$i]);
					    		$applycoupon->delete();
							}
						}else{
								$_collection_for_id = Mage::getModel('applycoupon/applycoupon')->getCollection()->addFieldToFilter('coupon_id', $coupon_id)->addFieldToFilter('websites', $website_ids[$i]);
								$array_id = $_collection_for_id->getData();
								$myid = $array_id[0]['id'];
								if (isset($myid)) {
									$applycoupon->setData(array('id' => $myid,'rule_name' => $rulename,'coupon_code' => $couponcode,'websites' => $website_ids[$i],'status' => $status,'sid' => $id,'coupon_id' => $data['coupon_id']));
									$applycoupon->save();		
								}
							}
					}
				}
			}
				$applycoupon = Mage::getModel('applycoupon/applycoupon');
				$collections = $applycoupon->getCollection();
				$collections->addFieldToFilter('sid',$id);
				if (count($collections) > $datas['totalRecords']) {
					foreach ($collections as $collection) {
						if (!in_array($collection->getCouponId(), $arr_coupon_id)) {
							$applycoupon->load($collection->getId());
						    		$applycoupon->delete();
						}
					}
				}
			 
		}
	}
	
	protected function _loadSalesRule($ruleId)
    {
        if (!$ruleId) {
            $this->_critical(Mage::helper('salesrule')
                ->__('Rule ID not specified.'), Mage_Api2_Model_Server::HTTP_BAD_REQUEST);
        }
        $rule = Mage::getModel('salesrule/rule')->load($ruleId);
        if (!$rule->getId()) {
            $this->_critical(Mage::helper('salesrule')
                ->__('Rule was not found.'), Mage_Api2_Model_Server::HTTP_NOT_FOUND);
        }
        return $rule;
    }
	
	public function deleteCouponEvent($observer)
	{
		if (Mage::getStoreConfig('applycoupon_section/applycoupon_group/active')) {
			$sid = Mage::app()->getRequest()->getParam('id');
			$obj = Mage::getModel('applycoupon/applycoupon');
			$collections = $obj->getCollection();
			$i = 0;
			foreach ($collections as $collection) {
					if ($collection->getSid() == $sid) {
	    				$id[$i]=$collection->getId();
	    				$i += 1;
					}
	    		}
	    		for ($i=0; $i < count($id); $i++) { 
	    			if($id[$i] > 0){
			    		$obj->load($id[$i]);
				   		$obj->delete();
					}
	    		}
	    }
	}
}