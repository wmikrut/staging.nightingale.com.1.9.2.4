<?php
/*
 *      ValidateCredentials.inc
 *
 *      Copyright 2015 wmikrut <wmikrut72@gmail.com>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 *
 *
 */

function ValidateCredentials($area='', $user='', $pass='') {                                                                                                                                
    include('mysql-credentials-production.php');
    
    $valid_credentials = 'N';
    
    $mysqli = new mysqli($mysql_hostname, $mysql_username, $mysql_password, $mysql_db);
    $ip = "localhost";
    
    if ( !$mysqli->connect_errno) {
    	$mysqlstm = <<<STRING
select count(*) as rcds
from nccustom_web_service_security
where service = '$area'
and user = '$user'
and password = '$pass'
and active = 1;
STRING;
    	$f001 = $mysqli->query($mysqlstm);
    	$db1 = $f001->fetch_assoc();
    	if ( $db1['rcds'] == 1 ) {
    		$valid_credentials = 'Y';
    	}
    }
    return $valid_credentials;
}
?>