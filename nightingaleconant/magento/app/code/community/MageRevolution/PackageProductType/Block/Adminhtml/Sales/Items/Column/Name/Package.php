<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Block_Adminhtml_Sales_Items_Column_Name_Package extends Mage_Adminhtml_Block_Sales_Items_Column_Name {

    /**
     * Add line breaks and truncate value
     *
     * @param string $value
     * @return array
     */
    public function getPackageOptions($options, $qtyOrdered)
    {
        $result = array();
        $orderedItems = array();
        $qtyShipped = array();

        $product = Mage::getModel('sales/order_item')->load($this->_getData('item')->getOrderItemId());
        if($product->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE) {
            $collection = Mage::getModel('sales/order')->load($product->getOrderId());
            $ordered_items = $collection->getAllItems();

            foreach($ordered_items as $items){
                if($items->getProductType() != MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE){
                    $orderedItems[$items->getProductId()] = $items->getItemId();
                    $qtyShipped[$items->getProductId()] = $items->getQtyShipped();
                }
            }
        }

        if(is_array($options) && is_numeric($qtyOrdered)){
            foreach($options as $option){
                $i = 0;
                foreach($option['associated'] as $associated){
                    $product = Mage::getModel('catalog/product')->load($associated);
                    $qty = $option['associated_qty'][$associated] * $qtyOrdered;

                    $result[] = array(
                        'name' => $product->getName(),
                        'sku' => $product->getSku(),
                        'qty_ordered' => $qty,
                        'product_id' => $product->getId(),
                        'ordered_item_id' => $orderedItems,
                        'qty_shipped' => $qtyShipped
                    );
                    $i++;
                }
            }
        }

        return $result;
    }

    public function getIsPackageChild(){
        $product = Mage::getModel('sales/order_item')->load($this->_getData('item')->getOrderItemId());

        if($product && $product->getProductType()){
            if($product->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE){
                return true;
            } elseif($product->getParentItemId()){
                $product = Mage::getModel('sales/order_item')->load($product->getParentItemId());
                if($product->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE){
                    return false;
                }
            }
        }
        return true;
    }

    public function getIsPackageChilds($_items){
        $product = Mage::getModel('sales/order_item')->load($_items->getItemId());

        if($product && $product->getProductType()){
            if($product->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE){
                return true;
            } elseif($product->getParentItemId()){
                    $product = Mage::getModel('sales/order_item')->load($product->getParentItemId());
                    if($product->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE){
                        return false;
                    }
                }
        }
        return true;
    }

    public function getParentSku($_item) {
        $product = Mage::getModel('sales/order_item')->load($_item->getItemId());

        if($product->getParentItemId()){
            $parent = Mage::getModel('sales/order_item')->load($product->getParentItemId());
            return $parent->getSku();
        } else {
            return false;
        }
    }
}