<?php
/**
 * WMD_Html5audio_Block_Adminhtml_Sample_New_Tab_Sample
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Block_Adminhtml_Sample_New_Tab_Sample extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare Form
     *
     * @return object
     */	
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        
//         $configValue = Mage::getStoreConfig('html5audio/settings/valuename');				

		    $fieldset = $form->addFieldset('edit_link', array('legend' => Mage::helper('html5audio')->__('New Sample')));
		
        $fieldset->addField('title', 'text', array(
            'name'      => 'title',
            'title'     => Mage::helper('html5audio')->__('Title'),
            'label'     => Mage::helper('html5audio')->__('Title'),
        		'required'	=> true,	
        ));	
        
        if (Mage::getStoreConfig('html5audio/sample/stream'))
        {        
            $fieldset->addField('mp3_file', 'file', array(
                'name'      => 'mp3_file',
                'title'     => Mage::helper('html5audio')->__('mp3 File'),
                'label'     => Mage::helper('html5audio')->__('mp3 File'),
            		'required'	=> true,	
            ));	
            
            $fieldset->addField('ogg_file', 'file', array(
                'name'      => 'ogg_file',
                'title'     => Mage::helper('html5audio')->__('ogg File'),
                'label'     => Mage::helper('html5audio')->__('ogg File'),
            		'required'	=> true,	
            ));	
        }
        else
        {     
            $fieldset->addField('mp3_path', 'text', array(
                'name'      => 'mp3_path',
                'title'     => Mage::helper('html5audio')->__('mp3 URL'),
                'label'     => Mage::helper('html5audio')->__('mp3 URL'),
            		'required'	=> true,	
            ));	
            
            $fieldset->addField('ogg_path', 'text', array(
                'name'      => 'ogg_path',
                'title'     => Mage::helper('html5audio')->__('ogg URL'),
                'label'     => Mage::helper('html5audio')->__('ogg URL'),
            		'required'	=> true,	
            ));	
        }
        $this->setForm($form);
        return parent::_prepareForm();	
    }
    
}