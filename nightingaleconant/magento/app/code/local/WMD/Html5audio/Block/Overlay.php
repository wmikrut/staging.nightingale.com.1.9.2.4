<?php
/**
 * WMD_Html5audio_Block_Overlay
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://eklatant.ch/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Editbloginplace
 * @author    Dominik Wyss <info@ e k l a t a n t .ch>
 * @copyright 2013 Dominik Wyss | WMD Extensions (http:// e k l a t a n t .ch)
 * @link      http:// e k l a t a n t .ch
 * @license   http://eklatant.ch/WMD-License-Community.txt
*/

class WMD_Html5audio_Block_Overlay extends Mage_Core_Block_Template
{
    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {        
        if (Mage::getStoreConfig('html5audio/player/overlay')) 
        { 
            return parent::_toHtml();
        } 
        else 
        {
            return;
        }
    }

}