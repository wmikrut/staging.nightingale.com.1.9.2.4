jQuery.noConflict();
(function( $ ){

  var methods = {
    init : function( options ) { 
 
      return this.each(function() {
				var $this = $(this);     
        var pos = jQuery(window).scrollTop();
				 
        $('#jplayer_window').css('display', 'block');
        $('#jplayer_window').css('top', (100 + pos) + 'px');
        $('#jplayer_window').css('left', ($(window).width() / 2) - ($('#jplayer_window').width() / 1.8) + 'px');
        $('#jplayer_overlay').css('display', 'block');
        $('#jplayer_overlay').css('height', $(document).height() + 'px');
        $('#jplayer_overlay').click(function () {
            $('#jplayer_overlay').css('display', 'none');
            $this.css('display', 'none');
            $('#jquery_jplayer').jPlayer('destroy');
        }); 
        
        jQuery(window).scroll(function() {
            var pos = jQuery(window).scrollTop();
            jQuery('#jplayer_window').css('top', (100 + pos) + 'px');
        });
        
        jQuery(window).resize(function() {
            $('#jplayer_window').css('left', ($(window).width() / 2) - ($('#jplayer_window').width() / 1.8) + 'px');        
        });
        
			});
    }
  };
  
  $.fn.overlayplayer = function( method ) { 
      
    if ( methods[method] ) {
      return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' does not exist on jQuery.overlayplayer' );
    }    
  
  };
})( jQuery );