<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Block_Widget_Grid_Column_Renderer_Imagepreview extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function _getValue(Varien_Object $row)
    {
        if ($row->getData('content_type') == AW_Islider_Model_Source_Images_Contenttype::CONTENT_TYPE_VIDEO) {
            return $this->_prepareVideoValue($row);
        } else {
            return $this->_prepareImageValue($row);
        }
    }

    protected function _prepareVideoValue($row)
    {
        $videoUrl = $row->getData('location');
        if ($videoUrl) {
            return sprintf('<iframe class="video-slide" type="text/html" width="150" height="150" src="%s" frameborder="0">&lt;/li&gt;</iframe>', $videoUrl.'?controls=0');
        }
        return sprintf('<div style="text-align:center;">%s</div>', '<p>' . $this->__('No Video') . '</p>');
    }

    protected function _prepareImageValue($row)
    {
        $imageName = parent::_getValue($row);
        if($row->getData('type') == AW_Islider_Model_Source_Images_Type::FILE) {
            $resizedImageName = Mage::helper('awislider/image')->imageResize($imageName);
        } else {
            $resizedImageName =  Mage::helper('awislider/image')->imageResizeRemote($imageName);
        }
        if (!$resizedImageName) {
            return sprintf('<div style="text-align:center;">%s</div>', '<p>' . $this->__('No Image') . '</p>');
        }
        return sprintf('<div style="text-align:center;">%s</div>', '<img src="' . AW_Islider_Helper_Image::getImageUrl($resizedImageName) . '" />');
    }

}