<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */




class Itoris_ProductSlider_Block_ProductSlider extends Mage_Catalog_Block_Product_Abstract implements Mage_Widget_Block_Interface {

	const COLLECTION_RULE_TOP_RATED = 1;
	const COLLECTION_RULE_TOP_SELLERS = 2;
	const COLLECTION_RULE_JUST_ADDED = 3;
	const COLLECTION_RULE_NEW_FROM_TO = 9;
	const COLLECTION_RULE_MOST_REVIEWED = 4;
	const COLLECTION_RULE_WISHLIST_TOP = 5;
	const COLLECTION_RULE_ON_SALE = 6;
	const COLLECTION_RULE_RANDOM = 7;
	const COLLECTION_RULE_SELECTED_CATEGORIES = 8;
	const ORDER_BY_RANDOMLY = 1;
	const ORDER_BY_ALPHABETICALLY = 2;
	const ORDER_BY_PRICE_LOW_TO_HIGH = 3;
	const ORDER_BY_PRICE_HIGH_TO_LOW = 4;
	const ORDER_BY_MOST_REVIEWED_FIRST = 5;
	const ORDER_BY_TOP_RATED_FIRST = 6;
	const ORDER_BY_TOP_SELLERS_FIRST = 7;
	const ORDER_BY_MOST_DISCOUNTED_FIRST = 8;
	const ORDER_BY_NEWEST_FIRST = 9;
    const AUTO_SLIDING_ENABLED = 1;
    const AUTO_SLIDING_NO_ENABLED = 0;

	protected static $widgetTotalCount = 0;
	static protected $cssJsLoaded = false;

	protected $widgetId = 0;
	/** @var null|Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection */
	protected $productCollection = null;
	protected $notSortedProductIds = array();
	protected $blockId = null;

	protected function _construct() {
		if ($this->isEnabled()) {
            if ($this->hasData('template')) {
                $this->setTemplate($this->getData('template'));
            } else {
                $this->setTemplate('itoris/productslider/product_slider.phtml');
            }
			$this->widgetId = ++self::$widgetTotalCount;
			$this->blockId = 'itoris_productslider' . $this->widgetId . md5(microtime(true));
		}
	}

	public function getBlockId() {
		return $this->blockId;
	}

	public function isEnabled() {
		return $this->getDataHelper()->getSettings()->getEnabled() && $this->getDataHelper()->isRegisteredAutonomous();
	}

    public function autoslidingEnabled() {
        return $this->getAutoSlidingEnabled();
    }

    public function delayInSeconds() {
        return $this->getDelaySeconds();
    }

	public function isAllowSliding() {
		return $this->getAllowSliding();
	}

    public function productPerSlide() {
        return $this->getProductsPerSlide();
    }

	public function setCssJsLoaded() {
		self::$cssJsLoaded = true;
	}

	public function isCssJsLoaded() {
		return self::$cssJsLoaded;
	}

	/**
	 * @param $product Mage_Catalog_Model_Product
	 * @return bool
	 */
	public function isInStock($product) {
		Mage::getModel('catalogInventory/stock_item')->assignProduct($product);
		if (!$product->getIsSalable()) {
			if ($this->getDisplayOut()) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	protected function getProductsRawSql($orderBy = null) {
		/** @var $resource Mage_Core_Model_Resource */
		$resource = Mage::getSingleton('core/resource');
		/** @var $connection Varien_Db_Adapter_Pdo_Mysql */
		$connection = $resource->getConnection('read');
		$storeId = (int)Mage::app()->getStore()->getId();
		$websiteId = (int)Mage::app()->getWebsite()->getId();
		$customerGroupId = (int)Mage::getSingleton('customer/session')->getCustomer()->getGroupId();
		$rootCategoryId = (int)Mage::app()->getStore()->getRootCategoryId();

		$tableCatalogProductEntity = $resource->getTableName('catalog_product_entity');

		$tableEavAttribute = $resource->getTableName('eav_attribute');
		$tableEavEntityType = $resource->getTableName('eav_entity_type');

		$tableCatalogProductIndexPrice = $resource->getTableName('catalog_product_index_price');
		$tableCatalogCategoryProductIndex = $resource->getTableName('catalog_category_product_index');

		$tableWishlistItem = $resource->getTableName('wishlist_item');
		$tableCataloginventoryStockItem = $resource->getTableName('cataloginventory_stock_item');
		$tableReviewEntity = $resource->getTableName('review_entity');
		$tableReview = $resource->getTableName('review');
		$tableReviewEntitySummary = $resource->getTableName('review_entity_summary');
		$tableSalesFlatOrderItem = $resource->getTableName('sales_flat_order_item');
		$tableSalesFlatOrder = $resource->getTableName('sales_flat_order');
		$entityAttributeTables = array(
			'int'     => $resource->getTableName('catalog_product_entity_int'),
			'varchar' => $resource->getTableName('catalog_product_entity_varchar'),
			'text'    => $resource->getTableName('catalog_product_entity_text'),
			'datetime'=> $resource->getTableName('catalog_product_entity_datetime'),
		);

		$attributes = $connection->fetchAll("select e.attribute_id, e.attribute_code, e.backend_type from {$tableEavAttribute} as e
				where e.entity_type_id = (select entity_type.entity_type_id from {$tableEavEntityType} as entity_type where entity_type.entity_type_code = 'catalog_product')
					and e.attribute_code in('name', 'small_image', 'thumbnail', 'image', 'short_description', 'visibility', 'status', 'url_path', 'news_from_date', 'news_to_date')
		");
		/** for main sql */
		$mainAttributesToSelect = array();
		$mainAttributesJoin = array();
		/** for id sql */
		$attributesToSelect = array();
		$attributesJoin = array();
		/** for visibility sql */
		$attributesToSelectTemp = array();
		$attributesJoinTemp = array();
		$allowedIdSelectAttributes = array('name', 'visibility', 'status', 'news_from_date', 'news_to_date');
		foreach ($attributes as $attribute) {
			if (isset($entityAttributeTables[$attribute['backend_type']])) {
				$tableAlias = 't_' . $attribute['attribute_code'];
				 $attributeJoinStr = "left join {$entityAttributeTables[$attribute['backend_type']]} as {$tableAlias}_def
					on {$tableAlias}_def.entity_id = e.entity_id and {$tableAlias}_def.attribute_id = {$attribute['attribute_id']} and {$tableAlias}_def.store_id = 0
					left join {$entityAttributeTables[$attribute['backend_type']]} as {$tableAlias}
					on {$tableAlias}.entity_id = e.entity_id and {$tableAlias}.attribute_id = {$attribute['attribute_id']} and {$tableAlias}.store_id = {$storeId}
				";
				$mainAttributesJoin[] = $attributeJoinStr;
				$attributeSelectStr = "if({$tableAlias}.store_id > 0, {$tableAlias}.value, {$tableAlias}_def.value) as {$attribute['attribute_code']}";
				$mainAttributesToSelect[] = $attributeSelectStr;
				if (in_array($attribute['attribute_code'], $allowedIdSelectAttributes)) {
					if ($attribute['attribute_code'] == 'name' && $this->getOrderBy() != self::ORDER_BY_ALPHABETICALLY) {
						continue;
					}
					$attributesToSelect[] = $attributeSelectStr;
					$attributesJoin[] = $attributeJoinStr;
					if ($attribute['attribute_code'] != 'name') {
						$attributesJoinTemp[] = $attributeJoinStr;
						$attributesToSelectTemp[] = $attributeSelectStr;
					}
				}
			}
		}
		$visibilityValues = 'in(' . Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG . ','
			. Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH . ')';
		$visibilityCondition = 'visibility ' . $visibilityValues;

		$useGroupBy = false;

		$reviewJoinSql = $this->_prepareReviewJoinSql('left', $tableReviewEntity, $tableReview, $tableReviewEntitySummary, $storeId);

		$ratingSummarySelectSql =  "IF(review_table.status_id=1,IF(t_rating_summary.primary_id > 0, t_rating_summary.rating_summary, t_rating_summary_default.rating_summary),0) as rating_summary";
		$addReviewJoin = false;
		if ($this->getOrderBy() == self::ORDER_BY_TOP_RATED_FIRST
			|| $this->getOrderBy() == self::ORDER_BY_MOST_REVIEWED_FIRST
			|| $this->getCollectionRule() == self::COLLECTION_RULE_TOP_RATED
			|| $this->getCollectionRule() == self::COLLECTION_RULE_MOST_REVIEWED
		) {
			$addReviewJoin = true;
			if ($this->getCollectionRule() == self::COLLECTION_RULE_MOST_REVIEWED) {
				$attributesJoinTemp[] = $reviewJoinSql;
			}
		}




		$orders = array();

		$categoryFilter = "cat_index.category_id={$rootCategoryId}";
		$priceCondition = "";
		$whereCondition = "";

		$orderByOrder = null;
		$orderByAttributeToSelect = null;
		$orderByAttributeJoin = null;
		switch ($this->getOrderBy()) {
			case self::ORDER_BY_ALPHABETICALLY:
				$orderByOrder = "name asc";
				break;
			case self::ORDER_BY_NEWEST_FIRST:
				$orderByOrder = "e.created_at desc";
				break;
			case self::ORDER_BY_PRICE_HIGH_TO_LOW:
				$orderByOrder = 'maximum_price desc';
				break;
			case self::ORDER_BY_PRICE_LOW_TO_HIGH:
				$orderByOrder = 'final_price asc';
				break;
			case self::ORDER_BY_MOST_DISCOUNTED_FIRST:
				$orderByAttributeToSelect = 'if(price_index.price > 0, (price_index.price - if(price_index.tier_price is not null, LEAST(price_index.min_price, price_index.tier_price), price_index.min_price))/price_index.price, 0) as price_discount';
				$orderByOrder = 'price_discount desc';
				break;
			case self::ORDER_BY_RANDOMLY:
				$orderByOrder = 'rand()';
				break;
			case self::ORDER_BY_TOP_RATED_FIRST:
				$orderByOrder = "rating_summary desc";
				break;
			case self::ORDER_BY_MOST_REVIEWED_FIRST:
				$orderByAttributeToSelect = "sum(IF(review_table.status_id=1,IF(t_rating_summary.primary_id > 0, t_rating_summary.reviews_count, t_rating_summary_default.reviews_count),0)) as product_reviews_count";
				$orderByOrder = "product_reviews_count desc";
				break;
			case self::ORDER_BY_TOP_SELLERS_FIRST:
				$orderByAttributeJoin = "left join {$tableSalesFlatOrderItem} as sales_item
						on sales_item.product_id = e.entity_id and (sales_item.store_id = {$storeId} or sales_item.store_id = 0)";
				$orderByAttributeToSelect = 'sum(sales_item.qty_ordered) as product_sale_count';
				$orderByOrder = "product_sale_count desc";
				$useGroupBy = true;
				break;
		}
		$mostReviewCondition = null;
		$newsFromToCondition = null;
		$whereConditions = array();
		switch ($this->getCollectionRule()) {
			case self::COLLECTION_RULE_WISHLIST_TOP:
				 $wishlistJoinSql = "inner join {$tableWishlistItem} as wishlist_item
					on wishlist_item.product_id = e.entity_id and wishlist_item.store_id = {$storeId}";
				$attributesJoin[] = $wishlistJoinSql;
				$attributesJoinTemp[] = $wishlistJoinSql;
				$attributesToSelect[] = 'sum(wishlist_item.qty) as product_wishlist_count';
				$orders[] = "product_wishlist_count desc";
				$useGroupBy = true;
				break;
			case self::COLLECTION_RULE_JUST_ADDED:
				$orders[] = "e.created_at desc";
				break;
			case self::COLLECTION_RULE_TOP_RATED:
				$orders[] = "rating_summary desc";
				break;
			case self::COLLECTION_RULE_RANDOM:
				$productIdsRaw = $connection->fetchAll("select entity_id from {$tableCatalogProductEntity} order by rand()" . ($this->getMaximumProducts() ? 'limit ' . (int)$this->getMaximumProducts() * 2 : ''));
				$productIds = array();
				foreach ($productIdsRaw as $productId) {
					$productIds[] = (int)$productId['entity_id'];
				}
				if (!empty($productIds)) {
					$whereConditions[] = "e.entity_id in(" . implode(',', $productIds) . ")";
				}
				break;
			case self::COLLECTION_RULE_MOST_REVIEWED:
				$mostReviewSelectSql = "sum(IF(review_table.status_id=1,IF(t_rating_summary.primary_id > 0, t_rating_summary.reviews_count, t_rating_summary_default.reviews_count),0)) as product_reviews_count";
				$attributesToSelect[] = $mostReviewSelectSql;
				$attributesToSelectTemp[] = $mostReviewSelectSql;
				$orders[] = "product_reviews_count desc";
				$mostReviewCondition = ' and product_reviews_count > 0';
				break;
			case self::COLLECTION_RULE_TOP_SELLERS:
				 $topSellersJoinSql = "inner join {$tableSalesFlatOrderItem} as sales_item
					on sales_item.product_id = e.entity_id and (sales_item.store_id = {$storeId} or sales_item.store_id = 0)";
				$attributesJoin[] = $topSellersJoinSql;
				$attributesJoinTemp[] = $topSellersJoinSql;
				$attributesToSelect[] = 'sum(sales_item.qty_ordered) as product_sale_count';
				$orders[] = "product_sale_count desc";
				$useGroupBy = true;
				break;
			case self::COLLECTION_RULE_ON_SALE:
				$priceCondition = " and (price_index.price - price_index.final_price > 0 or (price_index.tier_price > 0 and price_index.price > price_index.tier_price))";
				break;
			case self::COLLECTION_RULE_SELECTED_CATEGORIES:
				$categoryIds = $this->getSelectedCategoriesIds();
				if (!empty($categoryIds)) {
					$categoryFilter = array();
					foreach ($categoryIds as $categoryId) {
						$categoryFilter[] = '(cat_index.category_id=' . (int)$categoryId . ')';
					}
					$categoryFilter = '(' . implode(' or ', $categoryFilter) . ')';
				}
				if ($orderByOrder) {
					$orders[] = $orderByOrder;
				}
				if ($orderByAttributeToSelect) {
					$attributesToSelect[] = $orderByAttributeToSelect;
				}
				if ($orderByAttributeJoin) {
					$attributesJoin[] = $orderByAttributeJoin;
				}
				$useGroupBy = true;
				break;
            case self::COLLECTION_RULE_NEW_FROM_TO:
                $newsFromToCondition = ' and ( (news_from_date is not null and  news_to_date is null and NOW() >= news_from_date) or
                    (news_from_date is null and  news_to_date is not null and NOW() <= news_to_date) or
                    (news_from_date is not null and news_to_date is not null and NOW() between news_from_date and news_to_date) )';
                break;
		}

		$order = '';
		if (!empty($orders)) {
			$order = 'order by ' . implode(',', $orders);
		}

		if ($addReviewJoin) {
			//first query with inner join and second if needed without rating joins
			$attributesJoinAfterTopRated = $attributesJoin;
			$attributesSelectAfterTopRated = $attributesToSelect;
			if ($this->getCollectionRule() == self::COLLECTION_RULE_TOP_RATED) {
				$attributesJoin[] = $this->_prepareReviewJoinSql('inner', $tableReviewEntity, $tableReview, $tableReviewEntitySummary, $storeId);
			} else {
				$attributesJoin[] = $reviewJoinSql;
			}
			$attributesToSelect[] = $ratingSummarySelectSql;
			$useGroupBy = true;
		}

		$attributesToSelect = implode(', ', $attributesToSelect);
		$attributesJoin = implode(' ', $attributesJoin);
		$limit = $this->getMaximumProducts() ? 'limit ' . (int)$this->getMaximumProducts() : '';
		if (!empty($whereConditions)) {
			$whereCondition = "where " . implode(' and ', $whereConditions);
		}
        $configManageStock = (bool)Mage::getStoreConfig('cataloginventory/item_options/manage_stock');
        $manageStockCondition = $configManageStock ? 'cat_inventory.use_config_manage_stock=0 and cat_inventory.manage_stock=0'
                                                    : 'cat_inventory.use_config_manage_stock=1 or (cat_inventory.use_config_manage_stock=0 and cat_inventory.manage_stock=0)';

		$groupByStr = $useGroupBy ? ' group by e.entity_id ' : '';
		if ($this->getDataHelper()->isModuleEnabled('Itoris_ProductPriceVisibility')) {
			$attributesJoinTemp = implode(' ', $attributesJoinTemp);
			$attributesToSelectTemp = implode(', ', $attributesToSelectTemp);
			$sql = "select e.entity_id, {$attributesToSelectTemp} from {$tableCatalogProductEntity} as e
				" . ($this->getDisplayOut() && $this->getCollectionRule() != self::COLLECTION_RULE_ON_SALE ? 'left' : 'inner') . " join {$tableCatalogProductIndexPrice} as price_index
					on price_index.entity_id = e.entity_id and price_index.website_id = {$websiteId} and price_index.customer_group_id = {$customerGroupId} {$priceCondition}"
					. ($this->getDisplayOut() ? '' : " inner join {$tableCataloginventoryStockItem} as cat_inventory on cat_inventory.product_id=e.entity_id and (cat_inventory.is_in_stock = 1 or ({$manageStockCondition}))")
					. " inner join {$tableCatalogCategoryProductIndex} as cat_index
					on cat_index.product_id=e.entity_id and cat_index.store_id={$storeId} and cat_index.visibility {$visibilityValues} AND {$categoryFilter}
				{$attributesJoinTemp}
				{$whereCondition}
				{$groupByStr}
				having {$visibilityCondition} and status=1 {$mostReviewCondition} {$newsFromToCondition}
			";

			$ids = $connection->fetchAll($sql);
			$hideIds = Mage::helper('itoris_productpricevisibility/product')->getHideVisibilityConfig($ids);
			if (!empty($hideIds)) {
				$whereConditions[] = 'e.entity_id not in (' . implode(',', $hideIds) . ')';
			}

		}

		if (!empty($whereConditions)) {
			$whereCondition = "where " . implode(' and ', $whereConditions);
		}


		$sql = "select e.entity_id, e.created_at, price_index.tax_class_id, if(price_index.final_price > 0, price_index.final_price, price_index.min_price) as final_price,
			if(e.type_id='grouped',price_index.min_price,price_index.max_price) as maximum_price, {$attributesToSelect} from {$tableCatalogProductEntity} as e
			" . ($this->getDisplayOut() && $this->getCollectionRule() != self::COLLECTION_RULE_ON_SALE ? 'left' : 'inner') . " join {$tableCatalogProductIndexPrice} as price_index
				on price_index.entity_id = e.entity_id and price_index.website_id = {$websiteId} and price_index.customer_group_id = {$customerGroupId} {$priceCondition}"
			 . ($this->getDisplayOut() ? '' : " inner join {$tableCataloginventoryStockItem} as cat_inventory on cat_inventory.product_id=e.entity_id and (cat_inventory.is_in_stock = 1 or ({$manageStockCondition}))")
			. " inner join {$tableCatalogCategoryProductIndex} as cat_index
				on cat_index.product_id=e.entity_id and cat_index.store_id={$storeId} and cat_index.visibility {$visibilityValues} AND {$categoryFilter}
			{$attributesJoin}
			{$whereCondition}
			{$groupByStr}
			having {$visibilityCondition} and status=1 {$mostReviewCondition} {$newsFromToCondition}
			{$order}
			{$limit}
		";

		// get ids before sorting
		$items = $connection->fetchAll($sql);
		$ids = array();
		foreach ($items as $item) {
			$ids[] = $item['entity_id'];
		}

		if (empty($ids)) {
			return array();
		}

		if ($this->getMaximumProducts() > count($ids) && $this->getCollectionRule() == self::COLLECTION_RULE_TOP_RATED) {
			//select products without rating
			$attributesJoinAfterTopRated = implode(' ', $attributesJoinAfterTopRated);
			$attributesSelectAfterTopRated = implode(',', $attributesSelectAfterTopRated);
			$newLimit = (int)$this->getMaximumProducts() - count($ids);
			$whereConditions[] = 'e.entity_id not in (' . implode(',', $ids) . ')';
			$whereCondition = 'where ' . implode(' and ', $whereConditions);
			$sql = "select e.entity_id, e.created_at, price_index.tax_class_id, if(price_index.final_price > 0, price_index.final_price, price_index.min_price) as final_price,
				if(e.type_id='grouped',price_index.min_price,price_index.max_price) as maximum_price, {$attributesSelectAfterTopRated} from {$tableCatalogProductEntity} as e
				" . ($this->getDisplayOut() && $this->getCollectionRule() != self::COLLECTION_RULE_ON_SALE ? 'left' : 'inner') . " join {$tableCatalogProductIndexPrice} as price_index
					on price_index.entity_id = e.entity_id and price_index.website_id = {$websiteId} and price_index.customer_group_id = {$customerGroupId} {$priceCondition}"
					. ($this->getDisplayOut() ? '' : " inner join {$tableCataloginventoryStockItem} as cat_inventory on cat_inventory.product_id=e.entity_id and (cat_inventory.is_in_stock = 1 or ({$manageStockCondition}))")
					. " inner join {$tableCatalogCategoryProductIndex} as cat_index
					on cat_index.product_id=e.entity_id and cat_index.store_id={$storeId} and cat_index.visibility {$visibilityValues} AND {$categoryFilter}
				{$attributesJoinAfterTopRated}
				{$whereCondition}
				{$groupByStr}
				having {$visibilityCondition} and status=1 {$newsFromToCondition}
				order by e.entity_id desc
				limit {$newLimit}
			";

			// get ids before sorting
			$items = $connection->fetchAll($sql);
			$ids = array();
			foreach ($items as $item) {
				$ids[] = $item['entity_id'];
			}

		}

		$orders = array();
		$attributesToSelect = $mainAttributesToSelect;
		$attributesJoin = $mainAttributesJoin;
		$attributesJoin[] = $reviewJoinSql;
		$attributesToSelect[] = $ratingSummarySelectSql;

		if ($orderByOrder) {
			$orders[] = $orderByOrder;
		}
		if ($orderByAttributeToSelect) {
			$attributesToSelect[] = $orderByAttributeToSelect;
		}
		if ($orderByAttributeJoin) {
			$attributesJoin[] = $orderByAttributeJoin;
		}

		$order = '';
		if (!empty($orders)) {
			$order = 'order by ' . implode(',', $orders);
		}
		$attributesToSelect = implode(', ', $attributesToSelect);
		$attributesJoin = implode(' ', $attributesJoin);
		$whereCondition = 'where e.entity_id in (' . implode(',', $ids) . ')';
		$sql = "select e.*, if(price_index.price is not null, price_index.price, price_index.min_price) as price, price_index.tax_class_id, if(price_index.final_price > 0, price_index.final_price, price_index.min_price) as final_price,
		if(price_index.tier_price is not null, LEAST(price_index.min_price, price_index.tier_price), price_index.min_price) as minimal_price, if (price_index.entity_id is not null, 1, 0) as is_saleable_simple,
			price_index.min_price, price_index.max_price, if(e.type_id='grouped',price_index.min_price,price_index.max_price) as maximum_price, price_index.tier_price, {$attributesToSelect} from {$tableCatalogProductEntity} as e
			" . ($this->getDisplayOut() && $this->getCollectionRule() != self::COLLECTION_RULE_ON_SALE ? 'left' : 'inner') . " join {$tableCatalogProductIndexPrice} as price_index
				on price_index.entity_id = e.entity_id and price_index.website_id = {$websiteId} and price_index.customer_group_id = {$customerGroupId} {$priceCondition}
			inner join {$tableCatalogCategoryProductIndex} as cat_index
				on cat_index.product_id=e.entity_id and cat_index.store_id={$storeId} and cat_index.visibility {$visibilityValues} AND {$categoryFilter}
			{$attributesJoin}
			{$whereCondition}
			group by e.entity_id
			having {$visibilityCondition} and status=1
			{$order}
		";

		return $connection->fetchAll($sql);
	}

	private function _prepareReviewJoinSql($joinType, $tableReviewEntity, $tableReview, $tableReviewEntitySummary, $storeId) {
		return "{$joinType} join {$tableReviewEntity} as rating_entity on rating_entity.entity_code = 'product'
			{$joinType} join {$tableReview} as review_table
				on review_table.entity_pk_value = e.entity_id and review_table.entity_id = rating_entity.entity_id
			left join {$tableReviewEntitySummary} as t_rating_summary_default
			 on  t_rating_summary_default.entity_pk_value = e.entity_id and t_rating_summary_default.entity_type = rating_entity.entity_id
			 	and t_rating_summary_default.store_id = 0
			left join {$tableReviewEntitySummary} as t_rating_summary
			 on t_rating_summary.entity_pk_value = e.entity_id and t_rating_summary.entity_type = rating_entity.entity_id
			  	and t_rating_summary.store_id = {$storeId}
		";
	}

	protected function getSelectedCategoriesIds() {
		$selectedCategories = array();
		$categoriesId = str_replace('{', '', $this->getCategories());
		$categoriesId = explode('}', $categoriesId);

		$childrenCategoryIds = array();
		foreach ($categoriesId as $value) {
			if (!empty($value)) {
				$childrenCategoryIds = array_merge($childrenCategoryIds, Mage::getModel('catalog/category')->load($value)->getAllChildren(true));
			}
		}
		$selectedCategories = array_merge($categoriesId, $childrenCategoryIds);
		$selectedCategories = array_unique($selectedCategories);

		foreach ($selectedCategories as $key => $value) {
			if (empty($value)) {
				unset($selectedCategories[$key]);
			}
		}
		return $selectedCategories;
	}

	public function getSortedProductArray() {
		$preparedProducts = array();
		$eventObserver = new Varien_Event_Observer(array('event' => new Varien_Object()));
		/** @var $inventoryObserver Mage_CatalogInventory_Model_Observer */
		$inventoryObserver = Mage::getSingleton('cataloginventory/observer');

		$session = Mage::getSingleton('core/session');
		$isEnabledProductPreviews = $this->getDataHelper()->isModuleEnabled('Itoris_ProductPreviews');
		if ($isEnabledProductPreviews) {
			Mage::getSingleton('itoris_productpreviews/observer')->clearSession();
		}

		foreach ($this->getProductsRawSql() as $productData) {
			$product = Mage::getModel('catalog/product')->setData($productData);
			$eventObserver->getEvent()->setProduct($product);
			$inventoryObserver->addInventoryData($eventObserver);
			Mage::getModel('review/review')->getEntitySummary($product, Mage::app()->getStore()->getId());
            if ($product->getFinalPrice() && $product->getFinalPrice() != $product->getPrice()) {
                $product->setData('special_price', $product->getFinalPrice());
            }
            if ($isEnabledProductPreviews) {
				$urlPath = $product->getUrlPath();
				if (!$session->getData('itoris_previews_' . $urlPath)) {
					$session->setData('itoris_previews_' . $urlPath, Mage::helper('itoris_productpreviews')->getProductConfigJson($product));
				}
			}
			$preparedProducts[] = $product;
		}

		return $preparedProducts;
	}



	/**
	 * @return Itoris_ProductSlider_Helper_Data
	 */

	public function getDataHelper() {
		return Mage::helper('itoris_productslider/data');
	}

}
?>