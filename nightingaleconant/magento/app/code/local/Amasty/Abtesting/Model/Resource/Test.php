<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Model_Resource_Test extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('amabtesting/test', 'id');
    }

    protected function _beforeDelete(Mage_Core_Model_Abstract $object)
    {
        $entityIds = Mage::getResourceModel('amabtesting/test_variation_collection')
            ->addFieldToFilter('test_id', $object->getId())
            ->getColumnValues('metaproduct_id')
        ;

        if (!empty($entityIds)) {
            $this->_getWriteAdapter()->delete(
                $this->getTable('catalog/product'),
                array(
                    'entity_id IN (?)' => $entityIds,
                    'type_id=?'        => Amasty_Abtesting_Model_Test_Variation::META_PRODUCT_TYPE
                )
            );
        }

        return parent::_beforeDelete($object);
    }

    public function getInvolvedProducts($test)
    {
        $products = Mage::getResourceModel('catalog/product_collection');

        $products->getSelect()
            ->join(
                array('tp' => $this->getTable('amabtesting/test_product')),
                'tp.product_id = e.entity_id',
                array()
            )
            ->where('tp.test_id=?', $test->getId())
        ;

        return $products;
    }

    public function getAttributes($test)
    {
        $attributes = Mage::getResourceModel('catalog/product_attribute_collection');

        $attributes->getSelect()->join(
            array('ta' => $this->getTable('amabtesting/test_attribute')),
            'ta.attribute_id = main_table.attribute_id',
            array('title')
        )
            ->where('ta.test_id=?', $test->getId())
            ->order('ta.id ASC')
        ;

        return $attributes;
    }

    public function getTotalVisitors($test)
    {
        $connection = $this->getReadConnection();

        $select = $connection->select()
            ->from(
                $this->getTable(
                    'amabtesting/test_variation'
                ),
                'SUM(visits)'
            )
            ->where('test_id=?', $test->getId())
        ;

        $totalVisitors = $connection->fetchOne($select);

        return $totalVisitors;
    }
}
