<?php

require 'app/Mage.php';
Mage::app('admin')->setUseSessionInUrl(false);                                                                                                                 

$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer','nccustom_allow_mail');
$oAttribute->setData('used_in_forms', array('customer_account_edit','customer_account_create','adminhtml_customer','checkout_register'));
$oAttribute->save();

$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer','nccustom_allow_phone');
$oAttribute->setData('used_in_forms', array('customer_account_edit','customer_account_create','adminhtml_customer','checkout_register'));
$oAttribute->save();

$oAttribute = Mage::getSingleton('eav/config')->getAttribute('customer','nccustom_allow_email');
$oAttribute->setData('used_in_forms', array('customer_account_edit','customer_account_create','adminhtml_customer','checkout_register'));
$oAttribute->save();

?>