<?php
/**
 * Toogas Lda.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA (End-User License Agreement)
 * that is bundled with this package in the file licence_toogas_community.txt.
 * It is also available at this URL:
 * http://www.toogas.com/licences/toogas_community.txt
 *
 * @category   Toogas
 * @package    Toogas_Featuredpopup
 * @copyright  Copyright (c) 2013 Toogas Lda. (http://www.toogas.com)
 * @license    http://www.toogas.com/licences/toogas_community.txt
 */
 
$installer = $this;
$installer->startSetup();


$installer->getConnection()->changeColumn(
    $installer->getTable('toogas_featuredpopup'),
    'popup_name',
    'popup_name',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length'    => 256
    )
);

$installer->getConnection()->changeColumn(
    $installer->getTable('toogas_featuredpopup'),
    'priority',
    'priority',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER
    )
);

$installer->getConnection()->changeColumn(
    $installer->getTable('toogas_featuredpopup'),
    'num_aparece',
    'num_aparece',
    array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER
    )
);


$installer->endSetup(); 