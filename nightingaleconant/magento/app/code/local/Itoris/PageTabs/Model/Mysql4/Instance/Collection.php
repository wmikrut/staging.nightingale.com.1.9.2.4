<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGETABS
 * @copyright  Copyright (c) 2013 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */


class Itoris_PageTabs_Model_Mysql4_Instance_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

	protected $tabTable = 'itoris_pagetabs_tab';

	protected function _construct() {
		parent::_construct();
		$this->_init('itoris_pagetabs/instance');
		$this->tabTable = $this->getTable('tab');
		$this->_itemObjectClass = 'Itoris_PageTabs_Model_Instance';
	}

	public function addTabsCount() {
		$this->getSelect()
			->joinLeft(
				array('tabs' => $this->tabTable),
				'tabs.instance = main_table.instance_id',
				array('tabs_count' => 'COUNT(tabs.tab_id)')
			)
			->group('main_table.instance_id');
		return $this;
	}
}

?>