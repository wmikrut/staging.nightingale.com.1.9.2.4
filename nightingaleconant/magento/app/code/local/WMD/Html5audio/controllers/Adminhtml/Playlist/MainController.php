<?php
/**
 * WMD_Html5audio_Adminhtml_Playlist_MainController
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2013 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Adminhtml_Playlist_MainController extends Mage_Adminhtml_Controller_Action
{   
    
    /**
     * Initialize requested playlist and put it into registry.
     *
     * @return WMD_Html5audio_Model_Playlist
     */
    protected function _initPlaylist()
    {
        
        $playlistId = (int) $this->getRequest()->getParam('id',false);
        $playlist = Mage::getModel('html5audio/playlist');

        if ($playlistId) {
            $playlist->load($playlistId);
        }

        if ($activeTabId = (string) $this->getRequest()->getParam('active_tab_id')) {
            Mage::getSingleton('admin/session')->setActiveTabId($activeTabId);
        }

        Mage::register('playlist', $playlist);
        return $playlist;
    }
    
    /**
     * load layout, activate menu and add breadcrumbs
     *
     * @return object
     */                         
    protected function _initAction()
    {
        $this->loadLayout()
  		      ->_setActiveMenu('catalog/html5audio')
            ->_addBreadcrumb(
                Mage::helper('html5audio')->__('Playlists'), 
                Mage::helper('html5audio')->__('Playlists')
            );
        return $this;
    }
    	
	  /**
     * Show playlists
     *
     * @return void
     */	
    public function indexAction()
    {
  		  $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('html5audio/adminhtml_playlist_main'))
            ->renderLayout();	
    } 
    
    /**
     * Show playlist form
     *
     * @return void
     */	
    public function newAction()
    {
    		$this->editAction();
		}
    /**
     * Show playlist form
     *
     * @return void
     */	
    public function editAction()
    {
		    
        $playlist = Mage::getModel('html5audio/playlist');
  	    if ($id = $this->getRequest()->getParam('id')) 
        {
            $playlist->load($id);
        } 
        else
        {
            $playlist->isObjectNew(true);
        }
  	    Mage::register('playlist', $playlist);
        $this->loadLayout()
            ->_setActiveMenu('html5audio/playlist')
            ->_addBreadcrumb(
                Mage::helper('html5audio')->__('New Playlist'), 
                Mage::helper('html5audio')->__('New Playlist')
            )
            ->_addContent($this->getLayout()->createBlock('html5audio/adminhtml_playlist_edit'))
    				->_addLeft($this->getLayout()->createBlock('html5audio/adminhtml_playlist_edit_tabs'));
    		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
  	    $this->renderLayout();
    }	
      
    /**
     * Save playlist form
     *
     * @return void
     */	
    public function saveAction()
    {  
        $id = $this->getRequest()->getParam('playlist_id', false); 
        $redirectBack = $this->getRequest()->getParam('back', false);
        
        if ($postData = $this->getRequest()->getPost()) {
        
            $playlist = Mage::getModel('html5audio/playlist'); 
            
            if ($id)
            {
                $playlist->load($id); 
            } 
            
            try {   
                             
                $playlist->setData($postData);                          
             
                if (isset($postData['playlist_samples'])) {
                    $samples = array();
                    parse_str($postData['playlist_samples'], $samples);                  
                    $playlist->setPostedSamples($samples);
                }     
                
                $playlist->save();
                
                $id = $playlist->getPlaylistId(); 
    
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('html5audio')->__('Playlist was successfully saved.')
                );
    
                $this->_redirect('*/*/');
                
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        
        if ($redirectBack) {
            $this->_redirect('*/*/edit', array(
                'id'    => $id,
                '_current'=>true
            ));
        } else {
            $this->indexAction();
        }   
        
    }	

    /**
     * Delete sample 
     *
     * @return void
     */
    public function deleteAction()
    {
        
        $id = $this->getRequest()->getParam('id');
        try
        {
    				Mage::getModel('html5audio/playlist')->setId($id)->delete();					
            
            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('html5audio')->__('Entry was deleted successfully.')
            );
        } 
  			catch (Exception $e)
  			{
//                   Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
		
        $this->indexAction();	
    		
    }                   
    
    /**
     * Mass delete samples
     *
     * @return void
     */
    public function massDeleteAction()
    {
        
        $ids = $this->getRequest()->getParam('id');
        if(!is_array($ids)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('html5audio')->__('Please select at least one item.')
          	);
        }
    		else
    		{
            try 
      			{
                foreach ($ids as $id)
        				{
        					   Mage::getModel('html5audio/playlist')->setId($id)->delete();					
                }
				
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('html5audio')->__('Entries were deleted successfully.')
                );
            } 
      			catch (Exception $e)
      			{
//                 Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
		
        $this->indexAction();	
    		
    }           
    
    
    /**
     * Search grid-load for the samples ajax request
     * 
     * @return void          
     */
    public function gridAction()
    {  
        if (!$playlist = $this->_initPlaylist()) {
            return;
        }
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('html5audio/adminhtml_playlist_edit_tab_sample', 'playlist.sample.grid')
                ->toHtml()
        );
    } 
    
          
    /**
     * Save playlist file
     *
     * @return void
     */	
    public function uploadAction()
    {  
        if ($postData = $this->getRequest()->getPost()) {        
            $result = array();
            try {
                $model = Mage::getModel('html5audio/sample');                   
//                 $model = $model->quickSaveSample($postData);                  
                $model = $model->saveSample($postData);
                if ($sampleId = $model->getSampleId())
                { 
                    $model->saveToPlaylist($sampleId, $postData['playlist_id']); 
                } 
                else
                {
// Mage::log(__METHOD__);                
                }  
            } catch (Exception $e) {
                $result['message'] = $e->getMessage();
                $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));    
            }
            $result['success'] = true;
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }	         
         
}

