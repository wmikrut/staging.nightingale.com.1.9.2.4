<?php
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */



class Itoris_ProductSlider_Block_Admin_Widget_AllowSliding extends Mage_Core_Block_Template {

    public function prepareElementHtml(Varien_Data_Form_Element_Abstract $element) {
        $element->setData('onclick', 'if (this.checked) {this.up(\'tr\').next().show(); if (this.up(\'tr\').next().select(\'select\')[0].value == '. Itoris_ProductSlider_Block_ProductSlider::AUTO_SLIDING_ENABLED .') {
        this.up(\'tr\').next().next().show();
        }} else { this.up(\'tr\').next().hide(); this.up(\'tr\').next().next().hide();}');
        $element->setData('checked', $element->getValue());
        $element->setData('value', '1');
        return $element;
    }
}
?>