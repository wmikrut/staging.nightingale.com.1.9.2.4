<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Model_Image extends Mage_Core_Model_Abstract
{
    protected $_slider = null;

    public function _construct()
    {
        $this->_init('awislider/image');
    }

    public function isImageRemote()
    {
        return ($this->getData('type') == AW_Islider_Model_Source_Images_Type::REMOTEURL);
    }

    public function isImageUploaded()
    {
        return !$this->isImageRemote();
    }

    /**
     * @return AW_Islider_Model_Slider|null
     */
    public function getSlider()
    {
        if (null === $this->_slider && ($pid = $this->getData('pid'))) {
            $slider = Mage::getModel('awislider/slider')->load($pid);
            if ($slider->getId()) {
                $this->_slider = $slider;
            }
        }
        return $this->_slider;
    }

    protected function _removeImage()
    {
        if ($this->isImageUploaded() && $this->getLocation()) {
            $this->_removeImages($this->getLocation());
        }
        return $this;
    }

    protected function _afterLoad()
    {
        if ($this->getActiveFrom() == AW_Islider_Helper_Data::DEFAULT_DATE_VALUE) {
            $this->setActiveFrom(null);
        }
        if ($this->getActiveTo() == AW_Islider_Helper_Data::DEFAULT_DATE_VALUE) {
            $this->setActiveTo(null);
        }
        return parent::_afterLoad();
    }

    protected function _beforeSave()
    {
        if (!$this->getActiveFrom()) {
            $this->setActiveFrom(AW_Islider_Helper_Data::DEFAULT_DATE_VALUE);
        }
        if (!$this->getActiveTo()) {
            $this->setActiveTo(AW_Islider_Helper_Data::DEFAULT_DATE_VALUE);
        }
        return parent::_beforeSave();
    }

    protected function _afterSave()
    {
        if (null !== $this->getOrigData('location') && $this->getOrigData('location') != $this->getLocation()) {
            $this->_removeImages($this->getOrigData('location'));
        }
        return parent::_afterSave();
    }

    protected function _removeImages($filename)
    {
        @unlink(AW_Islider_Helper_Image::getImagePath() . $filename);
        @array_map('unlink', glob(AW_Islider_Helper_Image::getImagePath() . "*" . $filename));
        return $this;
    }

    protected function _beforeDelete()
    {
        $this->_removeImage();
        return parent::_beforeDelete();
    }
}