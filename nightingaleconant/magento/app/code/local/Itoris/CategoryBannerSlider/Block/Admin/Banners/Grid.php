<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_CATEGORYBANNERSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_CategoryBannerSlider_Block_Admin_Banners_Grid extends Mage_Adminhtml_Block_Widget_Grid {

	public function __construct() {
			parent::__construct();
	}

	protected function _prepareCollection() {
		/** @var $collection Itoris_CategoryBannerSlider_Model_Mysql4_Sets_Collection */
		$collection = Mage::getModel('itoris_categorybannerslider/sets')->getCollection();

		$this->setCollection($collection);
		$this->setDefaultSort('category_count');
  		$this->setDefaultDir('desc');
		return parent::_prepareCollection();
	}

	protected function _prepareColumns() {
		$this->addColumn('category', array(
			'header' => $this->__('Website: Store View: Category'),
			'width' => '310px',
			'index' => 'category_count',
			'filter_condition_callback' => array($this,'filterStoreCategories'),
			'filter' => 'itoris_categorybannerslider/admin_grid_column_filter_storeCategory',
			'renderer' => 'itoris_categorybannerslider/admin_grid_column_renderer_storeCategory',
			'sortable'  => false,
		));

		$this->addColumn('images', array(
			'header' => $this->__('Images'),
			'width' => '525px',
			'index' => 'banners_count',
			'filter'    => false,
			'renderer' => 'itoris_categorybannerslider/admin_grid_column_renderer_images',
			'sortable'  => false,
		));

		$this->addColumn('status',
			array(
				'header'=> $this->__('Status'),
				'width' => '70px',
				'type'  => 'options',
				'index' => 'status',
				'options' => array(Itoris_CategoryBannerSlider_Model_Sets::STATUS_ENABLED => $this->__('Enabled'),
								Itoris_CategoryBannerSlider_Model_Sets::STATUS_DISABLED => $this->__('Disabled'),
							)
				)
		);

		$this->addColumn('action',
			array(
				'header'    => $this->__('Action'),
				'width'     => '50px',
				'type'      => 'action',
				'getter'     => 'getId',
				'actions'   => array(
					array(
						'caption' => $this->__('Edit'),
						'url'     => array(
							'base'=>'*/*/edit',
						),
						'field'   => 'id'
					)
				),
				'filter'    => false,
				'sortable'  => false,
		));

		return parent::_prepareColumns();
	}

	protected function _prepareMassaction() {
			$this->setMassactionIdField('main_table.set_id');

			$this->getMassactionBlock()->setFormFieldName('set_id');

			$this->getMassactionBlock()->addItem('delete', array(
				 'label'=> $this->__('Delete'),
				 'url'  => $this->getUrl('*/*/massDelete'),
				 'confirm' => $this->__('Are you sure you want to delete selected banners?')
			));

			$this->getMassactionBlock()->addItem('status', array(
				 'label'=> $this->__('Change status'),
				 'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
				 'additional' => array(
						'visibility' => array(
							 'name' => 'status',
							 'type' => 'select',
							 'class' => 'required-entry',
							 'label' => $this->__('Status'),
							 'values' => array(Itoris_CategoryBannerSlider_Model_Sets::STATUS_ENABLED => $this->__('Enabled'),
							 						Itoris_CategoryBannerSlider_Model_Sets::STATUS_DISABLED => $this->__('Disabled')
							 			)
						 )
				 )
			));

			return $this;
	}

	/**
	 * Filter for category column
	 *
	 * @param $collection
	 * @param $column
	 */
	protected function filterStoreCategories($collection, $column) {
		$cond = $column->getFilter()->getCondition();
		$this->getCollection()->addFieldToFilter('store_id' , $cond['store']);
		$this->getCollection()->addFieldToFilter('category_id' , $cond['category']);
	}

}
?>
 
