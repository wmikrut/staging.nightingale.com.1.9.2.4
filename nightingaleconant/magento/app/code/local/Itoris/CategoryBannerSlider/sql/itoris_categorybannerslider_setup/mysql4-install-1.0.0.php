<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_CATEGORYBANNERSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

$this->startSetup();

$enable = Itoris_CategoryBannerSlider_Model_Settings::ENABLE_YES;

$this->run("

	CREATE TABLE {$this->getTable('itoris_categorybannerslider_settings')} (
		`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		`scope` ENUM('default', 'website', 'store') NOT NULL ,
		`scope_id` INT UNSIGNED NOT NULL ,
		`key` VARCHAR( 255 ) NOT NULL ,
		`value` INT UNSIGNED NOT NULL ,
		UNIQUE(`scope`, `scope_id`, `key`)
	) ENGINE = InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

	INSERT INTO {$this->getTable('itoris_categorybannerslider_settings')} (`scope`, `scope_id`, `key`, `value`) VALUES
		('default', 0, 'enable', $enable);

	CREATE TABLE {$this->getTable('itoris_categorybannerslider_sets')}(
		`set_id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
		`status` BOOLEAN NOT NULL,
		`effect` INT UNSIGNED NOT NULL,
		`skin` INT UNSIGNED NOT NULL,
		`autorotation` BOOLEAN NOT NULL,
		`width` INT UNSIGNED NOT NULL,
		`height` INT UNSIGNED NOT NULL
	) ENGINE = InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

	CREATE TABLE {$this->getTable('itoris_categorybannerslider_banners')}(
		`banner_id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
		`set_id` INT UNSIGNED NOT NULL,
		`title` VARCHAR(255) NOT NULL,
		`alt` VARCHAR(255) NOT NULL,
		`delay` INT UNSIGNED NOT NULL,
		`link_url` VARCHAR(255) NOT NULL,
		`open_in` INT UNSIGNED NOT NULL,
		`file_name` VARCHAR(255) NOT NULL,
		`enabled` BOOLEAN NOT NULL,
		`order` INT UNSIGNED NOT NULL,
		KEY `FK_BANNERS_SET` (`set_id`),
		CONSTRAINT `FK_BANNERS_SET` FOREIGN KEY (`set_id`) REFERENCES {$this->getTable('itoris_categorybannerslider_sets')} (`set_id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE = InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

	CREATE TABLE {$this->getTable('itoris_categorybannerslider_categories')}(
		`set_id` INT UNSIGNED NOT NULL,
		`store_id` SMALLINT(5) UNSIGNED NOT NULL,
		`category_id` INT(10) UNSIGNED NOT NULL,
		UNIQUE(`set_id`,`store_id`, `category_id`),
		KEY `FK_CATEGORIES_SET` (`set_id`),
		KEY `FK_CATEGORIES_STORE` (`store_id`),
		KEY `FK_CATEGORIES_CATEGORY_ENTITY` (`category_id`),
		CONSTRAINT `FK_CATEGORIES_SET` FOREIGN KEY (`set_id`) REFERENCES {$this->getTable('itoris_categorybannerslider_sets')} (`set_id`) ON DELETE CASCADE ON UPDATE CASCADE,
		CONSTRAINT `FK_CATEGORIES_STORE` FOREIGN KEY (`store_id`) REFERENCES {$this->getTable('core_store')} (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE,
		CONSTRAINT `FK_CATEGORIES_CATEGORY_ENTITY` FOREIGN KEY (`category_id`) REFERENCES {$this->getTable('catalog_category_entity')} (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
	) ENGINE = InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

");

$this->endSetup();
?>