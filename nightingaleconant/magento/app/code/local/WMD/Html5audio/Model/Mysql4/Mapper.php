<?php
/**
 * WMD_Html5audio_Model_Mysql4_Mapper
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Model_Mysql4_Mapper extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Construct
     *
     * @return void
     */	
    protected function _construct()
    {
        $this->_init('html5audio/mapper', 'mapper_id');
    }	
    
    public function loadByLinkId($model, $id)
    {      
        $id = (int)$id;
        $select = $this->_getReadAdapter()->select()
            ->from($this->getMainTable(), 'mapper_id')
            ->where("link_id='{$id}'");
        if ($mapperId = $this->_getReadAdapter()->fetchOne($select))
        {
            $model->load($mapperId);
        }    
        return $model;      
    }    
    
}
