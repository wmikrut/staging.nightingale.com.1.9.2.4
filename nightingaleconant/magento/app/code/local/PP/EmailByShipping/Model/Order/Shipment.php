<?php
/**
 * Pulsar Plugins
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.pulsarplugins.com/pulsarplugins-license.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento community edition
 * Pulsar Plugins does not guarantee correct work of this extension
 * on any other Magento edition except Magento community edition.
 * Pulsar Plugins does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   PP
 * @package    PP_EmailByShipping
 * @version    1.1
 * @copyright  Copyright (c) 2010-2014 Pulsar Plugins (http://www.pulsarplugins.com/)
 * @license    http://www.pulsarplugins.com/pulsarplugins-license.txt
 *
*/
class PP_EmailByShipping_Model_Order_Shipment extends Mage_Sales_Model_Order_Shipment {
	/**
     * Send email with shipment data
     *
     * @param boolean $notifyCustomer
     * @param string $comment
     * @return Mage_Sales_Model_Order_Shipment
     */
    public function sendEmail($notifyCustomer = true, $comment = '')
    {
        $order = $this->getOrder();
        $storeId = $order->getStore()->getId();

        if (!Mage::helper('sales')->canSendNewShipmentEmail($storeId)) {
            return $this;
        }
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_EMAIL_COPY_METHOD, $storeId);
        // Check if at least one recepient is found
        if (!$notifyCustomer && !$copyTo) {
            return $this;
        }

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($order->getPayment())
                ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        // Retrieve corresponding email template id and customer name
		// shipping detect
		$method 		= $order->getShippingMethod(true);
		
		$carrierCode	= $method->getCarrierCode();
		$carrierMethod	= $method->getMethod();
		$shippingCode	= $carrierCode.'_'.$carrierMethod;
		//echo $shippingCode; exit;
		
		//$carrierModel	= $this->getShippingCarrier();
		//$shipData		= $carrierModel->getData();
		
		// Retrieve corresponding email template id and customer name
		$pwHelper		= Mage::helper('ordermailtable');
        if ($order->getCustomerIsGuest()) {
			$templateId		= $pwHelper->getShippingTemplateIdFromShippingCode($shippingCode, 'Guest');
			if (!is_numeric($templateId)) {				
				$templateId 	= Mage::getStoreConfig(self::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId);
				$customerName 	= $order->getBillingAddress()->getName();
			}
        } else {
			$templateId		= $pwHelper->getShippingTemplateIdFromShippingCode($shippingCode, 'Registered');
			if (!is_numeric($templateId)) {
				$templateId 	= Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE, $storeId);
				$customerName 	= $order->getCustomerName();
			}
        }
		/*
        if ($order->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $order->getBillingAddress()->getName();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE, $storeId);
            $customerName = $order->getCustomerName();
        }
		*/

        $mailer = Mage::getModel('core/email_template_mailer');
        if ($notifyCustomer) {
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($order->getCustomerEmail(), $customerName);
            if ($copyTo && $copyMethod == 'bcc') {
                // Add bcc to customer email
                foreach ($copyTo as $email) {
                    $emailInfo->addBcc($email);
                }
            }
            $mailer->addEmailInfo($emailInfo);
        }

        // Email copies are sent as separated emails if their copy method is 'copy' or a customer should not be notified
        if ($copyTo && ($copyMethod == 'copy' || !$notifyCustomer)) {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
                'order'        => $order,
                'shipment'     => $this,
                'comment'      => $comment,
                'billing'      => $order->getBillingAddress(),
                'payment_html' => $paymentBlockHtml
            )
        );
        $mailer->send();

        return $this;
    }
}
?>