<?php

class NCCUSTOM_Ncreporting_NcreportingController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
	//How to pull paremeters from the URL for the controller
	//echo $this->getRequest()->getParam('bdate');
	//echo $this->getRequest()->getParam('edate');
	//echo $this->getRequest()->getParam('coupon');
	
	$this->loadLayout()
    	    ->_addContent(
            $this->getLayout()
        	->createBlock('nccustom_adminblock/adminhtml_adminblock')
        	->setTemplate('nccustom/adminblock.phtml'))
        	->renderLayout();
    }
    protected function _isAllowed(){
	return Mage::getSingleton('admin/session')->isAllowed('nccustom/adminblock.phtml');
    }
}    
