<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_CATEGORYBANNERSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_CategoryBannerSlider_Block_Admin_Grid_Column_Renderer_Images extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

	public function render(Varien_Object $row) {
        $banners = $row->getBanners();
		$banners = explode('.-,', $banners);
		$html = '';
		$count = 1;
		foreach($banners as $banner){
				if($count > 6){
					$html .= '<br/><a href="'. $this->getUrl('*/*/edit/id/' . $row->getSetId()) .'" style="color:red">'. $this->__('more...') .'</a>';
					break;
				}
				$banner = explode('._,', $banner);
				if(!empty($banner[0])){
				$html .= '<img src="'. $this->getUrl('adminhtml/itoriscategorybannerslider_banners/getImage/id/'. $banner[0]) .'"
								class="thumb_banner" alt="'. $banner[2] .'" title="'. $banner[1] .'"/>';
				$count++;
				}
		}
        return $html;
    }

	/**
	 * Get data helper
	 *
	 * @return Itoris_CategoryBannerSlider_Helper_Data
	 */
	public function getDataHelper(){
		return Mage::helper('itoris_categorybannerslider');
	}
}

?>