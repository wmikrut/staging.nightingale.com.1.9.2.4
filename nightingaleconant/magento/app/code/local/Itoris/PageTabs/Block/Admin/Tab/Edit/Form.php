<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGETABS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */


class Itoris_PageTabs_Block_Admin_Tab_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

	protected function _prepareForm() {
		$form = new Varien_Data_Form(array(
			'id'        => 'edit_form',
			'action'    => $this->getData('action'),
			'method'    => 'post',
			'enctype'   => 'multipart/form-data'
		));

		/** @var $tab Itoris_PageTabs_Model_Tab */
		$tab = Mage::registry('current_tab');

		$fieldset = $form->addFieldset('details_fieldset', array(
			'legend' => $this->__('Slide Details'),
		));

		$fieldset->addField('id', 'hidden', array(
			'name'     => 'id',
			'value'    => $tab->getId(),
		));

		$fieldset->addField('title', 'text', array(
			'label'    => $this->__('Title'),
			'title'    => $this->__('Title'),
			'name'     => 'tab[title]',
			'required' => true,
			'value'    => $tab->getTitle(),
		));

		$fieldset->addField('tabs_instance', 'select', array(
			'label'    => $this->__('Tabs Instance'),
			'title'    => $this->__('Tabs Instance'),
			'name'     => 'tab[instance]',
			'value'    => $tab->getInstance(),
			'values'   => Mage::getModel('itoris_pagetabs/instance')->toOptionArray(),
		));

		$fieldset->addField('tab_order', 'text', array(
			'label'    => $this->__('Order'),
			'title'    => $this->__('Order'),
			'name'     => 'tab[tab_order]',
			'required' => true,
			'value'    => $tab->getTabOrder() ? $tab->getTabOrder() : $tab->calculateTabOrder(),
			'class'    => 'validate-number-range number-range-1-1000',
		));

		$fieldset->addField('enabled', 'checkbox', array(
			'label'   => $this->__('Enabled'),
			'title'   => $this->__('Enabled'),
			'name'    => 'tab[enabled]',
			'checked' => $tab->getEnabled(),
			'value'   => 1,
		));

		$fieldset->addField('start_publish_date', 'date', array(
			'label'        => $this->__('Start publish on'),
			'title'        => $this->__('Start publish on'),
			'name'         => 'tab[start_publish_date]',
			'value'        => $tab->getStartPublishDate(),
			'image'        => $this->getSkinUrl('images/grid-cal.gif'),
			'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
			'format'       => Varien_Date::DATE_INTERNAL_FORMAT,
			'class'        => 'validate-date',
		));

		$fieldset->addField('end_publish_date', 'date', array(
			'label'        => $this->__('End publish on'),
			'title'        => $this->__('End publish on'),
			'name'         => 'tab[end_publish_date]',
			'value'        => $tab->getEndPublishDate(),
			'image'        => $this->getSkinUrl('images/grid-cal.gif'),
			'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
			'format'       => Varien_Date::DATE_INTERNAL_FORMAT,
			'class'        => 'validate-date',
		));

		$fieldset->addField('store_views', 'multiselect', array(
			'label'    => $this->__('Store Views'),
			'title'    => $this->__('Store Views'),
			'name'     => 'tab[store_views]',
			'values'   => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
			'value'    => $tab->getStoreViews(),
		));

		$userGroups = Mage::getResourceModel('customer/group_collection')->toOptionArray();
		array_unshift($userGroups, array(
			'label' => $this->__('All Groups'),
			'value' => '',
		));

		$fieldset->addField('user_groups', 'multiselect', array(
			'label'  => $this->__('User Groups'),
			'title'  => $this->__('User Groups'),
			'name'   => 'tab[user_groups]',
			'values' => $userGroups,
			'value'  => $tab->getUserGroups(),
		));

		$fieldset->addField('content', 'editor', array(
			'label'    => $this->__('Content'),
			'title'    => $this->__('Content'),
			'name'     => 'tab[content]',
			'required' => true,
			'value'    => $tab->getContent(),
			'wysiwyg'  => true,
			'config'   => Mage::getSingleton('itoris_pagetabs/wysiwyg_config')->getConfig(),
		));

		$form->setUseContainer(true);
		$this->setForm($form);
		return parent::_prepareForm();
	}

	protected function _prepareLayout() {
		parent::_prepareLayout();
		if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
			$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
		}
	}
}
?>