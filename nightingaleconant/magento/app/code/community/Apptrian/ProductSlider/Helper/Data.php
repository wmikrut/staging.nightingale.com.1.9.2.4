<?php
/**
 * @category   Apptrian
 * @package    Apptrian_ProductSlider
 * @author     Apptrian
 * @copyright  Copyright (c) 2015 Apptrian (http://www.apptrian.com)
 * @license    http://www.apptrian.com/license    Proprietary Software License (EULA)
 */
class Apptrian_ProductSlider_Helper_Data extends Mage_Core_Helper_Abstract
{
	
	/**
	 * Info elements array is used to determine info elements sort order.
	 *
	 * @var array
	 */
	protected $infoElements = array(
			'name'        => '1',
			'description' => '2',
			'image'       => '3',
			'sku'         => '4',
			'price'       => '5',
			'rating'      => '6',
			'cart'        => '7',
			'compare'     => '8',
			'wishlist'    => '9'
	);
	
	/**
	 * Returns extension version.
	 *
	 * @return string
	 */
	public function getExtensionVersion()
	{
		return (string) Mage::getConfig()->getNode()->modules->Apptrian_ProductSlider->version;
	}
	
	/**
	 * Checks if Magento review module is enabled.
	 * 
	 * @return boolean
	 */
	public function isRatingEnabled()
	{
		
		$moduleEnabled  = Mage::helper('core')->isModuleEnabled('Mage_Review');
		$outputDisabled = Mage::getStoreConfigFlag('advanced/modules_disable_output/Mage_Review');
		
		if ($moduleEnabled == true && $outputDisabled == false) {
			return true;
		} else {
			return false;
		}
		
	}
	
	/**
	 * Returns ordered array of info elements.
	 * 
	 * @param string $pageType
	 * @return array
	 */
	public function getInfoSortOrder($pageType)
	{
		
		$elements = array();
		
		$ids = explode(',', trim(Mage::getStoreConfig('apptrian_productslider/' . $pageType . '_page/info_sort_order'), ','));
		
		$rEnabled = $this->isRatingEnabled();
		$wEnabled = Mage::helper('wishlist')->isAllow();
		
		foreach ($ids as $id) {
			// If review module is not enabled in magento skip rating
			if ($id == '6' && $rEnabled == false) {
				continue;
			}
			// If wishlist module is not enabled in magento skip wishlist link
			if ($id == '9' && $wEnabled == false) {
				continue;
			}
			$elements[] = array_search($id, $this->infoElements);
		}
		
		return $elements;
		
	}
	
	/**
	 * Based on config returns sort order of info elements in javascript array format.
	 *
	 * @return string
	 */
	public function getInfoSortOrderForJs($pageType)
	{
	
		$ids = $this->getInfoSortOrder($pageType);
	
		$sortOrder = '[';
		$elements  = '';
	
		foreach ($ids as $id) {
			$elements .= '"' . $id . '",';
		}
	
		$sortOrder .= rtrim($elements, ',') . ']';
	
		return $sortOrder;
	
	}
	
	/**
	 * This is used to filter string. Removes tags and trims it.
	 *
	 * @param string $s
	 */
	public function filterParam($s)
	{
	
		$filterChain = new Zend_Filter();
		$filterChain->addFilter(new Zend_Filter_StripTags())
					->addFilter(new Zend_Filter_StringTrim());
	
		return $filterChain->filter($s);
	
	}
	
	/**
	 * This method is used in AjaxController to
	 * filter and validate page param.
	 *
	 * @param string $s
	 * @return boolean|string
	 */
	public function filterAndValidatePageParam($s)
	{
		
		if ($s === null) {
			return false;
		}
		
		$filtered = $this->filterParam($s);
	
		if ($filtered != '' 
			&& Zend_Validate::is($filtered, 'Digits')
			&& Zend_Validate::is($filtered, 'GreaterThan', array(0))
		) {
				
			return $filtered;
				
		} else {
				
			return false;
				
		}
	
	}
	
	/**
	 * This method is used in AjaxController to
	 * filter and validate type param.
	 * 
	 * @param string $s
	 * @return boolean|string
	 */
	public function filterAndValidateTypeParam($s)
	{
	
		if ($s === null) {
			return false;
		}
		
		$filtered = $this->filterParam($s);
		
		if ($filtered != ''
			&& Zend_Validate::is($filtered, 'Regex', array('pattern' => '/(\W|^)(category|cms|home|product)(\W|$)/iu'))
		) {
			return $filtered;
		} else {
			return false;
		}
		
	
	}
	
	/**
	 * This method is used in AjaxController to
	 * filter and validate id param.
	 * 
	 * @param string $s
	 * @return boolean|unknown
	 */
	public function filterAndValidateIdParam($s)
	{
	
		if ($s === null) {
			return false;
		}
		
		$filtered = $this->filterParam($s);
	
		if ($filtered != '' 
			&& Zend_Validate::is($filtered, 'Digits')
			&& Zend_Validate::is($filtered, 'GreaterThan', array(-1))
		) {
				
			return $filtered;
				
		} else {
				
			return false;
				
		}
		
	}
	
	/**
	 * This method is used in AjaxController to
	 * filter and validate uenc param.
	 * 
	 * @param string $s
	 * @return boolean|string
	 */
	public function filterAndValidateUencParam($s)
	{
	
		if ($s === null) {
			return false;
		}
	
		$filtered = $this->filterParam($s);
	
		if ($filtered != ''
			&& Zend_Validate::is($filtered, 'Regex', array('pattern' => '/^[a-zA-Z0-9,]+$/iu'))
		) {
	
			return $filtered;
	
		} else {
	
			return false;
	
		}
	
	}
	
	/**
	 * Returns string length.
	 * 
	 * @param string $string
	 * @return integer
	 */
	public function stringLength($string)
	{
		
		if (function_exists('mb_strlen')) {
	
			$length = mb_strlen($string, 'UTF-8');
	
		} else {

			$length = strlen($string);
			
		}
	
		return (int) $length;
	
	}
	
	/**
	 * Limits text and adds &hellip; at the end if text is longer than $limit.
	 * 
	 * @param string $string
	 * @param integer $limit
	 * @return string
	 */
	public function stringLimit($string, $limit)
	{
	
		if (function_exists('mb_substr')) {
	
			$str = mb_substr($string, 0, $limit-1, 'UTF-8') . '&hellip;';
	
		} else {
	
			$str = substr($string, 0, $limit-1) . '&hellip;';
				
		}
	
		return $str;
	
	}
	
	/**
	 * Filters string. Removes tags, entities are decoded, removes new lines, multiple spaces.
	 * This way text is prepared for limitDescription($text, $limit) method.
	 *
	 * @param string $s
	 * @return string
	 */
	public function prepareString($s)
	{
		return trim(preg_replace("/ {2,}/", " ", str_replace(array("\r\n", "\r", "\n", "\t"), " " , html_entity_decode(strip_tags($s), ENT_QUOTES, 'UTF-8'))));
	}
	
	/**
	 * Based on provided string (text) limits it to limit and adds ellipsis.
	 *
	 * @param string $text
	 * @param int $limit
	 * @returns string
	 */
	public function limitDescription($text, $limit)
	{
		
		$trimmed = $this->prepareString($text);
	
		if ($limit > 0) {
	
			$result = $trimmed;
			
			$length = $this->stringLength($trimmed);
			
			if ($length > $limit) {
				
				$result = $this->stringLimit($trimmed, $limit);
				
			}
				
			return $result;
				
		} else {
				
			return $trimmed;
				
		}
	
	}
	
	/**
	 * Returns current category id.
	 * 
	 * @param string $pageType
	 * @return integer
	 */
	public function getCategoryId($pageType)
	{
		if ($pageType == 'category') {
			$id = (int) Mage::registry('current_category')->getId();
		} else {
			$id = 0;
		}
		
		return $id;
	}
	
	/**
	 * Returns product total count.
	 * 
	 * @param string $pageType
	 * @return integer
	 */
	public function getProductCount($pageType)
	{
		
		$showAll = Mage::getStoreConfigFlag('apptrian_productslider/category_page/show_all');
		$ids     = explode(',', trim(Mage::getStoreConfig('apptrian_productslider/' . $pageType . '_page/specific'), ','));
		
		if ($pageType == 'category') {
			$category = Mage::registry('current_category');
		} else {
			$category = null;
		}
		
		// Specific
		if (count($ids) > 0 && $ids[0] != '') {
				
			$count = Mage::getResourceModel('catalog/product_collection')
				->addAttributeToSelect('entity_id')
				->addAttributeToFilter('status', 1)
				->addAttributeToFilter('visibility', array('in' => array(2, 4)))
				->addStoreFilter()
				->addIdFilter($ids)
				->getSize();
				
		// Category
		} elseif ($pageType == 'category' && $showAll == false && $category->getId()) {
				
			$count = Mage::getResourceModel('catalog/product_collection')
				->addAttributeToSelect('entity_id')
				->addAttributeToFilter('status', 1)
				->addAttributeToFilter('visibility', array('in' => array(2, 4)))
				->addStoreFilter()
				->addCategoryFilter($category)
				->getSize();
				
		// Default
		} else {
				
			$count = Mage::getResourceModel('catalog/product_collection')
				->addAttributeToSelect('entity_id')
				->addAttributeToFilter('status', 1)
				->addAttributeToFilter('visibility', array('in' => array(2, 4)))
				->addStoreFilter()
				->getSize();
				
		}
		
		return $count;
		
	}
	
	/**
	 * Returns data for slider.phtml template.
	 * 
	 * @param integer $curPage
	 * @return array
	 */
	public function getData($curPage = 1)
	{
		
		$data = array();
		
		$pageType = $this->getPageType();
		
		if ($pageType == 'category') {
			$category = Mage::registry('current_category');
		} else {
			$category = null;
		}
		
		$data['products']     = $this->getProducts($curPage, $pageType, $category);
		$data['type']         = $pageType;
		$data['elements']     = $this->getInfoSortOrder($pageType);
		$data['customRating'] = (int) Mage::getStoreConfig('apptrian_productslider/' . $pageType . '_page/custom_rating');
		$data['scrollBar']    = (int) Mage::getStoreConfig('apptrian_productslider/' . $pageType . '_page/scrollbar');
		
		return $data;
		
	}
	
	/**
	 * Returns data for ajax response. Used in ajaxController.
	 * 
	 * @param string $page
	 * @param string $type
	 * @param integer $id
	 * @param string $uenc
	 * @return array
	 */
	public function getDataAjax($page, $type, $id, $uenc)
	{
		
		if ($type == 'category' && $id !== false) {
			$category = Mage::getModel('catalog/category')->load($id);
		} else {
			$category = null;
		}
		
		return $this->getProducts($page, $type, $category, $uenc);
		
	}
	
	/**
	 * Returns products. Used in getData() and getDataAjax() methods.
	 * 
	 * @param integer $curPage
	 * @param string $pageType
	 * @param Mage_Catalog_Model_Category $category
	 * @param string $uenc
	 * @return array
	 */
	public function getProducts($curPage, $pageType = false, $category = null, $uenc = '')
	{
		$products = array();
		
		if ($curPage === false) {
			return $products;
		}
		
		if ($pageType === false) {
			return $products;
		}
		
		$pageSize      = Mage::getStoreConfig('apptrian_productslider/' . $pageType . '_page/page_size');
		$sortAttribute = Mage::getStoreConfig('apptrian_productslider/' . $pageType . '_page/sort_attribute');
		$sortDirection = Mage::getStoreConfig('apptrian_productslider/' . $pageType . '_page/sort_direction');
		$description   = Mage::getStoreConfig('apptrian_productslider/' . $pageType . '_page/description_source');
		$limit         = Mage::getStoreConfig('apptrian_productslider/' . $pageType . '_page/description_limit');
		$width         = Mage::getStoreConfig('apptrian_productslider/' . $pageType . '_page/image_width');
		$height        = Mage::getStoreConfig('apptrian_productslider/' . $pageType . '_page/image_height');
		$ids           = explode(',', trim(Mage::getStoreConfig('apptrian_productslider/' . $pageType . '_page/specific'), ','));
		$showAll       = Mage::getStoreConfigFlag('apptrian_productslider/category_page/show_all');
		
		$attributesToSelect = array('name', 'created_at', 'url_key', 'url_path', 'status', 'visibility', 'sku', 'price', 'special_price', 'small_image', 'thumbnail', 'required_options');
		
		$attributesToSelect[] = $description;
		
		// Specific
		if (count($ids) > 0 && $ids[0] != '') {
			
			$collection = Mage::getResourceModel('catalog/product_collection')
				->addAttributeToSelect($attributesToSelect)
				->addAttributeToFilter('status', 1)
				->addAttributeToFilter('visibility', array('in' => array(2, 4)))
				->addAttributeToSort($sortAttribute, $sortDirection)
				->setPage($curPage, $pageSize)
				->addStoreFilter()
				->addIdFilter($ids)
				->addUrlRewrite()
				->load();
			
		// Category
		} elseif ($pageType == 'category' && $showAll == false && $category->getId()) {
			
			$collection = Mage::getResourceModel('catalog/product_collection')
				->addAttributeToSelect($attributesToSelect)
				->addAttributeToFilter('status', 1)
				->addAttributeToFilter('visibility', array('in' => array(2, 4)))
				->addAttributeToSort($sortAttribute, $sortDirection)
				->setPage($curPage, $pageSize)
				->addStoreFilter()
				->addCategoryFilter($category)
				->addUrlRewrite()
				->load();
			
		// Default
		} else {
			
			$collection = Mage::getResourceModel('catalog/product_collection')
				->addAttributeToSelect($attributesToSelect)
				->addAttributeToFilter('status', 1)
				->addAttributeToFilter('visibility', array('in' => array(2, 4)))
				->addAttributeToSort($sortAttribute, $sortDirection)
				->setPage($curPage, $pageSize)
				->addStoreFilter()
				->addUrlRewrite()
				->load();
			
		}
		
		$coreHelper     = Mage::helper('core');
		$imageHelper    = Mage::helper('catalog/image');
		$checkoutHelper = Mage::helper('checkout/cart');
		$compareHelper  = Mage::helper('catalog/product_compare');
		$wishlistHelper = Mage::helper('wishlist');
		
		$ratings = $this->getRatings($collection);
		
		$id = 0;
		
		$i = 0;
		
		foreach ($collection as $product) {
			
			// Must use this because description and meta_description
			// are not loaded by default
			$product->load($description);
			
			$id = $product->getId();
			
			$products[$i]['id']           = $coreHelper->htmlEscape($id);
			$products[$i]['name']         = $coreHelper->htmlEscape($product->getName());
			$products[$i]['url']          = $product->getProductUrl();
			$products[$i]['sku']          = $coreHelper->htmlEscape($product->getSku());
			
			$productType = $product->getTypeId();
			
			if ($productType == 'grouped') {
			
				$associatedProducts = $product->getTypeInstance(true)->getAssociatedProducts($product);
			
				$prices = array();
				$finalPrices = array();
				
				foreach($associatedProducts as $associatedProduct) {
					$prices[] = $associatedProduct->getPrice();
					
					$finalPrices[] = $associatedProduct->getFinalPrice();
				}
				
				if (count($prices) > 0) {
					$products[$i]['price'] = $coreHelper->currency(min($prices), true, false);
				} else {
					$products[$i]['price'] = 0;
				}
				
				if (count($finalPrices) > 0) {
					$products[$i]['final_price'] = $coreHelper->currency(min($finalPrices), true, false);
				} else {
					$products[$i]['final_price'] = 0;
				}
				
			} else {
				$products[$i]['price']       = $coreHelper->currency($product->getPrice(), true, false);
				$products[$i]['final_price'] = $coreHelper->currency($product->getFinalPrice(), true, false);
			}
			
			$products[$i]['type_id']      = $productType;
			
			$products[$i]['image']        = $imageHelper->init($product, 'small_image')->resize($width, $height)->__toString();
			$products[$i]['is_saleable']  = (int) $product->isSaleable();
			
			if (!$product->getTypeInstance(true)->hasRequiredOptions($product)) {
				$products[$i]['cart_url'] = $this->setUenc($checkoutHelper->getAddUrl($product), $uenc);
			} else {
				$products[$i]['cart_url'] = '';
			}
			
			$products[$i]['compare_url']  = $this->setUenc($compareHelper->getAddUrl($product), $uenc);
			$products[$i]['wishlist_url'] = $wishlistHelper->getAddUrl($product);
			
			switch ($description) {
				case 'description':
					$products[$i]['description'] = (string) $coreHelper->htmlEscape($this->limitDescription($product->getDescription(), $limit));
				break;
				case 'meta_description':
					$products[$i]['description'] = (string) $coreHelper->htmlEscape($this->limitDescription($product->getMetaDescription(), $limit));
				break;
				case 'short_description':
					$products[$i]['description'] = (string) $coreHelper->htmlEscape($this->limitDescription($product->getShortDescription(), $limit));
				break;
				default:
					$products[$i]['description'] = '';
				break;
			}
			
			if (isset($ratings[$id])) {
				
				$products[$i]['rating_summary'] = $ratings[$id]['rating_summary'];
				$products[$i]['reviews_count']  = $ratings[$id]['reviews_count'];
				
			} else {
				
				$products[$i]['rating_summary'] = '0';
				$products[$i]['reviews_count']  = '0';
				
			}
			
			$i++;
			
		}
		
		return $products;
		
	}
	
	/**
	 * Returns ratings for all products in a provided collection.
	 * 
	 * @param Mage_Catalog_Model_Resource_Product_Collection $collection
	 * @return array
	 */
	public function getRatings($collection)
	{
		
		$ratings    = array();
		
		if ($this->isRatingEnabled() == false) {
			return $ratings;
		}
		
		$productIds = array();
		
		foreach($collection as $p)
		{
			$productIds[] = $p->getId();
		}
		
		$storeId = Mage::app()->getStore()->getId();
		
		$summary = Mage::getResourceModel('review/review_summary_collection')
			->addEntityFilter($productIds)
			->addStoreFilter($storeId)
			->load();
	
		foreach ($summary as $s) {
			
			$ratings[$s['entity_pk_value']]['rating_summary'] = $s['rating_summary'];
			$ratings[$s['entity_pk_value']]['reviews_count']  = $s['reviews_count'];
			
		}
		
		return $ratings;
		
	}
	
	/**
	 * Returns fixed "add to cart" and "compare" urls for ajax calls.
	 * 
	 * @param string $url
	 * @param string $uenc
	 * @return string
	 */
	public function setUenc($url, $uenc)
	{
		if ($uenc != '') {
			return preg_replace('/\/uenc\/[a-zA-Z0-9,]+\//iu', '/uenc/' . $uenc . '/', $url);
		} else {
			return $url;
		}
	}
	
	/**
	 * Returns type of page product/category/home/cms.
	 * 
	 * @return string|bool
	 */
	public function getPageType()
	{
		$p = Mage::registry('current_product');
		$c = Mage::registry('current_category');
		$a = Mage::app()->getFrontController()->getAction()->getFullActionName();
		$r = Mage::app()->getFrontController()->getRequest()->getRouteName();
		 
		if ($p && $p->getId() && $a == 'catalog_product_view') {
	
			return 'product';
	
		} elseif ($c && $c->getId() && $a == 'catalog_category_view') {
	
			return 'category';
	
		} elseif ($r == 'cms') {
	
			$cmsIdentifier  = Mage::getSingleton('cms/page')->getIdentifier();
			$homeIdentifier = Mage::app()->getStore()->getConfig('web/default/cms_home_page');
			 
			if($cmsIdentifier === $homeIdentifier){
				return 'home';
			}
			
			return 'cms';
	
		} else {
	
			return null;
	
		}
		 
	}
    
}