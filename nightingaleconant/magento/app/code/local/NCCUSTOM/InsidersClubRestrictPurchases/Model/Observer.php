<?php

class NCCUSTOM_InsidersClubRestrictPurchases_Model_Observer {
    public function exportFeed($observer) {
		
		$order = Mage::getModel('sales/order')->load(Mage::getSingleton('checkout/session')->getLastRealOrderId(), 'increment_id');
		//$order = $observer->getEvent()->getOrder();	    
		$order_entity_id = $order->getEntityId();
		
		$customer_id = $order->getCustomerId();
		$_customer = Mage::getModel('customer/customer')->load($customer_id);        
				
		$date = new DateTime('now');	
		$date->modify('first day of this month');	
		$fromDate = (string) $date->format('Y-m-d'). " 23:59:59";
		
		$date = new DateTime('now');	
		$date->modify('last day of this month');	
		$toDate = (string) $date->format('Y-m-d'). " 23:59:59";
		
		$group_id = $_customer->getGroupId();	
		$simple_items_ordered = 0;
		$ary_simple_items = array();
		$hold_flag = 0;
			
		if ( $group_id == 4 ) {
				
		    $_customer_orders = Mage::getModel('sales/order')
			->getCollection()
			->addFieldToFilter('customer_id', array('eq' => $customer_id))
			->addFieldToFilter('entity_id', array('neq' => $order_entity_id))
			->addAttributeToFilter('created_at', array('from'=>$fromDate, 'to'=>$toDate))
			->addAttributeToFilter( 'status', array( 'neq' => "canceled" ));
				    
		    if ($_customer_orders->getSize() > 0) {
				foreach ( $_customer_orders as $_customer_order ) {
			    	    $entity_id = $_customer_order->getEntityId();
				    Mage::log("Order ID: ". $entity_id);
				    
				    $_prv_order = Mage::getModel("sales/order")->load($entity_id);
				    $_ordered_items = $_prv_order->getAllItems();
			    
			    	foreach ( $_ordered_items as $item ) {
						$product_id = $item->getProductId();
						$qty_ordered = $item->getQtyOrdered();
						$base_price = $item->getBasePrice();
						$product_type = $item->getProductType();
				
						if ( $product_type == "simple" ) {
						    if ( $base_price > 0 ) {
								$simple_items_ordered++;
								$ary_simple_items[$product_id] = $qty_ordered;
						    }
						}
			    	}		    
				}
		    }		
		    
		    if ( $simple_items_ordered <= 3 ) {
				foreach ($order->getAllItems() as $item) {
				    $product_id = $item->getProductId();
				    $qty_ordered = $item->getQtyOrdered();
				    $base_price = $item->getBasePrice();
				    $product_type = $item->getProductType();
			    
			    	if ( $product_type == 'simple' ) {
			    	    if ( $base_price > 0 ) {
						$simple_items_ordered++;
				    }
			    	}
			    
			    	if ( $qty_ordered == 1 ) {
	
						if ( array_key_exists($product_id, $ary_simple_items)) {
						    $hold_flag = 1;
						    Mage::log($order_entity_id. " - This item has already been ordered in this calendar month.");
						    $order->setState(Mage_Sales_Model_Order::STATE_HOLDED, true);
						    $order->save();
						}
			    	} else {
						$hold_flag = 1;
						Mage::log($order_entity_id. " - You cannot order multiple quantities of the same item.");
						$order->setState(Mage_Sales_Model_Order::STATE_HOLDED, true);
						$order->save();
					}
				}
		    } else {
				$hold_flag = 1;
				Mage::log($order_entity_id. " - Too many physical orders in this calendar month");
				$order->setState(Mage_Sales_Model_Order::STATE_HOLDED, true);
				$order->save();
		    }
		    
		    if ( $simple_items_ordered > 3 ) {
		    	if ( $hold_flag == 0 ) {
		    		Mage::log($order_entity_id. " - This order will be more than three items in a calendar month");
		    		$order->setState(Mage_Sales_Model_Order::STATE_HOLDED, true);
		    		$order->save();
		    	}		   
		    }
		}
    }
}
?>