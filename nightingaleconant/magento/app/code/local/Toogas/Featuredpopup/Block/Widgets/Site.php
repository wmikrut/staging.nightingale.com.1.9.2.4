<?php
/**
 * Toogas Lda.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA (End-User License Agreement)
 * that is bundled with this package in the file licence_toogas_community.txt.
 * It is also available at this URL:
 * http://www.toogas.com/licences/toogas_community.txt
 *
 * @category   Toogas
 * @package    Toogas_Featuredpopup
 * @copyright  Copyright (c) 2013 Toogas Lda. (http://www.toogas.com)
 * @license    http://www.toogas.com/licences/toogas_community.txt
 */
 
class Toogas_Featuredpopup_Block_Widgets_Site
    extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface
{
	
	protected  function _construct() {
		parent::_construct();
		$this->setTemplate('toogas_featuredpopup/widgets/site/iframe.phtml');
	}
	
	public function obtainPageUrl() {
		return $this->hasData('page_url') ? 
		$this->_getData('page_url') : 
		'';
	}
	
	public function obtainIframeWidth() {
		return $this->hasData('iframe_width') ? 
		$this->_getData('iframe_width') : 
		1;		
	}
	
	public function obtainIframeHeight() {
		return $this->hasData('iframe_height') ? 
		$this->_getData('iframe_height') : 
		1;		
	}
    
}