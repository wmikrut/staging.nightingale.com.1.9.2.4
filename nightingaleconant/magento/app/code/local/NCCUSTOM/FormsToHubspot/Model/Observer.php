<?php
class NCCUSTOM_FormsToHubspot_Model_Observer 
{
    public function processRequest($observer) 
    {		
    	$nccustom_active = Mage::getStoreConfig('formstohubspot/formstohubspot_group/formstohubspot_select',Mage::app()->getStore());
    	if ( $nccustom_active == 1 )
    	{
			include('hubspot-credentials.php');
		    
			try 
			{
				$_result = $observer->getResult();
		
				$fields = $observer->getResult()->getData('field');
				$webform = $observer->getResult()->getData('webform_id');
				$hubspot_list = 0;
			
				switch ( $webform ) 
				{
				    case 5:
					$name = $fields[19];
					$coupon_code = $fields[20];
					$email_address = $fields[21];
					$hubspot_list = 497;
					break;
				    case 6:
					$name = $fields[22];
					$coupon_code = $fields[23];
					$email_address = $fields[24];
					$hubspot_list = 497;
					break;
				    case 7:
					$name = $fields[25];
					$coupon_code = $fields[26];
					$email_address = $fields[27];
					$hubspot_list = 497;
					break;
				    case 16:
					$name = $fields[52];
					$coupon_code = $fields[53];
					$email_address = $fields[54];
					$hubspot_list = 641;
					break;
				    case 17:
					$name = $fields[66];
					$coupon_code = $fields[67];
					$email_address = $fields[68];
					$hubspot_list = 802;
					break;
				}
		
				if ( $hubspot_list > 0 ) 
				{
					$hubspot_total = 0;
					//$url = "https://api.hubapi.com/contacts/v1/search/query?q=$email_address&hapikey=$hubspot_api_key";
					//$hubspot_response = file_get_contents($url);
					//$hubspot_json = json_decode($hubspot_response, true);
					//$hubspot_total = $hubspot_json['total'] + 0;
				
					if ( $hubspot_total == 0 ) 
					{
						$first_name = '';
						$last_name = '';
						$ary_name = explode(" ", $name);
						if ( count($ary_name) > 1 )
						{
							$last_name = ucfirst(strtolower(array_pop($ary_name)));
							$first_name = ucfirst(strtolower(implode(" ", $ary_name)));
						}
						else
						{
							$first_name = ucfirst(strtolower($name));
							$last_name = '';
						}
				
						$endpoint = 'https://api.hubapi.com/contacts/v1/contact/?hapikey='. $hubspot_api_key;
	
						$request_json = <<<STRING
{
"properties": [
{
"property": "email",
"value": "$email_address"
},
{
"property": "firstname",
"value": "$first_name"
},
{
"property": "lastname",
"value": "$last_name"
},
{
"property": "coupon_code",
"value": "$coupon_code"
}
]
}
STRING;
	
	
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $request_json);
						curl_setopt($ch, CURLOPT_URL, $endpoint);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						//curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);
						curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
						$output = curl_exec($ch);
						$errno = curl_errno($ch);
						$error = curl_error($ch);
						curl_close($ch);	
						
						$json_response = json_decode($output, true);
						$vids = $json_response['vid'];
						$endpoint = 'https://api.hubapi.com/contacts/v1/lists/' .$hubspot_list. '/add?hapikey='. $hubspot_api_key;
						$request_json = <<<STRING
{
"vids": [
$vids
]
}
STRING;
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_POST, true);
						curl_setopt($ch, CURLOPT_POSTFIELDS, $request_json);
						curl_setopt($ch, CURLOPT_URL, $endpoint);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						//curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);
						curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
						$output = curl_exec($ch);
						$errno = curl_errno($ch);
						$error = curl_error($ch);
						curl_close($ch);					    
					}    
				}
			} 
			catch (Exception $e) 
			{
				Mage::log($e->getMessage(), null, "FormsToHubspot.log");
			}
	    }
	}
}
?>
