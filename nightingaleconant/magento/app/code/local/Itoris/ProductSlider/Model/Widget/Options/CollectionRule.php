<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PRODUCTSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

 


class Itoris_ProductSlider_Model_Widget_Options_CollectionRule {

	public function toOptionArray() {
		return array(
			array(
				'label' => 'Top Rated',
				'value' => Itoris_ProductSlider_Block_ProductSlider::COLLECTION_RULE_TOP_RATED
			),
			array(
				'label' => 'Top Sellers',
				'value' => Itoris_ProductSlider_Block_ProductSlider::COLLECTION_RULE_TOP_SELLERS
			),
			array(
				'label' => 'Just Added',
				'value' => Itoris_ProductSlider_Block_ProductSlider::COLLECTION_RULE_JUST_ADDED
			),
            array(
				'label' => 'New From-To Products',
				'value' => Itoris_ProductSlider_Block_ProductSlider::COLLECTION_RULE_NEW_FROM_TO
			),
			array(
				'label' => 'Most Reviewed',
				'value' => Itoris_ProductSlider_Block_ProductSlider::COLLECTION_RULE_MOST_REVIEWED
			),
			array(
				'label' => 'Wishlist Top',
				'value' => Itoris_ProductSlider_Block_ProductSlider::COLLECTION_RULE_WISHLIST_TOP
			),
			array(
				'label' => 'On Sale',
				'value' => Itoris_ProductSlider_Block_ProductSlider::COLLECTION_RULE_ON_SALE
			),
			array(
				'label' => 'Random',
				'value' => Itoris_ProductSlider_Block_ProductSlider::COLLECTION_RULE_RANDOM
			),
			array(
				'label' => 'Selected Categories',
				'value' => Itoris_ProductSlider_Block_ProductSlider::COLLECTION_RULE_SELECTED_CATEGORIES
			)

		);
	}
}
?>