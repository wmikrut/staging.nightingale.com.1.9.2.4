<?php
/**
 * WMD_Html5audio Installer 0.0.4
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/

$installer = new Mage_Catalog_Model_Resource_Eav_Mysql4_Setup('core_setup');
$installer->startSetup();

// $setup = new Mage_Eav_Model_Entity_Setup('core_setup'); 

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'simple_product_id', array(
    'group'        => 'HTML5 Audio',
    'label'        => 'Simple Product Id',
    'type'         => 'int',
    'input'        => 'text',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'default'           => '',
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'used_in_product_listing' => true,
    'unique'            => false,
    'apply_to'          => 'downloadable',
    'is_configurable'   => false
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'downloadable_product_id', array(
    'group'        => 'HTML5 Audio',
    'label'        => 'Downloadable Product Id',
    'type'         => 'int',
    'input'        => 'text',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'default'           => '',
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'used_in_product_listing' => true,
    'unique'            => false,
    'apply_to'          => 'simple',
    'is_configurable'   => false
));

$installer->endSetup(); 

