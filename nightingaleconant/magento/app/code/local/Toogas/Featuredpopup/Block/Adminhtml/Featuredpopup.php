<?php
/**
 * Toogas Lda.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA (End-User License Agreement)
 * that is bundled with this package in the file licence_toogas_community.txt.
 * It is also available at this URL:
 * http://www.toogas.com/licences/toogas_community.txt
 *
 * @category   Toogas
 * @package    Toogas_Featuredpopup
 * @copyright  Copyright (c) 2010 Toogas Lda. (http://www.toogas.com)
 * @license    http://www.toogas.com/licences/toogas_community.txt
 */
class Toogas_Featuredpopup_Block_Adminhtml_Featuredpopup extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {   	
  	
    $this->_controller = 'adminhtml_featuredpopup';
    $this->_blockGroup = 'featuredpopup';
    $this->_headerText = Mage::helper('featuredpopup')->__('Featured Popup').$this->_getNotice();
    $this->_addButtonLabel = Mage::helper('featuredpopup')->__('Add Featured Popup');
    parent::__construct();
  }
  
  protected function _getNotice() {
	$block = new Toogas_Featuredpopup_Block_Featuredpopup();
	$serial = Mage::helper('featuredpopup')->getSerial();
	if ($block->validaDomAlias($block->limpaDominio($block->getStoreUrl()), $serial)) {
		$html = '';
	}
	else if (!strlen($serial)) {
			$html = ' - '.$this->__('License not valid. Please, enter key.');
		}
		else {
			$html = ' - '.$this->__('License not valid. Incorrect key.');
		}
	return $html;  			
  }
  
}