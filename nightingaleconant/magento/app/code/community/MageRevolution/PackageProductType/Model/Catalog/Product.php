<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Model_Catalog_Product extends Mage_Catalog_Model_Product
{
    /**
     * Retrieve array of package products
     *
     * @return array
     */
    public function getPackageProducts()
    {
        if (!$this->hasPackageProducts()) {
            $products = array();
            $collection = $this->getPackageProductCollection();
            foreach ($collection as $product) {
                $products[] = $product;
            }
            $this->setPackageProducts($products);
        }

        return $this->getData('package_products');
    }

    /**
     * Retrieve package products identifiers
     *
     * @return array
     */
    public function getPackageProductIds()
    {
        if (!$this->hasPackageProductIds()) {
            $ids = array();
            foreach ($this->getPackageProducts() as $product) {
                $ids[] = $product->getId();
            }
            $this->setPackageProductIds($ids);
        }
        return $this->getData('package_product_ids');
    }

    /**
     * Retrieve collection package product
     *
     * @return Mage_Catalog_Model_Resource_Product_Link_Product_Collection
     */
    public function getPackageProductCollection()
    {
        $collection = $this->getLinkInstance()->usePackageLinks()
            ->getProductCollection()
            ->setIsStrongMode();
        $collection->setProduct($this);
        return $collection;
    }


    /**
     * Retrieve collection package link
     *
     * @return Mage_Catalog_Model_Resource_Product_Link_Collection
     */
    public function getPackageLinkCollection()
    {
        $collection = $this->getLinkInstance()->usePackageLinks()
            ->getLinkCollection();
        $collection->setProduct($this);
        $collection->addLinkTypeIdFilter();
        $collection->addProductIdFilter();
        $collection->joinAttributes();
        return $collection;
    }


    public function getPackageAssociatedProducts($id) {
        $product = Mage::getModel('catalog/product')->load($id);

        $collection = $product->getPackageProductCollection()
            ->addAttributeToSelect('required_options')
            ->setPositionOrder()
            ->addStoreFilter();

        $productId = array();
        foreach($collection as $col) {
            $productId[] = $col->getId();
        }

        if($productId){
            return $productId;
        } else {
            return false;
        }
    }

}
