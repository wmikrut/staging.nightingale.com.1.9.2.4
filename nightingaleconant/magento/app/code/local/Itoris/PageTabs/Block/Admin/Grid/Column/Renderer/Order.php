<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGETABS
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */


class Itoris_PageTabs_Block_Admin_Grid_Column_Renderer_Order extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Number {

	public function render(Varien_Object $row) {
		$html = '';
		if ($this->getColumn()->getCanOrder()) {
			$orderValue = $row->getData($this->getColumn()->getIndex());
			$maxOrder = $this->getColumn()->getMaxOrder();
			if (strtolower($this->getColumn()->getOrderDir()) == 'desc') {
				$nextClass = 'itoris-arrow-up';
				$prevClass = 'itoris-arrow-down';
			} else {
				$nextClass = 'itoris-arrow-down';
				$prevClass = 'itoris-arrow-up';
			}
			if ((int)$maxOrder > (int)$orderValue) {
				$html .= '<a href="' . $this->getUrl('*/*/changeOrder', array('id' => $row->getData('tab_id'), 'order' => 'next')) . '" title="' . $this->__('move up') . '"><div class="' . $nextClass . '"></div></a>';
			}
			$minOrder = $this->getColumn()->getMinOrder();
			if ((int)$minOrder < (int)$orderValue) {
				$html .= '<a href="' . $this->getUrl('*/*/changeOrder', array('id' => $row->getData('tab_id'), 'order' => 'prev')) . '" title="' . $this->__('move down') . '"><div class="' . $prevClass . '"></div></a>';
			}
		}
		return $html . parent::render($row);
	}

}
?>