<?php

require_once 'AdminController.php';

class WP_AdvancedSliderAddonExportImport_DuplicateController
    extends WP_AdvancedSliderAddonExportImport_AdminController
{
    public function indexAction()
    {
        $sliderId = $this->getRequest()->getParam('id');
        try {
            $newSliderId = Mage::getModel('advancedslideraddonexportimport/process')->duplicate($sliderId);
            Mage::getSingleton('core/session')->addSuccess($this->__('The slider has been duplicated.'));
        } catch (Exception $e) {
            $newSliderId = $sliderId;
            Mage::getSingleton('core/session')->addError($e->getMessage());
        }
        $this->_redirect('advancedslider/sliders/edit', array('id' => $newSliderId));
    }
}
