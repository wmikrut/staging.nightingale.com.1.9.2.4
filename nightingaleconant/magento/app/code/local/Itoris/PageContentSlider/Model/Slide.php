<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_PAGECONTENTSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_PageContentSlider_Model_Slide extends Mage_Core_Model_Abstract {

	public function __construct() {
		$this->_init('itoris_pagecontentslider/slide');
	}

	/**
	 * Get max order from db + 1
	 *
	 * @return int
	 */
	public function calculateSlideOrder() {
		/** @var $collection Itoris_PageContentSlider_Model_Mysql4_Slide_Collection */
		$collection = $this->getCollection()
					->setOrder('slide_order', 'DESC')
					->setPageSize(1);
		if ($collection->getSize()) {
			return $collection->getFirstItem()->getSlideOrder() + 1;
		}
		return 1;
	}

	protected function _afterSave() {
		if ($this->getId()) {
			/** @var $resource Mage_Core_Model_Resource */
			$resource = Mage::getSingleton('core/resource');
			$connection = $resource->getConnection('write');
			$tableGroup = $resource->getTableName('itoris_pagecontentslider_slide_customergroup');
			$tableStore = $resource->getTableName('itoris_pagecontentslider_slide_store');
			$connection->query("delete from {$tableGroup} where slide_id = {$this->getId()}");
			$connection->query("delete from {$tableStore} where slide_id = {$this->getId()}");

			$this->insertIntoRelatedTable($this->getUserGroups(), $tableGroup, $connection, 'group_id');
			$this->insertIntoRelatedTable($this->getStoreViews(), $tableStore, $connection, 'store_id');
		}

		return parent::_afterSave();
	}

	protected function insertIntoRelatedTable($values, $tableName, $connection, $columnName) {
		if (is_array($values) && count($values)) {
			$valuesQuery = array();
			foreach ($values as $value) {
				if ($value != '') {
					$valuesQuery[] = '(' . $this->getId() . ', ' . (int)$value . ')';
				}
			}
			if (count($valuesQuery)) {
				$separatedValuesQuery = implode(',', $valuesQuery);
				$connection->query("insert into {$tableName} (slide_id, {$columnName}) values {$separatedValuesQuery}");
			}
		}
	}

	protected function _afterLoad() {
		if ($this->getId()) {
			/** @var $resource Mage_Core_Model_Resource */
			$resource = Mage::getSingleton('core/resource');
			$connection = $resource->getConnection('write');
			$groups = $connection->fetchAll("select group_id from {$resource->getTableName('itoris_pagecontentslider_slide_customergroup')} where slide_id = {$this->getId()}");
			if (is_array($groups) && count($groups)) {
				$this->setUserGroups($this->prepareValuesByField($groups, 'group_id'));
			} else {
				$this->setUserGroups(array(''));
			}
			$stores = $connection->fetchAll("select store_id from {$resource->getTableName('itoris_pagecontentslider_slide_store')} where slide_id = {$this->getId()}");
			if (is_array($stores) && count($stores)) {
				$this->setStoreViews($this->prepareValuesByField($stores, 'store_id'));
			} else {
				$this->setStoreViews(array(0));
			}
		} else {
			$this->setUserGroups(array(''));
			$this->setStoreViews(array(0));
		}
		return parent::_afterLoad();
	}

	protected function prepareValuesByField($values, $field) {
		$preparedValues = array();
		if (is_array($values)) {
			foreach ($values as $value) {
				if (isset($value[$field])) {
					$preparedValues[] = $value[$field];
				}
			}
		}
		return $preparedValues;
	}

	public function getMinMaxOrder() {
		$collection = $this->getCollection()->getMinMaxOrder();
		if ($collection->getSize()) {
			$item = $collection->getFirstItem();
			$result = array(
				'min_order' => $item->getMinOrder(),
				'max_order' => $item->getMaxOrder(),
			);
		}

		return $result;
	}
}
?>