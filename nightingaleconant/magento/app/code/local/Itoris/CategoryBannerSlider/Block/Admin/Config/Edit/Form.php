<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_CATEGORYBANNERSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_CategoryBannerSlider_Block_Admin_Config_Edit_Form extends Mage_Adminhtml_Block_System_Config_Form {

	protected function _prepareForm() {
		try{
			$defaultSettings = Mage::getModel('itoris_categorybannerslider/settings');
			$defaultSettings->load($this->getWebsiteId(), $this->getStoreId());
		}catch(Exception $e){
			Mage::getSingleton('core/session')->addError($e->getMessage());
		}
		$useWebsite = (bool)$this->getStoreId();
		
		if(!$useWebsite){
			$useDefault = (bool)$this->getWebsiteId();
		}else{
			$useDefault = false;
		}
        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>$this->__('Settings')));

		$fieldset->addField('enable', 'select', array(
                'name'  => 'settings[enable][value]',
                'label' => $this->__('Extension Enabled'),
                'title' => $this->__('Extension Enabled'),
                'required' => true,
				'values' => array(
									array('label' => $this->__('Yes'),
										'value' => Itoris_CategoryBannerSlider_Model_Sets::STATUS_ENABLED),
									array('label' => $this->__('No'),
										'value' => Itoris_CategoryBannerSlider_Model_Sets::STATUS_DISABLED),
							),
				'use_default' => $useDefault,
				'use_website' => $useWebsite,
				'use_parent_value' => $defaultSettings->isParentValue('enable', $useWebsite),
				)
        )->getRenderer()->setTemplate('itoris/categorybannerslider/config/form/element.phtml');


        $form->setValues($defaultSettings->getDefaultData());

        $form->setAction($this->getUrl('*/*/save', array( 'website_id' => $this->getWebsiteId(), 'store_id' => $this->getStoreId())));
        $form->setMethod('post');
        $form->setUseContainer(true);
        $form->setId('edit_form');

        $this->setForm($form);

        return parent::_prepareForm();
    }

	/**
	 * Retrieve store id by store code from the request
	 *
	 * @return int
	 */
	protected function getStoreId() {
		if ($this->getStoreCode()) {
            return Mage::app()->getStore($this->getStoreCode())->getId();
        }
		return 0;
	}

	/**
	 * Retrieve website id by website code from the request
	 *
	 * @return int
	 */
	protected function getWebsiteId() {
		if ($this->getWebsiteCode()) {
            return Mage::app()->getWebsite($this->getWebsiteCode())->getId();
        }
		return 0;
	}
}
?>