<?php
/**
 * @category  Apptrian
 * @package   Apptrian_Subcategories
 * @author    Apptrian
 * @copyright Copyright (c) 2016 Apptrian (http://www.apptrian.com)
 * @license   http://www.apptrian.com/license Proprietary Software License EULA
 */
class Apptrian_Subcategories_Model_Config_Sortdirection
{
    
    protected $_options;
    const ASC  = 'asc';
    const DESC = 'desc';
    
    public function toOptionArray()
    {
        if (!$this->_options) {
            $this->_options[] = array(
                'value' => self::ASC,
                'label' => Mage::helper('apptrian_subcategories')
                    ->__('Ascending')
            );
            $this->_options[] = array(
                'value' => self::DESC,
                'label' => Mage::helper('apptrian_subcategories')
                    ->__('Descending')
            );
        }
        return $this->_options;
    }
    
}
