<?php
/**
 * Pulsar Plugins
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.pulsarplugins.com/pulsarplugins-license.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento community edition
 * Pulsar Plugins does not guarantee correct work of this extension
 * on any other Magento edition except Magento community edition.
 * Pulsar Plugins does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   PP
 * @package    PP_EmailByShipping
 * @version    1.1
 * @copyright  Copyright (c) 2010-2014 Pulsar Plugins (http://www.pulsarplugins.com/)
 * @license    http://www.pulsarplugins.com/pulsarplugins-license.txt
 *
*/

$this->startSetup();

/*
$this->run("
CREATE TABLE `{$this->getTable('ordermailtable/ordermail')}` (
  `setting_id` bigint(12) NOT NULL AUTO_INCREMENT,
  `method_type` varchar(32) NOT NULL,
  `method_code` varchar(255) NOT NULL,
  `template_id` varchar(255) NOT NULL,
  `customer_type` varchar(255) NOT NULL,
  `stores` varchar(255) NOT NULL,
  PRIMARY KEY (`setting_id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


");
*/

$this->endSetup();