var Banner = {};

Banner.Gallery = Class.create();
Banner.Gallery.prototype = {
    images : [],
    file2id : {
        'no_selection' :0
    },
    idIncrement :1,
    containerId :'',
    container :null,
    uploader :null,
	banners: null,
    initialize : function(containerId, uploader, banners) {
        this.containerId = containerId, this.container = $(this.containerId);
        this.uploader = uploader;
        if (this.uploader) {
            this.uploader.onFilesComplete = this.handleUploadComplete
                    .bind(this);
        }
		this.banners = banners;
		this.prepareImages();
        this.template = new Template('<tr id="__id__" class="preview">' + this
                .getElement('template').innerHTML + '</tr>', new RegExp(
                '(^|.|\\r|\\n)(__([a-zA-Z0-9_]+)__)', ''));
        this.fixParentTable();
        this.updateImages();
    },
	prepareImages : function(){
		for(var i = 0; i < this.banners.length; i++){
			if(this.banners[i].link_url){
				var newImage = {};
				newImage.link_url = this.banners[i].link_url;
				newImage.file = this.banners[i].file_name;
				newImage.url = this.banners[i].url;
				newImage.title = this.banners[i].title;
				newImage.alt = this.banners[i].alt;
				newImage.delay = this.banners[i].delay;
				newImage.position = /*this.banners[i].order ||*/ this.getNextPosition();
				newImage.enabled = this.banners[i].enabled;
				newImage.open_in = this.banners[i].open_in;
				newImage.first = false;
				this.images.push(newImage);
			}
		}
	},
    fixParentTable : function() {
        this.container.ancestors().each( function(parentItem) {
            if (parentItem.tagName.toLowerCase() == 'td') {
                parentItem.style.width = '100%';
            }
            if (parentItem.tagName.toLowerCase() == 'table') {
                parentItem.style.width = '100%';
                throw $break;
            }
        });
    },
    getElement : function(name) {
        return $(this.containerId + '_' + name);
    },
    showUploader : function() {
        this.getElement('add_images_button').hide();
        this.getElement('uploader').show();
    },
    handleUploadComplete : function(files) {
		var firstInGroup = true;
        files.each( function(item) {
            /*if (!item.response.isJSON()) {
                try {
                    console.log(item.response);
                } catch (e2) {
                    alert(item.response);
                }
                return;
            }*/
			var response = {};
			try {
				response = item.response.evalJSON();
			} catch(e) {
				try {
					response =  eval('(' + item.response + ')');
				} catch (e) { console.log(item.response); return; }
			}
            if (response.error) {
                return;
            }
            var newImage = {};
            newImage.url = response.url;
            newImage.file = response.file;
			newImage.link_url = '';
            newImage.title = '';
            newImage.alt = '';
            newImage.delay = 5;
            newImage.position = this.getNextPosition();
            newImage.enabled = 1;
            newImage.open_in = targetBlank;
			if( firstInGroup){
				newImage.first = true;
				firstInGroup = false;
			}else{
				newImage.first = false;
			}
            this.images.push(newImage);
            this.uploader.removeFile(item.id);
        }.bind(this));
        this.container.setHasChanges();
        this.updateImages();
    },
    updateImages : function() {
        Object.toJSON(this.images);
        this.images.each( function(row) {
            if (!$(this.prepareId(row.file))) {
                this.createImageRow(row);
            }
            this.updateVisualisation(row.file);
        }.bind(this));
    },
	deleteImageRow : function(file){
		 if(!confirm(deleteMessage)){
			 return;
		 }
		 var order = parseInt(this.getFileElement(file, 'cell-position input').value);
		 if(this.images.length > 1){
			 if(order == 1){
				$$('.cell-position .sort_arrow_up')[0].remove();
			 }else if(order == this.images.length){
				$$('.cell-position .sort_arrow_down')[this.images.length - 2].remove();
			 }
		 }
		 if(order != this.images.length){
			 this.reorder(order);
		 }
		 this.images.splice(this.getIndexByFile(file), 1);
		 $(this.prepareId(file)).remove();
	},
	reorder : function(from){
		for(var order = from; order < this.images.length; order++){
			$$('.cell-position input')[order + 1].value = order;
			this.images[order].position = order;
		}
	},
    createImageRow : function(image) {
        var vars = Object.clone(image);
        vars.id = this.prepareId(image.file);
		var id = this.file2id[image.file];
		vars.file_name = 'set[banners][' + id + '][file_name]';
		vars.file_name_value = image.file;
		vars.title = 'set[banners][' + id + '][title]';
		vars.title_value = image.title;
		vars.alt = 'set[banners][' + id + '][alt]';
		vars.alt_value = image.alt;
		vars.delay = 'set[banners][' + id + '][delay]';
		vars.delay_value = image.delay;
		vars.link_url = 'set[banners][' + id + '][link_url]';
		vars.link_url_value = image.link_url;
		vars.open_in = 'set[banners][' + id + '][open_in]';
		vars.url = image.url;
		if(image.open_in == targetBlank){
			vars.target_blank = 'selected="selected"';
		}else{
			vars.target_blank = '';
		}
		if(image.open_in == targetSelf){
			vars.target_self = 'selected="selected"';
		}else{
			vars.target_self = '';
		}
		vars.order = 'set[banners][' + id + '][order]';
		vars.order_value = image.position;
		vars.enabled = 'set[banners][' + id + '][enabled]';
		vars.enabled_value = (parseInt(image.enabled)) ? 'checked' : '';
        var html = this.template.evaluate(vars);
        Element.insert(this.getElement('list'), {
            bottom :html
        });
		if(image.position != 1){
			this.addUpArrow(image.position, image.file);
		}
		if(this.getNextPosition() - 1 != image.position){
			this.addDownArrow(image.position, image.file);
		}
		if(image.first){
			var file = $$('.cell-image input')[image.position - 1].value;
			this.addDownArrow( (image.position - 1), file);
		}
    },
	addDownArrow : function(position, file){
		var arrowDown = document.createElement('div');
		Element.extend(arrowDown);
		arrowDown.addClassName('sort_arrow_down');
		var object = this;
		Element.observe(arrowDown, 'click', function(){
			object.moveRowDown(file);
		});
		if(!this.getFileElement(file, 'sort_arrow_down')){
			$$('.cell-position')[position].appendChild(arrowDown);
		}
	},
	addUpArrow : function(position,file){
		var arrowUp = document.createElement('div');
		Element.extend(arrowUp);
		arrowUp.addClassName('sort_arrow_up');
		var object = this;
		Element.observe(arrowUp, 'click', function(){
			object.moveRowUp(file);
		});
		if(!$$('#' + this.prepareId(file) + ' .sort_arrow_down')[0]){
			$$('.cell-position')[position].appendChild(arrowUp);
		}else{
			Element.insert($$('#' + this.prepareId(file) + ' .sort_arrow_down')[0], {
					before : arrowUp
			});
		}
	},
	moveRowDown : function(file){
		var order = this.getFileElement(file, 'cell-position input');
		var orderValue = order.value;
		var row = $$('.preview')[orderValue - 1];
		var rowDown = $$('.preview')[orderValue];
		var rowDownFile = $$('.preview .cell-image input')[orderValue].value;
		$$('.preview .cell-position input')[orderValue].value = orderValue;
		order.value = parseInt(orderValue) + 1;
		Element.insert(rowDown, {
            after : row
        });
		if(order.value == this.images.length){
			this.getFileElement(file, 'sort_arrow_down').remove();
			this.addDownArrow(orderValue, rowDownFile);
		}
		if(orderValue == 1){
			this.getFileElement(rowDownFile, 'sort_arrow_up').remove();
			this.addUpArrow(2, file);
		}
	},
	moveRowUp : function(file){
		var order = this.getFileElement(file, 'cell-position input');
		var orderValue = order.value;
		var row = $$('.preview')[orderValue - 1];
		var rowUp = $$('.preview')[orderValue - 2];
		var rowUpFile = $$('.preview .cell-image input')[orderValue - 2].value;
		$$('.preview .cell-position input')[orderValue - 2].value = orderValue;
		order.value = parseInt(orderValue) - 1;
		Element.insert(rowUp, {
            before : row
        });
		if(orderValue == 2){
			$$('.sort_arrow_up')[0].remove();
			this.addUpArrow(2, rowUpFile);
		}
		if(orderValue == this.images.length){
			this.addDownArrow( (orderValue - 1), file);
			this.getFileElement(rowUpFile, 'sort_arrow_down').remove();
		}
	},
    prepareId : function(file) {
		if (file === '') {
			file = null;
		}
        if (typeof this.file2id[file] == 'undefined') {
            this.file2id[file] = this.idIncrement++;
        }
        return this.containerId + '-image-' + this.file2id[file];
    },
    getNextPosition : function() {
        var maxPosition = 0;
        this.images.each( function(item) {
            if (parseInt(item.position) > maxPosition) {
                maxPosition = parseInt(item.position);
            }
        });
        return maxPosition + 1;
    },
    updateImage : function(file) {
        var index = this.getIndexByFile(file);
        this.updateState(file);
        this.container.setHasChanges();
    },
    loadImage : function(file) {
        var image = this.getImageByFile(file);
		if (image) {
			var htmlImage = this.getFileElement(file, 'cell-image img');
			htmlImage.src = image.url;
			var resize = function(){
				var height = htmlImage.getHeight();
				var width = htmlImage.getWidth();
				if(width > height){
					htmlImage.width = "300";
				}else{
					htmlImage.height = "200";
				}
				htmlImage.show();
			}
			setTimeout(resize,1000);
			this.getFileElement(file, 'cell-image .roll_over').hide();
		}
    },
    updateVisualisation : function(file) {
        var image = this.getImageByFile(file);
		if (image) {
	        this.updateState(file);
		}
    },
    updateState : function(file) {

    },
    getFileElement : function(file, element) {
        var selector = '#' + this.prepareId(file) + ' .' + element;
        var elems = $$(selector);
        if (!elems[0]) {
            try {
              //  console.log(selector);
            } catch (e2) {
             //   alert(selector);
            }
        }

        return $$('#' + this.prepareId(file) + ' .' + element)[0];
    },
    getImageByFile : function(file) {
        if (this.getIndexByFile(file) === null) {
            return false;
        }

        return this.images[this.getIndexByFile(file)];
    },
    getIndexByFile : function(file) {
        var index;
        this.images.each( function(item, i) {
            if (item.file == file) {
                index = i;
            }
        });
        return index;
    },
	popupImage : function(url){
		var background = document.createElement('div');
		Element.extend(background);
		background.addClassName('popup_background');
		$('html-body').appendChild(background);
		var image = document.createElement('img');
		Element.extend(image);
		image.src = url;
		var popup = document.createElement('div');
		Element.extend(popup);
		popup.addClassName('popup_box');
		Event.observe(image, 'click', function(){
			background.hide();
			popup.hide();
			$$('.flex')[0].show();
		});
		Event.observe(background, 'click', function(){
			background.hide();
			popup.hide();
			$$('.flex')[0].show();
		});
		popup.appendChild(image);
		var closeButton = document.createElement('div');
		Element.extend(closeButton);
		closeButton.addClassName('close_button');
		Event.observe(closeButton, 'click', function(){
			background.hide();
			popup.hide();
			$$('.flex')[0].show();
		});
		popup.appendChild(closeButton);
		$('html-body').appendChild(popup);
		popup.setStyle({'marginLeft' : '-' + image.width/2 + 'px', 'marginTop' : '-' + image.height/2 + 'px'});
		$$('.flex')[0].hide();
	}
};