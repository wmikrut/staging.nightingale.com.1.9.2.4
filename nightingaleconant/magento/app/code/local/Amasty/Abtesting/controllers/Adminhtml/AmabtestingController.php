<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2016 Amasty (https://www.amasty.com)
 * @package Amasty_Abtesting
 */


class Amasty_Abtesting_Adminhtml_AmabtestingController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/amabtesting');

        $this->_title($this->__('Sales'))
            ->_title($this->__('A/B Testing'))
        ;

        $this->_addContent(
            $this->getLayout()->createBlock('amabtesting/adminhtml_test')
        );

        $this->renderLayout();
    }

    protected function _initTest()
    {
        $this->_title($this->__('Sales'))
            ->_title($this->__('A/B Testing'))
        ;

        $test = Mage::getModel('amabtesting/test');

        $testId = +$this->getRequest()->getParam(
            $test->getIdFieldName()
        );

        if ($testId) {
            $test->load($testId);
            $this->_title($this->__('Edit Test'));
        }
        else {
            $this->_title($this->__('New Test'));
        }

        Mage::register('current_test', $test);

        return $this;
    }

    public function editAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/amabtesting');

        $this->_initTest();

        $this->_addContent(
            $this->getLayout()->createBlock('amabtesting/adminhtml_test_edit')
        );
        $this->_addLeft(
            $this->getLayout()->createBlock('amabtesting/adminhtml_test_edit_tabs')
        );

        $this->renderLayout();
    }

    public function newAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/amabtesting');

        $this->_initTest();

        $this->_addContent(
            $this->getLayout()->createBlock('amabtesting/adminhtml_test_new')
        );
        $this->_addLeft(
            $this->getLayout()->createBlock('amabtesting/adminhtml_test_new_tabs')
        );

        $this->renderLayout();
    }

    public function newPostAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('amabtesting/test');
            $model->setData(array(
                'created_at' => Mage::getModel('core/date')->gmtDate(),

                'name' => $this->getRequest()->getParam('name', false),

                'custom_design' => $this->getRequest()->getParam('custom_design', false),
                'custom_layout_update' => $this->getRequest()->getParam('custom_layout_update', false),
                'custom_layout' => $this->getRequest()->getParam('custom_layout', false),
                'css_override' => $this->getRequest()->getParam('css_override', false),
            ));

            try {
                $model->save();

                $attributes = $this->getRequest()->getParam('attribute');
                $products = $this->getRequest()->getParam('product_ids');
                $products = explode('&', $products);

                foreach ($products as $product) {
                    if ($product = +$product) {
                        $model->getResource()->getReadConnection()->insert(
                            $model->getResource()->getTable('amabtesting/test_product'),
                            array(
                                'test_id'    => $model->getId(),
                                'product_id' => $product,
                            )
                        );
                    }
                }

                if (is_array($attributes)) {
                    foreach ($attributes as $attribute) {
                        Mage::getModel('amabtesting/test_attribute')
                            ->setData(array(
                                'test_id'       => $model->getId(),
                                'attribute_id'  => $attribute['id'],
                                'title'         => $attribute['title'],
                            ))
                            ->save()
                        ;
                    }
                }

                $variants = +$this->getRequest()->getParam('variants');

                $resource = Mage::getSingleton('core/resource');
                $connection = $resource->getConnection('core_write');

                for ($i = 0; $i < min($variants, Amasty_Abtesting_Model_Test::MAX_VARIANTS); $i++) {
                    $connection->insert(
                        $resource->getTableName('catalog/product'),
                        array(
                            'entity_type_id' => Mage::getModel('catalog/product')->getResource()->getTypeId(),
                            'attribute_set_id' => Mage::getModel('catalog/product')->getDefaultAttributeSetId(),
                            'type_id' => Amasty_Abtesting_Model_Test_Variation::META_PRODUCT_TYPE,
                            'created_at' => Mage::getModel('core/date')->gmtDate(),
                        )
                    );

                    $metaproductId = $connection->lastInsertId();

                    if (sizeof($products) == 1 && $i == 0) {
                        $templateProduct = Mage::getModel('catalog/product')->load($products[0]);
                        $metaproduct = Mage::getModel('amabtesting/test_variation_metaproduct')->load($metaproductId);

                        $attributesCollection = $model->getAttributes();

                        foreach ($attributes as $attribute) {
                            $attributeModel = $attributesCollection->getItemById($attribute['id']);

                            $metaproduct->setData(
                                $attributeModel->getAttributeCode(),
                                $templateProduct->getData($attributeModel->getAttributeCode())
                            );
                        }
                        $metaproduct->save();
                    }

                    $variation = Mage::getModel('amabtesting/test_variation');

                    if ($i == 0) {
                        $variation->setName($this->__('Original'));
                    }

                    $variation
                        ->setTestId($model->getId())
                        ->setMetaproductId($metaproductId)
                        ->save()
                    ;
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('The test has been saved.')
                );

                $this->_redirect('*/*/edit', array('id' => $model->getId()));
                return;

            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/new');
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            $model = Mage::getModel('amabtesting/test');
            $id = +$this->getRequest()->getParam($model->getIdFieldName());

            $model->load($id);
            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError(
                    $this->__('This test no longer exists.')
                );
                $this->_redirect('*/*/');
                return;
            }

            $updateStatus = $this->getRequest()->getParam('update_status', false);

            if ($model->isReadonly() && !$updateStatus) {
                Mage::getSingleton('adminhtml/session')->addError(
                    $this->__('This test is read only.')
                );
                $this->_redirect(
                    '*/*/edit',
                    array('id' => $model->getId(), '_current' => true)
                );
                return;
            }

            $transaction = Mage::getModel('core/resource_transaction');

            try {
                if (!$model->isReadonly()) {
                    $model->addData($data);

                    $attributes = $model->getAttributes();

                    foreach ($data['variation'] as $id => $variationData) {
                        $variation = Mage::getModel('amabtesting/test_variation')
                            ->load($id);

                        $variation->setName($variationData['abvariation_name']);

                        $metaproduct = $variation->getMetaproduct();

                        foreach ($attributes as $attribute) {
                            $metaproduct->setData(
                                $attribute->getAttributeCode(),
                                $variationData[$attribute->getAttributeCode()]
                            );
                        }

                        $transaction
                            ->addObject($metaproduct)
                            ->addObject($variation);
                    }
                }

                if ($updateStatus) {
                    switch ($model->getStatus()) {
                        case Amasty_Abtesting_Model_Test::STATUS_SETUP:
                            $model
                                ->setStatus(Amasty_Abtesting_Model_Test::STATUS_RUNNING)
                                ->setStartedAt(Mage::getModel('core/date')->gmtDate())
                            ;
                            break;
                        case Amasty_Abtesting_Model_Test::STATUS_RUNNING:
                            $model->setStatus(Amasty_Abtesting_Model_Test::STATUS_PAUSED);
                            break;
                        case Amasty_Abtesting_Model_Test::STATUS_PAUSED:
                            $model->setStatus(Amasty_Abtesting_Model_Test::STATUS_RUNNING);
                            break;
                    }
                }

                $transaction
                    ->addObject($model)
                    ->save()
                ;

                if (!$model->isReadonly()) {
                    Mage::getSingleton('adminhtml/session')->addSuccess(
                        $this->__('The test has been saved.')
                    );
                }

                if ($updateStatus) {
                    Mage::getSingleton('adminhtml/session')->addSuccess(
                        $this->__('Test status has been updated.')
                    );
                }

                if ($updateStatus || $this->getRequest()->getParam('back')) {
                    $params = array(
                        'id' => $model->getId(),
                        '_current' => true
                    );

                    if ($updateStatus) {
                        $params['tab'] = 'testTabs_general';
                    }
                    $this->_redirect(
                        '*/*/edit',
                        $params
                    );

                    return;
                }

                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect(
                    '*/*/edit',
                    array(
                        $model->getIdFieldName() => $this->getRequest()->getParam($model->getIdFieldName())
                    )
                );
                return;
            }
        }
        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        $model = Mage::getModel('amabtesting/test');
        $id = Mage::app()->getRequest()->getParam($model->getIdFieldName());
        try {
            $model->load($id);
            if ($model->getId()) {
                $model->delete();
            }

            Mage::getSingleton('adminhtml/session')->addSuccess(
                $this->__('Test deleted.')
            );

        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($e->getMessage());
        }

        $this->_redirect('*/*/');
    }

    public function productGridAction()
    {
        $grid = $this->getLayout()->createBlock('amabtesting/adminhtml_test_new_tab_products')
            ->setProductIds($this->getRequest()->getPost('product_ids', null));

        $this->getResponse()->setBody($grid->toHtml());
    }

    public function applicableProductGridAction()
    {
        $id = $this->getRequest()->getParam('test');
        $test = Mage::getModel('amabtesting/test')->load($id);

        Mage::register('current_test', $test, true);
        $grid = $this->getLayout()->createBlock('amabtesting/adminhtml_test_edit_tab_products');

        $this->getResponse()->setBody($grid->toHtml());
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed(
            'sales/amabtesting'
        );
    }

    public function wysiwygAction()
    {
        $elementId = $this->getRequest()->getParam('element_id', md5(microtime()));
        $storeId = $this->getRequest()->getParam('store_id', 0);
        $storeMediaUrl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);

        $content = $this->getLayout()->createBlock('adminhtml/catalog_helper_form_wysiwyg_content', '', array(
            'editor_element_id' => $elementId,
            'store_id'          => $storeId,
            'store_media_url'   => $storeMediaUrl,
        ));

        $this->getResponse()->setBody($content->toHtml());
    }

    public function copyAction()
    {
        if ($id = $this->getRequest()->getParam('variation')) {
            $variation = Mage::getModel('amabtesting/test_variation');

            $variation->load($id);
            if (!$variation->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError(
                    $this->__('This variation no longer exists.')
                );
                $this->_redirect('*/*/');
                return;
            }


            try {
                $test = Mage::getModel('amabtesting/test')->load(
                    $variation->getTestId()
                );

                $products = $test->getInvolvedProducts();

                if ($products->getSize() > 1)
                    throw new Exception($this->__(
                        'Variation data can be copied to single-product tests only'
                    ));

                $attributes = $test->getAttributes();
                $metaProduct = $variation->getMetaProduct();

                foreach ($products as $product) {

                    foreach ($attributes as $attribute) {
                        $key = $attribute->getAttributeCode();

                        if ($value = $metaProduct->getData($key)) {
                            $product->setData($key, $value);
                        }
                    }
                    $product->save();

                    break;
                }

                Mage::getSingleton('adminhtml/session')->addSuccess(
                    $this->__('Variation data has been copied.')
                );

                $this->_redirect(
                    '*/*/edit',
                    array('id' => $test->getId(), '_current' => true)
                );

                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect(
                    '*/*/edit',
                    array(
                        $test->getIdFieldName() => $this->getRequest()->getParam($test->getIdFieldName())
                    )
                );
                return;
            }
        }
        $this->_redirect('*/*/');
    }
}