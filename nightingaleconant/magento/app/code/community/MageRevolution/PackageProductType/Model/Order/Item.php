<?php
/**
 * MageRevolution_PackageProductType extension
 * @category   MageRevolution
 * @package    MageRevolution_PackageProductType
 * @copyright  copyright novusweb llc
 * @author     Christopher Silvey <webmaster@beckindesigns.com> http://www.BeckinDesigns.com
 * @co author  Bret Williams <bret.williams@novusweb.com> http://www.novusweb.com/
 */
class MageRevolution_PackageProductType_Model_Order_Item extends Mage_Sales_Model_Order_Item {

    /**
     * Retrieve item qty available for ship
     *
     * @return float|integer
     */
    public function getSimpleQtyToShip()
    {
        if($this->getProductType() == MageRevolution_PackageProductType_Model_Product_Type_Package::TYPE_CODE){
            $qtyOrdered = null;
            $qtyShipped = null;
            $qtyRefunded = null;
            $qtyCanceled = null;
            foreach($this->getChildrenItems() as $child){
                if($qtyOrdered) {
                    $qtyOrdered+= $child->getQtyOrdered();
                } else {
                    $qtyOrdered = $child->getQtyOrdered();
                }

                if($qtyShipped) {
                    $qtyShipped+= $child->getQtyShipped();
                } else {
                    $qtyShipped = $child->getQtyShipped();
                }

                if($qtyRefunded) {
                    $qtyRefunded+= $child->getQtyRefunded();
                } else {
                    $qtyRefunded = $child->getQtyRefunded();
                }

                if($qtyCanceled) {
                    $qtyCanceled+= $child->getQtyCanceled();
                } else {
                    $qtyCanceled = $child->getQtyCanceled();
                }
            }
            $qty = $qtyOrdered
                - $qtyShipped
                - $qtyRefunded
                - $qtyCanceled;
        } else {
            $qty = $this->getQtyOrdered()
                - $this->getQtyShipped()
                - $this->getQtyRefunded()
                - $this->getQtyCanceled();
        }
        return max($qty, 0);
    }
}