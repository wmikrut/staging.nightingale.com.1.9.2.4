<?php 
/**
 * Mas_Mascartdirect extension by Makarovsoft.com
 * 
 * @category   	Mas
 * @package		Mas_Mascartdirect
 * @copyright  	Copyright (c) 2014
 * @license		http://makarovsoft.com/license.txt
 * @author		makarovsoft.com
 */
/**
 * Mascartdirect default helper
 *
 * @category	Mas
 * @package		Mas_Mascartdirect
 * 
 */
class Mas_Mascartdirect_Helper_Data extends Mage_Core_Helper_Abstract{
}