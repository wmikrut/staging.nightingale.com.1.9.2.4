var deleteCategoryMessage = '';
function category_delete(websiteId, storeId, categoryId){
	if(confirm(deleteCategoryMessage)){
		$('w_' + websiteId + 's_' + storeId + 'c_' + categoryId).remove();
		var categories = $$('#w_' + websiteId + 's_' + storeId + ' .category_label');
		if(!isNaN(categories)){
			$('w_' + websiteId + 's_' + storeId).remove();
		}
		var stores = $$('#w_' + websiteId + ' .store_label');
		if(!isNaN(stores)){
			$('w_' + websiteId).remove();
		}
		var websites = $$('.website_label');
		if(!isNaN(websites)){
			$('no_categories').show();
		}
	}
}

var stores = new Array();
var categories;
var homePageLabel = 'Home Page';
var deleteLabel = 'delete';
var showLabel = 'show';

function category_add(){
	var storeId = $F('store_dropdown');
	var categoryId = $F('category_dropdown');
	var websiteId = stores[storeId].websiteId;
	if(!($('w_' + websiteId + 's_' + storeId + 'c_' + categoryId))){
		var category = document.createElement('div');
		Element.extend(category);
		category.addClassName('category_label');
		category.id = 'w_' + websiteId + 's_' + storeId + 'c_' + categoryId;
		var categoryLabel = document.createElement('div');
		Element.extend(categoryLabel);
		categoryLabel.addClassName('text');
		categoryLabel.update(getCategoryLabel(storeId, categoryId));
		category.appendChild(categoryLabel);
		var categoryDelete = document.createElement('div');
		Element.extend(categoryDelete);
		categoryDelete.addClassName('category_delete');
		categoryDelete.update(deleteLabel);
		Event.observe(categoryDelete, 'click', function(){
			category_delete(websiteId, storeId, categoryId);
		});
		category.appendChild(categoryDelete);
		var categoryShow = document.createElement('div');
		Element.extend(categoryShow);
		categoryShow.addClassName('category_show');
		var categoryLink = document.createElement('a');
		Element.extend(categoryLink);
		categoryLink.target = '_blank';
		categoryLink.href = getCategoryUrl(storeId, categoryId);
		categoryLink.update(showLabel);
		categoryShow.appendChild(categoryLink);
		category.appendChild(categoryShow);
		var input = document.createElement('input');
		Element.extend(input);
		input.type = 'hidden';
		input.name = 'set[categories][' + storeId + '_' + categoryId +']';
		input.value = 1;
		category.appendChild(input);
		var br = document.createElement('br');
		Element.extend(br);
		category.appendChild(br);
		if(!($('w_' + websiteId + 's_' + storeId))){
			var store = document.createElement('div');
			Element.extend(store);
			store.addClassName('store_label');
			store.id = 'w_' + websiteId + 's_' + storeId;
			store.update(stores[storeId].storeName);
			store.appendChild(category);
			if(!($('w_' + websiteId))){
				var website = document.createElement('div');
				Element.extend(website);
				website.addClassName('website_label');
				website.id = 'w_' + websiteId;
				website.update(stores[storeId].websiteName);
				website.appendChild(store);
				$('categories_list').appendChild(website);
				$('no_categories').hide();
			}else{
				$('w_' + websiteId).appendChild(store);
			}
		}else{
			$('w_' + websiteId + 's_' + storeId).appendChild(category);
		}
	}
}

function validateForm(continueEdit){
	var validator = new Validation($('edit_form'));
	if(!validator.validate()){
		return false;
	}
	if(continueEdit){
		$('edit_form').action = saveContinueEditActionUrl;
	}
	$('edit_form').submit();
}

function getCategoryLabel(storeId, categoryId){
	if (categoryId == 1) {
		return '<b>' + categories[storeId][0].name + '</b>';
	} else {
		var catIndex = 0;
		for (var i = 0; i < categories[storeId].length; i++) {
			if (categoryId == categories[storeId][i].id) {
				catIndex = i;
				break;
			}
		}
		if (catIndex) {
			var category = categories[storeId][catIndex];
			var currentLevel = category.level;
			var title = [category.name];
			for (var i = catIndex - 1; i > 0; i--) {
				if (categories[storeId][i].id && categories[storeId][i].level < currentLevel) {
					title.unshift(categories[storeId][i].name);
					currentLevel = categories[storeId][i].level;
				}
			}
			return title.join(': ');
		}
	}
	return '';
}

function getCategoryUrl(storeId, categoryId) {
	for (var i = 0; i < categories[storeId].length; i++) {
		if (categoryId == categories[storeId][i].id) {
			return categories[storeId][i].url;
		}
	}
	return '';
}