<?php
/**
 * WMD_Html5audio_Model_Sample
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2013 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Model_Sample extends Mage_Core_Model_Abstract
{
    
//     /**
//      * Return value from system configuration
//      * 
//      * @param string | $value          
//      * @return string
//      */                       
//     protected function _configValue($value)
//     {
//         return Mage::getStoreConfig('html5audio/sample/' . $value);
//     }
        
    public function _construct()
    {
        parent::_construct();
        $this->_init('html5audio/sample');
    }
	  
	  /**
	   * Parse uploaded file and save sample
	   *
	   * @param array post 	   
	   * @return void	   
	   */          	  
	  public function saveSample($postData)
	  {
        if (Mage::getStoreConfig('html5audio/sample/stream'))
        { 
            if((isset($_FILES['mp3_file']['name']) && (file_exists($_FILES['mp3_file']['tmp_name']))) &&
            (isset($_FILES['ogg_file']['name']) && (file_exists($_FILES['ogg_file']['tmp_name'])))) 
            {
                                                    
                $mp3 = $ogg = '';
                $errors = array();
                $path = Mage::getBaseDir('media') . DS . 'html5audio';
                
                $uploader = new Varien_File_Uploader('mp3_file');          
                $uploader->setAllowedExtensions(array('mp3')); 
                $uploader->setAllowCreateFolders(true);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                try {                              
                    $uploader->save($path . DS, $_FILES['mp3_file']['name']);           
                    $mp3 = $uploader->getUploadedFileName();
                } 
                catch(exception $e)
                {
                    $errors[] = $e->getMessage();
                } 
                $uploader = new Varien_File_Uploader('ogg_file');              
                $uploader->setAllowedExtensions(array('ogg')); 
                $uploader->setAllowCreateFolders(true);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);  
                try {                              
                    $uploader->save($path . DS, $_FILES['ogg_file']['name']); 
                    $ogg = $uploader->getUploadedFileName();  
                } 
                catch(exception $e)
                {
                    $errors[] = $e->getMessage();
                } 
                if (!count($errors))
                {       
                    $model = Mage::getModel('html5audio/sample')
                        ->setMp3Path($mp3)
                        ->setOggPath($ogg)
                        ->setTitle($postData['title'])
                        ->setDate(Mage::getSingleton('core/date')->gmtDate())
                        ->save(); 
                        
                    return $model;
                }
                else 
                {   
                    Mage::throwException(Mage::helper('html5audio')->__('Could not save sample. ') . implode(', ', $errors));             
                } 
            }
        }
        else
        {
            $model = Mage::getModel('html5audio/sample')
                ->setMp3Path($postData['mp3_path'])
                ->setOggPath($postData['ogg_path'])
                ->setTitle($postData['title'])
                ->setDate(Mage::getSingleton('core/date')->gmtDate())
                ->save();
        }    
    }
    
    
	  /**
	   * Parse uploaded file and save file
	   *
	   * @param array post 	   
	   * @return void	   
	   */          	  
	  public function quickSaveSample($postData)
	  {
        if(isset($_FILES['mp3_file']['name']) && (file_exists($_FILES['mp3_file']['tmp_name'])) &&
            isset($_FILES['mp3_file']['name']) && (file_exists($_FILES['mp3_file']['tmp_name']))) {
          
                                                    
                $mp3 = $ogg = '';
                $errors = array();
                $path = Mage::getBaseDir('media') . DS . 'html5audio';
                
                $uploader = new Varien_File_Uploader('mp3_file');          
                $uploader->setAllowedExtensions(array('mp3')); 
                $uploader->setAllowCreateFolders(true);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);
                try {                              
                    $uploader->save($path . DS, $_FILES['mp3_file']['name']);           
                    $mp3 = $uploader->getUploadedFileName();
                } 
                catch(exception $e)
                {
                    $errors[] = $e->getMessage();
                } 
                $uploader = new Varien_File_Uploader('ogg_file');              
                $uploader->setAllowedExtensions(array('ogg')); 
                $uploader->setAllowCreateFolders(true);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);  
                try {                              
                    $uploader->save($path . DS, $_FILES['ogg_file']['name']); 
                    $ogg = $uploader->getUploadedFileName();  
                } 
                catch(exception $e)
                {
                    $errors[] = $e->getMessage();
                } 
                if (!count($errors))
                {       
                    $model = Mage::getModel('html5audio/sample')
                        ->setMp3Path($mp3)
                        ->setOggPath($ogg)
                        ->setTitle($postData['title'])
                        ->setDate(Mage::getSingleton('core/date')->gmtDate())
                        ->save(); 
                    return $model;
                }
                else 
                {   
                    Mage::throwException(Mage::helper('html5audio')->__('Could not save sample. ') . implode(', ', $errors));             
                } 
          
        }  
    }
    
    public function saveToPlaylist($sampleId, $playlistId)
    {
        return $this->_getResource()->saveToPlaylist($sampleId, $playlistId);
    }
    
    /**
     * Load sample by hash
     * 
     * @param string $hash
     * @return object WMD_Html5audio_Model_Sample
     */                       
    public function loadByHash($hash)
    {
        $this->_getResource()->loadByHash($this, $hash);
        return $this;
    }
    
}
