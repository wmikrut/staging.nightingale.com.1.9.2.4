<?php 
/**
 * ITORIS
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the ITORIS's Magento Extensions License Agreement
 * which is available through the world-wide-web at this URL:
 * http://www.itoris.com/magento-extensions-license.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to sales@itoris.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extensions to newer
 * versions in the future. If you wish to customize the extension for your
 * needs please refer to the license agreement or contact sales@itoris.com for more information.
 *
 * @category   ITORIS
 * @package    ITORIS_CATEGORYBANNERSLIDER
 * @copyright  Copyright (c) 2012 ITORIS INC. (http://www.itoris.com)
 * @license    http://www.itoris.com/magento-extensions-license.html  Commercial License
 */

class Itoris_CategoryBannerSlider_Block_Admin_Banners_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

	protected function _prepareForm() {
        $form = new Varien_Data_Form(array(
            'id'        => 'edit_form',
            'action'    => $this->getUrl('*/*/save'),
            'method'    => 'post',
            'enctype'   => 'multipart/form-data'
        ));

		$categoriesFieldset = $form->addFieldset('categories_fieldset', array('legend'=>$this->__('Categories')));
		$categoriesFieldset->setRenderer(new Itoris_CategoryBannerSlider_Block_Admin_Form_Element_Renderer_Fieldset());

		$configurationFieldset = $form->addFieldset('configuration_fieldset', array('legend'=>$this->__('Banners Configuration')));
		$configurationFieldset->setRenderer(new Itoris_CategoryBannerSlider_Block_Admin_Form_Element_Renderer_Fieldset());

		$bannersFieldset = $form->addFieldset('banners_fieldset', array('legend'=>$this->__('List of Banners')));

		$bannersFieldset->addType('banners', 'Itoris_CategoryBannerSlider_Block_Admin_Form_Element_Gallery');

		$bannersFieldset->addField('banners_gallery','banners',array());


        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
?>