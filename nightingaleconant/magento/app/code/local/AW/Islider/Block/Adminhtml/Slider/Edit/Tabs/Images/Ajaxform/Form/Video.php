<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Islider
 * @version    2.0.4
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Islider_Block_Adminhtml_Slider_Edit_Tabs_Images_Ajaxform_Form_Video extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);

        $this->_addGeneralFieldsetToForm($form);

        $formData = Mage::registry('current_image_slider');
        $form->setValues($formData);

        return parent::_prepareForm();
    }

    protected function _addGeneralFieldsetToForm($form)
    {
        $fieldset = $form->addFieldset('video_general_fieldset', array(
                'legend' => $this->__('General Information')
        ));
        $fieldset->addField('pid', 'hidden', array(
                'name' => 'pid',
        ));
        $readmeUrl = Mage::helper('awislider')->getReadmeUrl();
        $fieldset->addField('location', 'text', array(
                'name'     => 'location',
                'label'    => $this->__('Video URL'),
                'required' => true,
                'note'     => $this->__('See <a href="%s" target="_blank">readme</a> for more information', $readmeUrl)
        ));
        $fieldset->addField('status', 'select', array(
                'name'   => 'status',
                'label'  => $this->__('Status'),
                'values' => Mage::getModel('awislider/source_status')->toOptionArray()
        ));

        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
        $fieldset->addField('active_from', 'date', array(
                'name'         => 'active_from',
                'label'        => $this->__('Date From'),
                'image'        => $this->getSkinUrl('images/grid-cal.gif'),
                'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
                'format'       => $dateFormatIso,
                'locale'       => Mage::app()->getLocale()->getLocaleCode(),
                'required'     => false
        ));
        $fieldset->addField('active_to', 'date', array(
                'name'         => 'active_to',
                'label'        => $this->__('Date To'),
                'image'        => $this->getSkinUrl('images/grid-cal.gif'),
                'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
                'format'       => $dateFormatIso,
                'locale'       => Mage::app()->getLocale()->getLocaleCode(),
                'required'     => false
        ));
        $fieldset->addField('sort_order', 'text', array(
                'name'     => 'sort_order',
                'label'    => $this->__('Sort Order'),
                'required' => true
        ));

        return $this;
    }
}