<?php
/**
 * WMD_Html5audio_Block_Adminhtml_Mapper_Edit_Tab_Mapper
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Block_Adminhtml_Mapper_Edit_Tab_Mapper extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare Form
     *
     * @return object
     */	
    protected function _prepareForm()
    {               
    
        /** @var $model Html5audio_Model_Mapper */
        $model = Mage::registry('mapper');
        
        $form = new Varien_Data_Form();
        
        if (!$model->getGenerateMappers())
        {
    
    		    $fieldset = $form->addFieldset('edit_link', array('legend' => Mage::helper('html5audio')->__('New Mapper')));
    		         
            if ($model->getMapperId()) {
                $fieldset->addField('mapper_id', 'hidden', array(
                    'name' => 'mapper_id',
                ));
            }     
                                          
            $fieldset->addField('link_id', 'select', array(
                'name'      => 'link_id',
                'title'     => Mage::helper('html5audio')->__('Downloadable Link File'),
                'label'     => Mage::helper('html5audio')->__('Downloadable Link File'),
                'values'    => Mage::getModel('html5audio/system_config_source_link')->toOptionArray(), 
          			'required'	=> true,
            ));   
                                              
            $fieldset->addField('sample_id', 'select', array(
                'name'      => 'sample_id',
                'title'     => Mage::helper('html5audio')->__('Sample File'),
                'label'     => Mage::helper('html5audio')->__('Sample File'),
                'values'    => Mage::getModel('html5audio/system_config_source_sample')->toOptionArray(), 
          			'required'	=> true,
            ));    
                                              
            $fieldset->addField('product_id', 'text', array(
                'name'      => 'product_id',
                'title'     => Mage::helper('html5audio')->__('Product Id'),
                'label'     => Mage::helper('html5audio')->__('Product Id'),
          			'required'	=> false,
            ));   
                                            
            $fieldset->addField('playlist_id', 'text', array(
                'name'      => 'playlist_id',
                'title'     => Mage::helper('html5audio')->__('Playlist Id'),
                'label'     => Mage::helper('html5audio')->__('Playlist Id'),
          			'required'	=> false,
            )); 
              
            $form->setValues($model->getData());
            
        }
        else
        { 
        
    		    $fieldset = $form->addFieldset('edit_link', array('legend' => Mage::helper('html5audio')->__('Generate Mappers')));
    		    
            $fieldset->addField('generate_mappers', 'hidden', array(
                'name' => 'generate_mappers',
                'value' => 1,
            ));                                      
            
            $fieldset->addField('product_id', 'select', array(
                'name'      => 'product_id',
                'title'     => Mage::helper('html5audio')->__('Product'),
                'label'     => Mage::helper('html5audio')->__('Product'),
                'values'    => Mage::getModel('html5audio/system_config_source_product')->toOptionArray(), 
          			'required'	=> true,
            ));   
                                              
            $fieldset->addField('playlist_id', 'select', array(
                'name'      => 'playlist_id',
                'title'     => Mage::helper('html5audio')->__('Playlist'),
                'label'     => Mage::helper('html5audio')->__('Playlist'),
                'values'    => Mage::getModel('html5audio/system_config_source_playlist')->toOptionArray(), 
          			'required'	=> true,
            ));   
        
        }   
            
        $this->setForm($form);
        return parent::_prepareForm();	
    }
    
}