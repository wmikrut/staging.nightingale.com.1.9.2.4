<?php
/**
 * @category  Apptrian
 * @package   Apptrian_Subcategories
 * @author    Apptrian
 * @copyright Copyright (c) 2016 Apptrian (http://www.apptrian.com)
 * @license   http://www.apptrian.com/license Proprietary Software License EULA
 */
class Apptrian_Subcategories_Model_Config_Sortattribute
{
    
    protected $_options;
    const NAME       = 'name';
    const META_TITLE = 'meta_title';
    const POSITION   = 'position';
    const CREATED_AT = 'created_at';
    
    public function toOptionArray()
    {
        if (!$this->_options) {
            $this->_options[] = array(
                'value' => self::NAME,
                'label' => Mage::helper('apptrian_subcategories')
                    ->__('Name')
            );
            $this->_options[] = array(
                'value' => self::META_TITLE,
                'label' => Mage::helper('apptrian_subcategories')
                    ->__('Page Title')
            );
            $this->_options[] = array(
                'value' => self::POSITION,
                'label' => Mage::helper('apptrian_subcategories')
                    ->__('Position')
            );
            $this->_options[] = array(
                'value' => self::CREATED_AT,
                'label' => Mage::helper('apptrian_subcategories')
                    ->__('Created Date')
            );
        }
        return $this->_options;
    }
    
}
