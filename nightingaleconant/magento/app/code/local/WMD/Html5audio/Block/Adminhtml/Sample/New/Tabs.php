<?php
/**
 * WMD_Html5audio_Block_Adminhtml_Sample_New_Tabs
 *
 * WMD Web-Manufaktur/Digiswiss 
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that you find at http://wmdextensions.com/WMD-License-Community.txt
 *
 * @category  WMD
 * @package   WMD_Html5audio
 * @author    Dominik Wyss <info@wmdextensions.ch>
 * @copyright 2010 Dominik Wyss | WMD Extensions (http://wmdextensions.com)
 * @link      http://wmdextensions.com
 * @license   http://wmdextensions.com/WMD-License-Community.txt
*/
class WMD_Html5audio_Block_Adminhtml_Sample_New_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Construct
     *
     * @return void
     */	
    public function __construct()
    {
        parent::__construct();		
        $this->setId('html5audio_association_new_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('html5audio')->__('Sample'));
    }

    /**
     * Before HTML
     *
     * @return object
     */	
    protected function _beforeToHtml()
    {
        
        $this->addTab('association', array(
            'label'     => Mage::helper('html5audio')->__('Sample'),
            'title'     => Mage::helper('html5audio')->__('Sample'),
            'content'   => $this->getLayout()->createBlock('html5audio/adminhtml_sample_new_tab_sample
            ')->toHtml(),
            'active'    => true,
        )); 
        
        return parent::_beforeToHtml();
    }
}