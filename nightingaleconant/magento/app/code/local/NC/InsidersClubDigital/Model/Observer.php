<?php
/**
 * Created by PhpStorm.
 * User: ajt
 * Date: 10/20/14
 * Time: 2:09 PM
 */ 
class NC_InsidersClubDigital_Model_Observer extends Mage_Core_Model_Abstract
{
    const INSIDER_CLUB_SKU = '7002ICDR';

    public function setCustomerGroup($observer) {
        $order = $observer->getRecurringProfiles();
        if (!empty($order[0])) {
            $order = $order[0];
            if ( $order->getState() == Mage_Sales_Model_Recurring_Profile::STATE_ACTIVE
                 && !$this->_belongsToInsidersClub( $order->getCustomerId() )
            ) {
                $item = $order->getOrderItemInfo();
                $sku = $item['sku'];
                if ( $sku == self::INSIDER_CLUB_SKU ) {
                    $this->_setCustomerGroup( $order->getCustomerId() );
                }
            }
        }
    }

    private function _setCustomerGroup($customerId) {
        $customer = Mage::getModel('customer/customer')->load($customerId);

        $customer->setGroupId($this->_getInsidersClubCustomerGroup());

        $customer->save();
    }

    private function _setDefaultCustomerGroup($customerId) {
        $customer = Mage::getModel('customer/customer')->load($customerId);

        $customer->setGroupId($this->_getDefaultCustomerGroup());

        $customer->save();
    }

    private function _getInsidersClubCustomerGroup() {
        $code = "Insiders Digital Pricing";
        $collection = Mage::getModel('customer/group')->getCollection() //get a list of groups
                          ->addFieldToFilter('customer_group_code', $code);// filter by group code

        return $collection->getFirstItem()->getId();
    }

    private function _getDefaultCustomerGroup() {
        $code = "General";
        $collection = Mage::getModel('customer/group')->getCollection() //get a list of groups
                          ->addFieldToFilter('customer_group_code', $code);// filter by group code

        return $collection->getFirstItem()->getId();
    }

    public function addToHubspot($observer) {
        $order = $observer->getRecurringProfiles();
        if (!empty($order[0])) {
            $order = $order[0];
            if ( $order->getState() == Mage_Sales_Model_Recurring_Profile::STATE_ACTIVE
                 && $this->_belongsToInsidersClub( $order->getCustomerId() )
            ) {
                //  Find Insiders club product
                $item = $order->getOrderItemInfo();
                $sku = $item['sku'];
                if ( $sku == self::INSIDER_CLUB_SKU ) {
                    $this->_addCustomerToHubspot( $order->getCustomerId() );
                }
            }
        }
    }

    private function _addCustomerToHubspot($customerId) {
        $customer = Mage::getModel('customer/customer')->load($customerId);

        //Process a new form submission in HubSpot in order to create a new Contact.

        $hubspotutk = $_COOKIE['hubspotutk'];  //grab the cookie from the visitors browser.
        $ip_addr = $_SERVER['REMOTE_ADDR'];  //IP address too.
        $hs_context = array(
            'hutk' => $hubspotutk,
            'ipAddress' => $ip_addr,
            'pageName' => 'Insiders Digital - SIGN UP New Member'
        );
        $hs_context_json = json_encode($hs_context);

        //Need to populate these varilables with values from the form.
        $str_post = "firstname=" . urlencode($customer->getFirstname())
                    . "&lastname=" . urlencode($customer->getLastname())
                    . "&email=" . urlencode($customer->getEmail())
                    . "&phone=" . urlencode($customer->getPrimaryBillingAddress()->getTelephone())
                    . "&hs_context=" . urlencode($hs_context_json);  //Leave this one be :)

         //replace the values in this URL with your portal ID and your form GUID
        $endpoint = 'https://forms.hubspot.com/uploads/form/v2/349249/39a366fe-f000-4f5c-ac21-ff83c05aa656';

        $ch = @curl_init();
        @curl_setopt($ch, CURLOPT_POST, true);
        @curl_setopt($ch, CURLOPT_POSTFIELDS, $str_post);
        @curl_setopt($ch, CURLOPT_URL, $endpoint);
        @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = @curl_exec($ch);  //Log the response from HubSpot as needed.
        $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE); //Log the response status code
        @curl_close($ch);
    }

    public function setInsidersClub($observer) {
        $order = $observer->getRecurringProfiles();
        if (!empty($order[0])) {
            $order = $order[0];
            if ( $order->getState() == Mage_Sales_Model_Recurring_Profile::STATE_ACTIVE
                 && $this->_belongsToInsidersClub( $order->getCustomerId() )
            ) {
                $item = $order->getOrderItemInfo();
                $sku = $item['sku'];
                if ( $sku == self::INSIDER_CLUB_SKU ) {
                    Mage::getSingleton( 'customer/session' )->setInsidersClub( true );
                }
            }
        }
    }

    public function suspendActivateProfile($observer) {
        $profile = $observer->getObject();
        if ($profile instanceof Mage_Sales_Model_Recurring_Profile
            && $profile->_origData['state'] != $profile->getState()
        ) {
            switch ($profile->getState()) {
                case Mage_Sales_Model_Recurring_Profile::STATE_ACTIVE:
                    if (!$this->_belongsToInsidersClub( $profile->getCustomerId() )) {
                        $this->_setCustomerGroup( $profile->getCustomerId() );
                    }
                    break;
                case Mage_Sales_Model_Recurring_Profile::STATE_SUSPENDED:
                case Mage_Sales_Model_Recurring_Profile::STATE_CANCELED:
                case Mage_Sales_Model_Recurring_Profile::STATE_EXPIRED:
                    if ($this->_belongsToInsidersClub( $profile->getCustomerId() )) {
                        $this->_setDefaultCustomerGroup( $profile->getCustomerId() );
                    }
                    break;
            }
        }
    }

    /**
     * Check to see if customer belongs to insider club
     *
     * @param $customerId
     *
     * @return bool return true if customer belongs to insider club
     */
    private function _belongsToInsidersClub($customerId) {
        $customer = Mage::getModel( 'customer/customer' )->load( $customerId );

        return ( !is_null($customer) && $customer->getGroupId() == $this->_getInsidersClubCustomerGroup() );
    }
}